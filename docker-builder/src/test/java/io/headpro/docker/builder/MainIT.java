/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.docker.builder;

import java.io.FileNotFoundException;
import javax.json.JsonObject;
import javax.script.ScriptException;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.is;

/**
 *
 * @author alacambra
 */
public class MainIT {

    Main cut;

    @Before
    public void setUp() {
        cut = new Main();
    }

    @Test
    public void testGenerate() {
        cut.generate("/Users/albertlacambra1/git/docker");
    }
    
    @Test
    public void testFind() {
        cut.find("/Users/albertlacambra1/git/docker");
    }
    
    @Test
    public void testExecuteCommand() {
        cut.executeCommand("/usr/local/bin/docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=lacambra -d mysql:V1");
    }
    
     @Test
    public void testCommands() {
        cut.commands("/Users/albertlacambra1/git/docker");
    }

    @Test
    public void testInvokeMustache() throws Exception {

        String template = "Email addresses of {{contact.name}}:\n"
                + "{{#contact.emails}}\n"
                + "- {{.}}\n"
                + "{{/contact.emails}}";

        String contactJson = "{"
                + "\"contact\": {"
                + "\"name\": \"Mr A\", \"emails\": ["
                + "\"contact@some.tld\", \"sales@some.tld\""
                + "]}}";

        String expected = "Email addresses of Mr A:\n"
                + "- contact@some.tld\n"
                + "- sales@some.tld\n";

        String current = cut.invokeMustache(template, contactJson);

        assertThat(current, is(expected));
    }

}
