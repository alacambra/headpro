package io.headpro.docker.builder;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Optional;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author alacambra
 */
public class Item {

    private Path path;
    private String fileName;
    private JsonObject tplParams;
    private boolean internalDependency;
    private String baseImage;
    private String imageId;
    private String buildName;

    public Item(Path path, String fileName) {
        this.path = path;
        this.fileName = fileName;
    }

    public void setTplParams(JsonObject tplParams) {
        this.tplParams = tplParams;
        baseImage = tplParams.getString("baseImage");
        imageId = tplParams.getString("imageId");
        buildName = tplParams.getString("buildName");
    }

    public String getImageId() {
        return imageId;
    }

    public String getBaseImage() {
        return baseImage;
    }

    public String getBuildName() {
        return buildName;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isInternalDependency() {
        return internalDependency;
    }

    public void setInternalDependency(boolean internalDependency) {
        this.internalDependency = internalDependency;
    }

    public JsonObject getTplParams() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        tplParams.entrySet().stream().forEach((entry) -> {
            builder.add(entry.getKey(), entry.getValue());
        });

        builder.add("baseImage", Optional.ofNullable(baseImage).orElse(tplParams.getString("baseImage")))
                .add("imageId", Optional.ofNullable(imageId).orElse(tplParams.getString("imageId")))
                .add("buildName", Optional.ofNullable(buildName).orElse(tplParams.getString("buildName")));

        return builder.build();

    }

    public void setBaseImage(String baseImage) {
        this.baseImage = baseImage;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public void setBuildName(String buildName) {
        this.buildName = buildName;
    }

    public JsonObject getAsJson() {
        return Json.createObjectBuilder()
                .add("path", path.toAbsolutePath().resolve(fileName).toString())
                .add("params", getTplParams())
                .build();
    }

    @Override
    public String toString() {
        return getAsJson().toString();
    }

}
