package io.headpro.docker.builder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.tomitribe.crest.api.Command;
import org.tomitribe.crest.api.Option;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    String inputDir;
    String tplExtension = ".tpl";
    String paramsExtension = ".json";
    List<Item> itemsFound = new ArrayList<>();
    List<Item> buildOrder = new ArrayList();
    List<String> buildCommands;

    /**
     * Mapping between an image id an the name used in its build: ex: BASE -> ubuntu, KC -> keycloak
     */
    Map<String, String> idBuild = new HashMap<>();
    Map<String, String> dependencies = new HashMap<>();

    @Command
    public void generate(@Option("in") String inputDir) {
        if (!Files.isDirectory(Paths.get(inputDir))) {
            throw new RuntimeException("Not a directory: " + inputDir);
        }

        scan(Paths.get(inputDir));
        orderItems();
        writeAll();
        buildCommands = prepareBuildCommands();
    }
    
    @Command
    public void commands(@Option("in") String inputDir) {
        generate(inputDir);
        buildCommands.forEach(System.out::println);
    }

    @Command
    public void find(@Option("in") String inputDir) {
        if (!Files.isDirectory(Paths.get(inputDir))) {
            throw new RuntimeException("Not a directory: " + inputDir);
        }

        scan(Paths.get(inputDir));

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);

        StringWriter sw = new StringWriter();
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        try (JsonWriter jsonWriter = writerFactory.createWriter(sw)) {
            jsonWriter.writeArray(
                    itemsFound.stream()
                    .map(Item::getAsJson)
                    .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                    .build()
            );
        }
        String prettyPrinted = sw.toString();
        System.out.println(prettyPrinted);
    }

    public void scan(Path sourcePath) {
        try {
            Iterator<Path> directories = Files.newDirectoryStream(Paths.get(sourcePath.toUri())).iterator();
            Iterator<Path> dockerTemplates = Files.newDirectoryStream(Paths.get(sourcePath.toUri()), "*Dockerfile" + tplExtension).iterator();

            while (directories.hasNext()) {

                Path candidate = directories.next();
                if (Files.isDirectory(candidate)) {
                    scan(candidate);
                }

            }

            if (!dockerTemplates.hasNext()) {
                LOGGER.log(Level.FINEST, "no templates found on {0}", sourcePath.toString());
            }

            while (dockerTemplates.hasNext()) {

                Path path = dockerTemplates.next();
                String fileName = path.getFileName().toString();

                int extensionIndex = fileName.indexOf(tplExtension);
                String fileWithoutExtension = fileName.substring(0, extensionIndex);

                Path fullPath = Paths.get(path.getParent().toString() + "/" + fileWithoutExtension + paramsExtension);

                if (!Files.exists(fullPath)) {
                    continue;
                }

                Item item = new Item(path.getParent(), fileWithoutExtension);
                loadItem(item);
                itemsFound.add(item);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void loadItem(Item item) {
        JsonObject paramsJson = loadParameters(item);
        item.setTplParams(paramsJson);

        idBuild.put(item.getImageId(), item.getBuildName());

        if (item.getTplParams().containsKey("internalDependency") && item.getTplParams().containsKey("internalDependency")) {

            item.setInternalDependency(true);
            dependencies.put(item.getImageId(), item.getBaseImage());

        } else {

            buildOrder.add(item);

        }
    }

    private JsonObject loadParameters(Item item) {
        try {
            Path path = item.getPath().resolve(item.getFileName() + paramsExtension);
            return Json.createReader(new FileInputStream(path.toFile())).readObject();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void orderItems() {

        int i = 0;
        while (itemsFound.size() > buildOrder.size()) {

            Item nextItem = itemsFound.get(i % itemsFound.size());

            if (itemHasResolvedDependenciesOrItemReady(nextItem)) {

                if (idBuild.containsKey(nextItem.getBaseImage())) {
                    nextItem.setBaseImage(idBuild.get(nextItem.getBaseImage()));
                }

                buildOrder.add(nextItem);
                continue;
            }

            if (i > itemsFound.size() * itemsFound.size() * 1) {
                throw new RuntimeException("to many iterations: " + i);
            }
            i++;
        }
    }

    private boolean itemHasResolvedDependenciesOrItemReady(Item item) {
        return buildOrder.stream().filter(i -> i.getImageId().equals(item.getBaseImage())).count() > 0;
    }

    void writeAll() {
        buildOrder.forEach(this::write);
    }

    private void write(Item item) {

        try {

            Path output = item.getPath();
            String content = process(
                    read(Paths.get(item.getPath() + "/" + item.getFileName() + tplExtension)),
                    item.getTplParams().toString());

            String filename = item.getFileName() + tplExtension;

            filename = filename.split(tplExtension)[0];
            Path finalPath = output.resolve(filename);
            Files.write(finalPath, content.getBytes());

        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void print(String s) {

    }

    List<String> prepareBuildCommands() {
        return buildOrder.stream().map(this::prepareBuildCommand).collect(Collectors.toList());
    }

    String prepareBuildCommand(Item item) {
        String imageName = item.getBuildName();
        Path dockerFilePath = item.getPath();
        String commandToExecute = new StringBuilder("docker build -t ")
                .append(imageName).append(" ")
                .append(dockerFilePath.toAbsolutePath().toString())
                .toString();

        return commandToExecute;
    }

    void executeCommand(String command) {
        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec("/bin/sh");
            p.getOutputStream().write(command.getBytes());
            p.getOutputStream().close();
            p.waitFor();

            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println(output.toString());
    }

    String read(Path path) {
        try {
            return new String(Files.readAllBytes(path));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    String process(String content, String parameters) {
        String output = invokeMustache(content, parameters);
        return output;

    }

    String invokeMustache(String content, String parameters) {

        try {

            ScriptEngineManager engineManager = new ScriptEngineManager();
            ScriptEngine engine = engineManager.getEngineByName("nashorn");
            engine.eval(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("mustache.js")));
            Invocable invocable = (Invocable) engine;

            Object json = engine.eval("JSON");
            Object data = invocable.invokeMethod(json, "parse", parameters);

            Object mustache = engine.eval("Mustache");
            String text = (String) invocable.invokeMethod(mustache, "render", content, data);

            return text;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
