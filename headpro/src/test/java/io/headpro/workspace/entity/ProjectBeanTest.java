/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.entity;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.time.boundary.TimeRange;
import io.headpro.time.boundary.TimeRangeBuilder;
import io.headpro.utils.DatesService;
import io.headpro.utils.SlotChain;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author alacambra
 */
public class ProjectBeanTest {

    ProjectBean cut;
    SlotChain.SlotChainBuilder slotChainBuilder;
    Project project;
    DatesService datesService = new DatesService();
    TimeRangeBuilder timeRangeBuilder;

    @Before
    public void setUp() {
        cut = new ProjectBean();
        datesService.init();
        cut.timeRangeBuilder = new TimeRangeBuilder(datesService);
        project = new Project();
        project.setStartDate(new Date());
        project.setEndDate(new Date());
        cut.setProject(project);
        slotChainBuilder = new SlotChain.SlotChainBuilder();

        timeRangeBuilder = new TimeRangeBuilder(datesService);
    }

    @Test
    public void testGetSlotsMaxTime_notEmptyStep() {
        SlotChain slotChain = slotChainBuilder
                .neighborsExist(false)
                .existAbsorbedSlot(true)
                .setAbsorbedSlotDuration(5)
                .totalAbsorbedSlots(10)
                .build();

        project.resourceSlots = slotChain.getAllChainSlots().stream().map(s -> (ResourceSlot) s).collect(Collectors.toList());
        assertThat(cut.getSlotsMaxTimeRange().getRangeLength(), is(50 + 5));

    }

    @Test
    public void testGetSlotsMaxTime_noValuePerStep() {
        ResourceSlot slot = new ResourceSlot();
        slot.setStartPosition(1);
        slot.setDuration(5);
        slot.setValuePerStep(0);

        project.resourceSlots = Arrays.asList(slot);
        assertThat(cut.getSlotsMaxTimeRange().getRangeLength(), is(0));

    }

    @Test
    public void testGetSlotsMaxTime_multipleChains() {

        Service service1 = new Service();
        service1.setId(1);
        SlotChain slotChain1 = slotChainBuilder
                .neighborsExist(false)
                .existAbsorbedSlot(true)
                .setAbsorbedSlotDuration(5)
                .totalAbsorbedSlots(300)
                .useService(service1)
                .build();

        Service service2 = new Service();
        service2.setId(2);
        SlotChain slotChain2 = slotChainBuilder
                .neighborsExist(false)
                .existAbsorbedSlot(true)
                .setAbsorbedSlotDuration(5)
                .totalAbsorbedSlots(100)
                .useService(service2)
                .build();

        List<ResourceSlot> slots = new ArrayList<>();

        slots.addAll(slotChain1.getAllChainSlots().stream().map(s -> (ResourceSlot) s).collect(Collectors.toList()));
        slots.addAll(slotChain2.getAllChainSlots().stream().map(s -> (ResourceSlot) s).collect(Collectors.toList()));

        project.resourceSlots = slots;
        assertThat(cut.getSlotsMaxTimeRange().getRangeLength(), is(5 * 300 + 5));

    }

    @Test
    public void testGetSlotsMaxTime_multipleChains_spareSlots() {

        Service service1 = new Service();
        service1.setId(1);
        SlotChain slotChain1 = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(5)
                .setNeighborsAreContinguos(true)
                .existAbsorbedSlot(true)
                .setAbsorbedSlotDuration(5)
                .totalAbsorbedSlots(298)
                .useService(service1)
                .build();

        Service service2 = new Service();
        service2.setId(2);

        ResourceSlot rs = new ResourceSlot();
        rs.setService(service2);
        rs.setValuePerStep(1);
        rs.setStartPosition(2000);
        rs.setDuration(1);

        List<ResourceSlot> slots = new ArrayList<>();

        slots.addAll(slotChain1.getAllChainSlots().stream().map(s -> (ResourceSlot) s).collect(Collectors.toList()));
        slots.add(rs);

        project.resourceSlots = slots;
        TimeRange expected = timeRangeBuilder.fromIndexes(project.getStartDate(), 0, 2000).build();
        assertThat(cut.getSlotsMaxTimeRange(), is(expected));

    }

    @Test
    public void testGetSlotsMaxTime_spareSlot() {

        Service service2 = new Service();
        service2.setId(2);

        ResourceSlot rs = new ResourceSlot();
        rs.setService(service2);
        rs.setValuePerStep(1);
        rs.setStartPosition(2000);
        rs.setDuration(1);

        List<ResourceSlot> slots = new ArrayList<>();
        slots.add(rs);

        project.resourceSlots = slots;
        assertThat(cut.getSlotsMaxTimeRange().getRangeLength(), is(2001));

        rs = new ResourceSlot();
        rs.setService(service2);
        rs.setValuePerStep(1);
        rs.setStartPosition(100);
        rs.setDuration(10);

        slots = new ArrayList<>();
        slots.add(rs);

        project.resourceSlots = slots;
        assertThat(cut.getSlotsMaxTimeRange().getRangeLength(), is(110));

        ResourceSlot rs1 = new ResourceSlot();
        rs1.setService(service2);
        rs1.setValuePerStep(1);
        rs1.setStartPosition(1);
        rs1.setDuration(10);

        slots = new ArrayList<>();
        slots.add(rs);
        slots.add(rs1);

        project.resourceSlots = slots;
        assertThat(cut.getSlotsMaxTimeRange().getRangeLength(), is(110));

    }

    @Test
    public void testGetDifferentialTimeRanges() {
    }

    @Test
    public void testGetTimeRanges() {
    }

    @Test
    public void testGetResourceSlotsChains() {
    }

    @Test
    public void testGetFirstOverflowedSlots() {
    }

    @Test
    public void testRearrengeFirstOverflowedSlots() {
    }

    private void createSlotSerie() {
        Slot s = new Slot() {
        };
        s.setDuration(Integer.SIZE);
    }
}
