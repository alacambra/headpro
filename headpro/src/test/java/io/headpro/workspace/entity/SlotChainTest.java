/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.entity;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.time.boundary.TimeRange;
import io.headpro.time.boundary.TimeRangeBuilder;
import io.headpro.utils.DatesService;
import io.headpro.utils.LocalDateConverter;
import io.headpro.utils.SlotChain;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

public class SlotChainTest {

    DatesService datesService = new DatesService();
    SlotsChain<ResourceSlot> cut;
    TimeRangeBuilder timeRangeBuilder;
    SlotChain.SlotChainBuilder builder;
    Project project;

    @Before
    public void setUp() {

        project = new Project();
        project.setStartDate(new Date());
        project.setEndDate(new Date());
        datesService.init();
        timeRangeBuilder = new TimeRangeBuilder(datesService);
        builder = new SlotChain.SlotChainBuilder();

    }

    @Test
    public void testGetFirstAndLastSlot() {

        SlotChain chain = builder
                .neighborsExist(true)
                .setNeighborsAreContinguos(true)
                .build();

        List<ResourceSlot> slots
                = chain.getAllChainSlots().stream().map(s -> (ResourceSlot) s)
                .collect(Collectors.toList());

        cut = new SlotsChain<>(slots, timeRangeBuilder);

        project.resourceSlots = slots;

        assertThat(cut.getFirstSlot(), is(not(cut.getLastSlot())));
        assertThat(cut.getFirstSlot().getNextSlot(), is(cut.getLastSlot()));
        assertThat(cut.getFirstSlot(), is(slots.get(0)));
        assertThat(cut.getLastSlot(), is(slots.get(slots.size() - 1)));
    }

    @Test
    public void testGetFirstAndLastSlot_oneSlot() {

        ResourceSlot slot = new ResourceSlot();
        slot.setStartPosition(0);
        slot.setDuration(5);
        cut = new SlotsChain<>(Arrays.asList(slot), timeRangeBuilder);

        assertThat(cut.getFirstSlot(), is(slot));
        assertThat(cut.getLastSlot(), is(slot));

    }
    @Test
    public void testGetSlotsTimeRange_noSlots() {

        TimeRange expected = timeRangeBuilder.createEmptyTimeRange(datesService.getDateIndex(project.getStartDate()));
        cut = new SlotsChain<>(Collections.EMPTY_LIST, timeRangeBuilder);
        assertThat(cut.getTimeRange(datesService.getDateIndex(project.getStartDate())), is(expected));

    }

    /**
     * One unique slot exist as is overflowed
     */
    @Test
    public void testGetFirstOverflowedSlot_oneUniqueSlot_overflow() {
        LocalDate start = LocalDate.now();
        LocalDate end = start.plusWeeks(4);

        project.setStartDate(LocalDateConverter.toDate(start));
        project.setEndDate(LocalDateConverter.toDate(end));
        Service service = new Service();
        service.setId(1);

        ResourceSlot slot = new ResourceSlot();
        slot.setStartPosition(0);
        slot.setDuration(6);
        slot.setValuePerStep(BigDecimal.ONE);

        cut = new SlotsChain<>(Arrays.asList(slot), timeRangeBuilder);

        assertThat(
                cut.getFirstOverflowedSlot(
                        timeRangeBuilder.fromDates(project.getStartDate(), project.getEndDate()).build()
                ), is(slot));

    }

    @Test
    public void testGetFirstOverflowedSlot_severalSlots_overflowMiddle() {
        LocalDate start = LocalDate.now();
        LocalDate end = start.plusWeeks(4);

        project.setStartDate(LocalDateConverter.toDate(start));
        project.setEndDate(LocalDateConverter.toDate(end));
        Service service = new Service();
        service.setId(1);

        ResourceSlot slot1 = new ResourceSlot();
        slot1.setId(1);
        slot1.setStartPosition(0);
        slot1.setDuration(2);
        slot1.setValuePerStep(BigDecimal.ONE);

        ResourceSlot slot2 = new ResourceSlot();
        slot2.setId(2);
        slot2.setStartPosition(2);
        slot2.setDuration(20);
        slot2.setValuePerStep(BigDecimal.ONE);

        ResourceSlot slot3 = new ResourceSlot();
        slot3.setId(3);
        slot3.setStartPosition(22);
        slot3.setDuration(20);
        slot3.setValuePerStep(BigDecimal.ONE);

        slot1.setNextSlot(slot2);
        slot2.setPreviousSlot(slot1);
        slot2.setNextSlot(slot3);
        slot3.setPreviousSlot(slot2);

        cut = new SlotsChain<>(Arrays.asList(slot1, slot2, slot3), timeRangeBuilder);

        assertThat(cut.getFirstSlot(), is(slot1));
        assertThat(cut.getLastSlot(), is(slot3));
        assertThat(
                cut.getFirstOverflowedSlot(
                        timeRangeBuilder.fromDates(project.getStartDate(), project.getEndDate()).build()
                ), is(slot2));

    }

    @Test
    public void testGetFirstOverflowedSlot_severalSlots_overflowLast() {
        LocalDate start = LocalDate.now();
        LocalDate end = start.plusWeeks(4);

        project.setStartDate(LocalDateConverter.toDate(start));
        project.setEndDate(LocalDateConverter.toDate(end));
        Service service = new Service();
        service.setId(1);

        ResourceSlot slot1 = new ResourceSlot();
        slot1.setId(1);
        slot1.setStartPosition(0);
        slot1.setDuration(2);
        slot1.setValuePerStep(BigDecimal.ONE);

        ResourceSlot slot2 = new ResourceSlot();
        slot2.setId(2);
        slot2.setStartPosition(2);
        slot2.setDuration(2);
        slot2.setValuePerStep(BigDecimal.ONE);

        ResourceSlot slot3 = new ResourceSlot();
        slot3.setId(3);
        slot3.setStartPosition(4);
        slot3.setDuration(2);
        slot3.setValuePerStep(BigDecimal.ONE);

        slot1.setNextSlot(slot2);
        slot2.setPreviousSlot(slot1);
        slot2.setNextSlot(slot3);
        slot3.setPreviousSlot(slot2);

        cut = new SlotsChain<>(Arrays.asList(slot1, slot2, slot3), timeRangeBuilder);

        assertThat(cut.getFirstSlot(), is(slot1));
        assertThat(cut.getLastSlot(), is(slot3));
        assertThat(
                cut.getFirstOverflowedSlot(
                        timeRangeBuilder.fromDates(project.getStartDate(), project.getEndDate()).build()
                ), is(slot3));

    }

    @Test
    public void testGetFirstOverflowedSlot_severalSlots_overflowFirst() {
        LocalDate start = LocalDate.now();
        LocalDate end = start.plusWeeks(0);
        Service service = new Service();
        service.setId(1);

        project.setStartDate(LocalDateConverter.toDate(start));
        project.setEndDate(LocalDateConverter.toDate(end));

        ResourceSlot slot1 = new ResourceSlot();
        slot1.setId(1);
        slot1.setStartPosition(0);
        slot1.setDuration(2);
        slot1.setService(service);
        slot1.setValuePerStep(BigDecimal.ONE);

        ResourceSlot slot2 = new ResourceSlot();
        slot2.setId(2);
        slot2.setStartPosition(2);
        slot2.setDuration(20);
        slot2.setService(service);
        slot2.setValuePerStep(BigDecimal.ONE);

        ResourceSlot slot3 = new ResourceSlot();
        slot3.setId(3);
        slot3.setStartPosition(22);
        slot3.setDuration(20);
        slot3.setService(service);
        slot3.setValuePerStep(BigDecimal.ONE);

        slot1.setNextSlot(slot2);
        slot2.setPreviousSlot(slot1);
        slot2.setNextSlot(slot3);
        slot3.setPreviousSlot(slot2);

        cut = new SlotsChain<>(Arrays.asList(slot1, slot2, slot3), timeRangeBuilder);

        assertThat(cut.getFirstSlot(), is(slot1));
        assertThat(cut.getLastSlot(), is(slot3));
        assertThat(
                cut.getFirstOverflowedSlot(
                        timeRangeBuilder.fromDates(project.getStartDate(), project.getEndDate()).build()
                ), is(slot1));

    }

    /**
     * One unique slot exist as is not overflowed
     */
    @Test
    public void testGetFirstOverflowedSlot_oneUniqueSlot_noOverflow() {
        LocalDate start = LocalDate.now();
        LocalDate end = start.plusWeeks(4);

        project.setStartDate(LocalDateConverter.toDate(start));
        project.setEndDate(LocalDateConverter.toDate(end));
        Service service = new Service();
        service.setId(1);

        ResourceSlot slot = new ResourceSlot();
        slot.setStartPosition(0);
        slot.setDuration(5);
        slot.setService(service);
        slot.setValuePerStep(BigDecimal.ONE);

        cut = new SlotsChain<>(Arrays.asList(slot), timeRangeBuilder);

        assertThat(
                cut.getFirstOverflowedSlot(
                        timeRangeBuilder.fromDates(project.getStartDate(), project.getEndDate()).build()
                ), nullValue());

    }
    
    /**
     * One unique slot exist as is not overflowed
     */
    @Test
    public void testGetFirstOverflowedSlot_oneUniqueSlot_completelyOut() {
        LocalDate start = LocalDate.now();
        LocalDate end = start.plusWeeks(4);

        project.setStartDate(LocalDateConverter.toDate(start));
        project.setEndDate(LocalDateConverter.toDate(end));
        Service service = new Service();
        service.setId(1);

        ResourceSlot slot = new ResourceSlot();
        slot.setStartPosition(30);
        slot.setDuration(5);
        slot.setService(service);
        slot.setValuePerStep(BigDecimal.ONE);

        cut = new SlotsChain<>(Arrays.asList(slot), timeRangeBuilder);

        assertThat(
                cut.getFirstOverflowedSlot(
                        timeRangeBuilder.fromDates(project.getStartDate(), project.getEndDate()).build()
                ), is(slot));

    }

    @Test
    public void testRearrengeFirstOverflowedSlot() {
    }
}
