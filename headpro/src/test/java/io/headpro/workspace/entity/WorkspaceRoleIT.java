/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.entity;

import io.headpro.AbstractIntegrationTest;
import io.headpro.entity.User;
import io.headpro.security.boundary.Role;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.is;

/**
 *
 * @author alacambra
 */
public class WorkspaceRoleIT extends AbstractIntegrationTest {

    @Test
    public void testQueries() {

        User u = new User();
        u.setId("abc");

        Workspace ws = new Workspace();

        tx.begin();
        
        WorkspaceRole role = new WorkspaceRole();
        role.setUserRole(Role.OWNER);
        role.setUser(u);
        role.setWorkspace(ws);

        ws.setRoles(Arrays.asList(role));
        u.setWorkspaceRoles(Arrays.asList(role));
        
        em.persist(u);
        ws = em.merge(ws);
        
        tx.commit();

        tx.begin();
        List<WorkspaceRole> roles = em.createNamedQuery(WorkspaceRole.fetchRoleByUserAndWorkspace)
                .setParameter("userId", "abc")
                .setParameter("workspaceId", ws.getId())
                .getResultList();

        assertThat(roles.size(), is(1));

    }

}
