package io.headpro.utils;

import io.headpro.slots.control.ActionContext;
import io.headpro.slots.control.InteractedSlotsProvider;
import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;
import io.headpro.slots.entity.Slot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;

public class SlotChain {
    
    List<Slot> absorbedSlots;
    Slot sourceSlot;
    Slot leftNeighbor;
    Slot rightNeighbor;
    
    public List<Slot> getAbsorbedSlots() {
        return absorbedSlots;
    }
    
    public Slot getSourceSlot() {
        return sourceSlot;
    }
    
    public void setAbsorbedSlots(List<Slot> absorbedSlots) {
        this.absorbedSlots = absorbedSlots;
    }
    
    public void setSourceSlot(Slot sourceSlot) {
        this.sourceSlot = sourceSlot;
    }
    
    public Slot getLeftNeighbor() {
        return leftNeighbor;
    }
    
    public void setLeftNeighbor(Slot leftNeighbor) {
        this.leftNeighbor = leftNeighbor;
    }
    
    public Slot getRightNeighbor() {
        return rightNeighbor;
    }
    
    public void setRightNeighbor(Slot rightNeighbor) {
        this.rightNeighbor = rightNeighbor;
    }
    
    public List<Slot> getNeighbors() {
        return Arrays.asList(leftNeighbor, rightNeighbor);
    }
    
    public ActionContext generateContext() {
        
        ActionContext context = new ActionContext();
        context.setAbsorbbedSlots(absorbedSlots);
        List<Slot> overlappedSlot = new ArrayList<>();
        Optional.ofNullable(leftNeighbor).ifPresent(overlappedSlot::add);
        Optional.ofNullable(rightNeighbor).ifPresent(overlappedSlot::add);
        
        return context;
    }
    
    public List<Slot> getAllChainSlots() {
        
        List<Slot> slots = new ArrayList<>();
        
        Optional.ofNullable(leftNeighbor).ifPresent(slots::add);
        slots.add(sourceSlot);
        slots.addAll(absorbedSlots);
        Optional.ofNullable(rightNeighbor).ifPresent(slots::add);
        
        return slots;
        
    }
    
    @Override
    public String toString() {
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("new slot::(").append(sourceSlot.toString()).append(")\n");
        
        Optional.ofNullable(leftNeighbor).map(s -> "(" + s + ")-->\n").ifPresent(builder::append);
        absorbedSlots.stream().map(s -> "(" + s + ")-->\n").forEach(builder::append);
        Optional.ofNullable(rightNeighbor).map(s -> "(" + s + ")-->\n").ifPresent(builder::append);
        
        return builder.toString();
    }
    
    public static class SlotChainBuilder {
        
        private InteractedSlotsProvider interactedSlotsProvider;
        private boolean existAbsorbedSlots = false;
        private boolean neighborsExist = false;
        private int totalAbsorbedSlots = 1;
        private int startAbsorbedSlotsPosition = 5;
        private int absorbedSlotDuration = 5;
        private boolean leftSlotIsNearerThanRightSlot = false;
        private boolean shouldPersist = false;
        private EntityManager em;
        private Project project;
        private Service service;
        private int neighborsDuration = 1;
        private boolean sourceSlotMustOverlap = false;
        private boolean leftSlotMustOverlap = false;
        private boolean rightSlotMustOverlap = false;
        private boolean splitLeft = false;
        private boolean splitRight = false;
        private int baseValuePerStep = 1;
        private boolean allSlotsSameValuePerStep = false;
        private boolean neighborsAreContinguos = false;
        private boolean leftNeighborIsContiguos = false;
        private boolean rightNeighborIsContiguos = false;
        
        public SlotChainBuilder(InteractedSlotsProvider interactedSlotsProvider) {
            this.interactedSlotsProvider = interactedSlotsProvider;
        }
        
        public SlotChainBuilder() {
        }

        /**
         * absorbed slots exists and nearest slot
         *
         * @param existAbrobedSlot
         * @return
         */
        public SlotChainBuilder existAbsorbedSlot(boolean existAbrobedSlot) {
            this.existAbsorbedSlots = existAbrobedSlot;
            totalAbsorbedSlots = 0;
            return this;
        }
        
        public SlotChainBuilder totalAbsorbedSlots(int total) {
            totalAbsorbedSlots = total;
            return this;
        }
        
        public SlotChainBuilder neighborsExist(boolean neighborsExist) {
            this.neighborsExist = neighborsExist;
            return this;
        }
        
        public SlotChainBuilder setLeftSlotIsNearerThanRightSlot(boolean leftSlotIsNearerThanRightSlot) {
            this.leftSlotIsNearerThanRightSlot = leftSlotIsNearerThanRightSlot;
            return this;
        }
        
        public SlotChainBuilder persistSlots(boolean shouldPersist) {
            this.shouldPersist = shouldPersist;
            return this;
        }
        
        public SlotChainBuilder setEntityManager(EntityManager em) {
            this.em = em;
            return this;
        }
        
        public SlotChainBuilder useProject(Project project) {
            this.project = project;
            return this;
        }
        
        public SlotChainBuilder useService(Service service) {
            this.service = service;
            return this;
        }
        
        public SlotChainBuilder setNeighborsDuration(int neighborsDuration) {
            this.neighborsDuration = neighborsDuration;
            return this;
        }
        
        public SlotChainBuilder setSourceSlotMustOverlap(boolean sourceSlotMustOverlap) {
            this.sourceSlotMustOverlap = sourceSlotMustOverlap;
            
            leftSlotMustOverlap = sourceSlotMustOverlap;
            rightSlotMustOverlap = sourceSlotMustOverlap;
            
            neighborsAreContinguos = false;
            leftNeighborIsContiguos = false;
            rightNeighborIsContiguos = false;
            
            return this;
        }
        
        public SlotChainBuilder setLeftSlotMustOverlap(boolean leftSlotMustOverlap) {
            this.leftSlotMustOverlap = leftSlotMustOverlap;
            neighborsAreContinguos = !leftSlotMustOverlap && neighborsAreContinguos;
            leftNeighborIsContiguos = !leftSlotMustOverlap && leftNeighborIsContiguos;
            neighborsExist = true;
            return this;
        }
        
        public SlotChainBuilder setRightSlotMustOverlap(boolean rightSlotMustOverlap) {
            this.rightSlotMustOverlap = rightSlotMustOverlap;
            neighborsAreContinguos = !rightSlotMustOverlap && neighborsAreContinguos;
            rightNeighborIsContiguos = !rightSlotMustOverlap && rightNeighborIsContiguos;
            return this;
        }
        
        public SlotChainBuilder setNeighborsAreContinguos(boolean neighborsAreContinguos) {
            this.neighborsAreContinguos = neighborsAreContinguos;
            
            leftNeighborIsContiguos = neighborsAreContinguos;
            rightNeighborIsContiguos = neighborsAreContinguos;
            
            sourceSlotMustOverlap = !neighborsAreContinguos && sourceSlotMustOverlap;
            leftSlotMustOverlap = !neighborsAreContinguos && leftSlotMustOverlap;
            rightSlotMustOverlap = !neighborsAreContinguos && rightSlotMustOverlap;
            
            return this;
        }
        
        public SlotChainBuilder setLeftNeighborIsContiguos(boolean leftNeighborIsContiguos) {
            this.leftNeighborIsContiguos = leftNeighborIsContiguos;
            sourceSlotMustOverlap = !leftNeighborIsContiguos && sourceSlotMustOverlap;
            leftSlotMustOverlap = !leftNeighborIsContiguos && leftSlotMustOverlap;
            return this;
        }
        
        public SlotChainBuilder setRightNeighborIsContiguos(boolean rightNeighborIsContiguos) {
            this.rightNeighborIsContiguos = rightNeighborIsContiguos;
            sourceSlotMustOverlap = !rightNeighborIsContiguos && sourceSlotMustOverlap;
            rightSlotMustOverlap = !rightNeighborIsContiguos && rightSlotMustOverlap;
            return this;
        }
        
        public SlotChainBuilder setSplitLeft(boolean splitLeft) {
            this.splitLeft = splitLeft;
            return this;
        }
        
        public SlotChainBuilder setSplitRight(boolean splitRight) {
            this.splitRight = splitRight;
            return this;
        }
        
        public SlotChainBuilder setBaseValuePerStep(int baseValuePerStep) {
            this.baseValuePerStep = baseValuePerStep;
            return this;
        }
        
        public SlotChainBuilder setAllSlotsSameValuePerStep(boolean allSlotsSameValuePerStep) {
            this.allSlotsSameValuePerStep = allSlotsSameValuePerStep;
            return this;
        }
        
        public SlotChainBuilder setAbsorbedSlotDuration(int duration) {
            this.absorbedSlotDuration = duration;
            return this;
        }
        
        public SlotChain build() {
            
            IdGenerator idGenerator = new IdGenerator(shouldPersist);
            
            SlotChain chain = new SlotChain();
            Slot sourceSlot = new ResourceSlot();
            
            Slot leftAbsorbedSlot = null;
            Slot rightAbsorbedSlot = null;
            Slot leftNeighbor = null;
            Slot rightNeighbor = null;

            /**
             * Default neighbors configuration. Can be overwritten
             */
            if (neighborsExist) {
                
                leftNeighbor = new ResourceSlot();
                leftNeighbor.setStartPosition(0);
                leftNeighbor.setDuration(neighborsDuration);
                leftNeighbor.setId(idGenerator.getNext());
                
                rightNeighbor = new ResourceSlot();
                rightNeighbor.setStartPosition(totalAbsorbedSlots * absorbedSlotDuration + neighborsDuration);
                rightNeighbor.setDuration(neighborsDuration);
                rightNeighbor.setId(idGenerator.getNext());
                
                leftNeighbor.setNextSlot(rightNeighbor);
                rightNeighbor.setPreviousSlot(leftNeighbor);
            }

            /**
             * Source slot basic configuration depending on the neighbors existence. Can be overwritten
             */
            if (neighborsExist) {
                
                if (splitLeft) {
                    
                    if (leftNeighbor.getDuration() < 3) {
                        throw new RuntimeException("slot is to short to be splitted");
                    }
                    
                    int start = leftNeighbor.getStartPosition() + 1;
                    int duration = leftNeighbor.getDuration() - 2;
                    
                    sourceSlot.setStartPosition(start);
                    sourceSlot.setDuration(duration);
                    
                } else if (splitRight) {
                    
                    if (rightNeighbor.getDuration() < 3) {
                        throw new RuntimeException("slot is to short to be splitted");
                    }
                    
                    int start = rightNeighbor.getStartPosition() + 1;
                    int duration = rightNeighbor.getDuration() - 2;
                    
                    rightNeighbor.setStartPosition(start);
                    rightNeighbor.setDuration(duration);
                    
                } else {
                    
                    if (leftSlotMustOverlap || sourceSlotMustOverlap) {
                        
                        int startPosition = Optional.ofNullable(sourceSlot.getStartPosition()).orElse(leftNeighbor.getEndPosition() + 1);
                        int extension = startPosition - leftNeighbor.getEndPosition() - 1;
                        int duration = extension + Optional.ofNullable(sourceSlot.getDuration()).orElse(1);
                        
                        sourceSlot.setDuration(duration + 1);
                        sourceSlot.setStartPosition(leftNeighbor.getEndPosition());
                        
                    } else if (leftNeighborIsContiguos || neighborsAreContinguos) {
                        
                        int startPosition = Optional.ofNullable(sourceSlot.getStartPosition()).orElse(leftNeighbor.getEndPosition() + 1);
                        int extension = startPosition - leftNeighbor.getEndPosition() - 1;
                        int duration = extension + Optional.ofNullable(sourceSlot.getDuration()).orElse(1);
                        
                        sourceSlot.setDuration(duration);
                        sourceSlot.setStartPosition(leftNeighbor.getEndPosition() + 1);
                        
                    } else {
                        Slot ln = leftNeighbor;
                        int start = Optional.ofNullable(sourceSlot.getStartPosition())
                                .filter(s -> (s - 2) >= ln.getEndPosition())
                                .orElse(leftNeighbor.getEndPosition() + 2);
                        
                        int duration = Optional.ofNullable(leftNeighbor.getDuration()).orElse(1);
                        
                        sourceSlot.setStartPosition(start);
                        sourceSlot.setDuration(duration);
                    }
                    
                    if (rightSlotMustOverlap || sourceSlotMustOverlap) {
                        
                        int start = Optional.ofNullable(leftNeighbor.getEndPosition()).orElse(0);
                        int duration = 1 + rightNeighbor.getStartPosition() - start;
                        
                        sourceSlot.setStartPosition(start);
                        sourceSlot.setDuration(duration);
                        
                    } else if (rightNeighborIsContiguos || neighborsAreContinguos) {
                        
                        int start = Optional.ofNullable(sourceSlot.getStartPosition())
                                .orElse(rightNeighbor.getStartPosition() - 1);
                        
                        int duration = rightNeighbor.getStartPosition() - start;
                        
                        sourceSlot.setStartPosition(start);
                        sourceSlot.setDuration(duration);
                        
                    } else {
                        Slot rn = rightNeighbor;
                        int start = Optional.ofNullable(sourceSlot.getStartPosition())
                                .filter(s -> (s + 2) <= rn.getStartPosition())
                                .orElse(rightNeighbor.getStartPosition() - 2);
                        
                        int duration = rightNeighbor.getStartPosition() - start - 1;
                        
                        sourceSlot.setStartPosition(start);
                        sourceSlot.setDuration(duration);
                    }
                }
                
            } else {
                sourceSlot.setStartPosition(10);
                sourceSlot.setDuration(1);
            }
            
            List<Slot> absorbedSlots = new ArrayList<>();
            
            if (existAbsorbedSlots) {

                //Check compatibility with neighbors
                if (neighborsExist) {
                    startAbsorbedSlotsPosition = leftNeighbor.getEndPosition() + 1;
                }
                
                if (totalAbsorbedSlots == 1) {
                    leftAbsorbedSlot = rightAbsorbedSlot = new ResourceSlot();
                    leftAbsorbedSlot.setId(idGenerator.getNext());
                    leftAbsorbedSlot.setStartPosition(startAbsorbedSlotsPosition);
                    leftAbsorbedSlot.setDuration(absorbedSlotDuration);
                    absorbedSlots.add(leftAbsorbedSlot);
                }
                
                if (totalAbsorbedSlots >= 2) {
                    
                    leftAbsorbedSlot = new ResourceSlot();
                    leftAbsorbedSlot.setId(idGenerator.getNext());
                    leftAbsorbedSlot.setStartPosition(startAbsorbedSlotsPosition);
                    leftAbsorbedSlot.setDuration(absorbedSlotDuration);
                    absorbedSlots.add(leftAbsorbedSlot);
                    
                    rightAbsorbedSlot = new ResourceSlot();
                    rightAbsorbedSlot.setId(idGenerator.getNext());
                    rightAbsorbedSlot.setStartPosition((totalAbsorbedSlots - 1) * absorbedSlotDuration + startAbsorbedSlotsPosition);
                    rightAbsorbedSlot.setDuration(absorbedSlotDuration);
                    
                    absorbedSlots.add(rightAbsorbedSlot);
                }
                
                if (totalAbsorbedSlots > 2) {
                    for (int i = 1; i < totalAbsorbedSlots - 1; i++) {
                        Slot centralAbsorbedSlot = new ResourceSlot();
                        centralAbsorbedSlot.setId(idGenerator.getNext());
                        
                        centralAbsorbedSlot.setPreviousSlot(absorbedSlots.get(i - 1));
                        absorbedSlots.get(i - 1).setNextSlot(centralAbsorbedSlot);
                        
                        centralAbsorbedSlot.setNextSlot(absorbedSlots.get(i));
                        absorbedSlots.get(i).setPreviousSlot(centralAbsorbedSlot);
                        centralAbsorbedSlot.setStartPosition(i * absorbedSlotDuration + startAbsorbedSlotsPosition);
                        centralAbsorbedSlot.setDuration(absorbedSlotDuration);
                        
                        absorbedSlots.add(i, centralAbsorbedSlot);
                    }
                }

                //make the last absorbed slot to be contiguos with the right neighbor
                if (neighborsExist) {
                    Slot lastAbsorbedSlot = absorbedSlots.get(absorbedSlots.size() - 1);
                    lastAbsorbedSlot.setEndPosition(rightNeighbor.getStartPosition() - 1);
                }
                
                sourceSlot.setStartPosition(startAbsorbedSlotsPosition);
                sourceSlot.setDuration(totalAbsorbedSlots * absorbedSlotDuration);
                
                Optional<Slot> ln = Optional.ofNullable(leftNeighbor);
                
                Optional.ofNullable(leftAbsorbedSlot).ifPresent(lAbsorbedSlot -> {
                    ln.ifPresent(lNeighbor -> {
                        lAbsorbedSlot.setPreviousSlot(lNeighbor);
                        lNeighbor.setNextSlot(lAbsorbedSlot);
                    });
                });
                
                Optional<Slot> rn = Optional.ofNullable(rightNeighbor);
                
                Optional.ofNullable(rightAbsorbedSlot).ifPresent(rAbsorbedSlot -> {
                    rn.ifPresent(rNeighbor -> {
                        rAbsorbedSlot.setNextSlot(rNeighbor);
                        rNeighbor.setPreviousSlot(rAbsorbedSlot);
                    });
                });
                
            } else if (neighborsExist) {
                /**
                 * Configuration for the interactedSlotsProvider. No neighbors configuration will be done
                 */
                if (leftSlotIsNearerThanRightSlot) {
                    Slot n = leftNeighbor;
                    Optional.ofNullable(interactedSlotsProvider)
                            .ifPresent(isp -> when(isp.getNearestSlot(any(Slot.class))).thenReturn(n));
                } else {
                    Slot n = rightNeighbor;
                    Optional.ofNullable(interactedSlotsProvider)
                            .ifPresent(isp -> when(isp.getNearestSlot(any(Slot.class))).thenReturn(n));
                }
            }
            
            sourceSlot.setId(idGenerator.getNext());
            
            chain.setAbsorbedSlots(absorbedSlots);
            chain.setLeftNeighbor(leftNeighbor);
            chain.setRightNeighbor(rightNeighbor);
            chain.setSourceSlot(sourceSlot);
            
            Optional.ofNullable(project).ifPresent(
                    p -> chain.getAllChainSlots().forEach(s -> ((ResourceSlot) s).setProject(p)));
            
            Optional.ofNullable(service).ifPresent(
                    s -> chain.getAllChainSlots().forEach(sl -> sl.setService(s)));
            
            int value = baseValuePerStep;
            for (Slot s : chain.getAllChainSlots()) {
                if (allSlotsSameValuePerStep) {
                    s.setValuePerStep(baseValuePerStep);
                } else {
                    s.setValuePerStep(value++);
                }
            }
            
            if (shouldPersist) {
                em.persist(chain.getLeftNeighbor());
            }
            
            return chain;
        }
    }
    
    private static class IdGenerator {
        
        Integer current = 0;
        boolean isNullGenerator = false;
        
        public IdGenerator(boolean isNullGenerator) {
            this.isNullGenerator = isNullGenerator;
        }
        
        public Integer getNext() {
            
            return isNullGenerator ? null : current++;
        }
    }
}
