package io.headpro.time.boundary;

import io.headpro.utils.DatesService;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 *
 * @author alacambra
 */
public class TimeRangeTest {

    TimeRange cut;
    TimeRangeBuilder builder;
    DatesService datesService = new DatesService();

    @Before
    public void setUp() {
        datesService.init();
        builder = new TimeRangeBuilder(datesService);
    }

    @Test
    public void testGetDifferentialTimeRange() {
        TimeRange tr1 = builder.fromIndexes(0, 100).build();
        TimeRange tr2 = builder.fromIndexes(10, 20).build();

        TimeRange diff = tr1.getDifferentialTimeRange(tr2);

        TimeRange expected = builder.fromIndexes(21, 100).build();

        assertThat(diff, is(expected));
    }

    @Test
    public void testMoveEnd() {
        cut = builder.fromIndexes(20, 30).build();
        TimeRange expected = builder.fromIndexes(20, 40).build();

        assertThat(cut.moveEnd(10), is(expected));

        expected = builder.fromIndexes(20, 35).build();
        assertThat(cut.moveEnd(-5), is(expected));
    }

    @Test
    public void testMoveStart() {

        cut = builder.fromIndexes(20, 30).build();
        TimeRange expected = builder.fromIndexes(25, 30).build();

        assertThat(cut.moveStart(5), is(expected));

        expected = builder.fromIndexes(15, 30).build();
        assertThat(cut.moveStart(-10), is(expected));

    }

    @Test
    public void testBeginsBefore() {
        TimeRange tr1 = builder.fromIndexes(5, 100).build();
        TimeRange tr2 = builder.fromIndexes(10, 20).build();

        assertTrue(tr1.beginsBefore(tr2));
        assertFalse(tr2.beginsBefore(tr1));
        
        tr1 = builder.fromIndexes(10, 100).build();
        tr2 = builder.fromIndexes(10, 20).build();

        assertFalse(tr1.beginsBefore(tr2));
        assertFalse(tr2.beginsBefore(tr1));
    }

    @Test
    public void testEndsBefore() {
        TimeRange tr1 = builder.fromIndexes(30, 100).build();
        TimeRange tr2 = builder.fromIndexes(10, 20).build();

        assertTrue(tr2.endsBefore(tr1));
        assertFalse(tr1.endsBefore(tr2));
        
        tr1 = builder.fromIndexes(0, 20).build();
        tr2 = builder.fromIndexes(10, 20).build();

        assertFalse(tr1.endsBefore(tr2));
        assertFalse(tr2.endsBefore(tr1));
    }

    @Test
    public void testHasSameLength() {
        TimeRange tr1 = builder.fromIndexes(30, 40).build();
        TimeRange tr2 = builder.fromIndexes(10, 20).build();

        assertTrue(tr2.hasSameLength(tr1));
        assertTrue(tr1.hasSameLength(tr2));
        
       tr1 = builder.fromIndexes(30, 430).build();
       tr2 = builder.fromIndexes(10, 20).build();

        assertFalse(tr2.hasSameLength(tr1));
        assertFalse(tr1.hasSameLength(tr2));
    }

    @Test
    public void testIsEmpty() {
        assertTrue(builder.createEmptyTimeRange(100).isEmpty());
        assertFalse(builder.fromIndexes(30, 430).build().isEmpty());
    }

    @Test
    public void testGetRangeLength() {
        assertThat(builder.fromIndexes(0, 10).build().getRangeLength(), is(11));
    }

    @Test
    public void testCopy() {
        cut = builder.fromIndexes(0, 10).build();
        assertFalse(cut == cut.copy());
        assertThat(cut, is(cut.copy()));
    }
}
