/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.control.NeighborsSlots;
import io.headpro.slots.control.ActionContext;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

/**
 *
 * @author alacambra
 */
public class SimpleAddActionTest {

    SimpleAddAction cut;
    NeighborsSlots neighborsSlots;
    ActionContext actionContext;
    Slot newSlot;

    @Before
    public void setUp() {

        cut = new SimpleAddAction();
        neighborsSlots = mock(NeighborsSlots.class);
        actionContext = new ActionContext();
        cut.neighborsSlots = neighborsSlots;
        cut.context = actionContext;

        newSlot = new ResourceSlot();
        newSlot.setId(0);
        newSlot.setStartPosition(5);
        newSlot.setDuration(5);
        newSlot.setValuePerStep(123);

        actionContext.setAbsorbbedSlots(Collections.EMPTY_LIST)
                .setOverlappedSlots(Collections.EMPTY_LIST)
                .setNewSlot(newSlot);

        when(neighborsSlots.getNextNeighbor()).thenReturn(null);
        when(neighborsSlots.getPreviousNeighbor()).thenReturn(null);
        when(neighborsSlots.setAbsorbedSlots(any())).thenReturn(neighborsSlots);
        when(neighborsSlots.setSourceSlot(any())).thenReturn(neighborsSlots);
    }

    @Test
    public void testPerform_isFirstSlot() {

        assertThat(cut.isValid(), is(true));
        cut.perform();
        assertThat(newSlot.getNextSlot(), nullValue());
        assertThat(newSlot.getPreviousSlot(), nullValue());

    }

    @Test
    public void testPerform_actionNotValid() {

        List<Slot> overlappedSlots = new ArrayList<>();
        overlappedSlots.add(new ResourceSlot());
        actionContext.setOverlappedSlots(overlappedSlots);

        assertThat(cut.isValid(), is(false));

    }

    @Test
    public void testPerform_hasNeighbors() {

        Slot nextNeighbor = new ResourceSlot();
        nextNeighbor.setId(1);
        nextNeighbor.setStartPosition(newSlot.getEndPosition() + 1);
        nextNeighbor.setDuration(5);

        Slot previousNeighbor = new ResourceSlot();
        previousNeighbor.setId(2);
        previousNeighbor.setStartPosition(newSlot.getStartPosition() - 1);
        previousNeighbor.setDuration(1);

        nextNeighbor.setPreviousSlot(previousNeighbor);
        previousNeighbor.setNextSlot(nextNeighbor);

        when(neighborsSlots.getNextNeighbor()).thenReturn(nextNeighbor);
        when(neighborsSlots.getPreviousNeighbor()).thenReturn(previousNeighbor);

        assertThat(cut.isValid(), is(true));

        cut.perform();

        assertThat(newSlot.getNextSlot(), is(nextNeighbor));
        assertThat(newSlot.getPreviousSlot(), is(previousNeighbor));

        assertThat(previousNeighbor.getNextSlot(), is(newSlot));
        assertThat(nextNeighbor.getPreviousSlot(), is(newSlot));

    }

    @Test
    public void testPerform_hasOneNeighbor() {

        Slot neighbor = new ResourceSlot();
        neighbor.setId(1);
        neighbor.setStartPosition(newSlot.getEndPosition() + 1);
        neighbor.setDuration(5);

        when(neighborsSlots.getNextNeighbor()).thenReturn(neighbor);

        assertThat("Action is not valid because: " +  cut.getInvalidActionReason(), cut.isValid(), is(true));

        cut.perform();

        assertThat(newSlot.getNextSlot(), is(neighbor));
        assertThat(newSlot.getPreviousSlot(), nullValue());
        assertThat(neighbor.getNextSlot(), nullValue());
        assertThat(neighbor.getPreviousSlot(), is(newSlot));

        setUp();
        neighbor = new ResourceSlot();
        neighbor.setId(1);
        neighbor.setStartPosition(newSlot.getEndPosition() + 1);
        neighbor.setDuration(5);

        when(neighborsSlots.getPreviousNeighbor()).thenReturn(neighbor);

        assertThat(cut.isValid(), is(true));

        cut.perform();

        assertThat(newSlot.getPreviousSlot(), is(neighbor));
        assertThat(newSlot.getNextSlot(), nullValue());
        assertThat(neighbor.getPreviousSlot(), nullValue());
        assertThat(neighbor.getNextSlot(), is(newSlot));

    }

}
