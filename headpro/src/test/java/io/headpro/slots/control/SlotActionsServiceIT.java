package io.headpro.slots.control;

import io.headpro.AbstractIntegrationTest;
import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;
import io.headpro.slots.entity.ServiceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.testutils.SlotJsonLoader;
import io.headpro.utils.SlotChain;
import org.junit.Before;
import org.junit.Test;

import javax.enterprise.inject.Instance;
import java.math.BigDecimal;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 *
 * @author alacambra
 */
public class SlotActionsServiceIT extends AbstractIntegrationTest {

    SlotActionsService cut;
    SlotChain.SlotChainBuilder slotChainBuilder;
    InteractedSlotsProvider interactedSlotsProvider;
    Project dummyProject;
    Service dummyService;
    SlotJsonLoader slotJsonLoader;

    @Override
    @Before
    public void setUp() {
        super.setUp();

        dummyProject = createDummyProject();
        dummyService = createDummyService();

        slotJsonLoader = new SlotJsonLoader(em, tx, dummyProject, dummyService);

        cut = new SlotActionsService();
        cut.em = em;

        slotChainBuilder = new SlotChain.SlotChainBuilder()
                .persistSlots(true)
                .setEntityManager(em)
                .useProject(dummyProject)
                .useService(dummyService);

        interactedSlotsProvider = new InteractedSlotsProvider();
        interactedSlotsProvider.em = em;

        NeighborsSlots neighborsSlots = new NeighborsSlots();
        neighborsSlots.interactedSlotsProvider = interactedSlotsProvider;

        cut.actionContextInstance = mock(Instance.class);
        when(cut.actionContextInstance.get()).thenReturn(new ActionContext());

        cut.interactedSlotsProvider = interactedSlotsProvider;
        when(cut.actionContextInstance.get()).thenReturn(new ActionContext());

        cut.leftReductionActions = mock(Instance.class);
        LeftReductionAction leftReductionAction = new LeftReductionAction();
        leftReductionAction.neighborsSlots = neighborsSlots;
        when(cut.leftReductionActions.get()).thenReturn(leftReductionAction);

        cut.rightReductionActions = mock(Instance.class);
        RightReductionAction rightReductionAction = new RightReductionAction();
        when(cut.rightReductionActions.get()).thenReturn(rightReductionAction);

        cut.mergeActions = mock(Instance.class);
        MergeAction mergeAction = new MergeAction();
        mergeAction.neighborsSlots = neighborsSlots;
        when(cut.mergeActions.get()).thenReturn(mergeAction);

        cut.simpleAddActions = mock(Instance.class);
        SimpleAddAction simpleAddAction = new SimpleAddAction();
        when(cut.simpleAddActions.get()).thenReturn(simpleAddAction);

        simpleAddAction.neighborsSlots = neighborsSlots;

        cut.splitActions = mock(Instance.class);
        SplitAction splitAction = new SplitAction();
        splitAction.interactedSlotsProvider = interactedSlotsProvider;
        when(cut.splitActions.get()).thenReturn(splitAction);

        cut.unionActions = mock(Instance.class);
        UnionAction unionAction = new UnionAction();
        unionAction.neighborsSlots = neighborsSlots;
        when(cut.unionActions.get()).thenReturn(unionAction);

        cut.init();

    }

    @Test
    public void testAddSlot_simpleAddAction() {

        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsAreContinguos(false)
                .build();

        Slot slot = chain.getSourceSlot();
        tx.commit();

        tx.begin();

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);
        Slot actual = result.getSavedSlot();
        tx.commit();

        assertThat(result.getActionsRunned().get(0).getClass().isAssignableFrom(SimpleAddAction.class), is(true));
        assertThat(actual.getId(), notNullValue());
        assertThat(actual.getNextSlot(), is(chain.getRightNeighbor()));
        assertThat(actual.getPreviousSlot(), is(chain.getLeftNeighbor()));
    }

    @Test
    public void testAddSlot_leftReduction() {

        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setRightSlotMustOverlap(true)
                .setNeighborsDuration(5)
                .build();

        Slot slot = chain.getSourceSlot();
        slot.setValuePerStep(121);

        tx.commit();
        tx.begin();

        int initialDuration = chain.getRightNeighbor().getDuration();
        int overlappedArea = SlotsHelper.getOverlappedArea(chain.getSourceSlot(), chain.getRightNeighbor());

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);

        tx.commit();

        slot = result.getSavedSlot();

        assertThat(slot.getNextSlot(), is(chain.getRightNeighbor()));
        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(slot));
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(slot));
        assertThat(chain.getRightNeighbor().getDuration(), is(initialDuration - overlappedArea));
    }

    @Test
    public void testAddSlot_rightReduction() {

        tx.begin();
        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setLeftSlotMustOverlap(true)
                .setNeighborsDuration(5)
                .build();

        Slot slot = chain.getSourceSlot();
        slot.setValuePerStep(121);

        int initialDuration = chain.getLeftNeighbor().getDuration();
        int overlappedArea = SlotsHelper.getOverlappedArea(chain.getSourceSlot(), chain.getLeftNeighbor());

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);

        tx.commit();

        slot = result.getSavedSlot();

        assertThat(result.getActionsRunnedNames().size(), is(1));
        assertThat(result.getActionsRunnedNames().get(0), is(RightReductionAction.class.getSimpleName()));

        assertThat(chain.getLeftNeighbor().getDuration(), is(initialDuration - overlappedArea));
        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
        assertThat(slot.getNextSlot(), is(chain.getRightNeighbor()));

        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(slot));
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(slot));

    }

    @Test
    public void testAddSlot_split() {

        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSplitLeft(true)
                .build();

        Slot slot = chain.getSourceSlot();
        slot.setValuePerStep(121);

        int initialDuration = chain.getLeftNeighbor().getDuration();

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);

        tx.commit();

        slot = result.getSavedSlot();
        Slot rest = slot.getNextSlot();

        assertThat(result.getActionsRunnedNames().size(), is(1));
        assertThat(result.getActionsRunnedNames().get(0), is(SplitAction.class.getSimpleName()));

        assertThat(chain.getLeftNeighbor().getDuration(), is(slot.getStartPosition() - chain.getLeftNeighbor().getStartPosition()));
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(slot));

        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
        assertThat(slot.getNextSlot(), is(rest));

        assertThat(rest.getPreviousSlot(), is(slot));
        assertThat(rest.getNextSlot(), is(chain.getRightNeighbor()));
        assertThat(rest.getDuration(), is(initialDuration - slot.getDuration() - chain.getLeftNeighbor().getDuration()));
        assertThat(rest.getValuePerStep(), is(chain.getLeftNeighbor().getValuePerStep()));

        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(rest));

    }

    @Test
    public void testAddSlot_union_overlap() {

        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSourceSlotMustOverlap(true)
                .setBaseValuePerStep(5)
                .setAllSlotsSameValuePerStep(true)
                .build();

        Slot slot = chain.getSourceSlot();
        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);

        tx.commit();

        slot = result.getSavedSlot();

        assertActionsRunned(result.getActionsRunnedNames(), UnionAction.class.getSimpleName());
        assertThat(slot, is(chain.getLeftNeighbor()));

        int expectedDuration = chain.getRightNeighbor().getEndPosition() - chain.getLeftNeighbor().getStartPosition() + 1;
        assertThat(chain.getLeftNeighbor().getDuration(), is(expectedDuration));
    }

    @Test
    public void testAddSlot_union_contiguos() {

        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setNeighborsAreContinguos(true)
                .setBaseValuePerStep(5)
                .setAllSlotsSameValuePerStep(true)
                .build();

        Slot slot = chain.getSourceSlot();

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);
        slot = result.getSavedSlot();

        tx.commit();

        assertActionsRunned(result.getActionsRunnedNames(), UnionAction.class.getSimpleName());
        assertThat(slot, is(chain.getLeftNeighbor()));
        int expectedDuration = chain.getRightNeighbor().getEndPosition() - chain.getLeftNeighbor().getStartPosition() + 1;
        assertThat(chain.getLeftNeighbor().getDuration(), is(expectedDuration));
    }

    @Test
    public void testAddSlot_mergeLeft_reduceRight() {

        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSourceSlotMustOverlap(true)
                .setBaseValuePerStep(5)
                .setAllSlotsSameValuePerStep(true)
                .build();

        chain.getRightNeighbor().setValuePerStep(chain.getSourceSlot().getValuePerStep().add(BigDecimal.ONE));

        Slot slot = chain.getSourceSlot();
        int expectedMergeDuration = slot.getEndPosition() - chain.getLeftNeighbor().getStartPosition() + 1;
        int expectedReductionDuration = chain.getRightNeighbor().getEndPosition() - slot.getEndPosition();

        System.out.println(chain);

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);

        tx.commit();

        System.out.println(result);
        System.out.println(chain);

        assertActionsRunned(result.getActionsRunnedNames(),
                MergeAction.class.getSimpleName(), LeftReductionAction.class.getSimpleName());

        slot = result.getSavedSlot();
        assertThat(slot, is(chain.getLeftNeighbor()));
        assertChain(null, chain.getLeftNeighbor(), chain.getRightNeighbor());

        assertThat(chain.getLeftNeighbor().getDuration(), is(expectedMergeDuration));
        assertThat(chain.getRightNeighbor().getDuration(), is(expectedReductionDuration));

    }

    @Test
    public void testAddSlot_double_reduction() {

        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSourceSlotMustOverlap(true)
                .setAllSlotsSameValuePerStep(false)
                .build();

        Slot slot = chain.getSourceSlot();

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);

        tx.commit();

        slot = result.getSavedSlot();

        assertThat(result.getActionsRunned().size(), is(2));

        assertActionsRunned(result.getActionsRunnedNames(),
                LeftReductionAction.class.getSimpleName(), RightReductionAction.class.getSimpleName());

        assertChain(chain.getLeftNeighbor(), slot, chain.getRightNeighbor());
    }

    @Test
    public void testAddSlot_absorbedSlot_leftReduction() {

//        A -> B_ -> C_ -> C : update B_, C_ for B_
        tx.begin();

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .existAbsorbedSlot(true)
                .setAbsorbedSlotDuration(1)
                .setNeighborsAreContinguos(true)
                .totalAbsorbedSlots(1)
                .setNeighborsDuration(2)
                .setSourceSlotMustOverlap(true)
                .setAllSlotsSameValuePerStep(false)
                .build();

        tx.commit();

        Slot startSlot = chain.getAbsorbedSlots().get(0);

        assertThat(chain.getAbsorbedSlots().size(), is(1));
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(startSlot));
        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(startSlot));

        Slot newSlot = chain.getSourceSlot();

        newSlot.setStartPosition(startSlot.getStartPosition());
        newSlot.setValuePerStep(startSlot.getValuePerStep());
        newSlot.setDuration(2);

        tx.begin();

        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(newSlot);

        tx.commit();

        newSlot = result.getSavedSlot();

        assertThat(result.getActionsRunned().size(), is(1));
        assertThat(result.getActionsRunnedNames().get(0), is(LeftReductionAction.class.getSimpleName()));
        assertChain(chain.getLeftNeighbor(), newSlot, chain.getRightNeighbor());

    }

    @Test
    public void testAddSlot_flowWithAbsorbedSlotsAndLeftReductionAction() {

        /*
        Create a long slot
         */
        tx.begin();

        Project project = createDummyProject();
        Service service = createDummyService();

        ResourceSlot slot = new ResourceSlot();
        slot.setProject(project);
        slot.setService(service);
        slot.setValuePerStep(BigDecimal.ONE);
        slot.setStartPosition(0);
        slot.setDuration(10);

        slot = em.merge(slot);

        tx.commit();

        /*
        Split the slot
         */
        ResourceSlot splitter = new ResourceSlot();
        splitter.setProject(project);
        splitter.setService(service);
        splitter.setValuePerStep(BigDecimal.ZERO);
        splitter.setStartPosition(2);
        splitter.setDuration(1);

        tx.begin();
        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(splitter);
        tx.commit();

        assertActionsRunned(result.getActionsRunnedNames(), SplitAction.class.getSimpleName());

        splitter = (ResourceSlot) result.getSavedSlot();
        Slot restSlot = splitter.getNextSlot();
        assertChain(slot, splitter, restSlot);

        /*
        Left reduction
         */
        ResourceSlot reducer = new ResourceSlot();
        reducer.setProject(project);
        reducer.setService(service);
        reducer.setValuePerStep(BigDecimal.ZERO);
        reducer.setStartPosition(2);
        reducer.setDuration(2);

        /*
        Instance are mocked, and therefore returns the same action objects as before.
        With setUp, genertate new mocks
         */
        setUp();
        tx.begin();
        splitter = em.find(ResourceSlot.class, splitter.getId());
        assertThat(splitter, notNullValue());

        result = cut.addSlotReturnActionResult(reducer);
        tx.commit();

        assertActionsRunned(result.getActionsRunnedNames(), LeftReductionAction.class.getSimpleName());

        reducer = (ResourceSlot) result.getSavedSlot();

        slot = em.find(ResourceSlot.class, slot.getId());
        restSlot = em.find(ResourceSlot.class, restSlot.getId());
        splitter = em.find(ResourceSlot.class, splitter.getId());

        assertThat(splitter, nullValue());
        assertChain(slot, reducer, restSlot);
    }

    /*
    https://github.com/alacambra/headpro/issues/1
    Merge entity that has been removed.
    Merge with neighbors 114
    112,  116 are abosrbed and deleted
     */
    @Test
    public void testIssue_1() {
        String json
                = "[{\"id\":114,\"duration\":2,\"startPosition\":0,\"service\":2,\"valuePerStep\":3.00,\"previousSlot\":-1,\"nextSlot\":112}, "
                + "{\"id\":112,\"duration\":2,\"startPosition\":2,\"service\":2,\"valuePerStep\":2.00,\"previousSlot\":114,\"nextSlot\":116}, "
                + "{\"id\":116,\"duration\":1,\"startPosition\":4,\"service\":2,\"valuePerStep\":3.00,\"previousSlot\":112,\"nextSlot\":115}, "
                + "{\"id\":115,\"duration\":5,\"startPosition\":5,\"service\":2,\"valuePerStep\":2.00,\"previousSlot\":116,\"nextSlot\":8}, "
                + "{\"id\":8,\"duration\":2,\"startPosition\":10,\"service\":2,\"valuePerStep\":87.00,\"previousSlot\":115,\"nextSlot\":9}, "
                + "{\"id\":9,\"duration\":2,\"startPosition\":12,\"service\":2,\"valuePerStep\":86.00,\"previousSlot\":8,\"nextSlot\":10}, "
                + "{\"id\":10,\"duration\":2,\"startPosition\":14,\"service\":2,\"valuePerStep\":14.00,\"previousSlot\":9,\"nextSlot\":11}, "
                + "{\"id\":11,\"duration\":2,\"startPosition\":16,\"service\":2,\"valuePerStep\":24.00,\"previousSlot\":10,\"nextSlot\":12}, "
                + "{\"id\":12,\"duration\":2,\"startPosition\":18,\"service\":2,\"valuePerStep\":32.00,\"previousSlot\":11,\"nextSlot\":-1}]";

        Map<Integer, Integer> idsMap = slotJsonLoader.load(json);

        ResourceSlot slot = new ResourceSlot();

        slot.setStartPosition(1);
        slot.setDuration(4);
        slot.setValuePerStep(3);
        slot.setProject(dummyProject);
        slot.setService(dummyService);

        tx.begin();
        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);
        tx.commit();

        assertActionsRunned(result.getActionsRunnedNames(), MergeAction.class.getSimpleName());

        Slot savedSlot = result.getSavedSlot();

        assertThat(idsMap.get(savedSlot.getId()), is(114));
        assertThat(savedSlot.getDuration(), is(5));

        Slot next = em.find(ResourceSlot.class, 115);
        assertChain(null, savedSlot, next);

    }

    /*
    https://github.com/alacambra/headpro/issues/3
    Should produce union  121 + 124
    125 should be absorbed
     */
    @Test
    public void testIssue_3() {
        String json = "[{\"id\":114,\"duration\":2,\"startPosition\":0,\"service\":2,\"valuePerStep\":9.00,\"previousSlot\":-1,\"nextSlot\":121}, "
                + "{\"id\":121,\"duration\":1,\"startPosition\":2,\"service\":2,\"valuePerStep\":1.00,\"previousSlot\":114,\"nextSlot\":125}, "
                + "{\"id\":125,\"duration\":1,\"startPosition\":3,\"service\":2,\"valuePerStep\":2.00,\"previousSlot\":121,\"nextSlot\":124}, "
                + "{\"id\":124,\"duration\":5,\"startPosition\":4,\"service\":2,\"valuePerStep\":1.00,\"previousSlot\":125,\"nextSlot\":123}, "
                + "{\"id\":123,\"duration\":1,\"startPosition\":9,\"service\":2,\"valuePerStep\":33.00,\"previousSlot\":124,\"nextSlot\":8}, "
                + "{\"id\":8,\"duration\":2,\"startPosition\":10,\"service\":2,\"valuePerStep\":74.00,\"previousSlot\":123,\"nextSlot\":9}, "
                + "{\"id\":9,\"duration\":2,\"startPosition\":12,\"service\":2,\"valuePerStep\":73.00,\"previousSlot\":8,\"nextSlot\":10}, "
                + "{\"id\":10,\"duration\":2,\"startPosition\":14,\"service\":2,\"valuePerStep\":8.00,\"previousSlot\":9,\"nextSlot\":11}, "
                + "{\"id\":11,\"duration\":2,\"startPosition\":16,\"service\":2,\"valuePerStep\":6.00,\"previousSlot\":10,\"nextSlot\":12}, "
                + "{\"id\":12,\"duration\":2,\"startPosition\":18,\"service\":2,\"valuePerStep\":68.00,\"previousSlot\":11,\"nextSlot\":-1}]";

        Map<Integer, Integer> idsMap = slotJsonLoader.load(json);

        ResourceSlot slot = new ResourceSlot();

        slot.setStartPosition(3);
        slot.setDuration(2);
        slot.setValuePerStep(1);
        slot.setProject(dummyProject);
        slot.setService(dummyService);

        tx.begin();
        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);
        tx.commit();

        assertActionsRunned(result.getActionsRunnedNames(), UnionAction.class.getSimpleName());

        Slot savedSlot = result.getSavedSlot();
//          
        assertThat(idsMap.get(savedSlot.getId()), is(121));
        assertThat(savedSlot.getDuration(), is(7));

        Slot previous = em.find(ResourceSlot.class, 114);
        Slot next = em.find(ResourceSlot.class, 123);
        assertChain(previous, savedSlot, next);
    }

    /*
    https://github.com/alacambra/headpro/issues/4
    Too many actions found: LeftReductionAction, RightReductionAction, MergeAction 
     */
    @Test
    public void testIssue_4() {
        String json = "["
                + "{\"id\":272,\"duration\":1,\"startPosition\":318,\"service\":4,\"valuePerStep\":1.00,\"previousSlot\":-1,\"nextSlot\":255}]"
                + "{\"id\":255,\"duration\":20,\"startPosition\":319,\"service\":4,\"valuePerStep\":2.00,\"previousSlot\":272,\"nextSlot\":-1},";

        Map<Integer, Integer> idsMap = slotJsonLoader.load(json, () -> {
            Slot s = new ServiceSlot();
            s.setService(dummyService);
            return s;
        });

        ServiceSlot slot = new ServiceSlot();

        slot.setStartPosition(319);
        slot.setDuration(20);
        slot.setValuePerStep(1);
        slot.setService(dummyService);

        tx.begin();
        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);
        tx.commit();

        assertActionsRunned(result.getActionsRunnedNames(), MergeAction.class.getSimpleName());

        Slot savedSlot = result.getSavedSlot();

        assertThat(idsMap.get(savedSlot.getId()), is(272));
        assertThat(savedSlot.getDuration(), is(21));
        assertThat(em.find(ServiceSlot.class, 255), nullValue());

    }

}
