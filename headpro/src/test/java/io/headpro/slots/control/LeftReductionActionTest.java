/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author alacambra
 */
public class LeftReductionActionTest {

    LeftReductionAction cut;

    @Before
    public void setUp() {

        cut = new LeftReductionAction();
        cut.context = new ActionContext();
        cut.neighborsSlots = mock(NeighborsSlots.class);

    }

    @Test
    public void testPerform_isValid_withoutPreviousSlot() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(5);
        overlappedSlot.setDuration(10);
        overlappedSlot.setValuePerStep(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(0);
        reducerSlot.setDuration(10);
        reducerSlot.setValuePerStep(1);
        
        cut.context.setOverlappedSlots(Arrays.asList(overlappedSlot));
        cut.context.setNewSlot(reducerSlot);

        assertThat("Error: " + cut.getInvalidActionReason(), cut.isValid(), is(true));
        
        cut.perform();
        
        assertThat(overlappedSlot.getStartPosition(), is(10));
        assertThat(overlappedSlot.getDuration(), is(5));
        assertThat(overlappedSlot.getPreviousSlot(), is(reducerSlot));
        assertThat(reducerSlot.getNextSlot(), is(overlappedSlot));
    }

    @Test
    public void testPerform_isValid_withPreviousSlot() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(5);
        overlappedSlot.setDuration(10);
        overlappedSlot.setValuePerStep(5);

        Slot previous = new ResourceSlot();
        previous.setStartPosition(0);
        previous.setDuration(2);
        previous.setValuePerStep(2);

        overlappedSlot.setPreviousSlot(previous);
        previous.setNextSlot(overlappedSlot);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(2);
        reducerSlot.setDuration(8);
        reducerSlot.setValuePerStep(1);

        cut.context.setOverlappedSlots(Arrays.asList(overlappedSlot));
        cut.context.setNewSlot(reducerSlot);
        when(cut.neighborsSlots.getPreviousNeighbor()).thenReturn(previous);

        assertThat(cut.isValid(), is(true));
        
        cut.perform();
        
        assertThat(overlappedSlot.getStartPosition(), is(10));
        assertThat(overlappedSlot.getDuration(), is(5));
        assertThat(overlappedSlot.getPreviousSlot(), is(reducerSlot));
        assertThat(reducerSlot.getNextSlot(), is(overlappedSlot));

        assertThat(reducerSlot.getPreviousSlot(), is(previous));
        assertThat(previous.getNextSlot(), is(reducerSlot));
    }

    @Test
    public void testPerform_isInvalid_noOverlap() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(0);
        overlappedSlot.setDuration(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(10);
        reducerSlot.setDuration(10);
        
         ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);
        
        cut.setContext(context);

        assertThat(cut.isValid(), is(false));
        assertThat(cut.getInvalidActionReason(), is(InvalidActionReasons.noOverlappedArea));

    }

    @Test
    public void testPerform_isInvalid_falseReduction() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(0);
        overlappedSlot.setDuration(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(2);
        reducerSlot.setDuration(10);

        ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);
        
        cut.setContext(context);

        assertThat(cut.isValid(), is(false));
        assertThat(cut.getInvalidActionReason(), is(InvalidActionReasons.falseReductionSide));

    }

    @Test
    public void testPerform_isInvalid_mergeRequired() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(10);
        overlappedSlot.setDuration(5);
        overlappedSlot.setValuePerStep(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(2);
        reducerSlot.setDuration(10);
        reducerSlot.setValuePerStep(5);

        ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);
        
        cut.setContext(context);

        assertThat(cut.isValid(), is(false));
        assertThat(cut.getInvalidActionReason(), is(InvalidActionReasons.sameValuePerStep));

    }

}
