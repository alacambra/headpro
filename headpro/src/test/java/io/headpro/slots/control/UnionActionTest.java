/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.control.UnionAction;
import io.headpro.slots.control.NeighborsSlots;
import io.headpro.slots.control.ActionContext;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import java.util.Arrays;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.hamcrest.core.Is.is;

/**
 *
 * @author alacambra
 */
public class UnionActionTest {

    UnionAction cut;
    Slot leftSlot;
    Slot rightSlot;
    Slot newSlot;

    @Before
    public void setUp() {
        cut = new UnionAction();
        cut.context = new ActionContext();
        cut.neighborsSlots = mock(NeighborsSlots.class);

        newSlot = new ResourceSlot();

        leftSlot = new ResourceSlot();
        leftSlot.setDuration(10);
        leftSlot.setStartPosition(0);
        leftSlot.setValuePerStep(1);

        rightSlot = new ResourceSlot();
        rightSlot.setDuration(10);
        rightSlot.setStartPosition(20);
        rightSlot.setValuePerStep(1);

        cut.context.setNewSlot(newSlot);
    }

    @Test
    public void testSetSlots_fromOverlappedSlots() {

        newSlot.setStartPosition(5);
        newSlot.setDuration(10);
        newSlot.setValuePerStep(1);

        cut.context.setOverlappedSlots(Arrays.asList(leftSlot, rightSlot));

        boolean actual = cut.isValid();
        assertThat("fail because: " + cut.getInvalidActionReason(), actual, is(true));

        cut.perform();
        assertThat(leftSlot.getStartPosition(), is(0));
        assertThat(leftSlot.getDuration(), is(rightSlot.getEndPosition() + 1));
    }

    @Test
    public void testSetSlots_fromContiguosNeighbors() {

        newSlot.setStartPosition(10);
        newSlot.setDuration(10);
        newSlot.setValuePerStep(1);

        when(cut.neighborsSlots.getNextNeighbor()).thenReturn(rightSlot);
        when(cut.neighborsSlots.getPreviousNeighbor()).thenReturn(leftSlot);
        when(cut.neighborsSlots.bothNeighborsExist()).thenReturn(true);

        boolean actual = cut.isValid();
        assertThat("fail because: " + cut.getInvalidActionReason(), actual, is(true));

        cut.perform();
        assertThat(leftSlot.getStartPosition(), is(0));
        assertThat(leftSlot.getDuration(), is(rightSlot.getEndPosition() + 1));
    }

}
