/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.utils.SlotChain;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.core.Is.is;

/**
 *
 * @author alacambra
 */
public class MergeActionTest {

    MergeAction cut;
    SlotChain.SlotChainBuilder builder;

    @Before
    public void setUp() {

        cut = new MergeAction();
        cut.context = new ActionContext();
        cut.neighborsSlots = mock(NeighborsSlots.class);

        builder = new SlotChain.SlotChainBuilder();

    }

    @Test
    public void testMerge_overlapped_fromRight() {

        Slot currentSlot = new ResourceSlot();
        currentSlot.setDuration(10);
        currentSlot.setStartPosition(5);
        currentSlot.setValuePerStep(1);

        Slot addedSlot = new ResourceSlot();
        addedSlot.setDuration(10);
        addedSlot.setStartPosition(10);
        addedSlot.setValuePerStep(1);

        cut.context.setOverlappedSlots(Arrays.asList(currentSlot));
        cut.context.setNewSlot(addedSlot);

        when(cut.neighborsSlots.getNeighbors()).thenReturn(Arrays.asList(currentSlot));

        boolean actual = cut.isValid();
        assertThat("error: " + cut.getInvalidActionReason(), actual, is(true));

        cut.perform();

        assertThat(currentSlot.getStartPosition(), is(5));
        assertThat(currentSlot.getDuration(), is(15));

    }

    @Test
    public void testMerge_overlapped_fromLeft() {

        Slot currentSlot = new ResourceSlot();
        currentSlot.setDuration(10);
        currentSlot.setStartPosition(10);

        Slot addedSlot = new ResourceSlot();
        addedSlot.setDuration(10);
        addedSlot.setStartPosition(5);

        cut.context.setOverlappedSlots(Arrays.asList(currentSlot));
        cut.context.setNewSlot(addedSlot);

        when(cut.neighborsSlots.getNeighbors()).thenReturn(Arrays.asList(currentSlot));

        boolean expected = cut.isValid();

        assertThat(cut.getInvalidActionReason().name(), expected, is(true));

        cut.perform();

        assertThat(currentSlot.getStartPosition(), is(5));
        assertThat(currentSlot.getDuration(), is(15));

    }

    @Test
    public void testMerge_contiguos_leftSlot() {

        SlotChain chain = builder
                .neighborsExist(true)
                .setNeighborsAreContinguos(true)
                .setAllSlotsSameValuePerStep(true)
                .build();

        chain.getRightNeighbor().setValuePerStep(chain.getLeftNeighbor().getValuePerStep().add(BigDecimal.TEN));

        when(cut.neighborsSlots.getPreviousNeighbor()).thenReturn(chain.getLeftNeighbor());
        when(cut.neighborsSlots.getNextNeighbor()).thenReturn(chain.getRightNeighbor());

        when(cut.neighborsSlots.getNeighbors()).thenReturn(
                Arrays.asList(chain.getLeftNeighbor(), chain.getRightNeighbor()));

        assertThat(SlotsHelper.getOverlappedArea(chain.getSourceSlot(), chain.getLeftNeighbor()), is(0));

        cut.context.setNewSlot(chain.getSourceSlot());

        cut.initialize();

        int expectedDuration = chain.getSourceSlot().getDuration() + chain.getLeftNeighbor().getDuration();

        boolean actual = cut.isValid();
        cut.perform();
        assertThat(cut.getInvalidActionReason().name(), actual, is(true));
        assertThat(cut.getSlotToSave().getDuration(), is(expectedDuration));
        assertThat(cut.getSlotToSave(), is(chain.getLeftNeighbor()));

    }
}
