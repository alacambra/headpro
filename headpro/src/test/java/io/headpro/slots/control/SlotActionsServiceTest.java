package io.headpro.slots.control;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.utils.SlotChain;
import org.junit.Before;
import org.junit.Test;

import javax.enterprise.inject.Instance;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 *
 * @author alacambra
 */
public class SlotActionsServiceTest {

    SlotActionsService cut;
    SlotChain.SlotChainBuilder slotChainBuilder;
    InteractedSlotsProvider interactedSlotsProvider;

    @Before
    public void setUp() {
        cut = new SlotActionsService();

        interactedSlotsProvider = mock(InteractedSlotsProvider.class);
        slotChainBuilder = new SlotChain.SlotChainBuilder(interactedSlotsProvider);

        NeighborsSlots neighborsSlots = new NeighborsSlots();
        neighborsSlots.interactedSlotsProvider = interactedSlotsProvider;

        cut.actionContextInstance = mock(Instance.class);
        when(cut.actionContextInstance.get()).thenReturn(new ActionContext());

        cut.interactedSlotsProvider = interactedSlotsProvider;
        when(cut.actionContextInstance.get()).thenReturn(new ActionContext());

        cut.leftReductionActions = mock(Instance.class);
        LeftReductionAction leftReductionAction = new LeftReductionAction();
        leftReductionAction.neighborsSlots = neighborsSlots;
        when(cut.leftReductionActions.get()).thenReturn(leftReductionAction);

        cut.rightReductionActions = mock(Instance.class);
        RightReductionAction rightReductionAction = new RightReductionAction();
        when(cut.rightReductionActions.get()).thenReturn(rightReductionAction);

        cut.mergeActions = mock(Instance.class);
        MergeAction mergeAction = new MergeAction();
        mergeAction.neighborsSlots = neighborsSlots;
        when(cut.mergeActions.get()).thenReturn(mergeAction);

        cut.simpleAddActions = mock(Instance.class);
        SimpleAddAction simpleAddAction = new SimpleAddAction();
        when(cut.simpleAddActions.get()).thenReturn(simpleAddAction);

        simpleAddAction.neighborsSlots = neighborsSlots;

        cut.splitActions = mock(Instance.class);
        SplitAction splitAction = new SplitAction();
        splitAction.interactedSlotsProvider = interactedSlotsProvider;
        when(cut.splitActions.get()).thenReturn(splitAction);

        cut.unionActions = mock(Instance.class);
        UnionAction unionAction = new UnionAction();
        unionAction.neighborsSlots = neighborsSlots;
        when(cut.unionActions.get()).thenReturn(unionAction);

        cut.em = mock(EntityManager.class);

        cut.init();

    }

    @Test
    public void testAddSlot_simpleAddAction() {

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setSourceSlotMustOverlap(false)
                .setNeighborsAreContinguos(false)
                .build();

        Slot slot = chain.getSourceSlot();

        when(interactedSlotsProvider.getAbsorbedSlots(slot)).thenReturn(Collections.EMPTY_LIST);
        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(Collections.EMPTY_LIST);

        cut.addSlot(slot);
        when(cut.em.merge(slot)).thenReturn(slot);

        assertThat(slot.getNextSlot(), is(chain.getRightNeighbor()));
        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
    }

    @Test
    public void testAddSlot_leftReduction() {

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setRightSlotMustOverlap(true)
                .setNeighborsDuration(5)
                .build();

        Slot slot = chain.getSourceSlot();
        slot.setValuePerStep(121);

        when(interactedSlotsProvider.getAbsorbedSlots(slot)).thenReturn(Collections.EMPTY_LIST);
        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(Arrays.asList(chain.getRightNeighbor()));

        int initialDuration = chain.getRightNeighbor().getDuration();
        int overlappedArea = SlotsHelper.getOverlappedArea(chain.getSourceSlot(), chain.getRightNeighbor());

        cut.addSlot(slot);
        when(cut.em.merge(slot)).thenReturn(slot);

        assertThat(slot.getNextSlot(), is(chain.getRightNeighbor()));
        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(slot));
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(slot));
        assertThat(chain.getRightNeighbor().getDuration(), is(initialDuration - overlappedArea));
    }

    @Test
    public void testAddSlot_rightReduction() {

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setLeftSlotMustOverlap(true)
                .setNeighborsDuration(5)
                .build();

        Slot slot = chain.getSourceSlot();
        slot.setValuePerStep(121);

        when(interactedSlotsProvider.getAbsorbedSlots(slot)).thenReturn(Collections.EMPTY_LIST);
        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(Arrays.asList(chain.getLeftNeighbor()));
        System.out.println(chain.toString());

        int initialDuration = chain.getLeftNeighbor().getDuration();
        int overlappedArea = SlotsHelper.getOverlappedArea(chain.getSourceSlot(), chain.getLeftNeighbor());

        cut.addSlot(slot);
        when(cut.em.merge(slot)).thenReturn(slot);

        assertThat(chain.getLeftNeighbor().getDuration(), is(initialDuration - overlappedArea));
        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
        assertThat(slot.getNextSlot(), is(chain.getRightNeighbor()));

        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(slot));
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(slot));

    }

    @Test
    public void testAddSlot_split() {

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSplitLeft(true)
                .build();

        Slot slot = chain.getSourceSlot();
        slot.setValuePerStep(121);

        when(interactedSlotsProvider.getAbsorbedSlots(slot)).thenReturn(Collections.EMPTY_LIST);
        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(Arrays.asList(chain.getLeftNeighbor()));

        Slot rest = new ResourceSlot();
        when(interactedSlotsProvider.createNextSlot(chain.getLeftNeighbor())).thenReturn(rest);

        int initialDuration = chain.getLeftNeighbor().getDuration();

        chain.getAllChainSlots().stream().forEach(System.out::println);

        cut.addSlot(slot);
        when(cut.em.merge(slot)).thenReturn(slot);

        assertThat(chain.getLeftNeighbor().getDuration(), is(slot.getStartPosition() - chain.getLeftNeighbor().getStartPosition()));
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(slot));

        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
        assertThat(slot.getNextSlot(), is(rest));

        assertThat(rest.getPreviousSlot(), is(slot));
        assertThat(rest.getNextSlot(), is(chain.getRightNeighbor()));
        assertThat(rest.getDuration(), is(initialDuration - slot.getDuration() - chain.getLeftNeighbor().getDuration()));
        assertThat(rest.getValuePerStep(), is(chain.getLeftNeighbor().getValuePerStep()));

        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(rest));

    }

    @Test
    public void testAddSlot_union_overlap() {
        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSourceSlotMustOverlap(true)
                .setBaseValuePerStep(5)
                .setAllSlotsSameValuePerStep(true)
                .build();

        Slot slot = chain.getSourceSlot();
        System.out.println(chain.toString());

        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(
                Arrays.asList(chain.getLeftNeighbor(), chain.getRightNeighbor()));

        cut.addSlot(slot);

        int expectedDuration = chain.getRightNeighbor().getEndPosition() - chain.getLeftNeighbor().getStartPosition() + 1;
        assertThat(chain.getLeftNeighbor().getDuration(), is(expectedDuration));
    }

    @Test
    public void testAddSlot_union_contiguos() {
        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setNeighborsAreContinguos(true)
                .setBaseValuePerStep(5)
                .setAllSlotsSameValuePerStep(true)
                .build();

        Slot slot = chain.getSourceSlot();
        System.out.println(chain.toString());

        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(
                Arrays.asList(chain.getLeftNeighbor(), chain.getRightNeighbor()));

        cut.addSlot(slot);

        int expectedDuration = chain.getRightNeighbor().getEndPosition() - chain.getLeftNeighbor().getStartPosition() + 1;
        assertThat(chain.getLeftNeighbor().getDuration(), is(expectedDuration));
    }

    @Test
    public void testAddSlot_mergeLeft_reduceRight() {

        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSourceSlotMustOverlap(true)
                .setBaseValuePerStep(5)
                .setAllSlotsSameValuePerStep(true)
                .build();

        chain.getRightNeighbor().setValuePerStep(chain.getSourceSlot().getValuePerStep().add(BigDecimal.ONE));

        Slot slot = chain.getSourceSlot();
        int expectedMergeDuration = slot.getEndPosition() - chain.getLeftNeighbor().getStartPosition() + 1;
        int expectedReductionDuration = chain.getRightNeighbor().getEndPosition() - slot.getEndPosition();

        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(
                Arrays.asList(chain.getLeftNeighbor(), chain.getRightNeighbor()));

        cut.addSlot(slot);

        assertThat(chain.getLeftNeighbor().getDuration(), is(expectedMergeDuration));
        assertThat(chain.getRightNeighbor().getDuration(), is(expectedReductionDuration));

    }

    @Test
    public void testAddSlot_double_reduction() {
        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .setNeighborsDuration(10)
                .setSourceSlotMustOverlap(true)
                .setAllSlotsSameValuePerStep(false)
                .build();

        System.out.println(chain);

        Slot slot = chain.getSourceSlot();

        when(interactedSlotsProvider.getOverlappedSlots(slot)).thenReturn(
                Arrays.asList(chain.getLeftNeighbor(), chain.getRightNeighbor()));

        when(cut.em.merge(slot)).thenReturn(slot);
        SlotActionsService.ActionResult result = cut.addSlotReturnActionResult(slot);
        System.out.println(result);

        assertThat(result.getSavedSlot(), is(slot));
        assertThat(result.getActionsRunned().size(), is(2));

        result.getActionsRunned().forEach(action -> {
            assertTrue(action.getClass().isAssignableFrom(LeftReductionAction.class) 
                    || action.getClass().isAssignableFrom(RightReductionAction.class));
        });
        
        assertThat(chain.getLeftNeighbor().getNextSlot(), is(slot));

        assertThat(slot.getPreviousSlot(), is(chain.getLeftNeighbor()));
        assertThat(slot.getNextSlot(), is(chain.getRightNeighbor()));

        assertThat(chain.getRightNeighbor().getPreviousSlot(), is(slot));
    }
}
