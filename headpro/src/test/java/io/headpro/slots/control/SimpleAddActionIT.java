/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.AbstractIntegrationTest;
import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;
import io.headpro.slots.entity.Slot;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import io.headpro.utils.SlotChain;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class SimpleAddActionIT extends AbstractIntegrationTest {

    SimpleAddAction cut;
    SlotChain.SlotChainBuilder slotChainBuilder;
    InteractedSlotsProvider interactedSlotsProvider;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        cut = new SimpleAddAction();
        cut.neighborsSlots = new NeighborsSlots();
        interactedSlotsProvider = new InteractedSlotsProvider();
        interactedSlotsProvider.em = em;
        cut.neighborsSlots.interactedSlotsProvider = interactedSlotsProvider;
        slotChainBuilder = new SlotChain.SlotChainBuilder(null);
        clearDbAfterTest = true;
    }

    @Test
    public void testPerform() {

        clearDbAfterTest = false;
        tx.begin();
        
        Service service = createDummyService();
        Project project = createDummyProject();
        
        SlotChain chain = slotChainBuilder
                .neighborsExist(true)
                .persistSlots(true)
                .setEntityManager(em)
                .useProject(project)
                .useService(service)
                .build();

        tx.commit();
        tx.begin();
        ActionContext context = new ActionContext();
        context.setNewSlot(chain.getSourceSlot());
        context.setAbsorbbedSlots(chain.getAbsorbedSlots());
        context.setOverlappedSlots(Collections.EMPTY_LIST);
        cut.setContext(context);

        assertThat(cut.isValid(), is(true));
        
        cut.perform();
        Slot sourceSlot = em.merge(chain.getSourceSlot());

        tx.commit();

        assertThat(sourceSlot.getId(), notNullValue());
        assertThat(em.find(ResourceSlot.class, sourceSlot.getId()), notNullValue());
        assertChain(chain.getLeftNeighbor(), sourceSlot, chain.getRightNeighbor());

    }
}
