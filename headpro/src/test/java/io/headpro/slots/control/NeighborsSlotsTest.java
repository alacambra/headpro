/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import static org.hamcrest.CoreMatchers.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import io.headpro.utils.SlotChain;

public class NeighborsSlotsTest {

    NeighborsSlots cut;

    @Before
    public void setUp() {
        cut = new NeighborsSlots();
        cut.interactedSlotsProvider = Mockito.mock(InteractedSlotsProvider.class);
    }

    @Test
    public void testFindNeighbors_fromOneAbsorbedSlots_noNeighbors() {

        SlotChain slotChain = new SlotChain.SlotChainBuilder(cut.interactedSlotsProvider)
                .existAbsorbedSlot(true)
                .neighborsExist(false)
                .totalAbsorbedSlots(1).build();

        cut.setAbsorbedSlots(slotChain.getAbsorbedSlots());
        cut.setSourceSlot(slotChain.getSourceSlot());

        cut.findNeighbors();

        assertThat(cut.getNextNeighbor(), nullValue());
        assertThat(cut.getPreviousNeighbor(), nullValue());
    }

    @Test
    public void testFindNeighbors_fromManyAbsorbedSlots_noNeighbors() {

        for (int i = 1; i < 100; i++) {
            SlotChain slotChain = new SlotChain.SlotChainBuilder(cut.interactedSlotsProvider)
                    .existAbsorbedSlot(true)
                    .neighborsExist(false)
                    .totalAbsorbedSlots(i).build();

            cut.setAbsorbedSlots(slotChain.getAbsorbedSlots());
            cut.setSourceSlot(slotChain.getSourceSlot());

            cut.findNeighbors();

            assertThat(cut.getNextNeighbor(), nullValue());
            assertThat(cut.getPreviousNeighbor(), nullValue());
        }
    }

    @Test
    public void testFindNeighbors_fromOneAbsorbedSlots_withNeighbors() {

        SlotChain slotChain = new SlotChain.SlotChainBuilder(cut.interactedSlotsProvider)
                .existAbsorbedSlot(true)
                .neighborsExist(true)
                .totalAbsorbedSlots(1).build();

        cut.setAbsorbedSlots(slotChain.getAbsorbedSlots());
        cut.setSourceSlot(slotChain.getSourceSlot());

        cut.findNeighbors();

        assertThat(cut.getNextNeighbor(), is(slotChain.getRightNeighbor()));
        assertThat(cut.getPreviousNeighbor(), is(slotChain.getLeftNeighbor()));
    }

    @Test
    public void testFindNeighbors_fromManyAbsorbedSlots_withNeighbors() {

        for (int i = 1; i < 100; i++) {
            setUp();
            SlotChain slotChain = new SlotChain.SlotChainBuilder(cut.interactedSlotsProvider)
                    .existAbsorbedSlot(true)
                    .neighborsExist(true)
                    .totalAbsorbedSlots(i).build();

            cut.setAbsorbedSlots(slotChain.getAbsorbedSlots());
            cut.setSourceSlot(slotChain.getSourceSlot());

            cut.findNeighbors();

            assertThat(cut.getNextNeighbor(), is(slotChain.getRightNeighbor()));
            assertThat("fail at " + i, cut.getPreviousNeighbor(), is(slotChain.getLeftNeighbor()));
        }
    }

    @Test
    public void testFindNeighbors_useNearestSlot_neighborsExist() {

        SlotChain slotChain = new SlotChain.SlotChainBuilder(cut.interactedSlotsProvider)
                .neighborsExist(true)
                .setLeftSlotIsNearerThanRightSlot(true)
                .build();

        cut.setSourceSlot(slotChain.getSourceSlot());
        cut.findNeighbors();
        
       
        assertThat(cut.interactedSlotsProvider.getNearestSlot(slotChain.getSourceSlot()), is(slotChain.getLeftNeighbor()));
        assertThat(cut.getNextNeighbor(), is(slotChain.getRightNeighbor()));
        assertThat(cut.getPreviousNeighbor(), is(slotChain.getLeftNeighbor()));
        
        setUp();
        slotChain = new SlotChain.SlotChainBuilder(cut.interactedSlotsProvider)
                .neighborsExist(true)
                .setLeftSlotIsNearerThanRightSlot(false)
                .build();

        cut.setSourceSlot(slotChain.getSourceSlot());
        cut.findNeighbors();

        assertThat(cut.interactedSlotsProvider.getNearestSlot(slotChain.getSourceSlot()), is(slotChain.getRightNeighbor()));
        assertThat(cut.getNextNeighbor(), is(slotChain.getRightNeighbor()));
        assertThat(cut.getPreviousNeighbor(), is(slotChain.getLeftNeighbor()));
    }
    
    @Test
    public void testFindNeighbors_useNearestSlot_noNeighbors() {

        SlotChain slotChain = new SlotChain.SlotChainBuilder(cut.interactedSlotsProvider)
                .existAbsorbedSlot(false)
                .neighborsExist(false)
                .build();

        cut.setSourceSlot(slotChain.getSourceSlot());
        cut.findNeighbors();
       
        assertThat(cut.getNextNeighbor(), nullValue());
        assertThat(cut.getPreviousNeighbor(), nullValue());
        
    }
}
