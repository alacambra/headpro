/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import static org.hamcrest.core.Is.is;

/**
 *
 * @author alacambra
 */
public class RightReductionActionTest {

    RightReductionAction cut;

    @Before
    public void setUp() {

        cut = new RightReductionAction();
        cut.context = new ActionContext();

    }

    @Test
    public void testPerform_isValid_withoutNextSlot() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(10);
        overlappedSlot.setDuration(10);
        overlappedSlot.setValuePerStep(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(15);
        reducerSlot.setDuration(10);
        reducerSlot.setValuePerStep(1);

        ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);

        cut.setContext(context);

        assertThat(cut.isValid(), is(true));
        cut.perform();

        assertThat(overlappedSlot.getStartPosition(), is(10));
        assertThat(overlappedSlot.getEndPosition(), is(14));
        assertThat(overlappedSlot.getDuration(), is(5));
        assertThat(overlappedSlot.getNextSlot(), is(reducerSlot));
        assertThat(reducerSlot.getPreviousSlot(), is(overlappedSlot));
    }

    @Test
    public void testPerform_isValid_withNextSlot() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(10);
        overlappedSlot.setDuration(10);
        overlappedSlot.setValuePerStep(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(15);
        reducerSlot.setDuration(10);
        reducerSlot.setValuePerStep(1);

        Slot next = new ResourceSlot();
        next.setStartPosition(50);
        next.setDuration(2);
        next.setValuePerStep(2);

        overlappedSlot.setNextSlot(next);
        next.setPreviousSlot(overlappedSlot);

        ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);

        cut.setContext(context);
        cut.setNextSlot(next);

        assertThat(cut.isValid(), is(true));

        cut.perform();

        assertThat(overlappedSlot.getStartPosition(), is(10));
        assertThat(overlappedSlot.getEndPosition(), is(14));
        assertThat(overlappedSlot.getDuration(), is(5));
        assertThat(overlappedSlot.getNextSlot(), is(reducerSlot));
        assertThat(reducerSlot.getPreviousSlot(), is(overlappedSlot));

        assertThat(reducerSlot.getNextSlot(), is(next));
        assertThat(next.getPreviousSlot(), is(reducerSlot));
    }

    @Test
    public void testPerform_isInvalid_noOverlap() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(0);
        overlappedSlot.setDuration(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(10);
        reducerSlot.setDuration(10);

        ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);

        cut.setContext(context);

        assertThat(cut.isValid(), is(false));
        assertThat(cut.getInvalidActionReason(), is(InvalidActionReasons.noOverlappedArea));

    }

    @Test
    public void testPerform_isInvalid_falseReduction() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(2);
        overlappedSlot.setDuration(10);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(0);
        reducerSlot.setDuration(5);

        ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);

        cut.setContext(context);

        assertThat(cut.isValid(), is(false));
        assertThat(cut.getInvalidActionReason(), is(InvalidActionReasons.falseReductionSide));

    }

    @Test
    public void testPerform_isInvalid_mergeRequired() {

        Slot overlappedSlot = new ResourceSlot();
        overlappedSlot.setStartPosition(0);
        overlappedSlot.setDuration(5);
        overlappedSlot.setValuePerStep(5);

        Slot reducerSlot = new ResourceSlot();
        reducerSlot.setStartPosition(2);
        reducerSlot.setDuration(10);
        reducerSlot.setValuePerStep(5);

        ActionContext context = new ActionContext()
                .setOverlappedSlots(Arrays.asList(overlappedSlot))
                .setNewSlot(reducerSlot);

        cut.setContext(context);

        assertThat(cut.isValid(), is(false));
        assertThat(cut.getInvalidActionReason(), is(InvalidActionReasons.sameValuePerStep));

    }

}
