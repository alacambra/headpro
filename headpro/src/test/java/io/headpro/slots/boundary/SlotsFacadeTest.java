/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.boundary;

import io.headpro.workspace.entity.Service;
import io.headpro.slots.entity.ServiceSlot;
import io.headpro.utils.SlotChain;
import io.headpro.workspace.entity.Workspace;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.core.Is.is;

/**
 *
 * @author alacambra
 */
public class SlotsFacadeTest {

    SlotsFacade cut;
    SlotChain chain;

    @Before
    public void setUp() {
        cut = new SlotsFacade();
        cut.em = mock(EntityManager.class);

    }

    @Test
    public void testGetCalculatedResourceSlots() {
    }

    @Test
    public void testGetCalculatedServiceSlots() {
        
        Workspace workspace = new Workspace();

        Service s1 = new Service();
        s1.setId(1);
        ServiceSlot slot11 = createServiceSlot(s1, 234, 20, 1);
        ServiceSlot slot12 = createServiceSlot(s1, 244, 20, 2);
        
        Service s2 = new Service();
        s2.setId(2);
        ServiceSlot slot21 = createServiceSlot(s2, 234, 20, 1);
        ServiceSlot slot22 = createServiceSlot(s2, 234 + 20, 10, 2);
        
        Query q = mock(Query.class);
        when(cut.em.createNamedQuery(any())).thenReturn(q);
        when(q.setParameter("workspace", workspace)).thenReturn(q);
        when(q.getResultList()).thenReturn(Arrays.asList(slot11, slot12, slot21, slot22));
        
        Map<Service, List<BigDecimal>> actual = cut.getCalculatedServiceSlots(workspace);
        assertThat(actual.keySet().size(), is(2));
        assertThat(actual.values().size(), is(2));
        assertThat(actual.values().stream().findAny().get().size(), is(30));

    }

    @Test
    public void testInsertOrUpdateSlots() {
    }

    private ServiceSlot createServiceSlot(Service s, int start, int duration, int value) {

        ServiceSlot slot = new ServiceSlot();

        slot.setValuePerStep(value);
        slot.setDuration(duration);
        slot.setStartPosition(start);
        slot.setService(s);

        return slot;

    }
}
