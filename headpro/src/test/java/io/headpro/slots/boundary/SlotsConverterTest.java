package io.headpro.slots.boundary;

import io.headpro.boundary.InvalidRequestException;
import io.headpro.slots.control.SlotsFactory;
import io.headpro.slots.entity.ServiceSlot;
import io.headpro.slots.entity.Slot;
import java.math.BigDecimal;
import javax.json.Json;
import javax.json.JsonObject;
import static org.hamcrest.core.Is.is;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alacambra
 */
public class SlotsConverterTest {
    
    SlotsConverter cut;
    
    @Before
    public void setUp() {
        cut = new SlotsConverter();
        cut.slotsFactory = new SlotsFactory();
    }

    @Test
    public void testFromJson() {
        JsonObject object = Json.createObjectBuilder()
                .add("duration", 1)
                .add("startPosition", 1)
                .add("valuePerStep", 1)
                .build();
        
        Slot slot = cut.fromJson(object, ServiceSlot.class);
        assertThat(slot.getDuration(), is(1));
        assertThat(slot.getStartPosition(), is(1));
        assertThat(slot.getValuePerStep(), is(BigDecimal.ONE));
    }
    
    @Test(expected = InvalidRequestException.class)
    public void testFromJson_InvalidRequestException_paramNotGiven() {
        JsonObject object = Json.createObjectBuilder()
                .add("duration", 1)
                .add("startPosition", 1)
                .build();
        
        cut.fromJson(object, ServiceSlot.class);
    }
    
    @Test(expected = InvalidRequestException.class)
    public void testFromJson_InvalidRequestException_invalidParamType() {
        JsonObject object = Json.createObjectBuilder()
                .add("duration", 1)
                .add("startPosition", 1)
                .add("valuePerStep", "")
                .build();
        
        cut.fromJson(object, ServiceSlot.class);
    }

    @Test
    public void testToJson_Slot() {
    }

    @Test
    public void testToJsonWithAbsolutePosition() {
    }

    @Test
    public void testToJson_List() {
    }
    
}
