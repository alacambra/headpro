package io.headpro.accounting.boundary;

import io.headpro.AbstractIntegrationTest;
import io.headpro.entity.User;
import java.util.logging.Logger;
import javax.enterprise.event.Event;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class AccountFacadeIT extends AbstractIntegrationTest {

    AccountFacade cut;

    @Before
    @Override
    public void setUp() {

        super.setUp();
        cut = new AccountFacade();
        cut.em = em;
        cut.logEvent = mock(Event.class);
        cut.logger = Logger.getGlobal();
    }

    @Test
    public void testFirstAccountCreation() {

        tx.begin();

        User userEntity = new User();
        userEntity.setId("ldldldldl");

        cut.createAccount(userEntity);
        tx.commit();

        tx.begin();
        User ue = em.find(User.class, "ldldldldl");

        assertThat(ue, notNullValue());
        assertThat(ue.getAccounts().size(), is(1));
        assertThat(ue.getAccounts().get(0).getWorkspaces().size(), is(1));
        tx.commit();

    }

}
