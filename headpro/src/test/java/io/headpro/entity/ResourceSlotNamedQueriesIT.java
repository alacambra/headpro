package io.headpro.entity;

import io.headpro.workspace.entity.Service;
import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.AbstractIntegrationTest;
import io.headpro.testutils.ResourceSlotsGenerator;
import io.headpro.workspace.entity.Workspace;
import java.util.Arrays;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ResourceSlotNamedQueriesIT extends AbstractIntegrationTest {

    @Test
    public void fetchSlotsInPeriod() {

        List<ResourceSlot> createdSlots = addContiguousSlots(0, 1, 10);
        Project p = createdSlots.get(0).getProject();
        Service s = createdSlots.get(0).getService();

        List<ResourceSlot> slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", 0)
                .setParameter("end", 100)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(10));

        slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", 0)
                .setParameter("end", 0)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(1));

        slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", 2)
                .setParameter("end", 4)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(3));

        slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", 2)
                .setParameter("end", 9)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(8));

    }

    @Test
    public void testDeleteAbsorbedSlots() {

        int totalSlots = 10;

        List<ResourceSlot> createdSlots = addContiguousSlots(0, 1, totalSlots);

        tx.begin();

        Project p = createdSlots.get(0).getProject();
        Service s = createdSlots.get(0).getService();

        List<ResourceSlot> slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", 1)
                .setParameter("end", 100000)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(totalSlots - 1));

        slots.forEach(em::remove);

        tx.commit();

        slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", 0)
                .setParameter("end", 100000)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(1));
        assertThat(slots.get(0), is(createdSlots.get(0)));
    }

    @Test
    public void testPerformanceOnDeleteAbsorbedSlots() {

        int totalSlots = 100;
        int firstPos = 0;
        int slotDuration = 1;

        List<ResourceSlot> createdSlots = addContiguousSlots(firstPos, slotDuration, totalSlots);

        Long start = new Date().getTime();
        tx.begin();

        Project p = createdSlots.get(0).getProject();
        Service s = createdSlots.get(0).getService();

        List<ResourceSlot> slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", firstPos + 1)
                .setParameter("end", totalSlots - 2)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(totalSlots - 2));

        slots.forEach(em::remove);

        tx.commit();

        System.out.println("total:" + (new Date().getTime() - start));

        slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", firstPos - 1)
                .setParameter("end", totalSlots)
                .setParameter("project", p)
                .setParameter("service", s)
                .getResultList();

        assertThat(slots.size(), is(2));
        assertThat(slots.get(0), is(createdSlots.get(0)));
        assertThat(slots.get(1), is(createdSlots.get(createdSlots.size() - 1)));
        assertThat(slots.get(0).getNextSlot(), is(slots.get(1)));
        assertThat(slots.get(1).getNextSlot(), is(nullValue()));
        assertThat(slots.get(0).getPreviousSlot(), is(nullValue()));
        assertThat(slots.get(1).getPreviousSlot(), is(slots.get(0)));
    }

    @Test
    public void addSlotSamePosition() {
        tx.begin();
        ResourceSlot slot = new ResourceSlot();
        slot.setStartPosition(0);
        slot.setDuration(5);
        slot.setProject(createDummyProject());
        slot.setService(createDummyService());
        em.persist(slot);
        tx.commit();
        List<ResourceSlot> r;

        r = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                .setParameter("start", slot.getStartPosition())
                .setParameter("end", slot.getEndPosition())
                .setParameter("project", slot.getProject())
                .setParameter("service", slot.getService())
                .getResultList();

        assertThat(r.size(), is(1));

        r = em.createNamedQuery(ResourceSlot.fetchOverlappedSlots)
                .setParameter("start", slot.getStartPosition())
                .setParameter("end", slot.getEndPosition())
                .setParameter("project", slot.getProject())
                .setParameter("service", slot.getService())
                .getResultList();

        assertThat(r.size(), is(0));
    }

    @Test
    public void overlappedSlotWithReduction() {
        List<ResourceSlot> slots = addContiguousSlots(0, 5, 2);
        List<ResourceSlot> getOverlappedSLots = em.createNamedQuery(ResourceSlot.fetchOverlappedSlots)
                .setParameter("start", 5)
                .setParameter("end", 7)
                .setParameter("project", slots.get(0).getProject())
                .setParameter("service", slots.get(0).getService())
                .getResultList();

        assertThat(getOverlappedSLots.size(), is(1));

    }

    @Test
    public void getNearestSlot() {

        List<ResourceSlot> slots = em.createNamedQuery(ResourceSlot.fetchNearestSlots)
                .setParameter("project", createDummyProject())
                .setParameter("service", createDummyService())
                .setParameter("start", 0)
                .setMaxResults(1)
                .getResultList();

        assertTrue(slots.isEmpty());

        Project project = createDummyProject();
        Service service = createDummyService();

        int idFirst = rsGenerator
                .setProject(project)
                .setService(service)
                .setStart(0)
                .setSlotsDuration(10)
                .build(1).get(0).getId();

        int idLast = rsGenerator
                .setStart(100)
                .build(1).get(0).getId();

        slots = em.createNamedQuery(ResourceSlot.fetchNearestSlots)
                .setParameter("project", project)
                .setParameter("service", service)
                .setParameter("start", 5)
                .setMaxResults(1)
                .getResultList();

        assertThat(slots.size(), is(1));
        assertThat(slots.get(0).getId(), is(idFirst));

        slots = em.createNamedQuery(ResourceSlot.fetchNearestSlots)
                .setParameter("project", project)
                .setParameter("service", service)
                .setParameter("start", 90)
                .setMaxResults(1)
                .getResultList();

        assertThat(slots.size(), is(1));
        assertThat(slots.get(0).getId(), is(idLast));

        slots = em.createNamedQuery(ResourceSlot.fetchNearestSlots)
                .setParameter("project", project)
                .setParameter("service", service)
                .setParameter("start", 51)
                .setMaxResults(1)
                .getResultList();

        assertThat(slots.size(), is(1));
        assertThat(slots.get(0).getId(), is(idLast));

        slots = em.createNamedQuery(ResourceSlot.fetchNearestSlots)
                .setParameter("project", project)
                .setParameter("service", service)
                .setParameter("start", 0)
                .setMaxResults(1)
                .getResultList();

        assertThat(slots.size(), is(1));
        assertThat(slots.get(0).getId(), is(idFirst));

        slots = em.createNamedQuery(ResourceSlot.fetchNearestSlots)
                .setParameter("project", project)
                .setParameter("service", service)
                .setParameter("start", 100000)
                .setMaxResults(1)
                .getResultList();

        assertThat(slots.size(), is(1));
        assertThat(slots.get(0).getId(), is(idLast));

    }

    @Test
    public void fetchSlotsForServiceTest() {

        Service desiredService = createDummyService();
        Service anotherService = createDummyService();

        Project someProject = createDummyProject();
        Project anotherProject = createDummyProject();

        Workspace dummyWorkspace = createDummyWorkspace(false, Arrays.asList(someProject, anotherProject), Arrays.asList(desiredService, anotherService));

        int totalDesiredSlotsSomeProject = 1;
        int totalDesiredSlotsAnotherProject = 2;

        ResourceSlotsGenerator slotsGenerator = new ResourceSlotsGenerator(em, tx);
        List<ResourceSlot> desiredSlotsOfSomeProject = slotsGenerator.setProject(someProject).setService(desiredService).build(totalDesiredSlotsSomeProject);
        List<ResourceSlot> desiredSlotsOfAnotherProject = slotsGenerator.setProject(anotherProject).setService(desiredService).build(totalDesiredSlotsAnotherProject);
        List<ResourceSlot> nonDesiredSlotsOfSomeProject = slotsGenerator.setProject(someProject).setService(anotherService).build(3);
        List<ResourceSlot> nonDesiredSlotsOfAnotherProject = slotsGenerator.setProject(anotherProject).setService(anotherService).build(4);

        List<ResourceSlot> slots = em.createNamedQuery(ResourceSlot.fetchSlotsForService, ResourceSlot.class)
                .setParameter("service", desiredService).setParameter("workspace", dummyWorkspace)
                .getResultList();

        assertThat(slots.size(), is(totalDesiredSlotsAnotherProject + totalDesiredSlotsSomeProject));

        desiredSlotsOfAnotherProject.addAll(desiredSlotsOfSomeProject);
        assertThat(slots, hasItems(desiredSlotsOfAnotherProject.toArray(new ResourceSlot[]{})));

    }

}
