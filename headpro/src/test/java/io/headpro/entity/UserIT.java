package io.headpro.entity;

import io.headpro.AbstractIntegrationTest;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class UserIT extends AbstractIntegrationTest {

    @Test
    public void testUserIsCreated() throws Exception {
        
        User u = new User();
        
        u.setEmail("a@a.com");
        u.setFirstName("lololo");
        u.setId(UUID.randomUUID().toString());

        tx.begin();
        u = em.merge(u);
        tx.commit();

        assertThat(u.getId(), is(notNullValue()));
    }

    @Test
    public void testUserIsCreatedMultiple() throws Exception {

        tx.begin();

        for (int i = 0; i < 150; i++) {
            String prefix = new Date().getTime() + "";
            User u = new User();
            u.setEmail("a@a.com" + prefix);
            u.setFirstName(prefix + "lololo");
            u.setLastName(prefix + "dadasdad");
            u.setId(UUID.randomUUID().toString());
            em.merge(u);
        }

        tx.commit();

        tx.begin();
        List<User> users = em.createNamedQuery(User.selectAll).getResultList();
        
        assertThat(users.size(), is(150));
        
        users.stream().forEach(em::remove);
        tx.commit();
        
        tx.begin();
        users = em.createNamedQuery(User.selectAll).getResultList();
        
        assertThat(users.size(), is(0));
        tx.commit();
    }

    @Test
    public void updateDatesTest() {

        tx.begin();

        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user = em.merge(user);

        tx.commit();

        assertThat(user.getUpdateTracker(), notNullValue());
        assertThat(user.getUpdateTracker().getDateCreated(), notNullValue());
        assertThat(user.getUpdateTracker().getLastModified(), notNullValue());

        Date firstDate = user.getUpdateTracker().getDateCreated();

        assertThat(user.getUpdateTracker().getDateCreated(), is(user.getUpdateTracker().getLastModified()));

        tx.begin();
        user.setEmail("new@email.com");
        user = em.merge(user);
        tx.commit();

        assertThat(user.getUpdateTracker().getDateCreated(), is(firstDate));
        assertThat(user.getUpdateTracker().getLastModified(), not(firstDate));
        assertThat(user.getUpdateTracker().getLastModified(), greaterThan(user.getUpdateTracker().getDateCreated()));

    }

}
