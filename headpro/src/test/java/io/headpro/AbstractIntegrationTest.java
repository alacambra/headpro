/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.ServiceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.workspace.entity.Project;
import io.headpro.workspace.entity.Service;
import io.headpro.testutils.ResourceSlotsGenerator;
import io.headpro.workspace.entity.Workspace;
import org.junit.After;
import org.junit.Before;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author alacambra
 */
public class AbstractIntegrationTest {

    final static String INPUT = "./src/test/resources/";
    protected EntityManager em;
    protected EntityTransaction tx;

    protected static final String H2 = "it-h2";
    protected static final String MySql = "it-mysql";
    protected String dbToUse = MySql;
    protected ResourceSlotsGenerator rsGenerator;
    protected boolean clearDbAfterTest = true;

    @Before
    public void setUp() {

        this.em = Persistence.createEntityManagerFactory(dbToUse).createEntityManager();
        this.tx = this.em.getTransaction();
        rsGenerator = new ResourceSlotsGenerator(em, tx);
    }

    @After
    public void tearDown() {

        if (tx != null && tx.isActive()) {
            tx.commit();
        }

        if (clearDbAfterTest) {
            System.out.println("Cleaning db tables");
            clearTables();
        }

    }

    protected void clearTables() {

        tx.begin();

        Arrays.asList(
                em.createQuery("UPDATE ResourceSlot SET previousSlot = null, nextSlot=null "),
                em.createQuery("UPDATE ServiceSlot SET previousSlot = null, nextSlot=null "),
                em.createQuery("DELETE FROM ResourceSlot"),
                em.createQuery("DELETE FROM ServiceSlot"),
                em.createQuery("DELETE FROM Project"),
                em.createQuery("DELETE FROM Service"),
                em.createQuery("DELETE FROM WorkspaceRole"),
                em.createQuery("DELETE FROM Workspace"),
                em.createQuery("DELETE FROM Account"),
                em.createQuery("DELETE FROM User")
        ).forEach(Query::executeUpdate);

        tx.commit();
        em.close();
    }

    protected List<ResourceSlot> addContiguousSlots(int start, int duration, int totalSlots) {

        return addContiguousSlots(start, duration, totalSlots, 5);

    }

    protected List<ResourceSlot> addContiguousSlots(int start, int duration, int totalSlots, int valuePerStep) {
        tx.begin();

        Project project = new Project();
        project.setName("Dummy project");

        Service service = new Service();
        service.setName("Dummy service");

        ResourceSlot previousSlot = null;
        ResourceSlot middleSlot = new ResourceSlot();
        middleSlot.setValuePerStep(valuePerStep);

        ResourceSlot nextSlot = new ResourceSlot();
        nextSlot.setValuePerStep(valuePerStep);

        ResourceSlot firstSlot = middleSlot;

        List<ResourceSlot> slots = new ArrayList<>(totalSlots);

        for (int i = 0; i < totalSlots; i++) {
            start = previousSlot == null ? start : previousSlot.getEndPosition() + 1;
            configureSlot(middleSlot, project, service, start, duration, previousSlot, i == totalSlots - 1 ? null : nextSlot);

            previousSlot = middleSlot;
            middleSlot = nextSlot;
            nextSlot = new ResourceSlot();
            nextSlot.setValuePerStep(valuePerStep);

        }

        ResourceSlot slot = em.merge(firstSlot);

        while (slot != null) {
            slots.add(slot);
            slot = slot.getNextSlot();
        }

        tx.commit();
        return slots;
    }

    private void configureSlot(
            ResourceSlot toConfigure,
            Project project,
            Service service,
            int start,
            int duration,
            ResourceSlot previousSlot,
            ResourceSlot nextSlot
    ) {
        toConfigure.setStartPosition(start);
        toConfigure.setDuration(duration);
        toConfigure.setProject(project);
        toConfigure.setService(service);
        toConfigure.setPreviousSlot(previousSlot);
        toConfigure.setNextSlot(nextSlot);
    }

    protected List<ServiceSlot> addContiguousSSlots(int start, int duration, int totalSlots) {

        tx.begin();

        Service service = new Service();

        ServiceSlot previousSlot = null;
        ServiceSlot middleSlot = new ServiceSlot();
        ServiceSlot nextSlot = new ServiceSlot();
        ServiceSlot firstSlot = middleSlot;

        List<ServiceSlot> slots = new ArrayList<>(totalSlots);

        for (int i = 0; i < totalSlots; i++) {
            start = previousSlot == null ? start : previousSlot.getEndPosition() + 1;
            configureSSlot(middleSlot, service, start, duration, previousSlot, i == totalSlots - 1 ? null : nextSlot);

            previousSlot = middleSlot;
            middleSlot = nextSlot;
            nextSlot = new ServiceSlot();
        }

        ServiceSlot slot = em.merge(firstSlot);

        while (slot != null) {
            slots.add(slot);
            slot = slot.getNextSlot();
        }

        tx.commit();
        return slots;
    }

    private void configureSSlot(
            ServiceSlot toConfigure,
            Service service,
            int start,
            int duration,
            ServiceSlot previousSlot,
            ServiceSlot nextSlot
    ) {
        toConfigure.setStartPosition(start);
        toConfigure.setDuration(duration);
        toConfigure.setService(service);
        toConfigure.setPreviousSlot(previousSlot);
        toConfigure.setNextSlot(nextSlot);
    }

    public Project createDummyProject() {

        boolean commitRequired = false;

        if (!tx.isActive()) {
            tx.begin();
            commitRequired = true;
        }

        Project project = new Project();

        project.setName("DummyProject");
        project.setStartDate(new Date());
        project.setEndDate(new Date());

        if (commitRequired) {
            tx.commit();
        }

        return em.merge(project);
    }

    public Service createDummyService() {

        boolean commitRequired = false;

        if (!tx.isActive()) {
            tx.begin();
            commitRequired = true;
        }

        Service service = new Service();
        service.setName("DummyService");
        service.setActive(true);

        if (commitRequired) {
            tx.commit();
        }

        return em.merge(service);
    }

    public Workspace createDummyWorkspace(boolean withResources) {

        return createDummyWorkspace(withResources, null, null);
    }

    public Workspace createDummyWorkspace(boolean withResources, List<Project> projects, List<Service> services) {

        Workspace workspace = new Workspace();
        workspace.setTitle("DummyWorkspace");

        if (withResources) {
            workspace.setProjects(Arrays.asList(createDummyProject(), createDummyProject()));
            workspace.setServices(Arrays.asList(createDummyService(), createDummyService()));
        } else {

            if (projects != null && !projects.isEmpty()) {
                projects.forEach(r -> r.setWorkspace(workspace));
                workspace.setProjects(projects);
            }

            if (services != null && !services.isEmpty()) {
                services.forEach(r -> r.setWorkspace(workspace));
                workspace.setServices(services);

            }
        }

        return em.merge(workspace);
    }

    protected void assertChain(Slot leftSlot, Slot centralSlot, Slot rightSlot) {

        Optional.of(centralSlot);

        Optional.ofNullable(leftSlot).ifPresent(ls -> {

            assertThat(centralSlot.getPreviousSlot(), is(ls));
            assertThat(ls.getNextSlot(), is(centralSlot));

        });

        Optional.ofNullable(rightSlot).ifPresent(rs -> {

            assertThat(centralSlot.getNextSlot(), is(rs));
            assertThat(rs.getPreviousSlot(), is(centralSlot));

        });

    }

    protected void assertActionsRunned(List<String> actual, String... expected) {

        assertThat(actual.size(), is(expected.length));

        for (int i = 0; i < expected.length; i++) {
            assertThat("found:" + actual, actual.contains(expected[i]), is(true));
        }

    }
}
