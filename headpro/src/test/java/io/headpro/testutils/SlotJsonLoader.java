/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.testutils;

import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;
import io.headpro.slots.entity.Slot;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import org.hamcrest.core.Is;
import static org.junit.Assert.*;

/**
 *
 * @author alacambra
 */
public class SlotJsonLoader {

    protected EntityManager em;
    protected EntityTransaction tx;
    Project project;
    Service service;

    public SlotJsonLoader(EntityManager em, EntityTransaction tx, Project project, Service service) {

        Optional.of(project);
        Optional.of(service);

        this.em = em;
        this.tx = tx;
        this.project = project;
        this.service = service;

    }

    public SlotJsonLoader(EntityManager em, EntityTransaction tx) {
        this.em = em;
        this.tx = tx;
    }

    public  Map<Integer, Integer> load(String json) {

        return load(json, () -> {
            ResourceSlot slot = new ResourceSlot();
            slot.setService(service);
            slot.setProject(project);
            return slot;
        });
    }

    public Map<Integer, Integer> load(String json, Supplier<? extends Slot> slotSupplier) {
        JsonReader jsonReader = Json.createReader(new StringReader(json));

        JsonArray jsonArray = jsonReader.readArray();
        Map<Integer, Slot> slots = new HashMap<>();

        for (int i = 0; i < jsonArray.size(); i++) {

            JsonObject jsonSlot = jsonArray.getJsonObject(i);
            Slot slot = slotSupplier.get();

            slot.setStartPosition(jsonSlot.getInt("startPosition"));
            slot.setId(jsonSlot.getInt("id"));
            slot.setDuration(jsonSlot.getInt("duration"));
            slot.setValuePerStep(jsonSlot.getInt("valuePerStep"));

            slots.put(slot.getId(), slot);

        }

        for (int i = 0; i < jsonArray.size(); i++) {

            JsonObject jsonSlot = jsonArray.getJsonObject(i);
            Integer id = jsonSlot.getInt("id");

            int previous = jsonSlot.getInt("previousSlot");
            int next = jsonSlot.getInt("nextSlot");

            Slot slot = slots.get(id);

            Optional.ofNullable(slots.get(previous)).ifPresent(slot::setPreviousSlot);
            Optional.ofNullable(slots.get(next)).ifPresent(slot::setNextSlot);

        }

        tx.begin();
        Slot toPersit = slots.values().stream().findAny().get();
        toPersit = em.merge(toPersit);
        assertThat(toPersit.getId(), notNullValue());
        tx.commit();

        List<Slot> persistedSlots = new ArrayList<>();
        persistedSlots.add(toPersit);

        Slot currentSlot = toPersit;

        while (currentSlot.getPreviousSlot() != null) {
            currentSlot = currentSlot.getPreviousSlot();
            persistedSlots.add(currentSlot);

        }

        currentSlot = toPersit;
        while (currentSlot.getNextSlot() != null) {

            currentSlot = currentSlot.getNextSlot();
            persistedSlots.add(currentSlot);

        }
        
        assertThat(persistedSlots.size(), Is.is(jsonArray.size()));

        Map<Integer, Integer> newOldIds = new HashMap<>();

        for (Slot s : slots.values()) {
            for (Slot pSlot : persistedSlots) {
                if (s.getStartPosition().equals(pSlot.getStartPosition())) {
                    if (newOldIds.containsKey(pSlot.getId()) || newOldIds.containsValue(s.getId())) {
                        throw new RuntimeException("id already used");
                    }
                    
                    newOldIds.put(pSlot.getId(), s.getId());
                }
            }
        }

        return newOldIds;
    }
}
