package io.headpro.testutils;

import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alacambra on 28/11/15.
 */
public class ResourceSlotsGenerator {

    Project project;
    Service service;
    int start = 0;
    int slotsDuration = 1;
    int valuePerStep = 5;

    EntityTransaction tx;
    EntityManager em;

    public ResourceSlotsGenerator(EntityManager em, EntityTransaction tx) {
        this.em = em;
        this.tx = tx;
    }

    public ResourceSlotsGenerator setProject(Project project){
        this.project = project;
        return this;
    }

    public ResourceSlotsGenerator setService(Service service) {
        this.service = service;
        return this;
    }

    public ResourceSlotsGenerator setStart(int start) {
        this.start = start;
        return this;
    }

    public ResourceSlotsGenerator setSlotsDuration(int slotsDuration) {
        this.slotsDuration = slotsDuration;
        return this;
    }

    public ResourceSlotsGenerator setValuePerStep(int valuePerStep) {
        this.valuePerStep = valuePerStep;
        return this;
    }

    public List<ResourceSlot> build(int totalSlots){

        tx.begin();

        ResourceSlot previousSlot = null;
        ResourceSlot middleSlot = new ResourceSlot();
        middleSlot.setValuePerStep(valuePerStep);

        ResourceSlot nextSlot = new ResourceSlot();
        nextSlot.setValuePerStep(valuePerStep);

        ResourceSlot firstSlot = middleSlot;

        List<ResourceSlot> slots = new ArrayList<>(totalSlots);


        for (int i = 0; i < totalSlots; i++) {
            start = previousSlot == null ? start : previousSlot.getEndPosition() + 1;
            configureSlot(middleSlot, project, service, start, slotsDuration, previousSlot, i == totalSlots - 1 ? null : nextSlot);

            previousSlot = middleSlot;
            middleSlot = nextSlot;
            nextSlot = new ResourceSlot();
            nextSlot.setValuePerStep(valuePerStep);

        }

        ResourceSlot slot = em.merge(firstSlot);

        while (slot != null) {
            slots.add(slot);
            slot = slot.getNextSlot();
        }

        tx.commit();

        return slots;
    }

    private void configureSlot(
            ResourceSlot toConfigure,
            Project project,
            Service service,
            int start,
            int duration,
            ResourceSlot previousSlot,
            ResourceSlot nextSlot
    ) {
        toConfigure.setStartPosition(start);
        toConfigure.setDuration(duration);
        toConfigure.setProject(project);
        toConfigure.setService(service);
        toConfigure.setPreviousSlot(previousSlot);
        toConfigure.setNextSlot(nextSlot);
    }
}
