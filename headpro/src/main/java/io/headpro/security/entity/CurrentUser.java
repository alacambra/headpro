package io.headpro.security.entity;

import io.headpro.entity.UserBean;
import io.headpro.entity.User;
import java.security.Principal;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Typed;
import javax.inject.Inject;

@RequestScoped
@Typed(CurrentUser.class)
public class CurrentUser
        extends UserBean {

    @Inject
    Principal principal;
    
    @PostConstruct
    public void init(){
        loadEntity();
    }

    @Override
    public UserBean loadEntity() {
        return super.loadEntity(principal.getName());
    }

    public String getId() {
        return principal.getName();
    }

    @Override
    public UserBean loadEntity(String id) {
        throw new UnsupportedOperationException("current user is not changable");
    }

    @Override
    public UserBean setEntity(User entity) {
        throw new UnsupportedOperationException("current user is not changable");
    }

}
