package io.headpro.security.boundary;

import io.headpro.accounting.entity.Account;
import io.headpro.boundary.CurrentResourceBean;
import io.headpro.entity.UserBean;
import io.headpro.security.entity.CurrentUser;
import io.headpro.workspace.entity.Workspace;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Secured(Secured.RoleOver.NONE)
@Interceptor
public class Authorization {

    @Inject
    Logger logger;

    @Inject
    CurrentResourceBean resourceContext;

    @Inject
    CurrentUser currentUser;

    @AroundInvoke
    public Object checkRoles(InvocationContext context) throws Exception {
        
        logger.log(Level.FINE, "current resource context:{0}", resourceContext.toString());
        Secured securedAnnotation = context.getTarget().getClass().getAnnotation(Secured.class);
        
        if(securedAnnotation == null || securedAnnotation.value() == Secured.RoleOver.NONE){
            throw new RuntimeException("Invalid role over given");
        }

        currentUser.loadEntity();
        if (!isAllowed(context.getMethod(), currentUser, securedAnnotation.value())) {
            throw new NotAllowedException(
                    resourceContext.getResourceId(Workspace.class),
                    currentUser.getId(),
                    "Workspace");
        }
        
        return context.proceed();

    }

    boolean isAllowed(Method method, UserBean u, Secured.RoleOver roleOver) {
        
        AllowedTo allowedToAnnotation = method.getAnnotation(AllowedTo.class);
        PublicService publicServiceAnnotation = method.getAnnotation(PublicService.class);

        if(publicServiceAnnotation != null){
            return true;
        }
        
        if (allowedToAnnotation == null) {
            return false;
        }
        
        Role[] allowedRoles = allowedToAnnotation.value();
        
        Role userRole = null;
        logger.log(Level.FINEST, "roleOver is {0}", roleOver);
        
        if(null != roleOver)switch (roleOver) {
            case ACCOUNT:
                userRole = u.getRoleForAccount((Integer) resourceContext.getResourceId(Account.class));
                break;
            case WORKSPACE:
                userRole = u.getRoleForWorkspace((Integer) resourceContext.getResourceId(Workspace.class));
                break;
            default:
                throw new RuntimeException("Invalid RolleOver type:" + roleOver.name());
        }
        
        if(userRole == Role.NONE){
            return false;
        }

        for (Role role : allowedRoles) {

            if (userRole != null && (role == Role.ALL || userRole == role)) {
                return true;
            }
        }
        return false;
    }

}
