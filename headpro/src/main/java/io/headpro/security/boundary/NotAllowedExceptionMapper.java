package io.headpro.security.boundary;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.json.Json;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class NotAllowedExceptionMapper implements ExceptionMapper<NotAllowedException> {
    
    @Inject
    Logger logger;

    @Override
    public Response toResponse(NotAllowedException exception) {
        
        logger.log(Level.INFO, "User {0} got access denied for {1}:{2}", 
                new Object[]{exception.getUserId(), exception.getResourceType(), exception.getResourceId()});
        
        return Response.status(Response.Status.FORBIDDEN)
                .entity(Json.createObjectBuilder().add("error", "not authorized to access resource").build())
                .build();
    }
}
