package io.headpro.security.boundary;

import javax.ejb.ApplicationException;

@ApplicationException
public class NotAllowedException extends RuntimeException{
    private Object resourceId;
    private String userId;
    private String resourceType;

    public NotAllowedException(Object resourceId, String userId, String resourceType) {
        this.resourceId = resourceId;
        this.userId = userId;
        this.resourceType = resourceType;
    }

    public Object getResourceId() {
        return resourceId;
    }

    public String getUserId() {
        return userId;
    }

    public String getResourceType() {
        return resourceType;
    }

    
    
    
}
