package io.headpro.subject.boundary;

import io.headpro.accounting.boundary.AccountConverter;
import io.headpro.accounting.boundary.AccountResource;
import io.headpro.accounting.entity.UserAccount;
import io.headpro.entity.UserBean;
import io.headpro.security.entity.CurrentUser;
import io.headpro.workspace.boundary.WorkspaceConverter;
import io.headpro.workspace.entity.WorkspaceRole;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class UserResource {

    @PersistenceContext
    EntityManager em;

    @Inject
    private AccountConverter accountConverter;

    @Inject
    private WorkspaceConverter workspaceConverter;

    @Inject
    CurrentUser currentUser;

    @Inject
    Instance<UserBean> users;

    @POST
    public Response createAccount(@Context UriInfo uriInfo, JsonObject json) {

        UserAccount account = (UserAccount) accountConverter.fromJson(json, new UserAccount());
        account.setUser(currentUser.getEntity());
        account = em.merge(account);

        URI uri = uriInfo.getBaseUriBuilder()
                .path(AccountResource.class)
                .build(account.getId());

        return Response.created(uri).build();
    }

    @GET
    @Path("my/accounts")
    public JsonArray getMyAccounts() {
        return accountConverter.toJson(currentUser.getEntity().getAccounts());
    }

    @GET
    @Path("my/workspaces")
    public JsonArray getMyWorkspaces() {

        return Optional.ofNullable(currentUser.getEntity().getWorkspaceRoles()).orElse(new ArrayList<>(0)).stream()
                .map(WorkspaceRole::getWorkspace)
                .map(workspaceConverter::toJsonWithResources)
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();
    }

    @GET
    @Path("logout")
    public Response logout(@Context HttpServletRequest request) throws ServletException, URISyntaxException {
        request.logout();
        return Response.ok().build();
    }

}
