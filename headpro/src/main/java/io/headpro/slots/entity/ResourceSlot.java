package io.headpro.slots.entity;

import io.headpro.workspace.entity.Project;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "RESOURCE_SLOT")
@Inheritance//(strategy = InheritanceType.TABLE_PER_CLASS)
@NamedQueries({

        @NamedQuery(name = ResourceSlot.fetchAbsorbedSlots,
                /**
                 * Fetch a slot that is on the right site of a new node but that not all its
                 * position would be absorbed by the new node
                 */
                query = "SELECT rs FROM ResourceSlot as rs JOIN rs.project AS pr WHERE pr = :project AND " +
                        "rs.startPosition >= :start  AND rs.duration + rs.startPosition - 1 <= :end AND " +
                        "rs.service = :service " +
                        "ORDER BY rs.startPosition"),

        @NamedQuery(name = ResourceSlot.fetchOverlappedSlots,
                /**
                 * Fetch a slot that is on the right site of a new node but that not all its
                 * position would be absorbed by the new node
                 */
                query = "SELECT rs FROM ResourceSlot as rs " +
                        "JOIN rs.project AS pr WHERE pr = :project AND  rs.service = :service AND ((" +
                        "(rs.startPosition<=:end        AND rs.duration + rs.startPosition - 1 >=:end) OR " +
                        "(rs.startPosition<=:start - 1  AND rs.duration + rs.startPosition - 1 >=:start)) OR " +
                        "(rs.startPosition<=:start - 1  AND rs.duration + rs.startPosition - 1 >=:end)) " +
                        "AND NOT rs IN (" +
                        "   SELECT rs1 " +
                        "   FROM ResourceSlot as rs1 JOIN rs1.project AS pr1 WHERE pr1 = :project AND" +
                        "   rs1.startPosition >= :start  AND rs1.duration + rs1.startPosition - 1 <= :end) "
        ),

        @NamedQuery(name = ResourceSlot.fetchNearestSlots,
                query = "SELECT rs FROM ResourceSlot rs JOIN rs.project AS pr " +
                        "WHERE pr = :project AND rs.service = :service " +
                        "ORDER BY abs(rs.startPosition - :start )"
        ),

        @NamedQuery(name = ResourceSlot.fetchMultiProjectSlotsOfTeam,
                query = "SELECT rs FROM ResourceSlot rs JOIN rs.project AS pr " + 
                " WHERE pr.workspace = :workspace"
        ),

        @NamedQuery(name = ResourceSlot.fetchProjectSlots,
                query = "SELECT rs FROM ResourceSlot rs JOIN rs.project AS pr WHERE pr = :project " +
                        "ORDER BY rs.startPosition ASC "
        ),

        @NamedQuery(name = ResourceSlot.fetchSlotsForService,
                query = "SELECT rs FROM ResourceSlot rs JOIN rs.project AS pr " + 
                        "WHERE rs.service =:service AND pr.workspace = :workspace " +
                        "ORDER BY rs.startPosition ASC "
        )
}
)

public class ResourceSlot extends Slot<ResourceSlot> {

    protected static final String prefix = "io.headpro.entity.ResourceSlot.";
    public static final String fetchOverlappedSlots = prefix + "fetchOverlappedSlots";
    public static final String fetchAbsorbedSlots = prefix + "fetchAbsorbedSlots";
    public static final String fetchMultiProjectSlotsOfTeam = prefix + "fetchMultiProjectSlotsOfTeam";
    public static final String fetchProjectSlots = prefix + "fetchProjectSlots";
    public static final String fetchNearestSlots = prefix + "fetchNearestSlots";
    public static final String fetchSlotsForService = prefix + "fetchSlotsForService";

    @ManyToOne
    @NotNull
    Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
