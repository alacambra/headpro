package io.headpro.slots.entity;

import io.headpro.workspace.entity.Service;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Created by alacambra on 12/11/15.
 */
@MappedSuperclass
public abstract class Slot<S extends Slot> {

    @Transient
    Logger logger = Logger.getLogger(Slot.class.getSimpleName());

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @ManyToOne
    @NotNull
    protected Service service;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.MERGE})
    protected S previousSlot;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.MERGE})
    protected S nextSlot;

    @Column(precision = 5, scale = 2)
    @NotNull
    @Min(0)
    protected BigDecimal valuePerStep = BigDecimal.ZERO;

    @Min(0)
    protected Integer duration;

    @Min(0)
    protected Integer startPosition;

    @Version
    int version;

    @PreRemove
    public void updateLinks() {

        if (previousSlot != null) {
            previousSlot.setNextSlot(getNextSlot());
        }

        if (nextSlot != null) {
            nextSlot.setPreviousSlot(getPreviousSlot());
        }

        previousSlot = null;
        nextSlot = null;
    }

    @PrePersist
    @PreUpdate
    public void saving() {
        logger.finest(toString());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        if(duration < 0){
            throw new RuntimeException("Duration can not be negative");
        }
        this.duration = duration;
    }

    public BigDecimal getValuePerStep() {
        return valuePerStep;
    }

    public void setValuePerStep(BigDecimal valuePerStep) {
        this.valuePerStep = valuePerStep;
    }

    public void setValuePerStep(int valuePerStep) {
        this.valuePerStep = BigDecimal.valueOf(valuePerStep);
    }

    public Integer getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(Integer startPosition) {
        this.startPosition = startPosition;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public S getPreviousSlot() {
        return previousSlot;
    }

    public Optional<S> getPreviousSlotOptionally() {
        return Optional.ofNullable(previousSlot);
    }

    public void setPreviousSlot(S leftSlot) {
        this.previousSlot = leftSlot;
    }

    public S getNextSlot() {
        return nextSlot;
    }

    public Optional<S> getNextSlotOptionally() {
        return Optional.ofNullable(nextSlot);
    }

    public void setNextSlot(S rightSlot) {
        this.nextSlot = rightSlot;
    }

    public Integer getEndPosition() {
        return startPosition + duration - 1;
    }

    public void setEndPosition(Integer endPosition) {
        setDuration(endPosition - startPosition + 1);
    }

    public int getVersion() {
        return version;
    }
    
    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder()
                .append("id:").append(id)
                .append(", start:").append(startPosition)
                .append(", duration:").append(duration)
                .append(", end:").append(getEndPosition())
                .append(", valuePerStep:").append(getValuePerStep())
                .append(", previousSlot:").append(previousSlot == null ? "none" : previousSlot.getId())
                .append(", nextSlot:").append(nextSlot == null ? "none" : nextSlot.getId())
                .append(", service:").append(service == null ? "none" : service.getId());

        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Slot)) {
            return false;
        }

        Slot slot = (Slot) o;

        if (id != null) {
            return id.equals(slot.id);
        }

        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : super.hashCode();

    }
}
