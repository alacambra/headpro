package io.headpro.slots.entity;

import javax.persistence.*;

/**
 * Created by alacambra on 02/11/15.
 */
@Entity
@Table(name = "SERVICE_SLOT")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NamedQueries({
    @NamedQuery(name = ServiceSlot.fetchAbsorbedSlots,
            /**
             * Fetch a slot that is on the right site of a new node but that not all its position would be absorbed by
             * the new node
             */
            query = "SELECT ss FROM ServiceSlot as ss "
            + "WHERE ss.service = :service "
            + "AND ss.startPosition >= :start  AND ss.duration + ss.startPosition - 1 <= :end  "
            + "ORDER BY ss.startPosition"),

    @NamedQuery(name = ServiceSlot.fetchOverlappedSlots,
            /**
             * Fetch a slot that is on the right site of a new node but that not all its position would be absorbed by
             * the new node
             */
            query = "SELECT ss FROM ServiceSlot as ss "
            + "WHERE  ss.service = :service AND (("
            + "(ss.startPosition<=:end  AND ss.duration + ss.startPosition - 1 >=:end) OR "
            + "(ss.startPosition<=:start - 1  AND ss.duration + ss.startPosition - 1 >=:start)) OR "
            + "(ss.startPosition<=:start - 1  AND ss.duration + ss.startPosition - 1 >=:end)) "
            + "AND NOT ss IN ("
            + "   SELECT ss1 "
            + "   FROM ServiceSlot as ss1 WHERE ss1.service = :service AND"
            + "   ss1.startPosition >= :start  AND ss1.duration + ss1.startPosition - 1 <= :end) "
            + "ORDER BY ss.startPosition"),

    @NamedQuery(name = ServiceSlot.deleteAbsorbedSlots,
            query = "DELETE FROM ServiceSlot as ss "
            + "WHERE  ss.service = :service "
            + "AND ss.startPosition > :start  AND ss.duration + ss.startPosition - 1 < :end"),

    @NamedQuery(name = ServiceSlot.fetchNearestSlots,
            query = "SELECT ss FROM ServiceSlot ss "
            + "WHERE ss.service = :service "
            + "ORDER BY abs(ss.startPosition - :start )"
    ),
    @NamedQuery(name = ServiceSlot.fetchMultiServiceSlots,
            query = "SELECT ss FROM ServiceSlot ss JOIN ss.service AS service WHERE service IN :services"
    ),

    @NamedQuery(name = ServiceSlot.fetchMultiServiceSlotsOfWorkspace,
            query = "SELECT ss FROM ServiceSlot ss JOIN ss.service AS service " +
            "WHERE service.workspace = :workspace"
    ),

    @NamedQuery(name = ServiceSlot.fetchServiceSlots,
            query = "SELECT ss FROM ServiceSlot ss JOIN ss.service AS service WHERE service = :service "
            + "ORDER BY ss.startPosition ASC "
    ),})
public class ServiceSlot extends Slot<ServiceSlot> {

    protected static final String prefix = "io.headpro.entity.ServiceSlot.";
    public static final String fetchOverlappedSlots = prefix + "fetchOverlappedSlots";
    public static final String deleteAbsorbedSlots = prefix + "deleteAbsorbedSlots";
    public static final String fetchAbsorbedSlots = prefix + "fetchAbsorbedSlots";
    public static final String fetchNearestSlots = prefix + "fetchNearestSlots";
    public static final String fetchMultiServiceSlots = prefix + "fetchMultiServiceSlots";
    public static final String fetchMultiServiceSlotsOfWorkspace = prefix + "fetchMultiServiceSlotsOfTeam";
    public static final String fetchServiceSlots = prefix + "fetchServiceSlots";
}
