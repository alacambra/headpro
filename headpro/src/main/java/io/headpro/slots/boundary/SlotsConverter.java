package io.headpro.slots.boundary;

import io.headpro.boundary.InvalidRequestException;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;
import io.headpro.slots.control.SlotsFactory;
import io.headpro.slots.entity.Slot;
import io.headpro.utils.DatesService;

import java.math.BigDecimal;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import javax.inject.Inject;
import javax.json.JsonNumber;
import javax.json.JsonValue;

/**
 * Created by alacambra on 09/12/15.
 */
public class SlotsConverter {

    @PersistenceContext
    EntityManager em;

    @Inject
    SlotsFactory slotsFactory;

    @Inject
    DatesService datesService;

    public <S extends Slot> S fromJson(JsonObject json, Class<S> clazz) {

        S slot = slotsFactory.getInstance(clazz);

        int duration = Optional.ofNullable(json.get("duration"))
                .map((v) -> getValueOfType(v, (a) -> ((JsonNumber) a).intValue()))
                .orElseThrow(() -> new InvalidRequestException("Duration not given"));

        int startPosition = Optional.ofNullable(json.get("startPosition"))
                .map((v) -> getValueOfType(v, (a) -> ((JsonNumber) a).intValue()))
                .orElseThrow(() -> new InvalidRequestException("Start position per step not given"));

        BigDecimal valuePerStep = Optional.ofNullable(json.get("valuePerStep"))
                .map((v) -> getValueOfType(v, (a) -> ((JsonNumber) a).bigDecimalValue()))
                .orElseThrow(() -> new InvalidRequestException("Value per step not given"));

        slot.setValuePerStep(valuePerStep);
        slot.setStartPosition(startPosition);
        slot.setDuration(duration);

        if (json.containsKey("service")) {
            slot.setService(getServiceOrException(json.getInt("service")));
        }

        return slot;
    }

    private <T> T getValueOfType(JsonValue value, Function<JsonValue, T> f) {

        try {
            return f.apply(value);
        } catch (ClassCastException e) {
            throw new InvalidRequestException("Invalid type given");
        }
    }

    public JsonObject toJson(Slot slot) {
        ResourceSlot dummy = new ResourceSlot();
        dummy.setId(-1);

        JsonObjectBuilder object = Json.createObjectBuilder()
                .add("id", slot.getId())
                .add("duration", slot.getDuration())
                .add("startPosition", slot.getStartPosition())
                .add("service", slot.getService().getId())
                .add("valuePerStep", slot.getValuePerStep())
                .add("previousSlot", getIdOrDefault(slot.getPreviousSlot()))
                .add("nextSlot", getIdOrDefault(slot.getNextSlot()));

        return object.build();
    }

    public JsonObject toJsonWithAbsolutePosition(Slot slot) {
        ResourceSlot dummy = new ResourceSlot();
        dummy.setId(-1);

        ResourceSlot s = (ResourceSlot) slot;
        int start = datesService.getDateIndex(s.getProject().getStartDate());

        JsonObjectBuilder object = Json.createObjectBuilder()
                .add("id", slot.getId())
                .add("duration", slot.getDuration())
                .add("startPosition", slot.getStartPosition() + start)
                .add("service", slot.getService().getId())
                .add("valuePerStep", slot.getValuePerStep())
                .add("previousSlot", getIdOrDefault(slot.getPreviousSlot()))
                .add("nextSlot", getIdOrDefault(slot.getNextSlot()));

        return object.build();
    }

    private Integer getIdOrDefault(Slot slot) {
        return Optional.ofNullable(slot).flatMap(s -> Optional.of(s.getId())).orElse(-1);
    }

    public JsonArrayBuilder toJson(List<? extends Slot> slots) {

        JsonArrayBuilder arr = Json.createArrayBuilder();
        slots.stream().map(this::toJson).forEach(arr::add);

        return arr;

    }

    public JsonArrayBuilder toJsonWithAbsolutePosition(List<? extends Slot> slots) {

        JsonArrayBuilder arr = Json.createArrayBuilder();
        slots.stream().map(this::toJsonWithAbsolutePosition).forEach(arr::add);

        return arr;

    }

    Service getServiceOrException(int serviceId) {
        Optional<Service> service = Optional.ofNullable(em.find(Service.class, serviceId));
        return service.orElseThrow(()
                -> new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                .entity("Service " + serviceId + " not found").build()));
    }
}
