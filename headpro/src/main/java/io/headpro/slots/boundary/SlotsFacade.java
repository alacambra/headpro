/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.boundary;

import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;
import io.headpro.slots.entity.ServiceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.slots.control.SlotActionsService;
import io.headpro.utils.DatesService;
import io.headpro.workspace.entity.Workspace;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.*;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alacambra
 */
public class SlotsFacade {

    @PersistenceContext
    EntityManager em;

    @Inject
    DatesService datesService;

    @Inject
    SlotActionsService slotActionsService;

    /**
     * Return not persisted slots from length 1 containing the total required resources for all services together
     */
    public Map<Project, List<BigDecimal>> getCalculatedResourceSlots(Workspace workspace) {

        List<ResourceSlot> result = em.createNamedQuery(ResourceSlot.fetchMultiProjectSlotsOfTeam)
                .setParameter("workspace", workspace)
                .getResultList();

        if (result.isEmpty()) {
            return Collections.EMPTY_MAP;
        }

        Map<Project, List<BigDecimal>> simpleSlots = new HashMap<>();

        Map<Project, List<ResourceSlot>> projectSlots = result.stream().collect(groupingBy(ResourceSlot::getProject));

        for (Map.Entry<Project, List<ResourceSlot>> entry : projectSlots.entrySet()) {

            Project project = entry.getKey();
            int duration
                    = datesService.getDateIndex(project.getEndDate()) - datesService.getDateIndex(project.getStartDate());

            List<BigDecimal> slots = new ArrayList<>(Collections.nCopies(duration, BigDecimal.ZERO));
            simpleSlots.put(project, slots);

            for (Slot slot : entry.getValue()) {
                for (int i = slot.getStartPosition(); i <= slot.getEndPosition() && i < duration; i++) {
                    BigDecimal value = slots.get(i).add(slot.getValuePerStep());
                    slots.set(i, value);
                }
            }
        }

        return simpleSlots;
    }

    public Map<Service, List<BigDecimal>> getCalculatedServiceSlots(Workspace workspace) {

        List<ServiceSlot> result
                = em.createNamedQuery(ServiceSlot.fetchMultiServiceSlotsOfWorkspace)
                .setParameter("workspace", workspace)
                .getResultList();

        if (result.isEmpty()) {
            return Collections.EMPTY_MAP;
        }

        Map<Service, List<BigDecimal>> simpleSlots = new HashMap<>();

        int start = result.stream().mapToInt(Slot::getStartPosition).summaryStatistics().getMin();
        int end = result.stream().mapToInt(Slot::getEndPosition).summaryStatistics().getMax();
        int duration = end - start + 1;

        Map<Service, List<ServiceSlot>> serviceSlots = result.stream().collect(groupingBy(ServiceSlot::getService));

        for (Map.Entry<Service, List<ServiceSlot>> entry : serviceSlots.entrySet()) {

            Service service = entry.getKey();

            service.setStartPosition(start);
            service.setLastIndex(end);

            List<BigDecimal> slots = new ArrayList<>(Collections.nCopies(duration, BigDecimal.ZERO));
            simpleSlots.put(service, slots);

            for (Slot slot : entry.getValue()) {
                for (int i = slot.getStartPosition(); i <= slot.getEndPosition(); i++) {
                    BigDecimal value = slots.get(i - start).add(slot.getValuePerStep());
                    slots.set(i - start, value);
                }
            }
        }

        return simpleSlots;
    }

    public Slot insertOrUpdateSlots(Slot newSlot) {
        return slotActionsService.addSlotReturnActionResult(newSlot).getSavedSlot();
    }
}
