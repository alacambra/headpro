package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author alacambra
 */
public abstract class AbstractSlotsAction {

    protected ActionContext context;
    private Enum<?> invalidActionReason = InvalidActionReasons.noReasonGiven;
    private boolean initialized = false;
    private List<Slot> uselessSlots = new ArrayList<>();
    private boolean persistNewSlot = true;
    private Slot slotToSave = null;

    public abstract AbstractSlotsAction perform();

    public abstract boolean isValid();

    public AbstractSlotsAction setContext(ActionContext context) {
        this.context = context;
        return this;
    }

    public final AbstractSlotsAction initialize() {
        if (!initialized) {
            actionInitialization();
            initialized = true;
        }
        return this;
    }

    protected abstract void actionInitialization();

    public Enum<?> getInvalidActionReason() {
        return invalidActionReason;
    }
    
    protected void setInvalidActionReason(Enum<?> reason) {
        
        invalidActionReason = Optional.ofNullable(reason).orElseThrow(() ->
                new RuntimeException("Reason can not be null"));
    }
    
    protected AbstractSlotsAction addUselessSlot(Slot s){
        
        s.setNextSlot(null);
        s.setPreviousSlot(null);
        uselessSlots.add(s);
        
        return this;
    }

    public List<Slot> getUselessSlots() {
        return Collections.unmodifiableList(uselessSlots);
    }

    public boolean slotWillBeOnlyUpdated() {
        return !persistNewSlot;
    }

    protected void setPersistNewSlot(boolean persistNewSlot) {
        this.persistNewSlot = persistNewSlot;
    }

    public Slot getSlotToSave() {
        return slotToSave;
    }

    protected AbstractSlotsAction setSlotToSave(Slot slotToSave) {
        this.slotToSave = slotToSave;
        return this;
    }
}
