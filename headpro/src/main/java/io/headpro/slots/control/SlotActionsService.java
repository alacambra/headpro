/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@Stateless
public class SlotActionsService {

    Logger logger = Logger.getLogger(getClass().getName());

    @PersistenceContext
    EntityManager em;

    @Inject
    Instance<SimpleAddAction> simpleAddActions;

    @Inject
    Instance<MergeAction> mergeActions;

    @Inject
    Instance<UnionAction> unionActions;

    @Inject
    Instance<LeftReductionAction> leftReductionActions;

    @Inject
    Instance<RightReductionAction> rightReductionActions;

    @Inject
    Instance<SplitAction> splitActions;

    @Inject
    Instance<ActionContext> actionContextInstance;

    @Inject
    InteractedSlotsProvider interactedSlotsProvider;

    List<Instance<? extends AbstractSlotsAction>> actionInstances;

    @PostConstruct
    public void init() {

        actionInstances = new ArrayList<>();

        actionInstances.add(simpleAddActions);
        actionInstances.add(leftReductionActions);
        actionInstances.add(rightReductionActions);
        actionInstances.add(splitActions);
        actionInstances.add(mergeActions);
        actionInstances.add(unionActions);

    }

    public ActionResult addSlotReturnActionResult(Slot newSlot) {

        List<Slot> overlappedSlots = interactedSlotsProvider.getOverlappedSlots(newSlot);
        List<Slot> absorbedSlots = interactedSlotsProvider.getAbsorbedSlots(newSlot);

        ActionContext context = actionContextInstance.get()
                .setAbsorbbedSlots(absorbedSlots)
                .setOverlappedSlots(overlappedSlots)
                .setNewSlot(newSlot);

        List<AbstractSlotsAction> actions = actionInstances.stream()
                .map(instance -> instance.get().setContext(context).initialize())
                .collect(toList());

        ActionResult result = new ActionResult();
        List<AbstractSlotsAction> validActions = new ArrayList<>();

        for (AbstractSlotsAction action : actions) {
            if (!action.isValid()) {
                result.addInvalidAction(action.getClass(), action.getInvalidActionReason());
            } else {
                validActions.add(action);
            }
        }

        if (validActions.isEmpty()) {
            throw new RuntimeException("no valid action found:" + result);
        }

        if (validActions.size() > 2) {
            String msg = validActions.stream()
                    .map(Object::getClass)
                    .map(Class::getSimpleName)
                    .collect(joining(", "));

            throw new RuntimeException("too many actions found: " + msg);
        }

        logger.log(Level.FINE, "performing action {0}", validActions.get(0));

        absorbedSlots.stream().forEach(em::remove);

        validActions.stream().forEach(this::performActionAndRemoveRemainingSlots);

        Slot slotToSave = validActions.stream()
                .filter(AbstractSlotsAction::slotWillBeOnlyUpdated)
                .findFirst()
                .flatMap(action -> Optional.of(action.getSlotToSave()))
                .orElse(newSlot);

        result.setRunnedAction(validActions);
        result.setSavedSlot(em.merge(slotToSave));

        logger.fine(result.toString());

        return result;
    }

    public Slot addSlot(Slot newSlot) {
        return addSlotReturnActionResult(newSlot).getSavedSlot();
    }

    private void performActionAndRemoveRemainingSlots(AbstractSlotsAction action) {
        action.perform();
        action.getUselessSlots().forEach(em::remove);

    }

    public static class ActionResult {

        private List<AbstractSlotsAction> actionsRunned = new ArrayList<>();
        private Map<Class<?>, Enum<?>> invalidActions = new HashMap<>();
        private Slot savedSlot;
        private boolean newSlotAdded = true;

        private ActionResult setRunnedAction(List<AbstractSlotsAction> actions) {
            actionsRunned = actions;
            return this;
        }

        private ActionResult addInvalidAction(Class<?> actionClazz, Enum<?> reason) {

            if (invalidActions.containsKey(actionClazz)) {
                throw new RuntimeException("readding an added action");
            }

            invalidActions.put(actionClazz, reason);
            return this;
        }

        private void setSavedSlot(Slot savedSlot) {
            this.savedSlot = savedSlot;
        }

        public Slot getSavedSlot() {
            return savedSlot;
        }

        public Map<Class<?>, Enum<?>> getInvalidActions() {
            return invalidActions;
        }

        public List<AbstractSlotsAction> getActionsRunned() {
            return actionsRunned;
        }

        public List<String> getActionsRunnedNames() {
            return actionsRunned.stream().map(a -> a.getClass().getSimpleName()).collect(toList());
        }

        public boolean isNewSlotAdded() {
            return newSlotAdded;
        }

        private void setNewSlotAdded(boolean newSlotAdded) {
            this.newSlotAdded = newSlotAdded;
        }

        @Override
        public String toString() {

            StringBuilder builder = new StringBuilder("actions returned:\n");
            actionsRunned.forEach(action -> builder.append("* ").append(action.getClass().getSimpleName()).append("\n"));
            builder.append("Invalid actions:\n");
            invalidActions.forEach((clazz, reason)
                    -> builder.append(clazz.getSimpleName()).append("-").append(reason).append("\n")
            );

            builder.append("Saved slot:\n").append(Optional.ofNullable(savedSlot).map(Object::toString).orElse("none"));

            return builder.toString(); //To change body of generated methods, choose Tools | Templates.
        }

    }

}
