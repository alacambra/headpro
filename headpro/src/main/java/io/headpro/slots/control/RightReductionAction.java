/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;
import java.util.Optional;

/**
 *
 * @author alacambra
 */
public class RightReductionAction extends AbstractSlotsAction {

    Slot reducerSlot;
    Slot overlappedSlot;
    Optional<Slot> nextSlot = Optional.empty();

    @Override
    public AbstractSlotsAction perform() {

        reducerSlot.setPreviousSlot(overlappedSlot);
        nextSlot.ifPresent(reducerSlot::setNextSlot);

        int overlappedArea = SlotsHelper.getOverlappedArea(overlappedSlot, reducerSlot);
        
        overlappedSlot.setDuration(overlappedSlot.getDuration() - overlappedArea);
        overlappedSlot.setNextSlot(reducerSlot);

        nextSlot.ifPresent(s -> s.setPreviousSlot(reducerSlot));

        return this;
    }

    public void setNextSlot(Slot nextSlot) {
        this.nextSlot = Optional.of(nextSlot);
    }

    @Override
     protected void actionInitialization() {
        reducerSlot = context.getNewSlot();
    }

    private boolean validate() {

        if (SlotsHelper.getOverlappedArea(overlappedSlot, reducerSlot) == 0) {
            setInvalidActionReason(InvalidActionReasons.noOverlappedArea);
            return false;
        }

        if (overlappedSlot.getEndPosition() > reducerSlot.getEndPosition()) {
            setInvalidActionReason(InvalidActionReasons.falseReductionSide);
            return false;
        }

        if (SlotsHelper.slotsHaveSameValuePerStep(reducerSlot, overlappedSlot)) {
            setInvalidActionReason(InvalidActionReasons.sameValuePerStep);
            return false;
        }

        return true;
    }

    @Override
    public boolean isValid() {

        if (context.getOverlappedSlots().isEmpty()) {
            setInvalidActionReason(InvalidActionReasons.noOverlappedSlot);
            return false;
        }
        
        initialize();

        for (Slot s : context.getOverlappedSlots()) {
            overlappedSlot = s;
            nextSlot = Optional.ofNullable(overlappedSlot.getNextSlot());
            if (validate()) {
                return true;
            }
        }

        return false;

    }

}
