package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

public class NeighborsSlots {

    Logger logger = Logger.getLogger(getClass().getName());

    Slot previousNeighbor;
    Slot nextNeighbor;
    Slot sourceSlot;

    Slot nearestSlot;

//    List<Slot> seeds;
//    boolean seedsAreAbsorbedSlots = false;
    List<Slot> absorbedSlots = Collections.EMPTY_LIST;
    List<Slot> overlappedSlots = Collections.EMPTY_LIST;

    @Inject
    InteractedSlotsProvider interactedSlotsProvider;

    public NeighborsSlots() {
//        seeds = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
    }

    public NeighborsSlots findNeighbors() {

        if (overlappedSlots.isEmpty() && absorbedSlots.isEmpty()) {
            nearestSlot = interactedSlotsProvider.getNearestSlot(sourceSlot);
        }

        loadNeighboursSlots();

        return this;
    }

    /**
     * If absorbed slots are found, the first and the last will be used to take the previous and next slots of the new
     * slot. The next slot of the last slot, will be used as next of the new slot and the previous slot of the first
     * slot will be used as the previous slot of the new slot
     * <p/>
     * If no absorbed slots are found, but overlapped slots, empty Optionals will be returned since is the splitter
     * logic the responsible to decide which will be the previous and next slots of the new slot, since they will be
     * created
     * <p/>
     * In case that no absorbed nor overlapped slots are found, the nearest slot will be used. Depending of this slot is
     * previous or after to the new slot, the new will be placed after or before of the nearest slot
     */
    private void loadNeighboursSlots() {

        /**
         * no seed slots found. It is the first slot or exist overlapping slots
         */
        if (overlappedSlots.isEmpty() && absorbedSlots.isEmpty() && nearestSlot == null) {
            return;
        }

        if (!absorbedSlots.isEmpty()) {
            
            absorbedSlots.sort(this::compareBySlotsStartPosition);
            logger.fine("Slots absorbed. Using first and last absorbed slots to find neighbours");

            previousNeighbor = absorbedSlots.get(0).getPreviousSlot();
            nextNeighbor = absorbedSlots.get(absorbedSlots.size() - 1).getNextSlot();

        } else if (!overlappedSlots.isEmpty()) {
            if (overlappedSlots.get(0).getStartPosition() < sourceSlot.getStartPosition()) {
                previousNeighbor = overlappedSlots.get(0);
                nextNeighbor = previousNeighbor.getNextSlot();
            } else {
                nextNeighbor = overlappedSlots.get(0);
                previousNeighbor = nextNeighbor.getPreviousSlot();
            }

        } else if (sourceSlot.getStartPosition() < nearestSlot.getStartPosition()) {

            logger.fine("Not absorbed. Using nearest slot "
                    + "with newStartIsPreviousToNearestSlotStart = true");

            previousNeighbor = nearestSlot.getPreviousSlot();
            nextNeighbor = nearestSlot;

        } else {

            logger.fine("Not absorbed nor overlapping slots found. Using nearest slot "
                    + "with newStartIsPreviousToNearestSlotStart = false");

            previousNeighbor = nearestSlot;
            nextNeighbor = nearestSlot.getNextSlot();
        }

    }

    int compareBySlotsStartPosition(Slot s1, Slot s2) {
        return s1.getStartPosition() - s2.getStartPosition();
    }

    public NeighborsSlots setAbsorbedSlots(List<Slot> absorbedSlots) {

        this.absorbedSlots = new ArrayList<>();
        this.absorbedSlots.addAll(absorbedSlots);
        return this;
    }

    public void setOverlappedSlots(List<Slot> overlappedSlots) {
        this.overlappedSlots = overlappedSlots;
    }

    public Slot getNextNeighbor() {
        return nextNeighbor;
    }

    public Slot getPreviousNeighbor() {
        return previousNeighbor;
    }

    public NeighborsSlots setSourceSlot(Slot sourceSlot) {
        this.sourceSlot = sourceSlot;
        return this;
    }

    public boolean bothNeighborsExist() {
        return nextNeighbor != null && previousNeighbor != null;
    }
    
    public List<Slot> getNeighbors(){
        return Stream.of(previousNeighbor, nextNeighbor).filter(n -> n != null).collect(toList());
    }
}
