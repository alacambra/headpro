/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author alacambra
 */
public class ActionContext {
    
    List<Slot> absorbbedSlots;
    List<Slot> overlappedSlots;
    Slot newSlot;

    public ActionContext() {
        this.absorbbedSlots = Collections.EMPTY_LIST;
        this.overlappedSlots = Collections.EMPTY_LIST;
    }

    public List<Slot> getAbsorbbedSlots() {
        return absorbbedSlots;
    }

    public Slot getNewSlot() {
        return newSlot;
    }

    public List<Slot> getOverlappedSlots() {
        return overlappedSlots;
    }

    public ActionContext setAbsorbbedSlots(List<Slot> absorbbedSlots) {
        this.absorbbedSlots = Collections.unmodifiableList(absorbbedSlots);
        return this;
    }

    public ActionContext setNewSlot(Slot newSlot) {
        this.newSlot = newSlot;
        return this;
    }

    public ActionContext setOverlappedSlots(List<Slot> overlappedSlots) {
        this.overlappedSlots = Collections.unmodifiableList(overlappedSlots);
        return this;
    }
    
}
