package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;

import javax.inject.Inject;
import java.util.List;

import static io.headpro.slots.control.SlotsHelper.*;
import java.util.Optional;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author alacambra
 */
public class MergeAction extends AbstractSlotsAction {

    Slot currentSlot;
    Slot addedSlot;

    @Inject
    NeighborsSlots neighborsSlots;

    @Override
    public AbstractSlotsAction perform() {

        int start = currentSlot.getStartPosition() < addedSlot.getStartPosition()
                ? currentSlot.getStartPosition() : addedSlot.getStartPosition();

        int duration = currentSlot.getDuration() + addedSlot.getDuration()
                - SlotsHelper.getOverlappedArea(addedSlot, currentSlot);
        
        //It happens if a leftReduction has been performed
        Optional.ofNullable(addedSlot.getNextSlot()).ifPresent(ns -> {
            
            if(ns.equals(currentSlot)){
                return;
            }
            ns.setPreviousSlot(currentSlot);
            currentSlot.setNextSlot(ns);
            addedSlot.setNextSlot(null);
        });
        
        //It happens if a rightReduction has been performed
        Optional.ofNullable(addedSlot.getPreviousSlot()).ifPresent(ps -> {
            
            if(ps.equals(currentSlot)){
                return;
            }
            
            ps.setNextSlot(currentSlot);
            currentSlot.setPreviousSlot(ps);
            addedSlot.setPreviousSlot(null);
        });

        currentSlot.setStartPosition(start);
        currentSlot.setDuration(duration);
        setPersistNewSlot(false);
        setSlotToSave(currentSlot);

        return this;
    }

    @Override
    protected void actionInitialization() {

        addedSlot = context.getNewSlot();
        neighborsSlots.setAbsorbedSlots(context.getAbsorbbedSlots());
        neighborsSlots.setSourceSlot(addedSlot);
        neighborsSlots.setOverlappedSlots(context.getOverlappedSlots());
        neighborsSlots.findNeighbors();

    }

    private boolean validate(Slot slot) {

        currentSlot = slot;

        if (!slotsHaveSameValuePerStep(addedSlot, currentSlot)) {
            setInvalidActionReason(InvalidActionReasons.differentValuePerStep);
            return false;

        } else if (!slotsAreContiguos(addedSlot, currentSlot)
                && !slotsOverlaps(addedSlot, currentSlot)) {

            setInvalidActionReason(InvalidActionReasons.falseSlotPosition);
            return false;
        }

        return true;
    }

    private boolean validateArrayItems(List<Slot> items) {

        List<Slot> found = items.stream()
                .filter(this::validate)
                .collect(toList());
        
        long total = found.size();

        if (total == 1) {
            currentSlot = found.get(0);
            setInvalidActionReason(InvalidActionReasons.noReasonGiven);
            return true;
        } else if (total == 2) {
            setInvalidActionReason(InvalidActionReasons.shouldJoinNeighbors);
            return false;
        }else if(total == 0){
            setInvalidActionReason(InvalidActionReasons.noContiguosNorOverlappedSlots);
            return false;
        }

        throw new RuntimeException("Invalid items size: " + items.size());
        
    }

    @Override
    public boolean isValid() {

        initialize();
        
        if (context.getOverlappedSlots().size() == 2) {
            
            return validateArrayItems(context.getOverlappedSlots());
            
        }else{
            //Neighbors should be the same as the overlapped slots
            return validateArrayItems(neighborsSlots.getNeighbors());
        }
    }

}
