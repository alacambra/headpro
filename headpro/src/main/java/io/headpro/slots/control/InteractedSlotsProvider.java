package io.headpro.slots.control;

import io.headpro.workspace.control.InvalidSlotTypeException;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.ServiceSlot;
import io.headpro.slots.entity.Slot;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alacambra
 */
public class InteractedSlotsProvider {

    @PersistenceContext
    EntityManager em;
    
    public List<Slot> getOverlappedSlots(Slot slot) {

        List<Slot> slots = null;

        if (slot.getClass().isAssignableFrom(ResourceSlot.class)) {

            slots = em.createNamedQuery(ResourceSlot.fetchOverlappedSlots)
                    .setParameter("start", slot.getStartPosition())
                    .setParameter("end", slot.getEndPosition())
                    .setParameter("project", ((ResourceSlot) slot).getProject())
                    .setParameter("service", slot.getService())
                    .getResultList();

        } else if (slot.getClass().isAssignableFrom(ServiceSlot.class)) {
            slots = em.createNamedQuery(ServiceSlot.fetchOverlappedSlots)
                    .setParameter("start", slot.getStartPosition())
                    .setParameter("end", slot.getEndPosition())
                    .setParameter("service", slot.getService())
                    .getResultList();

        } else {
            throw new InvalidSlotTypeException("Type not found:" + slot.getClass().getName());
        }

        return slots;

    }

    public List<Slot> getAbsorbedSlots(Slot slot) {
        List<Slot> slots = null;

        if (slot.getClass().isAssignableFrom(ResourceSlot.class)) {

            slots = em.createNamedQuery(ResourceSlot.fetchAbsorbedSlots)
                    .setParameter("end", slot.getEndPosition())
                    .setParameter("start", slot.getStartPosition())
                    .setParameter("project", ((ResourceSlot) slot).getProject())
                    .setParameter("service", slot.getService())
                    .getResultList();

        } else if (slot.getClass().isAssignableFrom(ServiceSlot.class)) {

            slots = em.createNamedQuery(ServiceSlot.fetchAbsorbedSlots).setParameter("end", slot.getEndPosition())
                    .setParameter("start", slot.getStartPosition())
                    .setParameter("service", slot.getService())
                    .getResultList();

        } else {
            throw new InvalidSlotTypeException("Type not found:" + slot.getClass().getName());
        }

        return slots;
    }

    public Slot getNearestSlot(Slot slot) {

        Slot nearestSlot = null;
        List<? extends Slot> slots = Collections.EMPTY_LIST;

        if (slot.getClass().isAssignableFrom(ResourceSlot.class)) {

            slots = em.createNamedQuery(ResourceSlot.fetchNearestSlots)
                    .setParameter("project", ((ResourceSlot) slot).getProject())
                    .setParameter("service", slot.getService())
                    .setParameter("start", slot.getStartPosition())
                    .setMaxResults(1)
                    .getResultList();

        } else if (slot.getClass().isAssignableFrom(ServiceSlot.class)) {

            slots = em.createNamedQuery(ServiceSlot.fetchNearestSlots)
                    .setParameter("service", slot.getService())
                    .setParameter("start", slot.getStartPosition())
                    .setMaxResults(1)
                    .getResultList();

        } else {
            throw new InvalidSlotTypeException("Type not found:" + slot.getClass().getName());
        }

        nearestSlot = slots.isEmpty() ? null : slots.get(0);
        return nearestSlot;

    }

    public Slot createNextSlot(@NotNull Slot baseSlot) {
        Slot slot = null;

        if (baseSlot.getClass().isAssignableFrom(ResourceSlot.class)) {

            slot = new ResourceSlot();
            ((ResourceSlot) slot).setProject(((ResourceSlot) baseSlot).getProject());
            return slot;

        } else if (baseSlot.getClass().isAssignableFrom(ServiceSlot.class)) {
            
            slot = new ServiceSlot();
            
        } else {
            throw new InvalidSlotTypeException("Type not found:" + baseSlot.getClass().getName());
        }

        return slot;
    }

}
