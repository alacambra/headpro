/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;

import javax.inject.Inject;
import java.util.Optional;

/**
 *
 * @author alacambra
 */
public class SimpleAddAction extends AbstractSlotsAction {

    @Inject
    NeighborsSlots neighborsSlots;

    @Override
    public AbstractSlotsAction perform() {

        final Slot newSlot = context.getNewSlot();

        Optional.ofNullable(neighborsSlots.getPreviousNeighbor()).ifPresent(slot -> {
            newSlot.setPreviousSlot(slot);
            slot.setNextSlot(newSlot);
        });

        Optional.ofNullable(neighborsSlots.getNextNeighbor()).ifPresent(slot -> {
            newSlot.setNextSlot(slot);
            slot.setPreviousSlot(newSlot);
        });

        return this;
    }

    @Override
    public boolean isValid() {

        initialize();

        boolean canMergeWithPreviousSlot
                = Optional.ofNullable(neighborsSlots.getPreviousNeighbor()).map(this::slotCanMerge).orElse(false);

        boolean canMergeWithNextSlot
                = Optional.ofNullable(neighborsSlots.getNextNeighbor()).map(this::slotCanMerge).orElse(false);

        if (canMergeWithNextSlot || canMergeWithPreviousSlot) {
            setInvalidActionReason(InvalidActionReasons.shouldMerge);
            return false;
        }

        if (!context.getOverlappedSlots().isEmpty()) {
            setInvalidActionReason(InvalidActionReasons.overlappedSlots);
            return false;
        }

        return true;
    }

    private boolean slotCanMerge(Slot s) {

        return SlotsHelper.slotsAreMergable(context.getNewSlot(), s);

    }

    @Override
    protected void actionInitialization() {
        neighborsSlots
                .setAbsorbedSlots(context.getAbsorbbedSlots())
                .setSourceSlot(context.getNewSlot());

        neighborsSlots.findNeighbors();
    }

}
