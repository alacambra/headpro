package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;
import java.util.Optional;
import javax.inject.Inject;

/**
 *
 * @author alacambra
 */
public class UnionAction extends AbstractSlotsAction {

    Slot leftSlot;
    Slot rightSlot;

    @Inject
    NeighborsSlots neighborsSlots;

    @Override
    public AbstractSlotsAction perform() {

        int start = leftSlot.getStartPosition();
        int end = rightSlot.getEndPosition();

        int duration = end - start + 1;

        leftSlot.setStartPosition(start);
        leftSlot.setDuration(duration);
        leftSlot.setNextSlot(null);

        Optional.ofNullable(rightSlot.getNextSlot()).ifPresent(s -> {
            leftSlot.setNextSlot(s);
            s.setPreviousSlot(leftSlot);
        });

        addUselessSlot(rightSlot);
        setPersistNewSlot(false);
        setSlotToSave(leftSlot);

        return this;
    }

    @Override
    protected void actionInitialization() {

        neighborsSlots.setAbsorbedSlots(context.getAbsorbbedSlots());
        neighborsSlots.setSourceSlot(context.getNewSlot());
        neighborsSlots.setOverlappedSlots(context.overlappedSlots);
        neighborsSlots.findNeighbors();

    }

    @Override
    public boolean isValid() {

        initialize();

        if (context.getOverlappedSlots().size() == 2) {

            setLeftAndRightSlots(context.getOverlappedSlots().get(0), context.getOverlappedSlots().get(1));
            if (!slotsHaveSameValuePerStep()) {

                setInvalidActionReason(InvalidActionReasons.differentValuePerStep);
                return false;
            }
        } else if (context.getOverlappedSlots().isEmpty()) {

            if (!neighborsSlots.bothNeighborsExist()) {
                setInvalidActionReason(InvalidActionReasons.noNeighborsNorOverlappedSlots);
                return false;
            }

            setLeftAndRightSlots(neighborsSlots.getNextNeighbor(), neighborsSlots.getPreviousNeighbor());

            if (!slotsAreContiguos()) {
                setInvalidActionReason(InvalidActionReasons.falseSlotPosition);
                return false;
            }

            if (!slotsHaveSameValuePerStep()) {

                setInvalidActionReason(InvalidActionReasons.differentValuePerStep);
                return false;
            }

        } else if (context.getOverlappedSlots().size() == 1) {

            Slot overlappedSlot = context.getOverlappedSlots().get(0);
            Slot neighborSlot = overlappedSlot.equals(neighborsSlots.getNextNeighbor())
                    ? neighborsSlots.previousNeighbor : neighborsSlots.getNextNeighbor();

            if (neighborSlot == null) {
                setInvalidActionReason(InvalidActionReasons.oneContigousSlotNeeded);
                return false;
            }

            setLeftAndRightSlots(overlappedSlot, neighborSlot);

            if (!slotsHaveSameValuePerStep()) {

                setInvalidActionReason(InvalidActionReasons.differentValuePerStep);
                return false;
            }

            if (!SlotsHelper.slotsAreContiguos(neighborSlot, context.getNewSlot())) {

                setInvalidActionReason(InvalidActionReasons.falseSlotPosition);
                return false;
            }
        }

        return true;

    }

    private void setLeftAndRightSlots(Slot slot1, Slot slot2) {

        if (slot1.getStartPosition() < slot2.getStartPosition()) {

            leftSlot = slot1;
            rightSlot = slot2;

        } else {

            leftSlot = slot2;
            rightSlot = slot1;

        }
    }

    private boolean slotsAreContiguos() {
        return SlotsHelper.slotsAreContiguos(context.getNewSlot(), leftSlot)
                && SlotsHelper.slotsAreContiguos(context.getNewSlot(), rightSlot);
    }

    private boolean slotsHaveSameValuePerStep() {
        return SlotsHelper.slotsHaveSameValuePerStep(context.getNewSlot(), leftSlot)
                && SlotsHelper.slotsHaveSameValuePerStep(context.getNewSlot(), rightSlot);
    }
}
