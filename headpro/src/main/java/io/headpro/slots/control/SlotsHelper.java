/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;

/**
 *
 * @author alacambra
 */
public class SlotsHelper {

    public static int getOverlappedArea(Slot s1, Slot s2) {
        
        if (s1 == null || s2 == null) {
            return 0;
        }

        int area;

        int start = s1.getStartPosition() > s2.getStartPosition()
                ? s1.getStartPosition() : s2.getStartPosition();

        int end = s1.getEndPosition() < s2.getEndPosition()
                ? s1.getEndPosition() : s2.getEndPosition();

        area = end - start + 1;
        return area > 0 ? area : 0;
    }
    
    public static boolean slotsOverlaps(Slot s1, Slot s2) {
       return getOverlappedArea(s1, s2) > 0;
    }

    public static boolean slotsAreContiguos(Slot s1, Slot s2) {

        if (s1 == null || s2 == null) {
            return false;
        }

        Slot secondSlot = s1.getStartPosition() > s2.getStartPosition() ? s1 : s2;
        Slot firstSlot = s1.getEndPosition() < s2.getEndPosition() ? s1 : s2;

        if (secondSlot == firstSlot) {
            return false;
        }

        return firstSlot.getEndPosition().equals(secondSlot.getStartPosition() - 1);

    }

    public static boolean slotsHaveSameValuePerStep(Slot s1, Slot s2) {

        if (s1 == null || s2 == null) {
            return false;
        }
        
        return s1.getValuePerStep().compareTo(s2.getValuePerStep()) == 0;
        
    }

    public static boolean slotsAreMergable(Slot s1, Slot s2) {

        return slotsHaveSameValuePerStep(s1, s2) && (slotsAreContiguos(s1, s2) || slotsOverlaps(s1, s2));
        
    }

}
