/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;
import java.util.Optional;
import javax.inject.Inject;

/**
 *
 * @author alacambra
 */
public class LeftReductionAction extends AbstractSlotsAction {

    @Inject
    NeighborsSlots neighborsSlots;

    Slot reducerSlot;
    Slot overlappedSlot;
    Optional<Slot> previousSlot = Optional.empty();

    @Override
    public AbstractSlotsAction perform() {

        reducerSlot.setNextSlot(overlappedSlot);
        previousSlot.ifPresent(reducerSlot::setPreviousSlot);

        int overlappedArea = SlotsHelper.getOverlappedArea(overlappedSlot, reducerSlot);

        overlappedSlot.setDuration(overlappedSlot.getDuration() - overlappedArea);
        overlappedSlot.setStartPosition(overlappedSlot.getStartPosition() + overlappedArea);
        overlappedSlot.setPreviousSlot(reducerSlot);

        previousSlot.ifPresent(s -> s.setNextSlot(reducerSlot));

        return this;
    }

    @Override
    protected void actionInitialization() {
        
        reducerSlot = context.getNewSlot();
        neighborsSlots.setAbsorbedSlots(context.getAbsorbbedSlots());
        neighborsSlots.setSourceSlot(context.getNewSlot());
        neighborsSlots.setOverlappedSlots(context.getOverlappedSlots());
        neighborsSlots.findNeighbors();

    }

    private boolean validate() {

        if (SlotsHelper.getOverlappedArea(overlappedSlot, reducerSlot) == 0) {
            setInvalidActionReason(InvalidActionReasons.noOverlappedArea);
            return false;
        }

        if (overlappedSlot.getStartPosition() < reducerSlot.getStartPosition()) {
            setInvalidActionReason(InvalidActionReasons.falseReductionSide);
            return false;
        }

        if (SlotsHelper.slotsHaveSameValuePerStep(reducerSlot, overlappedSlot)) {
            setInvalidActionReason(InvalidActionReasons.sameValuePerStep);
            return false;
        }

        return true;
    }

    @Override
    public boolean isValid() {

        if (context.getOverlappedSlots().isEmpty()) {
            setInvalidActionReason(InvalidActionReasons.noOverlappedSlot);
            return false;
        }

        initialize();

        for (Slot s : context.getOverlappedSlots()) {

            overlappedSlot = s;
            previousSlot = Optional.ofNullable(neighborsSlots.getPreviousNeighbor());

            if (validate()) {
                return true;
            }
        }

        return false;
    }
}
