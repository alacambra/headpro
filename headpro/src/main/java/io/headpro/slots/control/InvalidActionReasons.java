/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

/**
 *
 * @author alacambra
 */
public enum InvalidActionReasons {
    
    noOverlappedSlot, noOverlappedArea, sameValuePerStep, falseReductionSide, 
    onlyOneOverlappedSlotsAllowed, differentValuePerStep, twoOverlappedSlotsNeeded,
    newSlotNotCompletelyContained, overlappedArea, overlappedSlots, shouldMerge, shouldJoinNeighbors, falseSlotPosition, noNeighborsNorOverlappedSlots, noContiguosNorOverlappedSlots, oneContigousSlotNeeded, noReasonGiven
    
}
