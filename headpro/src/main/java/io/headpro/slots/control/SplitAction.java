/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.slots.control;

import io.headpro.slots.entity.Slot;
import java.util.Optional;
import javax.inject.Inject;

/**
 *
 * @author alacambra
 */
public class SplitAction extends AbstractSlotsAction {

    Slot splitter;
    Slot overlappedSlot;
    Slot rest;
    Optional<Slot> next;

    @Inject
    InteractedSlotsProvider interactedSlotsProvider;

    @Override
    public AbstractSlotsAction perform() {

        rest = interactedSlotsProvider.createNextSlot(overlappedSlot);
        rest.setValuePerStep(overlappedSlot.getValuePerStep());
        rest.setStartPosition(splitter.getEndPosition() + 1);
        rest.setDuration(overlappedSlot.getEndPosition() - splitter.getEndPosition());
        rest.setService(overlappedSlot.getService());

        splitter.setNextSlot(rest);
        splitter.setPreviousSlot(overlappedSlot);

        overlappedSlot.setDuration(splitter.getStartPosition() - overlappedSlot.getStartPosition());
        overlappedSlot.setNextSlot(splitter);

        next.ifPresent(rest::setNextSlot);
        rest.setPreviousSlot(splitter);

        next.ifPresent(s -> s.setPreviousSlot(rest));

        return this;

    }

    public Slot getRest() {
        return rest;
    }
    
    @Override
    protected void actionInitialization() {

        if (!context.getOverlappedSlots().isEmpty()) {
            overlappedSlot = context.getOverlappedSlots().get(0);
            next = Optional.ofNullable(overlappedSlot.getNextSlot());
        }

        splitter = context.getNewSlot();
    }

    @Override
    public boolean isValid() {

        initialize();

        if (context.getOverlappedSlots().isEmpty()) {
            setInvalidActionReason(InvalidActionReasons.noOverlappedSlot);
            return false;
        }

        if (context.getOverlappedSlots().size() != 1) {
            setInvalidActionReason(InvalidActionReasons.onlyOneOverlappedSlotsAllowed);
            return false;
        }

        if (overlappedSlot.getStartPosition() >= splitter.getStartPosition()
                || splitter.getEndPosition() >= overlappedSlot.getEndPosition()) {

            setInvalidActionReason(InvalidActionReasons.newSlotNotCompletelyContained);

            return false;
        }
        
        if(SlotsHelper.slotsHaveSameValuePerStep(overlappedSlot, splitter)){
            
            setInvalidActionReason(InvalidActionReasons.differentValuePerStep);
            return false;
        }

        return true;
    }

}
