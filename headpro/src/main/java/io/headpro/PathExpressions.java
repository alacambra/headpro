/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro;

/**
 *
 * @author alacambra
 */
public class PathExpressions {

    public static final String accountId = "accountId";
    public static final String workspaceId = "workspaceId";
    public static final String projectId = "projectId";
    public static final String serviceId = "serviceId";
    public static final String userId = "userId";

    private static final String integerId = ":\\d+";
    
    public static final String accountIdExpr = "{" + accountId + integerId + "}";
    public static final String workspaceIdExpr = "{" + workspaceId + integerId + "}";
    public static final String projectIdExpr = "{" + projectId + integerId + "}";
    public static final String serviceIdExpr = "{" + serviceId + integerId + "}";
    public static final String userIdExpr = "{" + userId + integerId + "}";

}
