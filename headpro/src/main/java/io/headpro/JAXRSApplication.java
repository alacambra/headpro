package io.headpro;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by alacambra on 03/11/15.
 */
@ApplicationPath("resources")
public class JAXRSApplication extends Application{
}
