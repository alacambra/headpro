package io.headpro.entity;

import io.headpro.accounting.entity.OrganizationRole;
import io.headpro.accounting.entity.UserAccount;
import io.headpro.workspace.entity.WorkspaceRole;
import javax.persistence.*;
import java.util.List;

@Entity
@NamedQueries(
        @NamedQuery(name = User.selectAll, query = "select u from User as u")
)
@Table(name = "USER")
@EntityListeners(UpdateTrackerListener.class)
public class User implements TrackedEntity {
    
    private static final String prefix = "User.";
    public static final String selectAll = prefix + "selectAll";
            

    @Id
    @Column(length = 36, nullable = false, updatable = false)
    private String id;

    @Embedded
    private UpdateTracker updateTracker;

    private String firstName;
    private String lastName;
    private String email;

    @OneToMany(mappedBy = "user")
    private List<OrganizationRole> organizationRoles;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<WorkspaceRole> workspaceRoles;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<UserAccount> accounts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<OrganizationRole> getOrganizationRoles() {
        return organizationRoles;
    }

    public void setOrganizationRoles(List<OrganizationRole> organizationRoles) {
        this.organizationRoles = organizationRoles;
    }

    public List<WorkspaceRole> getWorkspaceRoles() {
        return workspaceRoles;
    }

    public void setWorkspaceRoles(List<WorkspaceRole> workspaceRoles) {
        this.workspaceRoles = workspaceRoles;
    }

    public List<UserAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<UserAccount> accounts) {
        this.accounts = accounts;
    }

    @Override
    public UpdateTracker getUpdateTracker() {
        return updateTracker;
    }

    @Override
    public void setUpdateTracker(UpdateTracker updateTracker) {
        this.updateTracker = updateTracker;
    }
}
