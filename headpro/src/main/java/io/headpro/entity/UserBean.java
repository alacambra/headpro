package io.headpro.entity;

import io.headpro.accounting.entity.Account;
import io.headpro.boundary.ResourceNotFoundException;
import io.headpro.security.boundary.Role;
import io.headpro.workspace.entity.Workspace;
import io.headpro.workspace.entity.WorkspaceRole;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class UserBean {

    @PersistenceContext
    EntityManager em;

    User entity;

    public User getEntity() {
        return entity;
    }

    public UserBean setEntity(User entity) {
        this.entity = entity;
        return this;
    }

    public UserBean loadEntity(int id) {

        entity = Optional.ofNullable(em.find(User.class, id)).orElse(new User());
        return this;

    }

    public UserBean loadEntity() {

        entity = em.find(User.class, entity.getId());
        return this;

    }

    public UserBean loadEntity(String id){

        entity = Optional.ofNullable(em.find(User.class, id))
                .orElseThrow(() -> new ResourceNotFoundException(getClass().getName(), id));

        return this;
    }

    public Role getRoleForWorkspace(Workspace workspace) {
        return getRoleForWorkspace(workspace.getId());
    }

    public Role getRoleForWorkspace(int workspaceId) {
        List<WorkspaceRole> roles = em.createNamedQuery(WorkspaceRole.fetchRoleByUserAndWorkspace)
                .setParameter("userId", entity.getId())
                .setParameter("workspaceId", workspaceId).getResultList();

        if(roles.isEmpty()){
            return null;
        }
        
        return roles.get(0).getUserRole();
    }

    public Role getRoleForAccount(Account account) {
        if(account.getOwner().getClass().isAssignableFrom(User.class) && 
                ((User)account.getOwner()).getId().equals(entity.getId())){
            return Role.OWNER;
        }
                
        return Role.NONE;
    }
    
    public Role getRoleForAccount(int accountId) {
        Account account = em.find(Account.class, accountId);
        
        if(account == null){
            return Role.NONE;
        }
        
        return getRoleForAccount(account);
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
    

}
