/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.entity;

/**
 *
 * @author alacambra
 */
public interface TrackedEntity {

    UpdateTracker getUpdateTracker();

    void setUpdateTracker(UpdateTracker updateTracker);
}
