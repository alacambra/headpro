/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.entity;

import java.util.Date;
import java.util.Optional;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 *
 * @author alacambra
 */
public class UpdateTrackerListener {
    
    @PrePersist
    @PreUpdate
    public void setTimes(TrackedEntity entity){
        
        UpdateTracker updateTracker = Optional.ofNullable(entity.getUpdateTracker()).orElse(new UpdateTracker());
        Date currentDate = new Date();
        
        if(updateTracker.getDateCreated() == null){
            updateTracker.setDateCreated(currentDate);
        }
        
        updateTracker.setLastModified(currentDate);
        entity.setUpdateTracker(updateTracker);
    }
    
}
