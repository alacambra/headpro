/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tentitylate file, choose Tools | Tentitylates
 * and open the tentitylate in the editor.
 */
package io.headpro.entity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class GenericRepository<T> {

    EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public T save(T entity) {
        entityManager.persist(entity);
        entityManager.flush();
        return entity;
    }

    public void delete(T entity) {
        entityManager.remove(entity);
    }

    protected T merge(T entity) {
        return entityManager.merge(entity);
    }

    public T find(Object id) {
        return (T) entityManager.find(getType(), id);
    }

    protected abstract Class<T> getType();
}
