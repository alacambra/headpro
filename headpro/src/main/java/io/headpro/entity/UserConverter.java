package io.headpro.entity;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author alacambra
 */
public class UserConverter {
    
    public JsonObject toJson(User user){
        return basicUserConversion(user).build();
    }
    
    JsonObjectBuilder basicUserConversion(User user) {
        return Json.createObjectBuilder()
                .add("id", user.getId())
                .add("email", user.getEmail())
                .add("firstName", user.getFirstName())
                .add("lastName", user.getLastName());
    }
    
}
