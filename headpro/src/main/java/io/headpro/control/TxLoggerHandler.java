/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.control;

import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;

/**
 *
 * @author alacambra
 */
public class TxLoggerHandler {
    public void logSuccessEvent(@Observes(during = TransactionPhase.AFTER_SUCCESS) TxLogEvent event){
        event.logSuccess();
    }
    
    public void logFailureEvent(@Observes(during = TransactionPhase.AFTER_FAILURE) TxLogEvent event){
        event.logFailure();
    }
}
