/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.control;

import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alacambra
 */
public class TxLogEvent {

    private Optional<String> onSuccessLog;
    private Object[] successParams;
    private Optional<String> onFailLog;
    private Object[] failParams;

    Logger logger;

    public TxLogEvent(Logger logger) {
        Optional.of(logger);
        this.logger = logger;
    }

    public Optional<String> getOnSuccessLog() {
        return onSuccessLog;
    }

    public TxLogEvent setOnSuccessLog(String onSuccessLog, Supplier<Object[]>  params) {
        this.onSuccessLog = Optional.of(onSuccessLog);
        successParams = params.get();
        return this;
    }

    public Optional<String> getOnFailLog() {
        return onFailLog;
    }

    public TxLogEvent setOnFailLog(String onFailLog, Object[] params) {
        this.onFailLog = Optional.of(onFailLog);
        failParams = params;
        return this;
    }

    public void logSuccess() {
        onSuccessLog.ifPresent(log -> logger.log(Level.INFO, log, successParams));
    }

    public void logFailure() {
        onFailLog.ifPresent(log -> logger.log(Level.SEVERE, log, failParams));
    }
}
