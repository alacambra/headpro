/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.control;

import io.headpro.boundary.CurrentResourceBean;
import java.security.Principal;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 *
 * @author alacambra
 */
@RequestScoped
@Alternative
public class PrincipalProducer {

    @Inject
    private CurrentResourceBean currentResource;

    @Produces
    public Principal getPrincipal() {
        return () -> currentResource.getKeycloakSecurityContext().getToken().getSubject();
    }
}
