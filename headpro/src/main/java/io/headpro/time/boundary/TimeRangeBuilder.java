package io.headpro.time.boundary;

import io.headpro.utils.DatesService;
import io.headpro.utils.LocalDateConverter;
import io.headpro.workspace.entity.Step;
import java.time.LocalDate;
import java.util.Date;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

public class TimeRangeBuilder {

    private LocalDate startDate;
    private LocalDate endDate;
    private int startIndex;
    private int endIndex;
    private int rangeLength;
    private Step step;
    DatesService datesService;

    @Inject
    public TimeRangeBuilder(DatesService datesService) {
        this.datesService = datesService;
    }

    public TimeRangeBuilder fromDates(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        setFromDates();
        return this;
    }

    public TimeRangeBuilder fromDates(@NotNull Date startDate, @NotNull Date endDate) {
        fromDates(LocalDateConverter.toLocalDate(startDate), LocalDateConverter.toLocalDate(endDate));
        return this;
    }
    
    public TimeRangeBuilder fromIndexes(int startIndex, int endIndex) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        setFromIndexes();
        return this;
    }
    
    /**
     * Used mainly for resourceSLots, allow to calculate absoluteIndex using the project date as offset
     * @param offsetDate
     * @param startIndex
     * @param endIndex
     * @return 
     */
    public TimeRangeBuilder fromIndexes(Date offsetDate, int startIndex, int endIndex) {
        
        int offsetIndex = datesService.getDateIndex(offsetDate);
        
        this.startIndex = startIndex + offsetIndex;
        this.endIndex = endIndex + offsetIndex;
        setFromIndexes();
        return this;
    }
    
    public TimeRangeBuilder fromIndexes(Integer offsetIndex, int startIndex, int endIndex) {
        
        this.startIndex = startIndex + offsetIndex;
        this.endIndex = endIndex + offsetIndex;
        setFromIndexes();
        return this;
    }

    private void setFromIndexes() {
        startDate = datesService.getDateForIndex(startIndex);
        endDate = datesService.getDateForIndex(endIndex);
        setRangeLength();
    }

    private void setFromDates() {
        startIndex = datesService.getDateIndex(startDate);
        endIndex = datesService.getDateIndex(endDate);
        setRangeLength();
    }
    
    public TimeRange createEmptyTimeRange(int startIndex){
        this.startIndex = startIndex;
        endIndex = startIndex;
        startDate = datesService.getDateForIndex(startIndex);
        endDate = startDate;
        rangeLength = 0;
        return build().forceEmptyRange();
    }
    
    public void createEmptyTimeRange(Date date){
        createEmptyTimeRange(datesService.getDateIndex(date));
    }
    
    public void createEmptyTimeRange(LocalDate date){
        createEmptyTimeRange(datesService.getDateIndex(date));
    }

    private void setRangeLength() {
        rangeLength = endIndex - startIndex + 1;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public int getRangeLength() {
        return rangeLength;
    }

    public Step getStep() {
        return step;
    }

    public TimeRangeBuilder withStep(Step step) {
        this.step = step;
        return this;
    }

    public TimeRange build() {
        return new TimeRange(startDate, endDate, startIndex, endIndex, step, datesService);
    }
}
