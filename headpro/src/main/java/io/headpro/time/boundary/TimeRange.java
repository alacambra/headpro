/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.time.boundary;

import io.headpro.utils.DatesService;
import io.headpro.workspace.entity.Step;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.Objects;
import javax.json.Json;
import javax.json.JsonObject;

/**
 * @author alacambra
 */
public class TimeRange {

    private LocalDate startDate;
    private LocalDate endDate;
    private int startIndex;
    private int endIndex;
    private int rangeLength;
    private Step step = Step.WEEKLY;
    DatesService datesService;

    TimeRange(LocalDate startDate, LocalDate endDate, int startIndex, int endIndex, Step step, DatesService datesService) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.step = step;
        this.datesService = datesService;
        calculateLength();
    }

    public TimeRange getDifferentialTimeRange(TimeRange range) {

        if (!overlapWith(range)) {
            throw new RuntimeException("no overlapping found, so no diff");
        }

        TimeRangeBuilder builder = new TimeRangeBuilder(datesService);

        TimeRange biggerEndTr = range.getEndIndex() > getEndIndex() ? range : this;
        TimeRange smallerEndTr = range.getEndIndex() <= getEndIndex() ? range : this;

        return builder.fromIndexes(smallerEndTr.getEndIndex() + 1, biggerEndTr.getEndIndex()).build();
    }

    public TimeRange moveEnd(int steps) {
        endIndex += steps;

        if (startIndex > endIndex) {
            throw new RuntimeException("end index can not be smaller than startIndex");
        }

        endDate = datesService.getDateForIndex(endIndex);
        calculateLength();
        return this;
    }

    public TimeRange moveStart(int steps) {
        startIndex += steps;

        if (startIndex > endIndex) {
            throw new RuntimeException("end index can not be smaller than startIndex");
        }

        startDate = datesService.getDateForIndex(startIndex);
        calculateLength();
        return this;
    }

    public JsonObject toJson() {

        return Json.createObjectBuilder().add("startIndex", startIndex)
                .add("endIndex", endIndex)
                .add("startDate", getStartDateMilis())
                .add("endDate", getEndDateMilis())
                .add("rangeLength", rangeLength).build();
    }

    public boolean beginsBefore(TimeRange tr) {
        return startIndex < tr.getStartIndex();
    }

    public boolean endsBefore(TimeRange tr) {
        return endIndex < tr.endIndex;
    }

    public boolean overlapWith(TimeRange range) {
        TimeRange biggerEndTr = range.getEndIndex() > getEndIndex() ? range : this;
        TimeRange smallerEndTr = range.getEndIndex() <= getEndIndex() ? range : this;

        return biggerEndTr.getStartIndex() <= smallerEndTr.getEndIndex();
    }

    public boolean hasSameLength(TimeRange timeRange) {
        if (timeRange.getStep() == getStep()) {
            return timeRange.getRangeLength() == getRangeLength();
        }

        throw new RuntimeException("Must have same step type");
    }

    private void calculateLength() {
        rangeLength = endIndex - startIndex + 1;
    }

    public boolean isEmpty() {
        return rangeLength == 0;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Long getStartDateMilis() {
        return startDate.getLong(ChronoField.EPOCH_DAY) * 24 * 3600000;
    }

    public Long getEndDateMilis() {
        return endDate.getLong(ChronoField.EPOCH_DAY) * 24 * 3600000;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public int getRangeLength() {
        return rangeLength;
    }

    public Step getStep() {
        return step;
    }

    public TimeRange copy() {
        return new TimeRange(startDate, endDate, startIndex, endIndex, step, datesService);
    }

    TimeRange forceEmptyRange() {
        if (startIndex != endIndex) {
            throw new RuntimeException("Indexes must be equals");
        }

        rangeLength = 0;
        return this;
    }

    @Override
    public String toString() {
        return Json.createObjectBuilder().add("startIndex", startIndex)
                .add("endIndex", endIndex)
                .add("startDate", new Date(getStartDateMilis()).toString())
                .add("endDate", new Date(getEndDateMilis()).toString())
                .add("rangeLength", rangeLength).build().toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TimeRange other = (TimeRange) obj;
        if (this.startIndex != other.startIndex) {
            return false;
        }
        if (this.endIndex != other.endIndex) {
            return false;
        }
        if (this.rangeLength != other.rangeLength) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        return this.step == other.step;
    }


}
