package io.headpro.time.boundary;

import io.headpro.utils.DateIndexOutOfRangeException;
import io.headpro.utils.DatesService;
import io.headpro.utils.LocalDateConverter;
import java.time.temporal.ChronoField;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import javax.ejb.Stateless;
import javax.json.JsonObjectBuilder;

@Path("time")
@Stateless
public class TimeResource {

    @Inject
    DatesService datesService;

    @GET
    @Path("timestamps")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getRangeTimestamps(
            @QueryParam("start") @DefaultValue("0") int startIndex,
            @QueryParam("end") @DefaultValue("0") int endIndex) {

        Long start = LocalDateConverter.toDate(datesService.getDateForIndex(startIndex)).getTime();
        Long end = LocalDateConverter.toDate(datesService.getDateForIndex(endIndex)).getTime();

        return Json.createObjectBuilder().add("start", start).add("end", end).build();
    }

    @GET
    @Path(("{timestamp : \\d+}"))
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getIndexForDate(@PathParam("timestamp") Long timestamp) {

        try {

            int index = datesService.getDateIndex(new Date(timestamp));
            Long date = LocalDateConverter.toDate(datesService.getDateForIndex(index)).getTime();
            return Json.createObjectBuilder().add("index", index).add("date", date).build();

        } catch (DateIndexOutOfRangeException e) {
            throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
        }
    }

    @GET
    @Path("range")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRange(@QueryParam("start") long start, @QueryParam("end") long end, @QueryParam("type") String givenType) {

        if (!("date".equals(givenType) || "index".equals(givenType))) {
            return Response.status(Response.Status.BAD_REQUEST).entity("invalid type given").build();
        }

        JsonObjectBuilder builder = Json.createObjectBuilder();

        if ("date".equals(givenType)) {
            int startIndex = datesService.getDateIndex(new Date(start));
            int endIndex = datesService.getDateIndex(new Date(end));
            builder.add("startIndex", startIndex)
                    .add("endIndex", endIndex)
                    .add("startDate", start)
                    .add("endDate", end)
                    .add("rangeLength", endIndex - startIndex + 1);
        } else {
            Long startDate = datesService.getDateForIndex((int) start).getLong(ChronoField.EPOCH_DAY) * 24 * 3600000;
            Long endDate =  datesService.getDateForIndex((int) end).getLong(ChronoField.EPOCH_DAY) * 24 * 3600000;

            builder.add("startIndex", start)
                    .add("endIndex", end)
                    .add("startDate", startDate)
                    .add("endDate", endDate)
                    .add("rangeLength", end - start + 1);
        }

        return Response.ok(builder.build()).build();
    }

}
