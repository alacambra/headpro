/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.accounting.entity;

import io.headpro.entity.TrackedEntity;
import io.headpro.entity.UpdateTracker;
import io.headpro.entity.UpdateTrackerListener;
import io.headpro.workspace.entity.Workspace;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@EntityListeners(UpdateTrackerListener.class)
public abstract class Account implements TrackedEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private int id;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Workspace> workspaces;
    
    @Embedded
    private UpdateTracker updateTracker;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public List<Workspace> getWorkspaces() {
        return workspaces;
    }

    public void setWorkspaces(List<Workspace> workspaces) {
        this.workspaces = workspaces;
    }

    public Account addWorkspace(Workspace workspace) {
        Optional.ofNullable(workspaces).orElse(new ArrayList<>()).add(workspace);
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public UpdateTracker getUpdateTracker() {
        return updateTracker;
    }

    @Override
    public void setUpdateTracker(UpdateTracker updateTracker){
        this.updateTracker = updateTracker;
    }
    
    public abstract Object getOwner();
    
}
