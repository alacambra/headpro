/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.accounting.entity;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author alacambra
 */
@Entity
@DiscriminatorValue(value = "O")
public class OrganizationAccount extends Account {

    @OneToOne(cascade = CascadeType.REMOVE)
    private Organization organization;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public Object getOwner() {
        return getOrganization();
    }
    

}
