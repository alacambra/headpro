package io.headpro.accounting.boundary;

import io.headpro.PathExpressions;
import io.headpro.accounting.entity.Account;
import io.headpro.boundary.CurrentResourceBean;
import io.headpro.security.boundary.AllowedTo;
import io.headpro.security.boundary.Role;
import io.headpro.security.boundary.Secured;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("account/" + PathExpressions.accountIdExpr)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
@Secured(Secured.RoleOver.ACCOUNT)
public class AccountResource {

    @PersistenceContext
    EntityManager em;
    
    @Inject
    private AccountConverter converter;
    
    @Inject
    protected CurrentResourceBean resourceContext;
    
    @PUT
    @AllowedTo({Role.OWNER})
    public Response updateAccount() {
        throw new UnsupportedOperationException();
    }
    
    @GET
    @AllowedTo({Role.OWNER})
    public JsonObject getAccount() {
        Account account = getAccountOrException();
        return converter.toJson(account);
    }

    @DELETE
    @AllowedTo({Role.OWNER})
    public void deleteAccount() {
        Account account = getAccountOrException();
        em.remove(account);
    }

    private Account getAccountOrException() {
        return resourceContext.getResourceOrException(Account.class);
    }
}
