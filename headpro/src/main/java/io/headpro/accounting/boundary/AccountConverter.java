package io.headpro.accounting.boundary;

import io.headpro.accounting.entity.Account;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.validation.constraints.NotNull;

public class AccountConverter {

    public Account fromJson(JsonObject json, Account account) {

        if (json.containsKey("id")) {
            if (account.getId() == null) {
                account.setId(json.getInt("id"));
            } else if (account.getId() != json.getInt("id")) {
                throw new RuntimeException("ids mismatch");
            }
        }

        return account;
    }

    public JsonObject toJson(Account account) {
        return basicAccountConversion(account).build();
    }

    public JsonObject toJson(Account account, @NotNull String view) {

        return basicAccountConversion(account).build();
    }
    
     public JsonArray toJson(List<? extends Account> accounts) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        accounts.stream().map(this::toJson).forEach(arr::add);

        return arr.build();
    }

    JsonObjectBuilder basicAccountConversion(Account account) {
        return Json.createObjectBuilder()
                .add("id", account.getId());
    }
    
    

}
