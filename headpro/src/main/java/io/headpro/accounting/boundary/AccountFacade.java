package io.headpro.accounting.boundary;

import io.headpro.accounting.entity.UserAccount;
import io.headpro.control.TxLogEvent;
import io.headpro.entity.User;
import io.headpro.security.boundary.Role;
import io.headpro.workspace.entity.Workspace;
import io.headpro.workspace.entity.WorkspaceRole;
import java.util.Arrays;
import java.util.Optional;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class AccountFacade {

    @PersistenceContext
    EntityManager em;

    @Inject
    Logger logger;

    @Inject
    Event<TxLogEvent> logEvent;

    public void createAccount(User ownerEntity) {

        Optional.ofNullable(ownerEntity.getId())
                .orElseThrow(() -> new RuntimeException("user entity without id can not be registered"));

//        if(owner.loadEntity().entityIsLoadedAndPersisted()){
//            throw new RuntimeException("user entity " + ownerEntity.getId() + " already exists");
//        }
        ownerEntity = em.merge(ownerEntity);

        UserAccount account = new UserAccount();
        account = em.merge(account);
        account.setUser(ownerEntity);
        ownerEntity.setAccounts(Arrays.asList(account));

        Workspace workspace = new Workspace();
        workspace = em.merge(workspace);
        workspace.setTitle("My first workspace");
        workspace.setAccount(account);
        account.setWorkspaces(Arrays.asList(workspace));

        WorkspaceRole workspaceRole = new WorkspaceRole();
        workspaceRole.setUserRole(Role.OWNER);
        workspaceRole.setUser(ownerEntity);
        workspaceRole.setWorkspace(workspace);

        workspace.setRoles(Arrays.asList(workspaceRole));

        final User finalEntity = ownerEntity;

        logEvent.fire(new TxLogEvent(logger)
                .setOnSuccessLog(
                        "new user added.  userId:{0}, accountId:{1}, default worksapceId:{2}",
                        () -> new Object[]{
                            finalEntity.getId(),
                            finalEntity.getAccounts().get(0).getId(),
                            finalEntity.getAccounts().get(0).getWorkspaces().get(0).getId()
                        })
                .setOnFailLog(
                        "failed to create user. userId:{0}",
                        new Object[]{ownerEntity.getId()})
        );

    }
}
