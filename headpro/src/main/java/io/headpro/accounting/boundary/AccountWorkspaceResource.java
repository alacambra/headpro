package io.headpro.accounting.boundary;

import io.headpro.PathExpressions;
import io.headpro.accounting.entity.Account;
import io.headpro.boundary.CurrentResourceBean;
import io.headpro.entity.User;
import io.headpro.security.boundary.AllowedTo;
import io.headpro.security.boundary.Role;
import io.headpro.security.boundary.Secured;
import io.headpro.security.entity.CurrentUser;
import io.headpro.workspace.boundary.WorkspaceConverter;
import io.headpro.workspace.boundary.WorkspaceResource;
import io.headpro.workspace.entity.Workspace;
import io.headpro.workspace.entity.WorkspaceRole;
import java.lang.reflect.Member;
import java.net.URI;
import java.util.logging.Logger;
import javax.ejb.AccessLocalException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Actions over workspaces that can be only done for the account owner.
 */
@Stateless
@Path("account/" + PathExpressions.accountIdExpr + "/workspace")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured(Secured.RoleOver.ACCOUNT)
public class AccountWorkspaceResource {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private WorkspaceConverter workspaceConverter;

    @Inject
    Logger logger;

    @Inject
    CurrentResourceBean currentResource;

    @Inject
    CurrentUser currentUser;

    @POST
    @AllowedTo({Role.OWNER})
    public Response createWorkspace(@Context UriInfo uriInfo, JsonObject json) {
        Workspace workspace = workspaceConverter.fromJson(json);
        workspace.setAccount(getCurrentAccount());
        workspace = em.merge(workspace);

        WorkspaceRole role = new WorkspaceRole();
        role.setUser(currentUser.getEntity());
        role.setUserRole(Role.OWNER);
        role.setWorkspace(workspace);

        em.persist(role);

        URI uri = uriInfo.getBaseUriBuilder()
                .path(WorkspaceResource.class)
                .resolveTemplate(PathExpressions.workspaceId, workspace.getId())
                .build();

        return Response.created(uri).build();
    }

    @GET
    @AllowedTo({Role.OWNER})
    public JsonArray getAccountWorkspaces() {
        return Json.createArrayBuilder().build();
    }

    private Account getCurrentAccount() {
        return currentResource.getResourceOrException(Account.class);
    }

}
