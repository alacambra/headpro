package io.headpro.boundary;

import java.util.logging.Logger;
import javax.inject.Inject;
import javax.json.Json;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ResourceNotFoundExceptionMapper implements ExceptionMapper<ResourceNotFoundException> {
    
    @Inject
    Logger logger;

    @Override
    public Response toResponse(ResourceNotFoundException e) {
        
        logger.info(e.getMessage());
        
        return Response.status(Response.Status.NOT_FOUND)
                .entity(Json.createObjectBuilder()
                        .add("resource", e.getResourceName())
                        .add("id", e.getId())
                        .build())
                .build();
    }
}
