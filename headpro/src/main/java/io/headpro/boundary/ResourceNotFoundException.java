/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.boundary;

import java.util.Optional;
import javax.ejb.ApplicationException;

/**
 *
 * @author alacambra
 */
@ApplicationException
public class ResourceNotFoundException extends RuntimeException {

    private String resourceName;
    private String id;

    @Override
    public String getMessage() {
        return "Resource " + getResourceName() + ":" + getId() + " not found";
    }

    public ResourceNotFoundException(String resourceName, Object id) {
        this.resourceName = resourceName;
        this.id = String.valueOf(id);
    }

    public ResourceNotFoundException(String resourceName, Object id, String message) {
        super(message);
        this.resourceName = resourceName;
        this.id = String.valueOf(id);
    }

    public ResourceNotFoundException(String resourceName, Object id, Throwable cause) {
        super(cause);
        this.resourceName = resourceName;
        this.id = String.valueOf(id);
    }

    public ResourceNotFoundException(String resourceName, Object id, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.resourceName = resourceName;
        this.id = String.valueOf(id);
    }

    public ResourceNotFoundException() {
    }

    public String getResourceName() {
        return Optional.ofNullable(resourceName).orElse("unknown resource");
    }

    public String getId() {
        return id;
    }
}
