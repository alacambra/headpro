package io.headpro.boundary;

import io.headpro.PathExpressions;
import io.headpro.accounting.entity.Account;
import io.headpro.entity.User;
import io.headpro.workspace.entity.Project;
import io.headpro.workspace.entity.Service;
import io.headpro.workspace.entity.Workspace;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.SecurityContext;
import org.keycloak.KeycloakSecurityContext;

@RequestScoped
public class CurrentResourceBean {

    @PersistenceContext
    EntityManager em;

    SecurityContext securityContext;
    KeycloakSecurityContext keycloakSecurityContext;
    Map<Class<?>, Object> impliedObjects = new HashMap<>();
    Map<Class<?>, Object> cache = new HashMap<>();

    public KeycloakSecurityContext getKeycloakSecurityContext() {
        return keycloakSecurityContext;
    }

    public CurrentResourceBean setKeycloakSecurityContext(KeycloakSecurityContext keycloakSecurityContext) {
        this.keycloakSecurityContext = keycloakSecurityContext;
        return this;
    }

    public SecurityContext getSecurityContext() {
        return securityContext;
    }

    public CurrentResourceBean setSecurityContext(SecurityContext securityContext) {
        this.securityContext = securityContext;
        return this;
    }

    public CurrentResourceBean addImpliedObject(@NotNull String param, String id) {

        switch (param) {
            case PathExpressions.accountId:
                impliedObjects.put(Account.class, Integer.parseInt(id));
                break;
            case PathExpressions.projectId:
                impliedObjects.put(Project.class, Integer.parseInt(id));
                break;
            case PathExpressions.serviceId:
                impliedObjects.put(Service.class, Integer.parseInt(id));
                break;
            case PathExpressions.workspaceId:
                impliedObjects.put(Workspace.class, Integer.parseInt(id));
                break;
            case PathExpressions.userId:
                impliedObjects.put(User.class, id);
                break;
            default:
                break;
        }

        return this;
    }

    public <T> T getResourceOrException(Class<T> type) {

        T object = null;
        
//        if(!cache.containsKey(type)){
            object = em.find(type, impliedObjects.get(type));
//            cache.put(type, object);
//        }else{
//            object = (T) cache.get(type);
//        }

        if (object == null) {
            throw new ResourceNotFoundException(type.getName(), impliedObjects.get(type));
        }

        return object;
    }

    public <T> Object getResourceId(Class<T> type) {
        return impliedObjects.get(type);
    }

    @Override
    public String toString() {

        String principalName = securityContext == null ? "none" : securityContext.getUserPrincipal().getName();

        return "ResourceContext{"
                + " principal : " + principalName
                + " impliedObjects :" + impliedObjects
                + " hash :" + hashCode()
                + "} ";
    }

}
