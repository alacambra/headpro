/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.boundary;

import java.io.IOException;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.keycloak.KeycloakSecurityContext;

@Provider
public class ResourceContextFilter implements ContainerRequestFilter {

    @Inject
    Logger logger;

    @Inject
    CurrentResourceBean resourceContext;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        resourceContext
                .setKeycloakSecurityContext((KeycloakSecurityContext) requestContext.getProperty(KeycloakSecurityContext.class.getName()))
                .setSecurityContext(requestContext.getSecurityContext());

        MultivaluedMap<String, String> parameters = requestContext.getUriInfo().getPathParameters();
        parameters.entrySet().forEach(param -> resourceContext.addImpliedObject(param.getKey(), param.getValue().get(0)));
    }

}
