package io.headpro.boundary;

import java.security.Principal;
import javax.ejb.EJBAccessException;
import javax.inject.Inject;
import javax.json.Json;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class EJBAccessExceptionMapper implements ExceptionMapper<EJBAccessException> {

    @Inject
    Principal p;

    @Override
    public Response toResponse(EJBAccessException e) {

        return Response.status(Response.Status.FORBIDDEN)
                .entity(
                        Json.createObjectBuilder()
                        .add("msg", e.getMessage())
                        .add("u", p.getName()).build()
                ).build();
    }
}
