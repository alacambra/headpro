/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.boundary;

import io.headpro.accounting.boundary.AccountFacade;
import io.headpro.entity.User;
import java.io.IOException;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import org.keycloak.KeycloakSecurityContext;

@WebFilter(urlPatterns = {"/resources/*"})
@Transactional
public class UserValidationFilter implements Filter {

    @PersistenceContext
    EntityManager em;

    @Inject
    CurrentResourceBean currentResource;

    @Inject
    AccountFacade accountFacade;

    @Inject
    Logger logger;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse r, FilterChain chain) throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) r;
        KeycloakSecurityContext ctx = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        currentResource.setKeycloakSecurityContext(ctx);

        User userEntity = em.find(User.class, ctx.getToken().getSubject());

        if (userEntity == null) {
            userEntity = new User();
            userEntity.setEmail(ctx.getToken().getEmail());
            userEntity.setFirstName(ctx.getToken().getName());
            userEntity.setLastName(ctx.getToken().getFamilyName());
            userEntity.setId(ctx.getToken().getSubject());
            accountFacade.createAccount(userEntity);
            response.setStatus(201);
            response.setHeader("x-headpro-registered", "reload");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }

}
