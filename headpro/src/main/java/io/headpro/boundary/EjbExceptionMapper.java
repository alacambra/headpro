package io.headpro.boundary;

import javax.ejb.EJBException;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class EjbExceptionMapper implements ExceptionMapper<EJBException> {

    @Override
    public Response toResponse(EJBException e) {

        Throwable exception = e;

        while (exception.getCause() != null) {

            if (WebApplicationException.class.isAssignableFrom(exception.getCause().getClass())) {
                WebApplicationException wae = (WebApplicationException) exception.getCause();
                return wae.getResponse();
            }

            exception = exception.getCause();

        }

        throw e;
    }
}
