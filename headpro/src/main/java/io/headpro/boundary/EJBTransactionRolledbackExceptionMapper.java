/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.boundary;

import javax.annotation.Priority;
import javax.ejb.EJBTransactionRolledbackException;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Priorities;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.USER)
public class EJBTransactionRolledbackExceptionMapper implements ExceptionMapper<EJBTransactionRolledbackException> {

    @Override
    public Response toResponse(EJBTransactionRolledbackException e) {

        if (ConstraintViolationException.class.isAssignableFrom(e.getCause().getClass())) {

            ConstraintViolationException ex = (ConstraintViolationException) e.getCause();
            JsonArray arr = ex.getConstraintViolations().stream().map(this::constrainViolationConverter)
                    .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add).build();

            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(arr).build();
        }

        throw new RuntimeException(e);
    }

    private JsonObject constrainViolationConverter(ConstraintViolation constraintViolation) {
        return Json.createObjectBuilder()
                .add("message", constraintViolation.getMessage())
                .add("value", constraintViolation.getInvalidValue().toString())
                .build();
    }

}
