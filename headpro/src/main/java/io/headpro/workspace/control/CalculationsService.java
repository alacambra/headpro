package io.headpro.workspace.control;

import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.workspace.entity.Project;
import io.headpro.workspace.entity.Service;
import io.headpro.workspace.entity.Workspace;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static java.util.stream.Collectors.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CalculationsService {

    @PersistenceContext
    EntityManager em;
    
    public Map<Service, List<ResourceSlot>> getProjectsUsingServices(Workspace workspace, List<Service> services) {

        Map<Service, List<ResourceSlot>> slots = workspace.getProjects().stream()
                .map(Project::getResourceSlots)
                .flatMap(List::stream)
                .filter(slot -> services.contains(slot.getService()))
                .collect(groupingBy(Slot::getService));

        return slots;
    }
    
    public Map<Service, Map<Project, List<ResourceSlot>>> getProjectsUsingServicesDoubleGrouped(Workspace workspace, List<Service> services) {

        Map<Service, List<ResourceSlot>> slots = workspace.getProjects().stream()
                .map(Project::getResourceSlots)
                .flatMap(List::stream)
                .filter(slot -> services.contains(slot.getService()))
                .collect(groupingBy(Slot::getService));

        Map<Service, Map<Project, List<ResourceSlot>>> result = new HashMap<>();

        for (Entry<Service, List<ResourceSlot>> entry : slots.entrySet()) {

            Map<Project, List<ResourceSlot>> pSlots
                    = entry.getValue().stream().collect(groupingBy(ResourceSlot::getProject));

            result.put(entry.getKey(), pSlots);
        }

        return result;
    }

    public Map<Project, List<ResourceSlot>> getServicesUsedInProjects(Workspace workspace, List<Project> projects) {

        Map<Project, List<ResourceSlot>> slots = workspace.getProjects().stream()
                .filter(projects::contains)
                .map(Project::getResourceSlots)
                .flatMap(List::stream)
                .collect(groupingBy(ResourceSlot::getProject));

        return slots;
    }

    public Map<Project, Map<Service, List<ResourceSlot>>> getServicesUsedInProjectsDoubleGrouped(Workspace workspace, List<Project> projects) {

        Map<Project, List<ResourceSlot>> slots = workspace.getProjects().stream().filter(projects::contains)
                .map(Project::getResourceSlots)
                .flatMap(List::stream)
                .collect(groupingBy(ResourceSlot::getProject));

        Map<Project, Map<Service, List<ResourceSlot>>> result = new HashMap<>();

        for (Entry<Project, List<ResourceSlot>> entry : slots.entrySet()) {

            Map<Service, List<ResourceSlot>> pSlots
                    = entry.getValue().stream().collect(groupingBy(ResourceSlot::getService));

            result.put(entry.getKey(), pSlots);
        }

        return result;
    }

    public Map<Service, List<ResourceSlot>> getWorkspaceUsedResourcesByServices(Workspace workspace) {
        Map<Service, List<ResourceSlot>> slots = workspace.getProjects().stream()
                .map(Project::getResourceSlots)
                .flatMap(List::stream)
                .collect(groupingBy(Slot::getService));

        return slots;
    }

    public Map<Project, List<ResourceSlot>> getWorkspaceUsedResourcesByProjects(Workspace workspace) {
        Map<Project, List<ResourceSlot>> slots = workspace.getProjects().stream()
                .map(Project::getResourceSlots)
                .flatMap(List::stream)
                .collect(groupingBy(ResourceSlot::getProject));

        return slots;
    }
}
