/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.control;

/**
 *
 * @author alacambra
 */
public class InvalidSlotTypeException extends RuntimeException{

    public InvalidSlotTypeException() {
    }

    public InvalidSlotTypeException(String message) {
        super(message);
    }

    public InvalidSlotTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSlotTypeException(Throwable cause) {
        super(cause);
    }

    public InvalidSlotTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
    
}
