package io.headpro.workspace.control;

import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;

import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * Created by alacambra on 03/11/15.
 */
public class ExceptionService {

    private ExceptionService() {}

    public static RuntimeException inconsistentSlots(Project project, int startPosition, int duration, List<ResourceSlot> slots){
        return new RuntimeException(
                "two many slots has been found for params "
                        + project.getId() + startPosition + duration
                + slots.stream().map(ResourceSlot::getId).map(String::valueOf).collect(joining(","))
        );
    }


}
