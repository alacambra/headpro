package io.headpro.workspace.entity;

/**
 * Created by alacambra on 02/11/15.
 */
public enum Step {
    WEEKLY, BIWEEKLY
}
