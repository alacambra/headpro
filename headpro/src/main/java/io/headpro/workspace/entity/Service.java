package io.headpro.workspace.entity;

import io.headpro.entity.TrackedEntity;
import io.headpro.entity.UpdateTracker;
import io.headpro.entity.UpdateTrackerListener;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.ServiceSlot;
import javax.persistence.*;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 * Created by alacambra on 02/11/15.
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Service.fetchServices,
            query = "SELECT pr FROM Service as pr WHERE pr.workspace = :workspace"), 
})
@Table(name = "SERVICE")
@EntityListeners(UpdateTrackerListener.class)
public class Service implements TrackedEntity {

    protected static final String prefix = "io.headpro.entity.Service.";
    public static final String fetchServices = prefix + "fetchServices";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private Integer id;

    @Embedded
    private UpdateTracker updateTracker;

    @OneToMany(mappedBy = "service", cascade = CascadeType.ALL)
    private List<ServiceSlot> serviceSlots;
    
    @OneToMany(mappedBy = "service", cascade = CascadeType.ALL)
    private List<ResourceSlot> resourceSlots;

    @ManyToOne
    @NotNull
    private Workspace workspace;

    private String name;
    private boolean active;

    @Transient
    private Integer startIndex;

    @Transient
    private Integer endIndex;

    @Version
    int version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ServiceSlot> getServiceSlots() {
        return serviceSlots;
    }

    public List<ResourceSlot> getResourceSlots() {
        return resourceSlots;
    }
    
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getStartPosition() {
        return startIndex;
    }

    public void setStartPosition(Integer startPosition) {
        this.startIndex = startPosition;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    public void setLastIndex(Integer lastIndex) {
        this.endIndex = lastIndex;
    }

    public int getVersion() {
        return version;
    }

    @Override
    public UpdateTracker getUpdateTracker() {
        return updateTracker;
    }

    @Override
    public void setUpdateTracker(UpdateTracker updateTracker) {
        this.updateTracker = updateTracker;
    }

}
