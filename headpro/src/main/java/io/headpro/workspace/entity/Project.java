package io.headpro.workspace.entity;

import io.headpro.entity.TrackedEntity;
import io.headpro.entity.UpdateTracker;
import io.headpro.entity.UpdateTrackerListener;
import io.headpro.slots.entity.ResourceSlot;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Entity
@Table(name = "PROJECT")
@NamedQueries({
    @NamedQuery(name = Project.fetchProjects,
            query = "SELECT pr FROM Project as pr WHERE pr.workspace = :workspace")
})
@EntityListeners(UpdateTrackerListener.class)
public class Project implements TrackedEntity {

    protected static final String prefix = "io.headpro.entity.Project.";
    public static final String fetchProjects = prefix + "fetchProjects";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private Integer id;

    @Embedded
    private UpdateTracker updateTracker;

    @ManyToOne
    @NotNull
    private Workspace workspace;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    List<ResourceSlot> resourceSlots;

    /**
     * TODO: include them on db
     */
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @NotNull
    private Date endDate = new Date();

    @Version
    private int version;

    private String name;
    private Step step;
    private Boolean active;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Workspace getTeam() {
        return workspace;
    }

    public void setTeam(Workspace team) {
        this.workspace = team;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<ResourceSlot> getResourceSlots() {
        return resourceSlots;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;

        return Objects.equals(this.id, other.id);
    }

    @Override
    public UpdateTracker getUpdateTracker() {
        return updateTracker;
    }

    @Override
    public void setUpdateTracker(UpdateTracker updateTracker) {
        this.updateTracker = updateTracker;
    }
}
