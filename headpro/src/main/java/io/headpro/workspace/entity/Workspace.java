package io.headpro.workspace.entity;

import io.headpro.accounting.entity.Account;
import io.headpro.entity.TrackedEntity;
import io.headpro.entity.UpdateTracker;
import io.headpro.entity.UpdateTrackerListener;
import javax.persistence.*;
import java.util.List;

@Entity
@NamedQueries({})
@Table(name = "WORKSPACE")
@EntityListeners(UpdateTrackerListener.class)
public class Workspace implements TrackedEntity {

    private static final String prefix = "Workspace.";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private Integer id;

    @Embedded
    private UpdateTracker updateTracker;

    @OneToMany(mappedBy = "workspace", cascade = CascadeType.ALL)
    private List<Project> projects;

    @OneToMany(mappedBy = "workspace", cascade = CascadeType.ALL)
    private List<Service> services;

    @OneToMany(mappedBy = "workspace", cascade = CascadeType.ALL)
    private List<WorkspaceRole> roles;

    @ManyToOne
    private Account account;

    @Transient
    private WorkspacePeriod workspacePeriod;

    @Version
    int version;

    private String title;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public WorkspacePeriod getWorkspacePeriod() {
        return workspacePeriod;
    }

    public void setWorkspacePeriod(WorkspacePeriod workspacePeriod) {
        this.workspacePeriod = workspacePeriod;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public UpdateTracker getUpdateTracker() {
        return updateTracker;
    }

    @Override
    public void setUpdateTracker(UpdateTracker updateTracker) {
        this.updateTracker = updateTracker;
    }

    public List<WorkspaceRole> getRoles() {
        return roles;
    }

    public void setRoles(List<WorkspaceRole> roles) {
        this.roles = roles;
    }
}
