/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.entity;

import io.headpro.entity.GenericRepository;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.slots.entity.Slot;
import io.headpro.time.boundary.TimeRange;
import io.headpro.time.boundary.TimeRangeBuilder;
import io.headpro.workspace.boundary.ServiceConverter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alacambra
 */
public class ProjectBean extends GenericRepository<Project> {

    @PersistenceContext
    EntityManager em;

    @Inject
    TimeRangeBuilder timeRangeBuilder;

    @Inject
    ServiceConverter serviceConverter;

    private Project project;
    private TimeRange projectTimeRange;

    @PostConstruct
    public void init() {
        this.setEntityManager(em);
    }

    public TimeRange getSlotsMaxTimeRange() {

        List<ResourceSlot> slots = project.getResourceSlots();

        if (slots.isEmpty()) {
            return timeRangeBuilder.createEmptyTimeRange(projectTimeRange.getStartIndex());
        }

        int min = project.getResourceSlots().stream()
                .filter(this::valuePerStepIsNotZero)
                .map(Slot::getStartPosition).min((a, b) -> {
            return  a - b;
        }).orElse(-1);
        
        if(min == -1){
            return timeRangeBuilder.createEmptyTimeRange(projectTimeRange.getStartIndex());
        }

        int max = project.getResourceSlots().stream()
                .filter(this::valuePerStepIsNotZero)
                .map(Slot::getEndPosition).max((a, b) -> {
            return a - b;
        }).orElse(-1);
        
        if(max == -1){
            throw new RuntimeException("Impossible state");
        }
        
        return timeRangeBuilder
                .fromIndexes(projectTimeRange.getStartIndex(), 0, max)
                .build();
    }
    
    private boolean valuePerStepIsNotZero(Slot slot){
        return !(BigDecimal.ZERO.compareTo(slot.getValuePerStep()) == 0);
    }

    public Map<Service, TimeRange> getDifferentialTimeRanges(TimeRange range, DiffType diffType) {
        

        Map<Service, TimeRange> servicesTimeRanges = new HashMap<>();

        for (Map.Entry<Service, TimeRange> entry : getTimeRanges().entrySet()) {
            
            if(!entry.getValue().overlapWith(range)){
                continue;
            }
            
            if(diffType == DiffType.INNER && entry.getValue().getEndIndex() > range.getEndIndex()){
                servicesTimeRanges.put(entry.getKey(), entry.getValue().getDifferentialTimeRange(range));
            }else if(diffType == DiffType.OUTER && entry.getValue().getEndIndex() < range.getEndIndex()){
                servicesTimeRanges.put(entry.getKey(), entry.getValue().getDifferentialTimeRange(range));
            }else if(diffType == DiffType.BOTH){
                servicesTimeRanges.put(entry.getKey(), entry.getValue().getDifferentialTimeRange(range));
            }
        }

        return servicesTimeRanges;
    }

    public JsonArrayBuilder getDifferentialTimeRangesAsJson(TimeRange range, DiffType diffType) {

        Map<Service, TimeRange> timeRanges = getDifferentialTimeRanges(range, diffType);
        JsonArrayBuilder builder = Json.createArrayBuilder();

        for (Map.Entry<Service, TimeRange> entry : timeRanges.entrySet()) {
            builder.add(Json.createObjectBuilder()
                    .add("service", serviceConverter.toJson(entry.getKey()))
                    .add("timeRange", entry.getValue().toJson()));
        }


        return builder;
    }
    
    public static enum DiffType{
        INNER, OUTER, BOTH
    }

    public Map<Service, TimeRange> getTimeRanges() {

        Map<Service, List<ResourceSlot>> classifiedSlots
                = project.getResourceSlots().stream().collect(groupingBy(ResourceSlot::getService));

        Map<Service, TimeRange> servicesTimeRanges = new HashMap<>();

        for (Map.Entry<Service, List<ResourceSlot>> entry : classifiedSlots.entrySet()) {
            SlotsChain<ResourceSlot> chain = new SlotsChain<>(entry.getValue(), timeRangeBuilder);
            servicesTimeRanges.put(entry.getKey(), chain.getTimeRange(projectTimeRange.getStartIndex()));
        }

        return servicesTimeRanges;
    }

    public Map<Service, SlotsChain<ResourceSlot>> getResourceSlotsChains() {
        Map<Service, List<ResourceSlot>> classifiedSlots
                = project.getResourceSlots().stream().collect(groupingBy(ResourceSlot::getService));

        Map<Service, SlotsChain<ResourceSlot>> servicesChains = new HashMap<>();

        for (Map.Entry<Service, List<ResourceSlot>> entry : classifiedSlots.entrySet()) {
            SlotsChain<ResourceSlot> chain = new SlotsChain<>(entry.getValue(), timeRangeBuilder);
            servicesChains.put(entry.getKey(), chain);
        }

        return servicesChains;
    }

    public Map<Service, ResourceSlot> getFirstOverflowedSlots() {

        Map<Service, SlotsChain<ResourceSlot>> slots = getResourceSlotsChains();
        Map<Service, ResourceSlot> overflowedSlots = new HashMap<>();

        for (Map.Entry<Service, SlotsChain<ResourceSlot>> entry : slots.entrySet()) {
            ResourceSlot overflowedSlot = entry.getValue().getFirstOverflowedSlot(projectTimeRange);
            overflowedSlots.put(entry.getKey(), overflowedSlot);
        }

        return overflowedSlots;
    }

    public ProjectBean rearrengeFirstOverflowedSlots() {

        Map<Service, SlotsChain<ResourceSlot>> slots = getResourceSlotsChains();

        for (Map.Entry<Service, SlotsChain<ResourceSlot>> entry : slots.entrySet()) {
            entry.getValue().rearrengeFirstOverflowedSlot(projectTimeRange);
        }

        return this;
    }

    public Project merge() {
        project = super.merge(project);
        refreshTimeRange();
        return project;
    }

    public ProjectBean setProject(@NotNull Project project) {

        if (this.project != null) {
            throw new RuntimeException("Project entity already set");
        }

        this.project = project;
        refreshTimeRange();

        return this;
    }

    void refreshTimeRange() {
        projectTimeRange = timeRangeBuilder.fromDates(project.getStartDate(), project.getEndDate()).build();
    }

    public TimeRange getProjectTimeRange() {
        return projectTimeRange;
    }

    public Integer getId() {
        return project.getId();
    }

    public ProjectBean setId(Integer id) {
        project.setId(id);
        return this;
    }

    public Workspace getTeam() {
        return project.getWorkspace();
    }

    public ProjectBean setTeam(Workspace team) {
        project.setTeam(team);
        return this;
    }

    public String getName() {
        return project.getName();
    }

    public ProjectBean setName(String name) {
        project.setName(name);
        return this;
    }

    public Date getStartDate() {
        return project.getStartDate();
    }

    public ProjectBean setStartDate(Date startDate) {
        project.setStartDate(startDate);
        refreshTimeRange();
        return this;
    }

    public Date getEndDate() {
        return project.getEndDate();
    }

    public ProjectBean setEndDate(Date endDate) {
        project.setEndDate(endDate);
        refreshTimeRange();
        return this;
    }

    public Step getStep() {
        return project.getStep();
    }

    public ProjectBean setStep(Step step) {
        project.setStep(step);
        return this;
    }

    public Boolean getActive() {
        return project.getActive();
    }

    public ProjectBean setActive(Boolean active) {
        project.setActive(active);
        return this;
    }

    public List<ResourceSlot> getResourceSlots() {
        return project.getResourceSlots();
    }

    public Map<Service, List<ResourceSlot>> getServiceWithResourceSlots() {

       return getResourceSlots().stream().collect(groupingBy((ResourceSlot s) -> s.getService()));

    }

    public Workspace getWorkspace() {
        return project.getWorkspace();
    }

    public ProjectBean setWorkspace(Workspace workspace) {
        project.setWorkspace(workspace);
        return this;
    }

    @Override
    protected Class<Project> getType() {
        return Project.class;
    }
}
