/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.entity;

import io.headpro.slots.entity.Slot;
import io.headpro.time.boundary.TimeRange;
import io.headpro.time.boundary.TimeRangeBuilder;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SlotsChain<S extends Slot<?>> {

    List<S> slots;
    TimeRangeBuilder timeRangeBuilder;
    S firstSlot;

    public SlotsChain(List<S> slots, TimeRangeBuilder timeRangeBuilder) {
        Optional.ofNullable(slots).orElseThrow(() -> new RuntimeException("slots cannot be null"));
        Optional.ofNullable(timeRangeBuilder).orElseThrow(() -> new RuntimeException("timeRangeBuilder cannot be null "));

        this.slots = new ArrayList<>(slots);
        this.timeRangeBuilder = timeRangeBuilder;
    }

    public S getFirstSlot() {

        if (firstSlot == null) {
            firstSlot = slots.stream().min((a, b) -> a.getStartPosition() - b.getStartPosition()).orElseThrow(() -> new RuntimeException("A first slot must exist"));
        }
        return firstSlot;
    }

    public S getLastSlot() {
        S s = getFirstSlot();

        while (s != null && s.getNextSlot() != null) {
            s = (S) s.getNextSlot();

        }
        return s;
    }

    public S getFirstOverflowedSlot(TimeRange containerTimeRange) {

        return getFirstOverflowedSlot(containerTimeRange, false);
    }

    private S getFirstOverflowedSlot(TimeRange containerTimeRange, boolean considerZeroValueSlot) {

        if (slots.isEmpty()) {
            return null;
        }

        S slot = getLastSlot();

        if (slot == null || slot.getValuePerStep().equals(BigDecimal.ZERO)) {
            return null;
        }

        if (slot.getEndPosition() + containerTimeRange.getStartIndex() <= containerTimeRange.getEndIndex()) {
            return null;
        }

        S candidate = null;
        for (S s = slot; s != null;) {

            /**
             * slot its partially in range and partially not
             */
            if (s.getStartPosition() <= containerTimeRange.getRangeLength()
                    && s.getEndPosition() >= containerTimeRange.getRangeLength()) {

                return isOverflowedSlot(s, considerZeroValueSlot);

                /**
                 * Slot completely in timerange. No overlap
                 */
            } else if (s.getEndPosition() < containerTimeRange.getRangeLength()) {
                return Optional.ofNullable(candidate).orElse(null);

                /**
                 * Slot completely out of range. Candidate.
                 */
            } else if (s.getStartPosition() > containerTimeRange.getRangeLength()) {
                candidate = isOverflowedSlot(s, considerZeroValueSlot);
            }

            s = (S) s.getPreviousSlot();
        }

        if (candidate != null) {
            return candidate;
        }

        throw new RuntimeException("Unexpected state");
    }

    private S isOverflowedSlot(S slot, boolean considerZeroValueSlot) {
        if (considerZeroValueSlot) {
            return slot;
        } else {
            return valuePerStepIsZero(slot) ? null : slot;
        }
    }
    
    private boolean valuePerStepIsZero(Slot slot){
        return (BigDecimal.ZERO.compareTo(slot.getValuePerStep()) == 0);
    }

    public TimeRange getTimeRange(Integer offset) {

        if (slots.isEmpty()) {
            return timeRangeBuilder.createEmptyTimeRange(Optional.ofNullable(offset).orElse(0));
        }

        return timeRangeBuilder.fromIndexes(
                Optional.ofNullable(offset).orElse(0),
                getFirstSlot().getStartPosition(),
                getLastSlot().getEndPosition())
                .build();
    }

    public void rearrengeFirstOverflowedSlot(TimeRange containerTimeRange) {

        Optional.ofNullable(getFirstOverflowedSlot(containerTimeRange, true))
                .ifPresent(slot -> slot.setEndPosition(containerTimeRange.getRangeLength() - 1));

    }
}
