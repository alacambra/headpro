/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.entity;

import io.headpro.entity.User;
import io.headpro.security.boundary.Role;
import static io.headpro.workspace.entity.WorkspaceRole.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "WORKSPACE_ROLE")
@NamedQueries({
    @NamedQuery(name = fetchRoleByUserAndWorkspace, query = 
            "SELECT role FROM WorkspaceRole as role "
                    + "JOIN role.user as u "
                    + "JOIN role.workspace as ws "
                    + "WHERE u.id = :userId AND ws.id = :workspaceId")
})
public class WorkspaceRole {
    
    private static final String prefix = "WorkspaceRole.";
    public static final String fetchRoleByUserAndWorkspace = prefix + "fetchRoleByUserAndWorkspace";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private int id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Workspace workspace;

    @Enumerated(EnumType.STRING)
    private Role userRole;

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getUserRole() {
        return userRole;
    }

    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkspaceRole other = (WorkspaceRole) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

}
