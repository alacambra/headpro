/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.entity;

import io.headpro.utils.LocalDateConverter;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author alacambra
 */
public class WorkspacePeriod {
    
    LocalDate startDate;
    LocalDate endDate;

    public WorkspacePeriod(Date startDate, Date endDate) {
        this.startDate = LocalDateConverter.toLocalDate(startDate);
        this.endDate = LocalDateConverter.toLocalDate(endDate);
    }

    public WorkspacePeriod(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
    
}
