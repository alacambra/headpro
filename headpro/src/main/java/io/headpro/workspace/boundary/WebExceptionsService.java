package io.headpro.workspace.boundary;

import javax.validation.constraints.NotNull;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Created by alacambra on 27/12/15.
 */
public class WebExceptionsService {

    public WebApplicationException buildNotFoundException(@NotNull String message){
        return new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity(message).build());
    }

}
