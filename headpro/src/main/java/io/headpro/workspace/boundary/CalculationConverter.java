/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.workspace.boundary;

import io.headpro.slots.boundary.SlotsConverter;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Project;
import io.headpro.workspace.entity.Service;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author alacambra
 */
public class CalculationConverter {

    @Inject
    SlotsConverter slotsConverter;

    @Inject
    ServiceConverter serviceConverter;

    @Inject
    ProjectConverter projectConverter;

    public JsonArray toJsonFromProject(Map<Project, List<ResourceSlot>> slots) {

        return slots.entrySet()
                .stream()
                .map(this::convertEntryFromProject)
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

    }

    private JsonObject convertEntryFromProject(Entry<Project, List<ResourceSlot>> entry) {
        return projectConverter.basicProjectConversion(entry.getKey())
                .add("slots",
                        entry.getValue()
                        .stream()
                        .map(slotsConverter::toJson)
                        .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add).build()
                ).build();
    }

    public JsonArray toJsonFromProjectDoubleGourpped(Map<Project, Map<Service, List<ResourceSlot>>> slots) {

        return slots.entrySet()
                .stream()
                .map(this::convertEntryFromProjectDoubleGourpped)
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

    }

    private JsonObject convertEntryFromProjectDoubleGourpped(Entry<Project, Map<Service, List<ResourceSlot>>> entry) {
        return projectConverter.basicProjectConversion(entry.getKey())
                .add("services", toJsonFromService(entry.getValue()))
                .build();
    }

    public JsonArray toJsonFromService(Map<Service, List<ResourceSlot>> slots) {

        return slots.entrySet()
                .stream()
                .map(this::convertEntryFromService)
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

    }

    private JsonObject convertEntryFromService(Entry<Service, List<ResourceSlot>> entry) {
        return serviceConverter.basicServiceConversion(entry.getKey())
                .add("slots",
                        entry.getValue()
                        .stream()
                        .map(slotsConverter::toJsonWithAbsolutePosition)
                        .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add).build()
                ).build();
    }

    public JsonArray toJsonFromServiceDoubleGourpped(Map<Service, Map<Project, List<ResourceSlot>>> slots) {

        return slots.entrySet()
                .stream()
                .map(this::convertEntryFromServiceDoubleGourpped)
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

    }

    private JsonObject convertEntryFromServiceDoubleGourpped(Entry<Service, Map<Project, List<ResourceSlot>>> entry) {
        return serviceConverter.basicServiceConversion(entry.getKey())
                .add("projects", toJsonFromProject(entry.getValue())).build();
    }

}
