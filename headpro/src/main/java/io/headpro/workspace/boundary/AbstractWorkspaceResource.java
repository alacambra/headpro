package io.headpro.workspace.boundary;

import io.headpro.boundary.ResourceNotFoundException;
import io.headpro.boundary.CurrentResourceBean;
import io.headpro.security.entity.CurrentUser;
import io.headpro.slots.boundary.SlotsConverter;
import io.headpro.slots.boundary.SlotsFacade;
import io.headpro.time.boundary.TimeRangeBuilder;
import io.headpro.utils.DatesService;
import io.headpro.workspace.entity.Workspace;
import java.util.logging.Logger;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractWorkspaceResource {

    @PersistenceContext
    protected EntityManager em;

    @Inject
    protected DatesService datesService;

    @Inject
    protected SlotsConverter slotsConverter;

    @Inject
    protected SlotsFacade slotsFacade;

    @Inject
    protected Logger logger;

    @Inject
    protected CurrentResourceBean currentResource;

    @Inject
    protected CurrentUser currentUser;
    
    @Inject
    protected Instance<TimeRangeBuilder> timeRangeBuilders;
    
    protected ResourceNotFoundException notFoundException() {
        return new ResourceNotFoundException();
    }

    protected Workspace getCurrentWorkspace() {
        return currentResource.getResourceOrException(Workspace.class);
    }

    protected int getCurrentWorkspaceId() {
        return (Integer) currentResource.getResourceId(Workspace.class);
    }
}
