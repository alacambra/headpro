package io.headpro.workspace.boundary;

import io.headpro.PathExpressions;
import io.headpro.security.boundary.AllowedTo;
import io.headpro.security.boundary.Role;
import io.headpro.security.boundary.Secured;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;
import io.headpro.slots.entity.ServiceSlot;
import io.headpro.workspace.entity.Project;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.groupingBy;
import javax.inject.Inject;
import javax.json.JsonArray;
import javax.json.JsonObject;

@Path("workspace/" + PathExpressions.workspaceIdExpr + "/service/" + PathExpressions.serviceIdExpr)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
@Secured(Secured.RoleOver.WORKSPACE)
public class ServiceResource extends AbstractWorkspaceResource {

    @Inject
    private ServiceConverter serviceConverter;
    
    @Inject
    private ProjectConverter projectConverter;

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @AllowedTo({Role.MEMBER, Role.OWNER})
    public JsonObject updateService(JsonObject json) {

        Service service = serviceConverter.fromJson(json, getCurrentService());
        return serviceConverter.toJson(service);
    }

    @DELETE
    @AllowedTo({Role.MEMBER, Role.OWNER})
    public void deleteService() {
        em.remove(getCurrentService());
    }

    @GET
    @AllowedTo(Role.ALL)
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getService() {
        return serviceConverter.toJson(getCurrentService());
    }

    @GET
    @Path("slots")
    @AllowedTo(Role.ALL)
    public String getSlots() {

        List<JsonObject> slots = (List<JsonObject>) em.createNamedQuery(ServiceSlot.fetchServiceSlots)
                .setParameter("service", getCurrentService())
                .getResultList()
                .stream()
                .map(slot -> slotsConverter.toJson((ServiceSlot) slot)).collect(Collectors.toList());

        return slots.toString();
    }
    

    @GET
    @Path("project/slots")
    @AllowedTo(Role.ALL)
    public JsonArray getSlotsForService() {

        List<ResourceSlot> slots = em.createNamedQuery(ResourceSlot.fetchSlotsForService, ResourceSlot.class)
                .setParameter("service", getCurrentService()).setParameter("workspace", getCurrentWorkspace())
                .getResultList();

        List<Project> projects = em.createNamedQuery(Project.fetchProjects).setParameter("workspace", getCurrentWorkspace()).getResultList();
        Map<Project, List<ResourceSlot>> classifiedSlots = slots.stream().collect(groupingBy(ResourceSlot::getProject));

        projects.forEach(p -> classifiedSlots.putIfAbsent(p, Collections.EMPTY_LIST));

        return projectConverter.toJsonWithSlots(classifiedSlots);

    }

    @POST
    @Path("slot")
    @AllowedTo({Role.MEMBER, Role.OWNER})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addSlot(@Context UriInfo uriInfo, @PathParam("serviceId") int serviceId, JsonObject jsonSlot) {

        ServiceSlot slot = slotsConverter.fromJson(jsonSlot, ServiceSlot.class);

        slot.setService(getCurrentService());
        slot = (ServiceSlot) slotsFacade.insertOrUpdateSlots(slot);

        URI uri = uriInfo.getBaseUriBuilder()
                .path(ProjectResource.class, "getSlots")
                .resolveTemplate(PathExpressions.workspaceId, getCurrentWorkspaceId())
                .resolveTemplate(PathExpressions.serviceId, getCurrentService().getId())
                .path(String.valueOf(slot.getId()))
                .build();
        
        return Response.created(uri).build();
    }
    
    private Service getCurrentService(){
        return currentResource.getResourceOrException(Service.class);
    }
}
