package io.headpro.workspace.boundary;

import io.headpro.slots.boundary.SlotsConverter;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.workspace.entity.Service;

import javax.inject.Inject;
import javax.json.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ServiceConverter {

    @Inject
    SlotsConverter slotsConverter;

    public Service fromJson(JsonObject json) {
        return fromJson(json, new Service());
    }

    public Service fromJson(JsonObject json, Service service) {

        if (json.containsKey("id")) {
            if (service.getId() == null) {
                service.setId(json.getInt("id"));
            } else if (service.getId() != json.getInt("id")) {
                throw new RuntimeException("ids mismatch");
            }
        }

        if (json.containsKey("active")) {
            service.setActive(json.getBoolean("active"));
        }
        if (json.containsKey("name")) {
            service.setName(json.getString("name"));
        }

        return service;
    }

    JsonObjectBuilder basicServiceConversion(Service service) {
        return Json.createObjectBuilder()
                .add("name", Optional.ofNullable(service.getName()).orElse("no title"))
                .add("id", service.getId());
    }

    public JsonObject toJson(Service service) {
        return basicServiceConversion(service).build();
    }

    public JsonObject toJsonWithSlots(Service service) {
        return basicServiceConversion(service)
                .add("slots", slotsConverter.toJson(service.getServiceSlots()))
                .build();
    }

    public JsonObject toJsonWithSimpleSlots(Service service, List<BigDecimal> simpleSlots) {

        JsonArrayBuilder arr = Json.createArrayBuilder();
        for (BigDecimal v : simpleSlots) {
            arr.add(v);
        }

        return basicServiceConversion(service)
                .add("startIndex", service.getStartPosition())
                .add("endIndex", service.getEndIndex())
                .add("slots", arr)
                .build();
    }

    public JsonArray toJson(List<Service> services) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Service service : services) {
            arr.add(toJson(service));
        }

        return arr.build();
    }

    public JsonArray toJsonWithSlots(List<Service> services) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Service service : services) {
            arr.add(toJsonWithSlots(service));
        }

        return arr.build();
    }

    public JsonArray toJsonWithSlots(Map<Service, List<ResourceSlot>> slots) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Map.Entry<Service, List<ResourceSlot>> entry : slots.entrySet()) {

            JsonObjectBuilder builder = basicServiceConversion(entry.getKey())
                    .add("slots", slotsConverter.toJsonWithAbsolutePosition(entry.getValue()));

            arr.add(builder.build());
        }

        return arr.build();
    }

    public JsonArray toJsonWithSimpleSlots(Map<Service, List<BigDecimal>> services) {
        return toJsonWithSimpleBuilder(services).build();
    }

    public JsonArrayBuilder toJsonWithSimpleBuilder(Map<Service, List<BigDecimal>> services) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Service service : services.keySet()) {
            arr.add(toJsonWithSimpleSlots(service, services.get(service)));
        }

        return arr;
    }

}
