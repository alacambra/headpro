package io.headpro.workspace.boundary;

import io.headpro.slots.boundary.SlotsConverter;
import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.utils.DatesService;
import io.headpro.utils.LocalDateConverter;

import javax.inject.Inject;
import javax.json.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by alacambra on 09/12/15.
 */
public class ProjectConverter {

    @Inject
    SlotsConverter slotsConverter;

    @Inject
    DatesService datesService;

    public Project fromJson(JsonObject json) {
        return fromJson(json, new Project());
    }

    public Project fromJson(JsonObject json, Project project) {

        if (json.containsKey("id")) {
            if (project.getId() == null) {
                project.setId(json.getInt("id"));
            } else if (project.getId() != json.getInt("id")) {
                throw new RuntimeException("ids mismatch");
            }
        }

        if (json.containsKey("active")) {
            project.setActive(json.getBoolean("active"));
        }
        if (json.containsKey("name")) {
            project.setName(json.getString("name"));
        }
        if (json.containsKey("endDate")) {
            long timeStamp = json.getJsonNumber("endDate").longValue();
            project.setEndDate(new Date(timeStamp));
        }
        if (json.containsKey("startDate")) {
            long timeStamp = json.getJsonNumber("startDate").longValue();
            project.setStartDate(new Date(timeStamp));
        }

        return project;
    }

    JsonObjectBuilder basicProjectConversion(Project project) {
        return Json.createObjectBuilder().add("name", Optional.ofNullable(project.getName()).orElse("no title"))
                .add("id", project.getId())
                .add("startDate", project.getStartDate().getTime())
                .add("endDate", project.getEndDate().getTime())
                .add("startIndex", datesService.getDateIndex(LocalDateConverter.toLocalDate(project.getStartDate())))
                .add("endIndex", datesService.getDateIndex(LocalDateConverter.toLocalDate(project.getEndDate())));
    }

    public JsonObject toJson(Project project) {
        return basicProjectConversion(project).build();
    }

    public JsonObject toJsonWithSlots(Project project) {
        return basicProjectConversion(project)
                .add("slots", slotsConverter.toJson(project.getResourceSlots()))
                .build();
    }

    public JsonObject toJsonWithSimpleSlots(Project project, List<BigDecimal> simpleSlots) {

        JsonArrayBuilder arr = Json.createArrayBuilder();
        for (BigDecimal v : simpleSlots) {
            arr.add(v);
        }

        return basicProjectConversion(project)
                .add("slots", arr)
                .build();
    }

    public JsonArray toJson(List<Project> projects) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Project project : projects) {
            arr.add(toJson(project));
        }

        return arr.build();
    }

    public JsonArray toJsonWithSlots(List<Project> projects) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Project project : projects) {
            arr.add(toJsonWithSlots(project));
        }

        return arr.build();
    }

    public JsonArray toJsonWithSlots(Map<Project, List<ResourceSlot>> slots) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Map.Entry<Project, List<ResourceSlot>> entry : slots.entrySet()) {

            JsonObjectBuilder builder = basicProjectConversion(entry.getKey())
                    .add("slots", slotsConverter.toJson(entry.getValue()));

            arr.add(builder.build());
        }

        return arr.build();
    }

    public JsonArray toJsonWithSimpleSlots(Map<Project, List<BigDecimal>> projects) {
        return toJsonWithSimpleSlotsBuilder(projects).build();
    }

    public JsonArrayBuilder toJsonWithSimpleSlotsBuilder(Map<Project, List<BigDecimal>> projects) {
        JsonArrayBuilder arr = Json.createArrayBuilder();

        for (Project project : projects.keySet()) {
            arr.add(toJsonWithSimpleSlots(project, projects.get(project)));
        }

        return arr;
    }

}
