package io.headpro.workspace.boundary;

import io.headpro.PathExpressions;
import io.headpro.boundary.InvalidRequestException;
import io.headpro.entity.UserConverter;
import io.headpro.entity.User;
import io.headpro.security.boundary.AllowedTo;
import io.headpro.security.boundary.Role;
import io.headpro.security.boundary.Secured;
import io.headpro.workspace.control.CalculationsService;
import io.headpro.workspace.entity.Project;
import io.headpro.workspace.entity.Service;
import io.headpro.workspace.entity.WorkspaceRole;
import java.math.BigDecimal;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Actions over a specific workspace that can be done for a workspace member.
 */
@Path("workspace/" + PathExpressions.workspaceIdExpr)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
@Secured(Secured.RoleOver.WORKSPACE)
public class WorkspaceResource extends AbstractWorkspaceResource {

    @Inject
    private WorkspaceConverter workspaceConverter;

    @Inject
    private ProjectConverter projectConverter;

    @Inject
    private ServiceConverter serviceConverter;

    @Inject
    CalculationsService calculationsService;

    @Inject
    UserConverter userConverter;

    @Inject
    CalculationConverter calculationConverter;

    @GET
    @AllowedTo({Role.ALL})
    public JsonObject getWorkspace(@QueryParam("view") @DefaultValue("alone") String view) {
        return workspaceConverter.toJson(getCurrentWorkspace(), view);
    }

    @PUT
    @AllowedTo({Role.MEMBER, Role.OWNER})
    public JsonObject updateWorkspace(JsonObject json) {
        workspaceConverter.fromJson(json, this.getCurrentWorkspace());
        return workspaceConverter.toJson(getCurrentWorkspace());
    }

    @DELETE
    @AllowedTo({Role.OWNER})
    public void removeWorkspace() {
        em.remove(getCurrentWorkspace());
    }

    @POST
    @GET
    @Path("project")
    @AllowedTo({Role.MEMBER, Role.OWNER})
    public Response createProject(@Context UriInfo uriInfo, JsonObject json) {

        Project project = projectConverter.fromJson(json);
        project.setWorkspace(getCurrentWorkspace());
        project = em.merge(project);

        URI uri = uriInfo.getBaseUriBuilder()
                .path(ProjectResource.class)
                .resolveTemplate(PathExpressions.workspaceId, getCurrentWorkspace().getId())
                .resolveTemplate(PathExpressions.projectId, project.getId())
                .build();

        return Response.created(uri).build();
    }

    @Path("/project/" + PathExpressions.projectIdExpr)
    @AllowedTo({Role.ALL})
    public ProjectResource getProjectResource() {
        return null;
    }

    @GET
    @Path("projects")
    @AllowedTo({Role.ALL})
    public JsonArray getProjects() {

        List<Project> projects = em.createNamedQuery(Project.fetchProjects)
                .setParameter("workspace", getCurrentWorkspace())
                .getResultList();

        return projectConverter.toJson(projects);
    }

    @GET
    @Path("services")
    @AllowedTo({Role.ALL})
    public JsonArray getServices() {

        List<Service> services = em.createNamedQuery(Service.fetchServices)
                .setParameter("workspace", getCurrentWorkspace())
                .getResultList();

        return serviceConverter.toJson(services);

    }

    @POST
    @Path("service")
    @AllowedTo({Role.MEMBER, Role.OWNER})
    public Response createService(@Context UriInfo uriInfo, JsonObject json) {

        Service service = serviceConverter.fromJson(json);
        service.setWorkspace(getCurrentWorkspace());
        service = em.merge(service);

        URI uri = uriInfo.getBaseUriBuilder()
                .path(ServiceResource.class)
                .resolveTemplate(PathExpressions.workspaceId, getCurrentWorkspace().getId())
                .resolveTemplate(PathExpressions.serviceId, service.getId())
                .build();

        return Response.created(uri).build();
    }

    @GET
    @Path("roles")
    @AllowedTo(Role.ALL)
    public JsonArray getWorkspaceUsers() {

        JsonArrayBuilder arr = Json.createArrayBuilder();

        getCurrentWorkspace().getRoles()
                .stream()
                .map(WorkspaceRole::getUser)
                .map(userConverter::toJson)
                .forEach(arr::add);

        return arr.build();
    }

    @POST
    @Path("roles" + PathExpressions.userIdExpr)
    @AllowedTo(Role.OWNER)
    public Response addUserToWorkspace(@PathParam(PathExpressions.userId) int userId, JsonObject jsonRole) {

        List<WorkspaceRole> roles = em.createNamedQuery(WorkspaceRole.fetchRoleByUserAndWorkspace)
                .setParameter("userId", currentUser.getId())
                .setParameter("workspaceId", getCurrentWorkspace().getId())
                .getResultList();

        if (roles.isEmpty()) {

            User userEntity = new User();
            userEntity.setId(currentUser.getId());

            Role r = Role.valueOf(jsonRole.getString("role"));

            WorkspaceRole role = new WorkspaceRole();
            role.setUserRole(r);
            role.setWorkspace(getCurrentWorkspace());
            role.setUser(userEntity);

            em.persist(role);

        }

        return Response.noContent().build();
    }

    @DELETE
    @Path("roles" + PathExpressions.userIdExpr)
    @AllowedTo(Role.OWNER)
    public Response removeUserFromWorkspace(@PathParam(PathExpressions.userId) int userId) {

        List<WorkspaceRole> roles = em.createNamedQuery(WorkspaceRole.fetchRoleByUserAndWorkspace)
                .setParameter("userId", currentUser.getId())
                .setParameter("workspaceId", getCurrentWorkspace().getId())
                .getResultList();

        if (!roles.isEmpty()) {
            em.remove(roles.get(0));
        }

        return Response.noContent().build();

    }

    @PUT
    @Path("roles" + PathExpressions.userIdExpr)
    @AllowedTo(Role.OWNER)
    public void updateUserRoleFromWorkspace(@PathParam(PathExpressions.userId) int userId, JsonObject jsonRole) {

        List<WorkspaceRole> roles = em.createNamedQuery(WorkspaceRole.fetchRoleByUserAndWorkspace)
                .setParameter("userId", currentUser.getId())
                .setParameter("workspaceId", getCurrentWorkspace().getId())
                .getResultList();

        if (roles.isEmpty()) {
            throw notFoundException();
        }

        WorkspaceRole role = roles.get(0);
        role.setUserRole(Role.valueOf(jsonRole.getString("role")));
    }

    @GET
    @Path("required")
    @AllowedTo({Role.ALL})
    public JsonArray getSlots(
            @QueryParam("classifier") String firstClassifier,
            @QueryParam("doubleClassifier") @DefaultValue("false") boolean doubleClassifier) {

        if ("project".equals(firstClassifier)) {
            if (doubleClassifier) {
                return calculationConverter.toJsonFromProjectDoubleGourpped(
                        calculationsService.getServicesUsedInProjectsDoubleGrouped(
                                getCurrentWorkspace(), getCurrentWorkspace().getProjects())
                );
            } else {
                return calculationConverter.toJsonFromProject(
                        calculationsService.getServicesUsedInProjects(
                                getCurrentWorkspace(), getCurrentWorkspace().getProjects()
                        ));
            }
        } else if ("service".equals(firstClassifier)) {
            if (doubleClassifier) {
                return calculationConverter.toJsonFromServiceDoubleGourpped(
                        calculationsService.getProjectsUsingServicesDoubleGrouped(
                                getCurrentWorkspace(), getCurrentWorkspace().getServices()));
            } else {
                return calculationConverter.toJsonFromService(
                        calculationsService.getProjectsUsingServices(
                                getCurrentWorkspace(), getCurrentWorkspace().getServices())
                );
            }
        }

        throw new InvalidRequestException(
                "invalid qualifier given:" + Optional.ofNullable(firstClassifier).orElse("none"));

    }

    @GET
    @Path("/available")
    @AllowedTo({Role.ALL})
    public JsonArray getSlotsForServices() {

        List<Service> services = em.createNamedQuery(Service.fetchServices)
                .setParameter("workspace", getCurrentWorkspace())
                .getResultList();

        return serviceConverter.toJsonWithSlots(services);

    }

    @GET
    @Path("required/simpleslots")
    @AllowedTo({Role.ALL})
    public JsonArray getRequiredSimpleSlots() {

        Map<Project, List<BigDecimal>> slots = null;
        slots = slotsFacade.getCalculatedResourceSlots(getCurrentWorkspace());
        return projectConverter.toJsonWithSimpleSlots(slots);
    }
    
    @GET
    @Path("available/simpleslots")
    @AllowedTo({Role.ALL})
    public JsonArray getAvailableSimpleSlots() {

        Map<Service, List<BigDecimal>> slots = null;
        slots = slotsFacade.getCalculatedServiceSlots(getCurrentWorkspace());
        return serviceConverter.toJsonWithSimpleSlots(slots);
    }

    @GET
    @Path("simpleslots")
    @AllowedTo({Role.ALL})
    public JsonObject getSimpleSlots() {

        Map<Project, List<BigDecimal>> resourceSlots = slotsFacade.getCalculatedResourceSlots(getCurrentWorkspace());
        Map<Service, List<BigDecimal>> serviceSlots = slotsFacade.getCalculatedServiceSlots(getCurrentWorkspace());

        JsonObject object = Json.createObjectBuilder()
                .add("projects", projectConverter.toJsonWithSimpleSlots(resourceSlots))
                .add("services", serviceConverter.toJsonWithSimpleSlots(serviceSlots))
                .build();

        return object;
    }
}
