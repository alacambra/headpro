package io.headpro.workspace.boundary;

import io.headpro.PathExpressions;
import io.headpro.security.boundary.AllowedTo;
import io.headpro.security.boundary.Role;
import io.headpro.security.boundary.Secured;
import io.headpro.slots.entity.Slot;
import io.headpro.workspace.entity.Project;
import io.headpro.slots.entity.ResourceSlot;
import io.headpro.time.boundary.TimeRange;
import io.headpro.workspace.entity.ProjectBean;
import io.headpro.workspace.entity.Service;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.json.Json;

@Path("workspace/" + PathExpressions.workspaceIdExpr + "/project/" + PathExpressions.projectIdExpr)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
@Secured(Secured.RoleOver.WORKSPACE)
public class ProjectResource extends AbstractWorkspaceResource {

    @Inject
    ProjectConverter projectConverter;

    @Inject
    ServiceConverter serviceConverter;

    @Inject
    Instance<ProjectBean> projectBeans;

    @GET
    @AllowedTo(Role.ALL)
    public JsonObject getProject() {
        return projectConverter.toJson(getCurrentProject());
    }

    @PUT
    @AllowedTo({Role.MEMBER, Role.OWNER})
    public Response updateProject(JsonObject json) {

        Project currentProject = getCurrentProject();
        ProjectBean currentProjectBean = projectBeans.get().setProject(currentProject);

        Project updateProject = projectConverter.fromJson(json);
        Response response = Response.noContent().build();

        currentProjectBean.setName(updateProject.getName())
                .setStartDate(updateProject.getStartDate())
                .setEndDate(updateProject.getEndDate())
                .setStep(updateProject.getStep())
                .setActive(updateProject.getActive());

        TimeRange slotsTimeRange = currentProjectBean.getSlotsMaxTimeRange();
        TimeRange updatedProjectTimeRange = timeRangeBuilders
                .get()
                .fromDates(currentProjectBean.getStartDate(), currentProjectBean.getEndDate())
                .build();

        if (updatedProjectTimeRange.endsBefore(slotsTimeRange) && updatedProjectTimeRange.overlapWith(slotsTimeRange)) {

            TimeRange diffTimeRange = updatedProjectTimeRange.getDifferentialTimeRange(slotsTimeRange);
            currentProjectBean.setEndDate(new Date(diffTimeRange.moveStart(-1).getEndDateMilis()));

            JsonObject jsonObject = Json.createObjectBuilder()
                    .add("desired", updatedProjectTimeRange.toJson())
                    .add("final", currentProjectBean.getProjectTimeRange().toJson())
                    .add("totalDiff", diffTimeRange.moveStart(1).toJson())
                    .add("diffs", currentProjectBean.getDifferentialTimeRangesAsJson(updatedProjectTimeRange, ProjectBean.DiffType.INNER))
                    .build();

            response = Response.ok(jsonObject).build();

        }

        currentProjectBean.merge();
        return response;
    }

    @DELETE
    @AllowedTo({Role.MEMBER, Role.OWNER})
    public void deleteProject() {
        em.remove(getCurrentProject());
    }

    @GET
    @Path("slots")
    @AllowedTo(Role.ALL)
    public String getSlots(@QueryParam("services") @DefaultValue("false") boolean withServices) {

        if (withServices) {
            ProjectBean pb =  projectBeans.get().setProject(getCurrentProject());
            return projectConverter.basicProjectConversion(getCurrentProject()).add("services",serviceConverter.toJsonWithSlots(pb.getServiceWithResourceSlots())).build().toString();
        } else {

            List<JsonObject> slots = (List<JsonObject>) em.createNamedQuery(ResourceSlot.fetchProjectSlots)
                    .setParameter("project", getCurrentProject())
                    .getResultList();

            return slots.stream()
                    .map(slot -> slotsConverter.toJson((ResourceSlot) slot)).collect(Collectors.toList()).toString();
        }
    }

    @POST
    @Path("slot")
    @AllowedTo({Role.MEMBER, Role.OWNER})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addSlot(@Context UriInfo uriInfo, JsonObject jsonSlot) {

        ResourceSlot slot = slotsConverter.fromJson(jsonSlot, ResourceSlot.class);

        slot.setProject(getCurrentProject());
        slot = (ResourceSlot) slotsFacade.insertOrUpdateSlots(slot);

        URI uri = uriInfo.getBaseUriBuilder()
                .path(ProjectResource.class, "getSlots")
                .resolveTemplate(PathExpressions.workspaceId, getCurrentWorkspaceId())
                .resolveTemplate(PathExpressions.projectId, getCurrentProjectId())
                .path(String.valueOf(slot.getId()))
                .build();

        return Response.created(uri).build();
    }

    private Project getCurrentProject() {
        return currentResource.getResourceOrException(Project.class);
    }

    private Integer getCurrentProjectId() {
        return (Integer) currentResource.getResourceId(Project.class);
    }
}
