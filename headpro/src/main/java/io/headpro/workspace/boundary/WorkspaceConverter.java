package io.headpro.workspace.boundary;

import io.headpro.workspace.entity.Workspace;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.validation.constraints.NotNull;
import static io.headpro.workspace.boundary.ResourceView.*;

public class WorkspaceConverter {

    @Inject
    ProjectConverter projectConverter;

    @Inject
    ServiceConverter serviceConverter;

    public Workspace fromJson(JsonObject jsonWs) {
       return fromJson(jsonWs, new Workspace());
    }

    public Workspace fromJson(JsonObject jsonWs, Workspace workspace) {

        if (jsonWs.containsKey("id")) {
            if (workspace.getId() == null) {
                workspace.setId(jsonWs.getInt("id"));
            }else if(workspace.getId() != jsonWs.getInt("id")){
                throw new RuntimeException("ids mismatch");
            }
        }

        if (jsonWs.containsKey("title")) {
            workspace.setTitle(jsonWs.getString("title"));
        }

        return workspace;
    }

    public JsonObject toJson(Workspace workspace) {
        return basicWorkspaceConversion(workspace).build();
    }

    public JsonObject toJson(Workspace workspace, @NotNull String view) {

        switch (view) {
            case slots:
                return toJsonWithResourcesAndSlots(workspace);
            case resources:
                return toJsonWithResources(workspace);
            default:
                return basicWorkspaceConversion(workspace).build();
        }
    }

    public JsonObject toJsonWithResources(Workspace workspace) {

        JsonObjectBuilder builder = basicWorkspaceConversion(workspace)
                .add("projects", projectConverter.toJson(workspace.getProjects()))
                .add("services", serviceConverter.toJson(workspace.getServices()));

        return builder.build();

    }

    public JsonObject toJsonWithResourcesAndSlots(Workspace workspace) {

        JsonObjectBuilder builder = basicWorkspaceConversion(workspace)
                .add("projects", projectConverter.toJsonWithSlots(workspace.getProjects()))
                .add("services", serviceConverter.toJsonWithSlots(workspace.getServices()));

        return builder.build();

    }

    JsonObjectBuilder basicWorkspaceConversion(Workspace workspace) {
        return Json.createObjectBuilder()
                .add("id", workspace.getId())
                .add("title", workspace.getTitle());
    }

}
