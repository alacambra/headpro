package io.headpro.scenario.loader;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alacambra
 */
public class TestFirmaScenario extends Scenario {

    @Override
    public List<String> getProjects() {

        List<String> projects = new ArrayList<>();

        projects.add("Projekt I");
        projects.add("Projekt II");
        projects.add("Projekt III");
        projects.add("Projekt IV");
        projects.add("Projekt V");
        projects.add("Projekt VI");
        projects.add("Projekt VII");
        projects.add("Person VIII");
        projects.add("Person IX");
        projects.add("Person X");
        projects.add("Projekt I");
        projects.add("Projekt II");

        return projects;
    }

    @Override
    public List<String> getServices() {
        List<String> services = new ArrayList<>();

        services.add("Person A");
        services.add("Person B");
        services.add("Person C");
        services.add("Person D");
        services.add("Person E");
        services.add("Person F");
        services.add("Person G");
        services.add("Person H");
        services.add("Person I");
        services.add("Person J");
        services.add("Person K");
        services.add("Person L");
        services.add("Person M");
        services.add("Person N");
        services.add("Person O");
        services.add("Person P");
        services.add("Person Q");
        services.add("Person R");
        services.add("Freier Mitarbeiter A");
        services.add("Freier Mitarbeiter B");
        services.add("Freier Mitarbeiter C");
        services.add("Freier Mitarbeiter D");
        services.add("Freier Mitarbeiter E");
        services.add("Raum 1");
        services.add("Raum 2");
        services.add("Raum 3");
        services.add("Raum 4");
        services.add("Raum 5");
        services.add("Person A");
        services.add("Person B");
        services.add("Person C");
        services.add("Person D");
        services.add("Person E");
        services.add("Person F");
        services.add("Person G");
        services.add("Person H");
        services.add("Person I");
        services.add("Person J");
        services.add("Person K");
        services.add("Person L");
        services.add("Person M");
        services.add("Person N");
        services.add("Person O");
        services.add("Person P");
        services.add("Person Q");
        services.add("Person R");
        services.add("Freier Mitarbeiter A");
        services.add("Freier Mitarbeiter B");
        services.add("Raum 1");
        services.add("Raum 2");
        services.add("Raum 3");
        services.add("Raum 4");
        services.add("Raum 5");

        return services;
    }

    @Override
    public String getWorkspaceTitle() {
        return "Headpro GmbH";
    }
}
