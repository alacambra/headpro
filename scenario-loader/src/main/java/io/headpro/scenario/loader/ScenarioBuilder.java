package io.headpro.scenario.loader;

import io.headpro.utils.DatesService;
import io.headpro.utils.LocalDateConverter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

public class ScenarioBuilder {

    private final static Logger LOGGER = Logger.getLogger(ScenarioBuilder.class.getName());
    private final DatesService datesService;

    private final Client client;
    private final WebTarget webTarget;
    private final WebTarget kcTarget;

    private final String pathToProject = "/resources/workspace/{workspaceId}/project";
    private final String pathToService = "/resources/workspace/{workspaceId}/service";
    private final String pathToWorkspace = "/resources/workspace/{workspaceId}";
    private final String pathToAccount = "/resources/account";
    private final String pathToAccountWorkspace = "/resources/account/{accountId}/workspace";

    private final String pathToUser = "/resources/user";

    private final String username;
    private final String password;

    private String accessToken;

    public ScenarioBuilder(DatesService datesService, String username, String password, String url, String keycloakUrl) {

        this.datesService = datesService;
        this.datesService.init();
        
        this.username = username;
        this.password = password;

        client = ClientBuilder.newClient();
        webTarget = client.target(url);
        kcTarget = client.target(keycloakUrl);
    }

    public void create(Scenario scenario) {

        getUserAccessToken();

        int accountId = extractId(createAccount());
        int workspaceId = extractId(createWorkspace(scenario.getWorkspaceTitle(), accountId));

        List<String> projects = scenario.getProjects();
        List<String> services = scenario.getServices();

        List<Integer> servicesIDs = new ArrayList<>();

        int offset = datesService.getDateIndex(LocalDate.now());
        int slotsPerProject = scenario.getSlotsPerProject();
        int slotDuration = scenario.getSlotsDuration();

        services.stream().parallel()
                .map((service) -> createService(workspaceId, service))
                .map((rs) -> rs.getLocation().getPath())
                .forEach((servicePath) -> {

                    int serviceId = extractId(servicePath);

                    servicesIDs.add(serviceId);

                    for (int j = 0; j < slotsPerProject; j++) {
                        addSlot(servicePath, offset + j * slotDuration, slotDuration, serviceId);
                    }

                });

        projects.stream().parallel()
                .map((project) -> createProject(workspaceId, project, LocalDate.now(), slotDuration * slotsPerProject))
                .map((rp) -> rp.getLocation().getPath())
                .forEach((projectPath) -> {
                    servicesIDs.stream().parallel().forEach((serviceId) -> {
                        for (int j = 0; j < slotsPerProject; j++) {
                            addSlot(projectPath, j * slotDuration, slotDuration, serviceId);
                        }
                    });
                });
    }

    private Invocation.Builder request(WebTarget target) {

        boolean auth = true;

        if (auth) {
            return target.request().header("Authorization", "bearer " + accessToken);
        } else {
            return target.request();
        }
    }

    private Response createAccount() {

        JsonObject account = Json.createObjectBuilder().build();
        Response r = request(webTarget.path(pathToUser)).post(Entity.json(account));

        assertResponse(r.getStatus(), 201);

        return r;
    }

    private Response createWorkspace(String title, int accountId) {
        JsonObject project = Json.createObjectBuilder().add("title", title).build();

        Invocation.Builder builder = request(webTarget.path(pathToAccountWorkspace).resolveTemplate("accountId", accountId));

        Entity<JsonObject> entity = Entity.json(project);
        Response r = builder.post(entity);

        assertResponse(r.getStatus(), 201);
        return r;
    }

    private String addSlot(String resourcePath, int startPosition, int duration, int serviceId, boolean randomValuePerStep) {

        Random rand = new Random();

        int rv = 1;

        if (randomValuePerStep && false) {
            rv = rand.nextInt(999);
        }

        JsonObject slot = Json.createObjectBuilder()
                .add("startPosition", startPosition)
                .add("duration", duration)
                .add("valuePerStep", rv)
                .add("service", serviceId)
                .build();

        Response r = request(webTarget.path("{pathToResource}/slot")
                .resolveTemplateFromEncoded("pathToResource", resourcePath))
                .post(Entity.json(slot));

        assertResponse(r.getStatus(), 201);

        return r.getLocation().getPath();
    }

    private String addSlot(String projectPath, int startPosition, int duration, int serviceId) {
        return addSlot(projectPath, startPosition, duration, serviceId, true);
    }

    private int extractId(String path) {
        return Integer.parseInt(path.substring(path.lastIndexOf("/") + 1));
    }

    private int extractId(Response response) {
        String path = response.getLocation().getPath();
        return Integer.parseInt(path.substring(path.lastIndexOf("/") + 1));
    }

    private Response createProject(int workspaceId, String title, LocalDate startDate, int weeks) {

        JsonObject project = Json.createObjectBuilder().add("name", title)
                .add("startDate", LocalDateConverter.toDate(startDate).getTime())
                .add("endDate", LocalDateConverter.toDate(startDate.plusWeeks(weeks)).getTime())
                .build();

        Response r = request(webTarget.path(pathToProject).resolveTemplate("workspaceId", workspaceId)).post(Entity.json(project));

        assertResponse(r.getStatus(), 201);

        return r;
    }

    private Response createService(int workspaceId, String serviceName) {

        JsonObject service = Json.createObjectBuilder().add("name", serviceName).build();
        Response r = request(webTarget.path(pathToService).resolveTemplate("workspaceId", workspaceId)).post(Entity.json(service));
        assertResponse(r.getStatus(), 201);

        return r;
    }

    private void getUserAccessToken() {

        Form form = new Form()
                .param("grant_type", "password")
                .param("client_id", "headprosystemtest")
                .param("username", username)
                .param("password", password);

        String path = "auth/realms/headpro/protocol/openid-connect/token";

        Response r = kcTarget.path(path).request().post(Entity.form(form));
        JsonObject accessData = r.readEntity(JsonObject.class);

        assertResponse(r.getStatus(), 200);
        assertResponse(accessData.containsKey("access_token"), true);

        accessToken = accessData.getString("access_token");

    }

    private void assertResponse(Object real, Object expected) {
        if (!real.equals(expected)) {
            LOGGER.log(Level.INFO, "real value is {0} and expected is {1}", new Object[]{real, expected});
        }
    }
}
