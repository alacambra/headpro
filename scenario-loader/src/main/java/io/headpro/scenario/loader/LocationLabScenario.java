/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.scenario.loader;

import java.util.ArrayList;
import java.util.List;

public class LocationLabScenario extends Scenario {

    @Override
    public List<String> getProjects() {
        
        List<String> projects = new ArrayList<>();

        projects.add("Rathausplatz HH");
        projects.add("Flughafen Hahn");
        projects.add("A&B Facilities");
        projects.add("Umgehung Fulda");
        projects.add("Stadtmitte Bonn");
        projects.add("Fraport AG");
        projects.add("Bad Homburg");
        projects.add("Kassel Neubaugebiet");

        return projects;
    }

    @Override
    public List<String> getServices() {
        List<String> services = new ArrayList<>();

        services.add("Teamleiter");
        services.add("CAD Team");
        services.add("Enviroment Team");
        services.add("Building Team");
        services.add("QA Team");
        services.add("Data Team");

        return services;
    }

    @Override
    public String getWorkspaceTitle() {
        return "LocationLab2";
    }
}
