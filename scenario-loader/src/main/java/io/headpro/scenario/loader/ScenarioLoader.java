/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.scenario.loader;

import io.headpro.utils.DatesService;

/**
 *
 * @author alacambra
 */
public class ScenarioLoader {
    
    public static void main(String[] args) {
        
//        ScenarioBuilder scenarioBuilder = new ScenarioBuilder(
//                new DatesService(), "a", "a", "http://localhost:7072", "http://localhost:7071");
        
        ScenarioBuilder scenarioBuilder = new ScenarioBuilder(
                new DatesService(), "a", "a", "http://localhost:8080", "http://localhost:6071");
        
        scenarioBuilder.create(new TestFirmaScenario());
        scenarioBuilder.create(new LocationLabScenario());
    }
    
}
