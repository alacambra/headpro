package io.headpro.scenario.loader;

import java.util.List;

public abstract class Scenario {

    public abstract String getWorkspaceTitle();

    public abstract List<String> getProjects();

    public abstract List<String> getServices();

    public int getSlotsPerProject() {
        return 5;
    }

    public int getSlotsDuration() {
        return 5;
    }

    protected int extractId(String path) {
        return Integer.parseInt(path.substring(path.lastIndexOf("/") + 1));
    }
}
