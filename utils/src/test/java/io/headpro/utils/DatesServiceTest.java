package io.headpro.utils;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DatesServiceTest {

    DatesService cut;

    @Before
    public void setUp() {
        cut = new DatesService();
        cut.init();
    }

    @Test
    public void testGetDateIndex() throws Exception {

        LocalDate currentDate = cut.startDate;

        for (int i = 0, currentYear = cut.getStartYear(); currentYear < cut.getEndYear() + 1; currentYear = currentDate.getYear(), i++) {

            assertThat(cut.getDateIndex(currentDate), is(i));
            currentDate = currentDate.plusWeeks(1);
        }
    }

    @Test
    @Ignore
    public void testGetIndexOfDate() throws Exception {
        LocalDate currentDate = cut.startDate;

        for (int i = 0, currentYear = cut.getStartYear(); currentYear < cut.getEndYear() + 1; currentYear = currentDate.getYear(), i++) {

            assertThat(cut.getDateForIndex(i), is(currentDate));
            currentDate = currentDate.plusWeeks(1);

        }
    }

    @Test
    public void testGetIndexOfNotIndexedDate() throws Exception {

        assertThat(cut.dateIndex.containsKey(cut.startDate), is(false));
        assertThat(cut.getDateIndex(cut.startDate), is(0));
        assertThat(cut.getDateIndex(cut.startDate.plusWeeks(1)), is(1));

    }

    @Test
    public void testGetOffset(){
        LocalDate startDate = cut.startDate;
        LocalDate endDate = startDate.plusWeeks(3);
        assertThat(cut.getOffset(startDate, endDate), is(3));
    }
}