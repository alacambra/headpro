package io.headpro.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author alacambra
 */
public class LocalDateConverter {

    private static final String defaultZone = "GMT+2";
    static {
        TimeZone.setDefault(TimeZone.getTimeZone(defaultZone));
    }

    public static Date toDate(LocalDate date) {
        return toDate(date, ZoneId.of(defaultZone));
    }

    public static Date toDate(LocalDate date, ZoneId zoneId) {
        Instant instant = date.atStartOfDay().atZone(zoneId).toInstant();
        return Date.from(instant);
    }

    public static LocalDate toLocalDate(Date date) {
        return toLocalDate(new Date(date.getTime()), ZoneId.of(defaultZone));
    }

    public static LocalDate toLocalDate(Date date, ZoneId zoneId) {
        if (date instanceof java.sql.Date) throw new RuntimeException("sql date not accepted");
        return date.toInstant().atZone(zoneId).toLocalDate();
    }

    public static LocalDate toLocalDate(java.sql.Date date) {
        return toLocalDate(date, ZoneId.of(defaultZone));
    }

    public static LocalDate toLocalDate(java.sql.Date date, ZoneId zoneId) {
        return toLocalDate(new Date(date.getTime()), zoneId);
    }
}