package io.headpro.utils;

import java.time.LocalDate;

/**
 * Created by alacambra on 03/11/15.
 */
public class ExceptionService {

    private ExceptionService() {
    }

    public static DateIndexOutOfRangeException dateOutOfRange(LocalDate date) {
        return new DateIndexOutOfRangeException("the given date (" + date + ") has not been found. Out of range");
    }

    public static RuntimeException dateIndexOutOfRange(int index) {
        return new RuntimeException("the given index (" + index + ") is out of range");
    }

}
