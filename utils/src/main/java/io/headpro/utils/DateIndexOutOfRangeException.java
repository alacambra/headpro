package io.headpro.utils;

/**
 * Created by alacambra on 28/12/15.
 */
public class DateIndexOutOfRangeException extends RuntimeException {
    public DateIndexOutOfRangeException() {
    }

    public DateIndexOutOfRangeException(String message) {
        super(message);
    }

    public DateIndexOutOfRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public DateIndexOutOfRangeException(Throwable cause) {
        super(cause);
    }

    public DateIndexOutOfRangeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
