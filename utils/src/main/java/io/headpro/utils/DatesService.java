package io.headpro.utils;

import static io.headpro.utils.ExceptionService.dateIndexOutOfRange;
import static io.headpro.utils.ExceptionService.dateOutOfRange;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Singleton
public class DatesService {

    public static final int startYear = 2010;
    public static final int endYear = 2100;

    Map<LocalDate, Integer> dateIndex;
    List<LocalDate> indexDate;
    public static final LocalDate startDate = LocalDate.of(startYear, Month.JANUARY, 1);

    @PostConstruct
    public void init() {

        dateIndex = new HashMap<>();
        indexDate = new ArrayList<>();

        LocalDate currentDate = getPreviousStartDay(startDate);

        for (int i = 0, currentYear = startDate.getYear(); currentYear < endYear + 1; currentYear = currentDate.getYear(), i++) {
            dateIndex.put(currentDate, i);
            indexDate.add(currentDate);
            currentDate = currentDate.plusWeeks(1);
        }
        
        dateIndex = Collections.unmodifiableMap(dateIndex);
        indexDate = Collections.unmodifiableList(indexDate);
    }

    public int getDateIndex(@NotNull LocalDate date) {

        if (dateIndex.containsKey(date)) {
            return dateIndex.get(date);
        } else if (startYear <= date.getYear() && date.getYear() <= endYear) {
            return dateIndex.get(getPreviousStartDay(date));
        }

        throw dateOutOfRange(date);
    }

    public int getDateIndex(@NotNull Date date) {
        return getDateIndex(LocalDateConverter.toLocalDate(date));
    }

    public LocalDate getDateForIndex(int index) {

        return dateIndex.entrySet().stream()
                .filter(entry -> entry.getValue().equals(index))
                .findAny()
                .orElseThrow(() -> dateIndexOutOfRange(index))
                .getKey();

    }
    
    public int getStartYear() {
        return startDate.getYear();
    }

    public int getEndYear() {
        return endYear;
    }

    public List<LocalDate> getAbsoluteIndexesInRange(@NotNull LocalDate starDate, @NotNull LocalDate endDate) {
        int start = dateIndex.get(starDate);
        int end = dateIndex.get(endDate);
        
        return Collections.unmodifiableList(indexDate.subList(start, end));
    }

    public int getOffset(@NotNull LocalDate starDate, @NotNull LocalDate endDate) {
        return getDateIndex(endDate) - getDateIndex(starDate);
    }

    public int getOffset(@NotNull Date starDate, @NotNull Date endDate) {
        return getOffset(LocalDateConverter.toLocalDate(starDate), LocalDateConverter.toLocalDate(endDate));
    }

    /**
     * Search for then first indexed date (if date is a thursday Date, returns the previous monday Date)
     *
     * @return
     */
    protected LocalDate getPreviousStartDay(LocalDate date) {
        return date.minusDays(date.getDayOfWeek().getValue() - 1);
    }
}
