package io.headpro.st;

import java.net.URI;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class KeycloakUsersIT {

    protected Client cut;
    protected WebTarget kcTarget;
    protected String accessToken;

    @Before
    public void setUp() {

        cut = ClientBuilder.newClient();
        kcTarget = cut.target("http://localhost:6071");

        getAccessToken();
    }

    @Test
    public void testCreateUser() {

        long random = System.nanoTime();
        String username = "mynewuser";
        String password = "psw";

        JsonObject payload = Json.createObjectBuilder()
                .add("enabled", true)
                .add("requiredActions", Json.createArrayBuilder().build())
                .add("username", username)
                .add("email", "test-" + random + "@test.com")
                .add("firstName", "name-" + random)
                .add("lastName", "lname-" + random)
                .add("emailVerified", "true")
                .build();

        Response r = kcTarget.path("/auth/admin/realms/headpro/users")
                .request()
                .header("Authorization", "bearer " + accessToken)
                .post(Entity.json(payload));

        assertThat(r.getStatus(), is(201));

        URI userUri = r.getLocation();

        payload = Json.createObjectBuilder()
                .add("type", "password")
                .add("value", password)
                .add("temporary", false)
                .build();

        r = kcTarget.path(userUri.getPath()).path("reset-password").request()
                .header("Authorization", "bearer " + accessToken)
                .put(Entity.json(payload));

        assertThat(r.getStatus(), is(204));
        
        r = kcTarget.path(userUri.getPath()).request()
                .header("Authorization", "bearer " + accessToken)
                .get();

        System.out.println(r.readEntity(String.class));
        assertThat(r.getStatus(), is(200));
    }

    private void getAccessToken() {

        Form form = new Form()
                .param("grant_type", "password")
                .param("client_id", "hp-user-registration")
                .param("username", "admin")
                .param("password", "lacambra1982.");

        String path = "auth/realms/master/protocol/openid-connect/token";

        Response r = kcTarget.path(path).request().post(Entity.form(form));
        JsonObject accessData = r.readEntity(JsonObject.class);

        assertThat(r.getStatus(), is(200));
        assertThat(accessData.containsKey("access_token"), is(true));

        accessToken = accessData.getString("access_token");
        assertThat(accessToken, notNullValue());

    }
}
