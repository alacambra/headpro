/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.st.boundary;

import javax.json.JsonArray;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;

/**
 *
 * @author alacambra
 */
public class AccountIT extends AbstractSystemTest{
    
    @Test
    public void testAccountCreation(){
        
        Response r = request(tut.path(pathToUser).path("my/accounts")).get();
        assertThat(r.getStatus(), is(200));
        assertThat(r.readEntity(JsonArray.class).size(), is(1));
        
        try {
            Thread.sleep(10);
        } catch (InterruptedException ex) {
        }
        
        r = request(tut.path(pathToUser)).post(Entity.json("{}"));
        assertThat("Not possible to create HP account", r.getStatus(), is(201));
        
        r = request(tut.path(pathToUser).path("my/accounts")).get();
        assertThat(r.getStatus(), is(200));
        assertThat(r.readEntity(JsonArray.class).size(), is(2));
        
        
    }
}
