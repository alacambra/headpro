package io.headpro.st.boundary;

import io.headpro.utils.LocalDateConverter;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.LongStream;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class ProjectResourceIT extends AbstractSystemTest {

    private ProjectResourceIT initializeForLoadTest() {
        ProjectResourceIT p = new ProjectResourceIT();
        p.setUp();
        return p;
    }

    public void performanceTest() throws ExecutionException {

        ExecutorService exec = Executors.newFixedThreadPool(1);
        List<Future<Long>> futures = new ArrayList<>();
        for (int i = 0; i < 200; i++) {

            Callable<Long> cl = () -> {
                Long start = System.currentTimeMillis();
                initializeForLoadTest().testProjectCycle();
                return System.currentTimeMillis() - start;
            };

            Future<Long> f = exec.submit(cl);
            futures.add(f);

            cl = () -> {
                Long start = System.currentTimeMillis();
                initializeForLoadTest().testGetSimpleSlots();
                return System.currentTimeMillis() - start;
            };

            f = exec.submit(cl);
            futures.add(f);

            cl = () -> {
                Long start = System.currentTimeMillis();
                initializeForLoadTest().testResourceSlotsOfProjectCycle();
                return System.currentTimeMillis() - start;
            };

            f = exec.submit(cl);
            futures.add(f);

            cl = () -> {
                Long start = System.currentTimeMillis();
                initializeForLoadTest().testGetSlots();
                return System.currentTimeMillis() - start;
            };

            f = exec.submit(cl);
            futures.add(f);

        }
        System.out.println("Begin results ........");
        LongStream filter = futures.stream()
                .mapToLong(f -> {
                    try {
                        return f.get().longValue();
                    } catch (InterruptedException | ExecutionException ex) {
                    }
                    return 0L;
                })
                .filter((long v) -> {
                    return v != 0;
                });

        System.out.println(filter.average().orElse(-1) + " ms");

    }

    @Test
    public void testProjectCycle() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        ObjectRequestContainer res = createProject(workspaceId);
        r = res.getResponse();
        assertThat(r.getStatus(), is(201));

        String projectPath = r.getLocation().getPath();
        JsonObject project = request(tut.path(projectPath)).get(JsonObject.class);
        assertThat(project, is(notNullValue()));
        assertThat(project.getString("name"), is("projectTitle"));
        int id = project.getInt("id");

        JsonArray slots
                = request(tut.path(pathToProject + "/{id}/slots")
                        .resolveTemplate("workspaceId", workspaceId)
                        .resolveTemplateFromEncoded("id", id))
                .get(JsonArray.class);

        assertThat(slots.size(), is(0));
        String projectName = "new project acc:" + accountId + ", wsId:" + workspaceId + ", user:" + username;
        project = Json.createObjectBuilder().add("name", projectName)
                .add("startDate", LocalDateConverter.toDate(LocalDate.of(2015, Month.OCTOBER, 23)).getTime())
                .add("endDate", LocalDateConverter.toDate(LocalDate.of(2015, Month.OCTOBER, 30)).getTime())
                .build();

        r = request(tut.path(projectPath)).put(Entity.json(project));
        assertThat(r.getStatus(), is(204));

        project = request(tut.path(projectPath)).get(JsonObject.class);

        assertThat(project, is(notNullValue()));
        assertThat(project.getString("name"), is(projectName));
        assertThat(project.getInt("id"), is(id));

        try {
            Thread.sleep(50L);
        } catch (InterruptedException ex) {
        }

        r = request(tut.path(projectPath)).delete();
        assertThat(r.getStatus(), is(204));
    }

    @Test
    public void testProjectNotFound() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());

        r = request(tut.path(pathToProject + "/12321").resolveTemplate("workspaceId", workspaceId)).get();

        assertThat(r.getStatus(), is(404));

        r = request(tut.path(pathToProject + "/12321").resolveTemplate("workspaceId", workspaceId)).delete();
        assertThat(r.getStatus(), is(404));

        r = request(tut.path(pathToProject + "/12321").resolveTemplate("workspaceId", workspaceId))
                .put(Entity.json(Json.createObjectBuilder().build()));

        assertThat(r.getStatus(), is(404));
    }

    @Test
    public void testAddSlot() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        ObjectRequestContainer res = createProject(workspaceId);
        int serviceId = extractId(createService(workspaceId).getResponse().getLocation().getPath());

        JsonObject slot = Json.createObjectBuilder()
                .add("startPosition", 0)
                .add("valuePerStep", 1)
                .add("duration", 5)
                .add("service", serviceId)
                .build();

        String path = res.getResponse().getLocation().getPath();
        r = request(tut.path(path + "/slot"))
                .post(Entity.json(slot));

        assertThat(r.readEntity(String.class), r.getStatus(), is(201));

        r = request(tut.path(path + "/slot")).post(Entity.json(slot));

        assertThat(r.getStatus(), is(201));

    }

    @Test
    public void testResourceSlotsOfProjectCycle() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        r = createProject(workspaceId).getResponse();
        int serviceId = extractId(createService(workspaceId).getResponse().getLocation().getPath());

        String path = r.getLocation().getPath();

        JsonArray arr = null;

        String id = addSlot(path, 0, 15, serviceId);
        int firstId = Integer.parseInt(id.substring(id.lastIndexOf("/") + 1));

        arr = getSlotsOfResource(path);
        assertThat(arr.size(), is(1));

        /**
         * Create new Slot
         */
        id = addSlot(path, 0, 5, serviceId);

        int secondId = Integer.parseInt(id.substring(id.lastIndexOf("/") + 1));
        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(2));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(firstId));
        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(-1));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(secondId));

        /**
         * Create new Slot
         */
        id = addSlot(path, 10, 10, serviceId);
        int thirdId = Integer.parseInt(id.substring(id.lastIndexOf("/") + 1));

        arr = getSlotsOfResource(path);
        assertThat(arr.size(), is(3));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(firstId));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(secondId));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(thirdId));
        assertThat(arr.getJsonObject(2).getInt("previousSlot"), is(firstId));
        assertThat(arr.getJsonObject(2).getInt("nextSlot"), is(-1));

        /**
         * Create new Slot: split
         */
        id = addSlot(path, 7, 5, serviceId);
        int fourthId = Integer.parseInt(id.substring(id.lastIndexOf("/") + 1));

        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(4));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(firstId));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(secondId));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(fourthId));
        assertThat(arr.getJsonObject(2).getInt("previousSlot"), is(firstId));
        assertThat(arr.getJsonObject(2).getInt("nextSlot"), is(thirdId));
        assertThat(arr.getJsonObject(3).getInt("previousSlot"), is(fourthId));
        assertThat(arr.getJsonObject(3).getInt("nextSlot"), is(-1));

        /**
         * Create new Slot: Overwrite
         */
        id = addSlot(path, 0, 50, serviceId);
        int fifthId = Integer.parseInt(id.substring(id.lastIndexOf("/") + 1));

        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(1));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(-1));
    }

    @Test
    public void testReduceProjectTimeRangeWithOverlappedSlots() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());
        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        r = createProject(workspaceId, "project name", LocalDate.now(), 5).getResponse();
        String projectPath = r.getLocation().getPath();
        int serviceId = extractId(createService(workspaceId).getResponse().getLocation().getPath());

        String path = r.getLocation().getPath();

        addSlot(path, 0, 2, serviceId);
        addSlot(path, 2, 2, serviceId);
        addSlot(path, 4, 2, serviceId);

        JsonObject project = Json.createObjectBuilder().add("name", "project name")
                .add("startDate", LocalDateConverter.toDate(LocalDate.now()).getTime())
                .add("endDate", LocalDateConverter.toDate(LocalDate.now().plusWeeks(4)).getTime())
                .build();

        r = request(tut.path(projectPath)).put(Entity.json(project));
        assertThat(r.getStatus(), is(200));

        JsonObject tr = r.readEntity(JsonObject.class);
        System.out.println(tr.toString());
        assertThat(tr.getJsonObject("totalDiff").getInt("rangeLength"), is(1));

    }

    @Test
    public void testResourceSlotsOfProjectCycle_mergeAndUnions() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        r = createProject(workspaceId).getResponse();
        int serviceId = extractId(createService(workspaceId).getResponse().getLocation().getPath());

        String path = r.getLocation().getPath();

        JsonArray arr = null;

        String id = addSlot(path, 5, 15, serviceId, false);
        int firstId = Integer.parseInt(id.substring(id.lastIndexOf("/") + 1));

        arr = getSlotsOfResource(path);
        assertThat(arr.size(), is(1));

        /**
         * Create new Slot
         */
        id = addSlot(path, 0, 5, serviceId, false);

        int secondId = Integer.parseInt(id.substring(id.lastIndexOf("/") + 1));
        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(1));
        assertThat(arr.getJsonObject(0).getInt("duration"), is(20));
        assertThat(arr.getJsonObject(0).getInt("startPosition"), is(0));
    }

    @Test
    public void testAddContiguousSlots() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        r = createProject(workspaceId).getResponse();
        int serviceId = extractId(createService(workspaceId).getResponse().getLocation().getPath());

        String path = r.getLocation().getPath();

        JsonArray arr;

        String id = addSlot(path, 0, 5, serviceId);
        int firstId = extractId(id);

        arr = getSlotsOfResource(path);
        assertThat(arr.size(), is(1));

        /**
         * Create new Slot
         */
        id = addSlot(path, 5, 5, serviceId);

        int secondId = extractId(id);
        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(2));
        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(secondId));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(firstId));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(-1));

        /**
         * Create new Slot
         */
        id = addSlot(path, 10, 10, serviceId);
        int thirdId = extractId(id);

        arr = getSlotsOfResource(path);
        assertThat(arr.size(), is(3));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(secondId));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(firstId));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(thirdId));
        assertThat(arr.getJsonObject(2).getInt("previousSlot"), is(secondId));
        assertThat(arr.getJsonObject(2).getInt("nextSlot"), is(-1));

        /**
         * Create new Slot: split
         */
        id = addSlot(path, 20, 10, serviceId);
        int fourthId = extractId(id);

        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(4));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(secondId));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(firstId));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(thirdId));
        assertThat(arr.getJsonObject(2).getInt("previousSlot"), is(secondId));
        assertThat(arr.getJsonObject(2).getInt("nextSlot"), is(fourthId));
        assertThat(arr.getJsonObject(3).getInt("previousSlot"), is(thirdId));
        assertThat(arr.getJsonObject(3).getInt("nextSlot"), is(-1));

    }

    @Test
    public void testGetSlots() {
        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        String projecPath = createProject(workspaceId).getResponse().getLocation().getPath();
        int serviceId = extractId(createService(workspaceId).getResponse().getLocation().getPath());

        addSlot(projecPath, 0, 10, serviceId);

        r = request(tut.path(pathToWorkspace + "/required").queryParam("classifier", "project")
                .resolveTemplate("workspaceId", workspaceId))
                .get();

        assertThat(r.getStatus(), is(200));

        JsonArray json = r.readEntity(JsonArray.class);
        assertThat(json.size(), is(1));
    }

    @Test
    public void testGetSimpleSlots() {
        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());

        Response prResponse = createProject(workspaceId).getResponse();
        Response serviceResponse = createService(workspaceId).getResponse();

        addSlot(prResponse.getLocation().getPath(), 0, 5, extractId(serviceResponse.getLocation().getPath()));

        r = request(tut.path(pathToWorkspace + "/required/simpleslots")
                .resolveTemplate("workspaceId", workspaceId))
                .get();

        assertThat(r.getStatus(), is(200));

        JsonArray arr = r.readEntity(JsonArray.class);
        assertThat(arr.size(), not(0));

        int duration = arr.getJsonObject(0).getInt("endIndex") - arr.getJsonObject(0).getInt("startIndex");
        assertThat(arr.getJsonObject(0).getJsonArray("slots").size(), is(duration));
    }
}
