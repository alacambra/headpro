/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.st.boundary;

import io.headpro.utils.DatesService;
import io.headpro.utils.LocalDateConverter;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.core.Response;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 *
 * @author alacambra
 */
public class TimeResourceIT extends AbstractSystemTest {

    String pathToResource = "/resources/time";

    @Test
    public void testGetIndexForDate() {
        Long timestamp = LocalDateConverter.toDate(DatesService.startDate).getTime();
        Response r = request(tut.path(pathToResource + "/{timestamp}").resolveTemplate("timestamp", timestamp)).get();

        sleep(5);
        
        assertThat(r.getStatus(), is(200));
        assertThat(r.readEntity(JsonObject.class).getInt("index"), is(0));

        timestamp = LocalDateConverter.toDate(DatesService.startDate.plusWeeks(10)).getTime();
        r = request(tut.path(pathToResource + "/{timestamp}").resolveTemplate("timestamp", timestamp)).get();

        assertThat(r.getStatus(), is(200));
        assertThat(r.readEntity(JsonObject.class).getInt("index"), is(10));

        timestamp = LocalDateConverter.toDate(DatesService.startDate.plusDays(10)).getTime();
        r = request(tut.path(pathToResource + "/{timestamp}").resolveTemplate("timestamp", timestamp)).get();

        assertThat(r.getStatus(), is(200));
        assertThat(r.readEntity(JsonObject.class).getInt("index"), is(2));
    }
}
