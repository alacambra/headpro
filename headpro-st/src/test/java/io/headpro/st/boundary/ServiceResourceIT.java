package io.headpro.st.boundary;

import org.junit.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class ServiceResourceIT extends AbstractSystemTest {

    @Test
    public void serviceCycleTest() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        JsonObject service = Json.createObjectBuilder().add("name", "serviceTitle").build();
        r = request(tut.path(pathToService).resolveTemplate("workspaceId", workspaceId)).post(Entity.json(service));
        assertThat(r.getStatus(), is(201));

        service = request(tut.path(r.getLocation().getPath())).get(JsonObject.class);
        assertThat(service, is(notNullValue()));
        assertThat(service.getString("name"), is("serviceTitle"));
        int id = service.getInt("id");

        service = Json.createObjectBuilder().add("name", "new service").build();
        service = request(tut.path(r.getLocation().getPath())).put(Entity.json(service)).readEntity(JsonObject.class);
        assertThat(service, is(notNullValue()));
        assertThat(service.getString("name"), is("new service"));
        assertThat(service.getInt("id"), is(id));

        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
        }
        r = request(tut.path(r.getLocation().getPath())).delete();
        assertThat(r.getStatus(), is(204));
    }

    @Test
    public void serviceNotFound() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        r = request(tut.path("/resources/workspace/" + workspaceId + "/service/12321")).get();
        assertThat(r.getStatus(), is(404));

        r = request(tut.path("/resources/workspace/" + workspaceId + "/service/12321")).delete();
        assertThat(r.getStatus(), is(404));

        r = request(tut.path("/resources/workspace/" + workspaceId + "/service/12321"))
                .put(Entity.json(Json.createObjectBuilder().build()));
        
        assertThat(r.getStatus(), is(404));
    }

    @Test
    public void testResourceSlotsOfServiceCycle() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());

        int workspaceId = extractId(createWorkspace(accountId).getResponse().getLocation().getPath());
        r = createService(workspaceId).getResponse();
        String path = r.getLocation().getPath();
        int serviceId = extractId(path);

        JsonArray arr = null;

        int firstId = extractId(addSlot(path, 0, 15, serviceId));

        arr = getSlotsOfResource(path);
        assertThat(arr.size(), is(1));

        /**
         * Create new Slot
         */

        int secondId = extractId(addSlot(path, 0, 5, serviceId));
        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(2));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(firstId));
        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(-1));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(secondId));

        /**
         * Create new Slot
         */
        int thirdId = extractId(addSlot(path, 10, 10, serviceId));

        arr = getSlotsOfResource(path);
        assertThat(arr.size(), is(3));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(firstId));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(secondId));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(thirdId));
        assertThat(arr.getJsonObject(2).getInt("previousSlot"), is(firstId));
        assertThat(arr.getJsonObject(2).getInt("nextSlot"), is(-1));

        /**
         * Create new Slot: split
         */
        int fourthId = extractId(addSlot(path, 7, 5, serviceId));

        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(4));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(firstId));
        assertThat(arr.getJsonObject(1).getInt("previousSlot"), is(secondId));
        assertThat(arr.getJsonObject(1).getInt("nextSlot"), is(fourthId));
        assertThat(arr.getJsonObject(2).getInt("previousSlot"), is(firstId));
        assertThat(arr.getJsonObject(2).getInt("nextSlot"), is(thirdId));
        assertThat(arr.getJsonObject(3).getInt("previousSlot"), is(fourthId));
        assertThat(arr.getJsonObject(3).getInt("nextSlot"), is(-1));

        /**
         * Create new Slot: Overwrite
         */
        int fifthId = extractId(addSlot(path, 0, 50, serviceId));

        arr = getSlotsOfResource(path);

        assertThat(arr.size(), is(1));

        assertThat(arr.getJsonObject(0).getInt("previousSlot"), is(-1));
        assertThat(arr.getJsonObject(0).getInt("nextSlot"), is(-1));
    }
}
