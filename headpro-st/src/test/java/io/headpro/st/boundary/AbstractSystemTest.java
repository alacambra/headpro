package io.headpro.st.boundary;

import io.headpro.utils.LocalDateConverter;
import java.net.URI;
import org.junit.Before;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.time.LocalDate;
import java.time.Month;
import java.util.Random;
import javax.ws.rs.client.Invocation;

import javax.ws.rs.core.Form;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import org.junit.After;
import static org.junit.Assert.assertThat;

public abstract class AbstractSystemTest {

    protected Client cut;
    protected WebTarget tut;
    protected WebTarget kcTarget;

    protected String pathToProject = "/resources/workspace/{workspaceId}/project";
    protected String pathToService = "/resources/workspace/{workspaceId}/service";
    protected String pathToResourceSlot = "/resources/resourceslot";
    protected String pathToWorkspace = "/resources/workspace/{workspaceId}";
    protected String pathToAccount = "/resources/account";
    protected String pathToAccountWorkspace = "/resources/account/{accountId}/workspace";

    protected String pathToUser = "/resources/user";

    protected String username;
    protected String password;

    protected URI kcUserURI;
    protected String adminToken;
    protected String accessToken;

    @Before
    public void setUp() {
        cut = ClientBuilder.newClient();
        tut = cut.target("http://localhost:8080");
        kcTarget = cut.target("http://localhost:6071");

        registerUserInKeycloak();
        getUserAccessToken();
        Response r = request(tut.path(pathToAccount)).get();
        assertThat(r.getStatus(), is(201));
        
        r = request(tut.path(pathToUser).path("my/accounts")).get();
        assertThat(r.getStatus(), is(200));
    }

    @After
    public void tearDown() {
        deleteKCUser();
    }

    private void registerUserInKeycloak() {

        long random = System.nanoTime();
        username = "test-" + random;
        password = "psw";
        adminToken = getAdminAccessToken();

        JsonObject payload = Json.createObjectBuilder()
                .add("enabled", true)
                .add("requiredActions", Json.createArrayBuilder().build())
                .add("username", username)
                .add("email", "test-" + random + "@test.com")
                .add("firstName", "name-" + random)
                .add("lastName", "lname-" + random)
                .add("emailVerified", "true")
                .build();

        Response r = kcTarget.path("/auth/admin/realms/headpro/users")
                .request()
                .header("Authorization", "bearer " + adminToken)
                .post(Entity.json(payload));

        assertThat("Not possible to create user in Keycloak", r.getStatus(), is(201));

        kcUserURI = r.getLocation();

        payload = Json.createObjectBuilder()
                .add("type", "password")
                .add("value", password)
                .add("temporary", false)
                .build();

        r = kcTarget.path(kcUserURI.getPath()).path("reset-password").request()
                .header("Authorization", "bearer " + adminToken)
                .put(Entity.json(payload));

        assertThat("not possible to set user psw on KC", r.getStatus(), is(204));

        JsonArray payloadArr = Json.createArrayBuilder().add(
                Json.createObjectBuilder()
                .add("id", "917d7b94-902f-45ce-8543-ba10e97682ce")
                .add("name", "user")
                .add("scopeParamRequired", false)
                .add("composite", false)
                .build()
        ).build();
        r = kcTarget.path(kcUserURI.getPath()).path("role-mappings/realm").request()
                .header("Authorization", "bearer " + adminToken)
                .post(Entity.json(payloadArr));

        assertThat("not possible to set user role on keycloak", r.getStatus(), is(204));
        
    }

    private void deleteKCUser() {
        Response r = kcTarget.path(kcUserURI.getPath()).request()
                .header("Authorization", "bearer " + adminToken).delete();

        assertThat(r.getStatus(), is(204));
    }

    private String getAdminAccessToken() {

        Form form = new Form()
                .param("grant_type", "password")
                .param("client_id", "hp-user-registration")
                .param("username", "admin")
                .param("password", "lacambra1982.");

        String path = "auth/realms/master/protocol/openid-connect/token";

        Response r = kcTarget.path(path).request().post(Entity.form(form));
        JsonObject accessData = r.readEntity(JsonObject.class);

        assertThat(r.getStatus(), is(200));
        assertThat(accessData.containsKey("access_token"), is(true));
        assertThat(accessData.getString("access_token"), notNullValue());

        return accessData.getString("access_token");

    }

    private void getUserAccessToken() {

        Form form = new Form()
                .param("grant_type", "password")
                .param("client_id", "headprosystemtest")
                .param("username", username)
                .param("password", password);

        String path = "auth/realms/headpro/protocol/openid-connect/token";

        Response r = kcTarget.path(path).request().post(Entity.form(form));
        JsonObject accessData = r.readEntity(JsonObject.class);

        assertThat(r.getStatus(), is(200));
        assertThat(accessData.containsKey("access_token"), is(true));

        accessToken = accessData.getString("access_token");
        assertThat(accessToken, notNullValue());

    }

    protected Invocation.Builder request(WebTarget target) {

        boolean auth = true;

        if (auth) {
            return target.request().header("Authorization", "bearer " + accessToken);
        } else {
            return target.request();
        }
    }

    protected ObjectRequestContainer createAccount() {

        JsonObject account = Json.createObjectBuilder().build();
        Response r = request(tut.path(pathToUser)).post(Entity.json(account));
        assertThat("Not possible to create HP account", r.getStatus(), is(201));

        return new ObjectRequestContainer(account, r);
    }

    protected ObjectRequestContainer createWorkspace(int accountId) {
        return createWorkspace("wsTitle acc:" + accountId + ", user:" + username, accountId);
    }

    protected ObjectRequestContainer createWorkspace(String title, int accountId) {
        JsonObject project = Json.createObjectBuilder().add("title", title).build();

        Invocation.Builder builder = request(tut.path(pathToAccountWorkspace).resolveTemplate("accountId", accountId));

        Entity<JsonObject> entity = Entity.json(project);
        Response r = builder.post(entity);

        assertThat(r.getStatus(), is(201));
        return new ObjectRequestContainer(project, r);
    }

    protected String addSlot(String resourcePath, int startPosition, int duration, int serviceId, boolean randomValuePerStep) {

        Random rand = new Random();

        int rv = 5;

        if (randomValuePerStep) {
            rv = rand.nextInt(999);
        }

        JsonObject slot = Json.createObjectBuilder()
                .add("startPosition", startPosition)
                .add("duration", duration)
                .add("valuePerStep", rv)
                .add("service", serviceId)
                .build();

        Response r = request(tut.path("{pathToResource}/slot")
                .resolveTemplateFromEncoded("pathToResource", resourcePath))
                .post(Entity.json(slot));

        assertThat(r.getStatus(), is(201));

        return r.getLocation().getPath();
    }

    protected String addSlot(String projectPath, int startPosition, int duration, int serviceId) {
        return addSlot(projectPath, startPosition, duration, serviceId, true);
    }

    protected JsonArray getSlotsOfResource(String projectPath) {
        Response r = request(tut.path("{pathToProject}/slots")
                .resolveTemplateFromEncoded("pathToProject", projectPath))
                .get();

        assertThat(r.getStatus(), is(200));
        return r.readEntity(JsonArray.class);
    }

    protected int extractId(String path) {
        return Integer.parseInt(path.substring(path.lastIndexOf("/") + 1));
    }

    protected ObjectRequestContainer createProject(int workspaceId) {
        return createProject(workspaceId, "projectTitle", LocalDate.of(2015, Month.OCTOBER, 23), 15);
    }

    protected ObjectRequestContainer createProject(int workspaceId, String title) {
        return createProject(workspaceId, title, LocalDate.of(2015, Month.OCTOBER, 23), 10);
    }

    protected ObjectRequestContainer createProject(int workspaceId, String title, LocalDate startDate, int weeks) {

        JsonObject project = Json.createObjectBuilder().add("name", title)
                .add("startDate", LocalDateConverter.toDate(startDate).getTime())
                .add("endDate", LocalDateConverter.toDate(startDate.plusWeeks(weeks)).getTime())
                .build();

        Response r = request(tut.path(pathToProject).resolveTemplate("workspaceId", workspaceId)).post(Entity.json(project));

        assertThat(r.getStatus(), is(201));

        return new ObjectRequestContainer(project, r);
    }

    protected ObjectRequestContainer createService(int workspaceId) {
        return createService(workspaceId, "serviceTitle");
    }

    protected ObjectRequestContainer createService(int workspaceId, String serviceName) {

        JsonObject service = Json.createObjectBuilder().add("name", serviceName).build();
        Response r = request(tut.path(pathToService).resolveTemplate("workspaceId", workspaceId)).post(Entity.json(service));
        assertThat(r.getStatus(), is(201));

        return new ObjectRequestContainer(service, r);
    }

    protected class ObjectRequestContainer {

        JsonObject object;
        Response response;

        ObjectRequestContainer(JsonObject object, Response r) {
            this.object = object;
            this.response = r;
        }

        public Response getResponse() {
            return response;
        }

        public JsonObject getObject() {
            return object;
        }

        public int getCreatedResourceId() {
            return extractId(response.getLocation().getPath());
        }
    }

    protected void sleep(long milis) {
        try {
            Thread.sleep(milis);
        } catch (InterruptedException ex) {
        }
    }

}
