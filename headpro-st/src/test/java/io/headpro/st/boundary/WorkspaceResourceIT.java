package io.headpro.st.boundary;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 *
 * @author alacambra
 */
public class WorkspaceResourceIT extends AbstractSystemTest {

    @Test
    public void testWorkspaceCycle() {

        Response r = createAccount().getResponse();
        int accountId = extractId(r.getLocation().getPath());
        ObjectRequestContainer wsContainer = createWorkspace(accountId);
        String wsTitle = wsContainer.getObject().getString("title");
        r = wsContainer.getResponse();

        String workspaceLocation = r.getLocation().getPath();

        JsonObject workspace = request(tut.path(workspaceLocation)).get(JsonObject.class);
        assertThat(workspace, is(notNullValue()));

        assertThat(workspace.getString("title"), is(wsTitle));
        int workspaceId = workspace.getInt("id");

        workspace = Json.createObjectBuilder().add("title", "new ws").build();

        workspace = request(tut.path(workspaceLocation)).put(Entity.json(workspace)).readEntity(JsonObject.class);
        assertThat(workspace, is(notNullValue()));
        assertThat(workspace.getString("title"), is("new ws"));
        assertThat(workspace.getInt("id"), is(workspaceId));

        r = request(tut.path(workspaceLocation)).delete();
        assertThat(r.getStatus(), is(204));
    }
}
