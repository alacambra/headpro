/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.headpro.st.boundary;

import javax.json.JsonArray;
import javax.ws.rs.core.Response;
import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


/**
 *
 * @author alacambra
 */
public class UserResourceIT extends AbstractSystemTest {

    @Test
    public void testGetMyWorkspaces() {
        int accountId = createAccount().getCreatedResourceId();
        createWorkspace(accountId);
        createWorkspace(accountId);
        createWorkspace(accountId);

        Response r = request(tut.path(pathToUser).path("my/workspaces")).get();
        assertThat(r.getStatus(), is(200));
        JsonArray workspaces = r.readEntity(JsonArray.class);
        assertThat(workspaces.size(), is(4));
    }
}
