package io.headpro.st;

import io.headpro.st.boundary.KeycloakConfig;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.junit.Before;
import org.junit.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.junit.After;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class KeycloakUsersApiIT {

    protected Client client;
    protected WebTarget kcTarget;
    protected String accessToken;
    Keycloak kcAdmin;
    Keycloak kcUser;
    KeycloakConfig config;
    String id;

    @Before
    public void setUp() {

        config = new KeycloakConfig("http://localhost:8080", "http://localhost:6071/auth");
//        config = new KeycloakConfig("https://auth.headpro.io", "http://localhost:6071/auth");
        client = ClientBuilder.newClient();
        kcAdmin = Keycloak.getInstance(
                config.getKeycloakEndpoint(),
                "master", // the realm to log in to
                "admin",
                "lacambra1982.", // the user
                "hp-user-registration");
    }

    @After
    public void down() {
        kcAdmin.realm("headpro").users().get(id).remove();
        kcAdmin.close();
    }

    @Test
    public void testCreateUser() {

        String username = "usertest";
        String psw = "test123";

        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(psw);
        credential.setTemporary(false);

        UserRepresentation user = new UserRepresentation();
        user.setUsername(username);
        user.setFirstName("Test");
        user.setLastName("User");
        user.setEnabled(true);
        user.setEmail("as@dsa.es");
        user.setEmailVerified(true);

        Response r = kcAdmin.realm("headpro").users().create(user);
        String[] fragments = r.getLocation().getRawPath().split("/");
        r.close();
        id = fragments[fragments.length - 1];

        kcAdmin.realm("headpro").users().get(id).resetPassword(credential);
        r.close();

        kcUser = Keycloak.getInstance(
                config.getKeycloakEndpoint(),
                "headpro", // the realm to log in to
                username,
                psw,
                "headprosystemtest");

        String token = kcUser.tokenManager().getAccessTokenString();

        WebTarget t = client.target(config.getApplicationEndpoint());
        Response r1 = request(t.path("/resources/user").path("my/workspaces"), token).get();

        assertThat(r1.getStatus(), is(201));
        r1.close();

        kcUser.close();

    }

    protected Invocation.Builder request(WebTarget target, String token) {
        return target.request().header("Authorization", "bearer " + token);
    }
}
