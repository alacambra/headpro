package io.headpro.st.boundary;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 *
 * @author alacambra
 */
public class KeycloakConfig {

    String applicationEndpoint;
    String keycloakEndpoint;

    String adminToken;
    String accessToken;
    String adminUserName;
    String adminpPassword;

    Client client;
    WebTarget target;

    public KeycloakConfig(String applicationEndpoint, String keycloakEndpoint) {

        this.applicationEndpoint = applicationEndpoint;
        this.keycloakEndpoint = keycloakEndpoint;

        adminUserName = System.getenv("KC_ST_ADMIN_USERNAME");
        adminpPassword = System.getenv("KC_ST_ADMIN_PASSWORD");

//        init();
    }

    public void init() {

        client = ClientBuilder.newClient();
        target = client.target(keycloakEndpoint);

        loadAccessToken();
    }

    private void loadAccessToken() {

        Form form = new Form()
                .param("grant_type", "password")
                .param("client_id", "hp-user-registration")
                .param("username", adminUserName)
                .param("password", adminpPassword);

        String path = "auth/realms/master/protocol/openid-connect/token";

        Response r = target.path(path).request().post(Entity.form(form));
        JsonObject accessData = r.readEntity(JsonObject.class);

        assertThat(r.getStatus(), is(200));
        assertThat(accessData.containsKey("access_token"), is(true));

        accessToken = accessData.getString("access_token");
        assertThat(accessToken, notNullValue());

    }

    public String getApplicationEndpoint() {
        return applicationEndpoint;
    }

    public String getKeycloakEndpoint() {
        return keycloakEndpoint;
    }

    public String getAdminToken() {
        return adminToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getAdminUserName() {
        return adminUserName;
    }

    public String getAdminpPassword() {
        return adminpPassword;
    }

}
