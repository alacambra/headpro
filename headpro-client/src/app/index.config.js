(function () {
    'use strict';

    angular
            .module('headproClient')
            .config(config);

    function config($logProvider, toastrConfig, $httpProvider) {
        
        moment().utcOffset(120);

        $logProvider.debugEnabled(true);

        toastrConfig.allowHtml = true;
        toastrConfig.timeOut = 1000;
        toastrConfig.positionClass = 'toast-top-right';
        toastrConfig.preventDuplicates = true;
        toastrConfig.progressBar = true;

        $httpProvider.interceptors.push("HttpInterceptor");

        Object.defineProperty(Array.prototype, 'unique', {
            enumerable: false,
            configurable: false,
            writable: false,
            value: function () {
                var a = this.concat();
                for (var i = 0; i < a.length; ++i) {
                    for (var j = i + 1; j < a.length; ++j) {
                        if (a[i] === a[j])
                            a.splice(j--, 1);
                    }
                }

                return a;
            }
        });
    }

})();
