(function () {
    'use strict';
    angular
            .module('headproClient')
            .controller('ChartsController', ChartsController);

    function ChartsController($scope, $q, $timeout, DataManager, $log, SlotService, ChartProcessor, LoadManager) {

        var vm = this;
        var logPrefix = "ChartsController:";
        var currentTimeRange = {};

        vm.projectRowHeaders = [];
        vm.serviceRowHeaders = [];
        vm.availableServicesRowHeaders = [];
        vm.remainingResourcesByServicesRowHeaders = [];

        registerEvents();

        if (LoadManager.appInitilized) {
            load();
        }

        function registerEvents() {
            [LoadManager.workspaceChangedEvent, LoadManager.appInitilizedEvent].forEach(function (event) {
                $scope.$on(event, function () {
                    load();
                });
            });
        }

        function load() {

            var periodDeferred = $q.defer();
            var periodPromise = periodDeferred.promise;

            var totalRequiredSlotsDeferred = $q.defer();
            var totalRequiredSlotsPromise = totalRequiredSlotsDeferred.promise;

            var totalAvailableSlotsDeferred = $q.defer();
            var totalAvailableSlotsPromise = totalAvailableSlotsDeferred.promise;

            var totalAvailableSlotsByServiceDeferred = $q.defer();
            var totalAvailableSlotsByServicePromise = totalAvailableSlotsByServiceDeferred.promise;

            var totalRequiredSlotsByServiceDeferred = $q.defer();
            var totalRequiredSlotsByServicePromise = totalRequiredSlotsByServiceDeferred.promise;

            DataManager.getRequiredResources("project", false).then(function (projectsWithSlots) {
                periodPromise.then(function () {

                    var options = {
                        implicitTimeRange: false,
                        timeRange: currentTimeRange
                    };

                    projectsWithSlots.forEach(function (project) {
                        vm.projectRowHeaders.push(project.name);
                    });

                    vm.pData = SlotService.toSimpleSlots(projectsWithSlots, options);
                    totalRequiredSlotsDeferred.resolve(SlotService.aggregateSimpleSlots(vm.pData));
                    loadRequiredByProjectsChart();
                });

                SlotService.getTimeRangeWithDates(projectsWithSlots, true).then(function (timeRange) {
                    currentTimeRange = timeRange;
                    $log.debug(logPrefix, "currentTimeRange is", currentTimeRange);
                    periodDeferred.resolve();
                });
            });

            DataManager.getAvailableResources().then(function (servicesWithSlots) {
                periodPromise.then(function () {
                    var options = {
                        implicitTimeRange: false,
                        timeRange: currentTimeRange
                    };

                    servicesWithSlots.forEach(function (service) {
                        vm.availableServicesRowHeaders.push(service.name);
                    });

                    vm.availableServicesData = SlotService.toSimpleSlots(servicesWithSlots, options);

                    totalAvailableSlotsByServiceDeferred.resolve([vm.availableServicesRowHeaders.slice(0), vm.availableServicesData]);
                    totalAvailableSlotsDeferred.resolve(SlotService.aggregateSimpleSlots(vm.availableServicesData));

                    loadAvailabilityChart();
                });
            });

            DataManager.getRequiredResources("service", true).then(function (servicesWithSlots) {
                periodPromise.then(function (period) {
                    var options = {
                        implicitTimeRange: false,
                        timeRange: currentTimeRange
                    };

                    var data = [];
                    servicesWithSlots.forEach(function (service) {

                        var simpleSlots = [];
                        simpleSlots = SlotService.toSimpleSlots(service.projects, options);

                        var aggregatedSlots = SlotService.aggregateSimpleSlots(simpleSlots);
                        data.push(aggregatedSlots);
                        vm.serviceRowHeaders.push(service.name);
                    });

                    totalRequiredSlotsByServiceDeferred.resolve([vm.serviceRowHeaders.slice(0), data]);
                    vm.sData = data;
                    loadRequiredByServiceChart();

                });
            });

            $q.all([totalAvailableSlotsPromise, totalRequiredSlotsPromise]).then(function (simpleSlots) {

                var availableSimpleSlots = simpleSlots[0];
                var requiredSimpleSlots = simpleSlots[1];

                vm.remainingResourcesData = SlotService.substractSimpleSlots(availableSimpleSlots, requiredSimpleSlots);
                vm.remainingResourcesRowHeaders = ["remaining resources"];
                loadRemainingResourcesChart();
            });

            $q.all([totalAvailableSlotsByServicePromise, totalRequiredSlotsByServicePromise]).then(function (simpleSlots) {

                var availableSimpleSlots = {columns: simpleSlots[0][0], slots: simpleSlots[0][1]};
                var requiredSimpleSlots = {columns: simpleSlots[1][0], slots: simpleSlots[1][1]};

                var data = {};

                var i = 0;
                var slots = [];
                var name = "";

                for (i = 0; i < availableSimpleSlots.columns.length; i++) {
                    name = availableSimpleSlots.columns[i];
                    slots = availableSimpleSlots.slots[i];
                    if (angular.isUndefined(data[name])) {
                        data[name] = {
                            available: [],
                            required: []
                        };
                    }

                    data[name].available = slots;
                }

                for (i = 0; i < requiredSimpleSlots.columns.length; i++) {
                    name = requiredSimpleSlots.columns[i];
                    slots = requiredSimpleSlots.slots[i];
                    if (angular.isUndefined(data[name])) {
                        data[name] = {
                            available: [],
                            required: []
                        };
                    }

                    data[name].required = slots;
                }

                vm.remainingResourcesByServicesData = [];

                for (name in data) {
                    vm.remainingResourcesByServicesRowHeaders.push(name);
                    vm.remainingResourcesByServicesData.push(SlotService.substractSimpleSlots(data[name].available, data[name].required));
                }

                loadRemainingResourcesByServicesChart();

            });
        }

        function loadRequiredByProjectsChart() {
            var chartProcessor = new ChartProcessor();
            chartProcessor.setStackedResourcesSteps(vm.pData);
            chartProcessor.setChartKeys(vm.projectRowHeaders);
            chartProcessor.setStartTime(currentTimeRange.startDate);

            vm.chartProjectsOptions = chartProcessor.getChartOptions();
            vm.chartProjectsData = chartProcessor.load().getChartItems();

            $timeout(function () {
                vm.projectsApi.updateWithOptions(chartProcessor.getChartOptions());
            });
        }

        function loadRequiredByServiceChart() {
            var chartProcessor = new ChartProcessor();
            chartProcessor.setStackedResourcesSteps(vm.sData);
            chartProcessor.setChartKeys(vm.serviceRowHeaders);
            chartProcessor.setStartTime(currentTimeRange.startDate);

            vm.chartServicesOptions = chartProcessor.getChartOptions();
            vm.chartServicesData = chartProcessor.load().getChartItems();

            $timeout(function () {
                vm.servicesApi.updateWithOptions(chartProcessor.getChartOptions());
            });
        }

        function loadAvailabilityChart() {
            var chartProcessor = new ChartProcessor();
            chartProcessor.setStackedResourcesSteps(vm.availableServicesData);
            chartProcessor.setChartKeys(vm.availableServicesRowHeaders);
            chartProcessor.setStartTime(currentTimeRange.startDate);

            vm.chartAvailableServicesOptions = chartProcessor.getChartOptions();
            vm.chartAvailableServicesData = chartProcessor.load().getChartItems();

            $timeout(function () {
                vm.availableServicesApi.updateWithOptions(chartProcessor.getChartOptions());
            });
        }

        function loadRemainingResourcesChart() {
            var chartProcessor = new ChartProcessor();
            chartProcessor.setStackedResourcesSteps([vm.remainingResourcesData]);
            chartProcessor.setChartKeys(vm.remainingResourcesRowHeaders);
            chartProcessor.setStartTime(currentTimeRange.startDate);

            vm.chartRemainingResourcesOptions = chartProcessor.getChartOptions();
            vm.chartRemainingResourcesData = chartProcessor.load().getChartItems();

            $timeout(function () {
                vm.remainingResourcesApi.updateWithOptions(chartProcessor.getChartOptions());
            });
        }

        function loadRemainingResourcesByServicesChart() {

            var chartProcessor = new ChartProcessor();
            chartProcessor.setStackedResourcesSteps(vm.remainingResourcesByServicesData);
            chartProcessor.setChartKeys(vm.remainingResourcesByServicesRowHeaders);
            chartProcessor.setStartTime(currentTimeRange.startDate);

            vm.chartRemainingResourcesByServicesOptions = chartProcessor.getChartOptions();
            vm.chartRemainingResourcesByServicesData = chartProcessor.load().getChartItems();

            $timeout(function () {
                vm.remainingResourcesByServiceApi.updateWithOptions(chartProcessor.getChartOptions());
            });
        }

        vm.update = update;

        function update() {
            $log.info("updating charts.-....")
            loadRemainingResourcesChart();
            loadAvailabilityChart();
            loadRequiredByServiceChart();
            loadRequiredByProjectsChart();
            loadRemainingResourcesByServicesChart();


        }

    }
})();
