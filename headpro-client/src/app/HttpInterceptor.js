(function () {
    'use strict';
    angular.module('headproClient').factory('HttpInterceptor', HttpInterceptorFactory);


    function HttpInterceptorFactory($log, $window, $location, $q) {

        var logPrefix = "HttpInterceptor";

        return {
            request: function (config) {
                return config;
            },
            requestError: function (rejection) {
            },
            response: function (response) {
                if (response.headers("x-headpro-registered") === "reload") {
                    $window.location.reload();
                }
                return response;
            },
            responseError: function (rejection) {
                
                if(angular.isDefined(rejection.status) && rejection.status !== -1){
                    return $q.reject(rejection);
                }
                
                var url = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/public/redirect.html";
                $log.debug(logPrefix, "responseError", rejection, url);
                $window.location.href = url;
            }
        };
    }
})();
