/* global moment */

(function () {
    'use strict';
    angular
            .module('headproClient')
            .controller('AvailabilityEditionController', AvailabilityEditionController);

    function AvailabilityEditionController($scope, $timeout, $log, $q, toastr, DataManager,
            LoadManager, SimpleSlotsTable, SlotService,
            ChartProcessor, GlobalTimeRangeService) {

        var vm = this;
        var logPrefix = "AvailabilityEditionController:";

        var simpleSlotsTable = undefined;
        var chartProcessor = undefined;
        var currentMasterTimeRange = undefined;
        var currentFocusTimeRange = undefined;
        var currentResourceRequiredSlots;
        var currentResourceAvailabilitySlots;
        var requiredSimpleSlots;
        var availableSimpleSlots;
        var availableResources;
        var totalAvailablePerSimpleSlot;
        var totalRequiredPerSimpleSlot;
        var totalAvailablePerServicePerSimpleSlot;
        var slotsPairing = {};
        var currentResourceRequiredSlots;

        vm.table = {};
        vm.table.show = false;
        vm.appInitialized = false;

        preinitConf();
        registerEvents();

        if (LoadManager.appInitilized) {
            init();
        }

        function preinitConf() {
            vm.datePicker = {
                options: {
                    locale: {
                        format: "DD-MM-YYYY"
                    }
                },
                date: {
                    startDate: new Date(0),
                    endDate: new Date(0)
                }
            };

            simpleSlotsTable = new SimpleSlotsTable({
                addSlotAction: addSlot,
                editReady: loadChart
            });
            vm.table.settings = simpleSlotsTable.getSettings();
        }

        function init() {

            var requiredResourcesReceived = DataManager.getRequiredResources("service", false);
            var availableResourcesReceived = DataManager.getServicesWithSlots();

            $q.all([availableResourcesReceived, requiredResourcesReceived]).then(function (data) {

                var availableResourcesWithSlots = data[0];
                currentResourceRequiredSlots = data[1];

                var orderFn = function (r1, r2) {
                    return r1.getId() > r2.getId() ? 1 : -1;
                };

                currentResourceAvailabilitySlots = availableResourcesWithSlots.sort(orderFn);
                currentResourceRequiredSlots = currentResourceRequiredSlots.sort(orderFn);

                prepareData();

                initDatePicker();
                initTable();
                vm.appInitialized = true;

                $timeout(function () {
                    initTable();
                    initSlider();
                    loadChart();
                });
            });

            currentMasterTimeRange = GlobalTimeRangeService.getTimeRange("master");
            currentFocusTimeRange = GlobalTimeRangeService.getTimeRange("focus");
        }

        function registerEvents() {
            [LoadManager.workspaceChangedEvent, LoadManager.appInitilizedEvent].forEach(function (event) {
                $scope.$on(event, function () {
                    init();
                });
            });
            $scope.$on("slideEnded", function (event) {
                GlobalTimeRangeService.setDatesRange(new Date(vm.slider.minValue), new Date(vm.slider.maxValue), "focus");
            });

            $scope.$on(GlobalTimeRangeService.timeRangeChangedEvent, function (event, timeRange) {

                if (timeRange.getId() === "master") {
                    currentMasterTimeRange = timeRange;
                } else if (timeRange.getId() === "focus") {
                    currentFocusTimeRange = timeRange;
                } else {
                    return;
                }

                initDatePicker();

                if (!initSlider()) {
                    //Dates from slider do not pass into masterRange and will be updated, so no initialization from 
                    //resources dependent on the focus timerange should run
                    return;
                }

                prepareData();

                $log.debug(logPrefix, availableSimpleSlots[0].length);
                initTable();
                loadChart();

            });
        }

        function prepareData() {

            requiredSimpleSlots = SlotService.toSimpleSlots(currentResourceRequiredSlots, {timeRange: currentFocusTimeRange});
            availableSimpleSlots = SlotService.toSimpleSlots(currentResourceAvailabilitySlots, {timeRange: currentFocusTimeRange});

            var i = 0;
            currentResourceAvailabilitySlots.forEach(function (resource) {
                slotsPairing[resource.getId()] = {required: [], available: availableSimpleSlots[i]};
                i++;
            });

            i = 0;
            currentResourceRequiredSlots.forEach(function (resource) {
                if (angular.isUndefined(slotsPairing[resource.getId()])) {
                    //only those that commes with availability are important
                    return;
//                        slotsPairing[resource.getId()] = {required: [], available: []};
                }
                slotsPairing[resource.getId()].required = requiredSimpleSlots[i];
                i++;
            });

            totalAvailablePerSimpleSlot = SlotService.aggregateSimpleSlots(availableSimpleSlots);
            totalRequiredPerSimpleSlot = SlotService.aggregateSimpleSlots(requiredSimpleSlots);

            totalAvailablePerServicePerSimpleSlot = [];

            for (var pair in slotsPairing) {
                if (slotsPairing.hasOwnProperty(pair)) {
                    totalAvailablePerServicePerSimpleSlot.push(SlotService.substractSimpleSlots(slotsPairing[pair].available, slotsPairing[pair].required));
                }
            }
        }

        function initDatePicker() {
            vm.datePicker = {
                options: {
                    showDropdowns: true,
                    showWeekNumbers: true,
                    eventHandlers: {
                        "apply.daterangepicker": function () {
                            var newStart = vm.datePicker.date.startDate.toDate();
                            var newEnd = vm.datePicker.date.endDate.toDate();
                            GlobalTimeRangeService.setDatesRange(newStart, newEnd, "master");
                        }
                    },
                    locale: {
                        format: "DD-MM-YYYY",
                        fromLabel: "From"
                    },
                    ranges: {
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'Last 90 Days': [moment().subtract(89, 'days'), moment()],
                        'Last Year': [moment().subtract(365, 'days'), moment()],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Next 30 Days': [moment(), moment().add(29, 'days')],
                        'Next 90 Days': [moment(), moment().add(89, 'days')],
                        'Next Year': [moment(), moment().add(365, 'days')]
                    }
                },
                date: {
                    startDate: currentMasterTimeRange.getStartDate(),
                    endDate: currentMasterTimeRange.getEndDate()
                }
            };
        }

        function initSlider() {
            var day = 8.64e7;
            var week = day * 7;
            var year = 365 * day;
            var minValue = currentFocusTimeRange.getStartDate();
            var maxValue = currentFocusTimeRange.getEndDate();

            /**
             * for some reason js add 1 milisecond to maxvalue. 
             * Should use just the date
             */
            if (minValue < currentMasterTimeRange.getStartDate() || maxValue > currentMasterTimeRange.getEndDate() + 100) {
                currentFocusTimeRange = currentMasterTimeRange.clone("focus");
                GlobalTimeRangeService.setTimeRange(currentFocusTimeRange);
                return false;
            }

            vm.slider = {
                minValue: minValue,
                maxValue: maxValue,
                id: "someID",
                options: {
                    step: week,
                    floor: currentMasterTimeRange.getStartDate(),
                    ceil: currentMasterTimeRange.getEndDate(),
                    draggableRangeOnly: false,
                    draggableRange: true,
                    showTicks: currentMasterTimeRange.getEndDate() - currentMasterTimeRange.getStartDate() < 2 * year,
                    translate: function (value) {
                        return moment(value).format("DD/MM/YYYY");
                    }
                }
            };

            return true;
        }

        function initTable() {

            var options = {
                addSlotAction: addSlot,
                editReady: loadChart,
                implicitTimeRange: false,
                useTimeRangeOfBlockResource: false,
                timeRange: currentFocusTimeRange
            };

            simpleSlotsTable.setOptions(options);
            vm.table.settings = simpleSlotsTable.getSettings();
            vm.table.$scope = $scope;
            vm.table.totalAvailablePerServicePerSimpleSlot = totalAvailablePerServicePerSimpleSlot;

            simpleSlotsTable.setResouresWithSlots(currentResourceAvailabilitySlots);
            simpleSlotsTable.loadTable(vm.table);

            var tableData = [];
            availableSimpleSlots.forEach(function (row) {
                tableData.push(row);
            });

            simpleSlotsTable.addRow("TOTAL AVAILABLE", {
                editable: false,
                isResult: "static"
            });
            tableData.push(totalAvailablePerSimpleSlot);

            simpleSlotsTable.addRow("TOTAL REQUIRED", {
                editable: false,
                isResult: "static"
            });
            tableData.push(totalRequiredPerSimpleSlot);

            simpleSlotsTable.addRow("REMAINING", {
                editable: false,
                isResult: "posNeg"
            });
            tableData.push(SlotService.substractSimpleSlots(totalAvailablePerSimpleSlot, totalRequiredPerSimpleSlot));

            vm.table.db = {
                items: tableData
            };

            vm.table.colHeaders = simpleSlotsTable.getColHeaders();
            vm.table.rowHeaders = simpleSlotsTable.getRowHeaders();
            vm.table.columns = simpleSlotsTable.getColumns();
            vm.table.show = currentResourceAvailabilitySlots.length > 0;
        }

        function addSlot(service, slotPosition, duration, value) {
            return DataManager.addServiceSlot(service.getId(), {
                duration: duration,
                startPosition: slotPosition,
                valuePerStep: value
            }).then(function () {
                init();
            }).catch(function (reason) {
                toastr.error("Value not saved: " + reason);
            });

        }

        function loadChart() {
            loadTotalRemainingResourcesChart();

            chartProcessor = new ChartProcessor();
            chartProcessor.setStartTime(currentFocusTimeRange.getStartDate());
            vm.chart.options = chartProcessor.getChartOptions();

            chartProcessor.setStackedResourcesSteps(availableSimpleSlots);
            chartProcessor.setChartKeys(vm.table.rowHeaders);
            vm.chart.data = chartProcessor.load().getChartItems();
            $timeout(function () {
                vm.chart.api.updateWithOptions(vm.chart.options);
            });
        }

        function loadTotalRemainingResourcesChart() {

            chartProcessor = new ChartProcessor();
            chartProcessor.setStartTime(currentFocusTimeRange.getStartDate());
            vm.chart.remaining.options = chartProcessor.getChartOptions();

            chartProcessor.setStackedResourcesSteps(totalAvailablePerServicePerSimpleSlot);
            chartProcessor.setChartKeys(vm.table.rowHeaders);
            vm.chart.remaining.data = chartProcessor.load().getChartItems();
            $timeout(function () {
                vm.chart.remaining.api.updateWithOptions(vm.chart.options);
            });
        }

    }
})();
