/* global moment */

(function () {
    'use strict';
    angular
            .module('headproClient')
            .controller('RequiredEditionController', RequiredEditionController);

    function RequiredEditionController($scope, $timeout, $q, $stateParams, $log,
            DataManager, LoadManager, toastr, Service, localStorageService,
            SimpleSlotsTable, SlotService, ChartProcessor, GlobalTimeRangeService) {

        var vm = this;
        var logPrefix = "RequiredEditionController:";

        var simpleSlotsTable = undefined;
        var chartProcessor = undefined;
        var currentMasterTimeRange = undefined;
        var currentFocusTimeRange = undefined;
        var currentResourceRequiredSlots;
        var currentResourceAvailabilitySlots;
        var requiredSimpleSlots;
        var availableSimpleSlots;
        var availableResources;
        var aggRequiredSlots;
        var aggAvailableSlots;
        var remaining;

        vm.table = {};
        vm.table.show = false;
        vm.currentService = undefined;
        vm.services = undefined;
        vm.servicesAttributes = undefined;
        vm.changeService = changeService;
        vm.serviceIsActive = serviceIsActive;
        vm.appInitialized = false;

        preinitConf();
        registerEvents();

        if (LoadManager.appInitilized) {
            init();
        }

        function preinitConf() {
            vm.datePicker = {
                options: {
                    locale: {
                        format: "DD-MM-YYYY"
                    }
                },
                date: {
                    startDate: new Date(0),
                    endDate: new Date(0)
                }
            };

            simpleSlotsTable = new SimpleSlotsTable({
                addSlotAction: addSlot,
                editReady: loadSlotsForCurrentService
            });
            vm.table.settings = simpleSlotsTable.getSettings();
        }

        function init() {

            if (angular.isDefined($stateParams.serviceId)) {
                vm.currentService = new Service({id: $stateParams.serviceId});
                localStorageService.set("currentService", $stateParams.serviceId);
            } else if (localStorageService.get("currentService")) {
                vm.currentService = new Service({id: localStorageService.get("currentService")});
            }

            if (angular.isUndefined(vm.services)) {
                DataManager.getServices().then(function (services) {
                    vm.servicesAttributes = services.map(function (service) {
                        return service.getAttributes();
                    });
                    vm.services = services;

                    if (angular.isDefined(vm.currentService)) {
                        services.forEach(function (service) {
                            if (vm.currentService.getId() === service.getId()) {
                                vm.currentService = service;
                            }
                        });
                    }
                    vm.appInitialized = true;
                }).catch(function (reason) {
                    toastr.error("impossible to load servcies" + angular.isDefined(reason) ? (": " + reason) : "");
                });
            }

            loadSlotsForCurrentService();

        }

        function loadSlotsForCurrentService() {

            if (angular.isUndefined(vm.currentService)) {
                return;
            }

            var receivingAvailableResources = $q.defer();
            var availableResourcesReceived = receivingAvailableResources.promise;

            if (angular.isDefined(availableResources)) {
                receivingAvailableResources.resolve(availableResources);
            } else {
                DataManager.getServicesWithSlots().then(function (resources) {
                    receivingAvailableResources.resolve(resources);
                });
            }

            var requiredResourcesReceived = DataManager.getProjectsWithSlotsOfService(vm.currentService.getId());

            $q.all([availableResourcesReceived, requiredResourcesReceived]).then(function (data) {
                availableResources = data[0];
                var resourcesWithSlots = data[1];
                /**
                 * Order the projects first by date and then  alphabetically
                 */
                currentResourceRequiredSlots = resourcesWithSlots.sort(function (p1, p2) {

                    if (p1.getStartIndex() === p2.getStartIndex()) {
                        return p1.getName().toLowerCase() > p2.getName().toLowerCase() ? 1 : -1;
                    }

                    return (p1.getStartIndex() - p2.getStartIndex());
                });

                currentResourceAvailabilitySlots = {};
                availableResources.some(function (resource) {
                    if (resource.getId() === vm.currentService.getId()) {
                        currentResourceAvailabilitySlots = resource;
                        return true;
                    }
                });

                prepareData();

                initDatePicker();
                initTable();
                loadChart();

                $timeout(function () {
                    initTable();
                    initSlider();
                });
            });

            currentMasterTimeRange = GlobalTimeRangeService.getTimeRange("master");
            currentFocusTimeRange = GlobalTimeRangeService.getTimeRange("focus");
        }

        function registerEvents() {
            [LoadManager.workspaceChangedEvent, LoadManager.appInitilizedEvent].forEach(function (event) {
                $scope.$on(event, function () {
                    init();
                });
            });
            $scope.$on("slideEnded", function (event) {
                GlobalTimeRangeService.setDatesRange(new Date(vm.slider.minValue), new Date(vm.slider.maxValue), "focus");
            });

            $scope.$on(GlobalTimeRangeService.timeRangeChangedEvent, function (event, timeRange) {

                if (timeRange.getId() === "master") {
                    currentMasterTimeRange = timeRange;
                } else if (timeRange.getId() === "focus") {
                    currentFocusTimeRange = timeRange;
                } else {
                    return;
                }

                initDatePicker();

                if (!initSlider()) {
                    //Dates from slider do not pass into masterRange and will be updated, so no initialization from 
                    //resources dependent on the focus timerange should run
                    return;
                }

                prepareData();
                initTable();
                loadChart();

            });
        }

        function prepareData() {
            var start = window.performance.now();
            requiredSimpleSlots = SlotService.toSimpleSlots(currentResourceRequiredSlots, {timeRange: currentFocusTimeRange});
            availableSimpleSlots = SlotService.toSimpleSlots([currentResourceAvailabilitySlots], {timeRange: currentFocusTimeRange});
            aggRequiredSlots = SlotService.aggregateSimpleSlots(requiredSimpleSlots);
            aggAvailableSlots = SlotService.aggregateSimpleSlots(availableSimpleSlots);
            remaining = SlotService.substractSimpleSlots(availableSimpleSlots[0], aggRequiredSlots);
            var end = window.performance.now();
//            $log.debug(logPrefix, end, start, end - start);
        }

        function initDatePicker() {
            vm.datePicker = {
                options: {
                    showDropdowns: true,
                    showWeekNumbers: true,
                    eventHandlers: {
                        "apply.daterangepicker": function () {
                            var newStart = vm.datePicker.date.startDate.toDate();
                            var newEnd = vm.datePicker.date.endDate.toDate();
                            GlobalTimeRangeService.setDatesRange(newStart, newEnd, "master");
                        }
                    },
                    locale: {
                        format: "DD-MM-YYYY",
                        fromLabel: "From"
                    },
                    ranges: {
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'Last 90 Days': [moment().subtract(89, 'days'), moment()],
                        'Last Year': [moment().subtract(365, 'days'), moment()],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Next 30 Days': [moment(), moment().add(29, 'days')],
                        'Next 90 Days': [moment(), moment().add(89, 'days')],
                        'Next Year': [moment(), moment().add(365, 'days')]
                    }
                },
                date: {
                    startDate: currentMasterTimeRange.getStartDate(),
                    endDate: currentMasterTimeRange.getEndDate()
                }
            };
        }

        function initSlider() {

            var day = 8.64e7;
            var week = day * 7;
            var year = 365 * day;
            var minValue = currentFocusTimeRange.getStartDate();
            var maxValue = currentFocusTimeRange.getEndDate();

            /**
             * for some reason js add 1 milisecond to maxvalue. 
             * Should use just the date
             */
            if (minValue < currentMasterTimeRange.getStartDate() || maxValue > currentMasterTimeRange.getEndDate() + 100) {
                currentFocusTimeRange = currentMasterTimeRange.clone("focus");
                GlobalTimeRangeService.setTimeRange(currentFocusTimeRange);
                return false;
            }

            vm.slider = {
                minValue: minValue,
                maxValue: maxValue,
                id: "someID",
                options: {
                    step: week,
                    floor: currentMasterTimeRange.getStartDate(),
                    ceil: currentMasterTimeRange.getEndDate(),
                    draggableRangeOnly: false,
                    draggableRange: true,
                    showTicks: currentMasterTimeRange.getEndDate() - currentMasterTimeRange.getStartDate() < 2 * year,
                    translate: function (value) {
                        return moment(value).format("DD/MM/YYYY");
                    }
                }
            };

            return true;
        }

        function initTable() {

            var options = {
                addSlotAction: addSlot,
                editReady: loadSlotsForCurrentService,
                implicitTimeRange: false,
                useTimeRangeOfBlockResource: false,
                timeRange: currentFocusTimeRange
            };

            simpleSlotsTable.setOptions(options);
            vm.table.settings = simpleSlotsTable.getSettings();
            vm.table.$scope = $scope;

            simpleSlotsTable.setResouresWithSlots(currentResourceRequiredSlots);
            simpleSlotsTable.loadTable(vm.table);

            simpleSlotsTable.addRow("TOTAL REQUIRED", {
                editable: false,
                isResult: "static"
            });
            simpleSlotsTable.addRow("TOTAL AVAILABLE", {
                editable: false,
                isResult: "static"
            });
            simpleSlotsTable.addRow("REMAINING", {
                editable: false,
                isResult: "posNeg"
            });

            var tableData = [];
            requiredSimpleSlots.forEach(function (row) {
                tableData.push(row);
            });

            tableData.push(aggRequiredSlots);
            tableData.push(aggAvailableSlots);
            tableData.push(remaining);

            vm.table.db = {
                items: tableData
            };

            vm.table.colHeaders = simpleSlotsTable.getColHeaders();
            vm.table.rowHeaders = simpleSlotsTable.getRowHeaders();
            vm.table.columns = simpleSlotsTable.getColumns();
            vm.table.show = currentResourceRequiredSlots.length > 0;
        }

        function addSlot(project, slotPosition, duration, newValue, oldValue, row, col) {

            var updating = $q.defer();
            var updated = updating.promise;

            DataManager.addSlot(project.getId(), {
                duration: duration,
                startPosition: slotPosition,
                valuePerStep: newValue,
                service: vm.currentService.getId()
            }).then(function () {
                updating.resolve();

            }).catch(function (reason) {
                toastr.error("not possible to update value: " + reason);
                updating.reject(reason);
            });

            return updated;
        }

        function loadChart() {
            loadTotalRemainingResourcesChart();
            chartProcessor = new ChartProcessor();
            chartProcessor.setStartTime(currentFocusTimeRange.getStartDate());
            vm.chart.options = chartProcessor.getChartOptions();

            chartProcessor.setStackedResourcesSteps(requiredSimpleSlots);
            chartProcessor.setChartKeys(vm.table.rowHeaders);
            vm.chart.data = chartProcessor.load().getChartItems();
            $timeout(function () {
                vm.chart.api.updateWithOptions(vm.chart.options);
            });
        }

        function loadTotalRemainingResourcesChart() {

            chartProcessor = new ChartProcessor();
            chartProcessor.setStartTime(currentFocusTimeRange.getStartDate());
            vm.chart.remaining.options = chartProcessor.getChartOptions();

            chartProcessor.setStackedResourcesSteps([remaining]);
            chartProcessor.setChartKeys([vm.currentService.getName() + " remaining resources"]);
            vm.chart.remaining.data = chartProcessor.load().getChartItems();
            $timeout(function () {
                vm.chart.remaining.api.updateWithOptions(vm.chart.options);
            });
        }

        function changeService(service) {
            vm.currentService = new Service(service);
            localStorageService.set("currentService", vm.currentService.getId());
            loadSlotsForCurrentService();
        }

        function serviceIsActive(serviceId) {
            if (angular.isUndefined(vm.currentService)) {
                return false;
            }
            return serviceId === vm.currentService.getId();
        }
    }
})();
