(function () {
    'use strict';

    angular
            .module('headproClient')
            .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
                .state('requiredWithId',
                        {
                            url: '/workspace/{workspaceId:int}/required/{serviceId:int}',
                            templateUrl: 'app/edition/required.edition.html',
                            controller: 'RequiredEditionController',
                            controllerAs: 'ctrl'
                        })
                .state('required',
                        {
                            url: '/workspace/{workspaceId:int}/required',
                            templateUrl: 'app/edition/required.edition.html',
                            controller: 'RequiredEditionController',
                            controllerAs: 'ctrl'
                        })
                .state('availability',
                        {
                            url: '/workspace/{workspaceId:int}/availability',
                            templateUrl: 'app/edition/availability.edition.html',
                            controller: 'AvailabilityEditionController',
                            controllerAs: 'ctrl'
                        })
                .state('optimize',
                        {
                            url: '/workspace/{workspaceId:int}/optimizer',
                            templateUrl: 'app/optimization/optimization.html',
                            controller: 'OptimizationController',
                            controllerAs: 'ctrl'
                        })
                .state('projects',
                        {
                            url: '/workspace/{workspaceId:int}/projects',
                            templateUrl: 'app/modules/projects/projects.html',
                            controller: 'ProjectsController',
                            controllerAs: 'ctrl'
                        })
                .state('services',
                        {
                            url: '/workspace/{workspaceId:int}/services',
                            templateUrl: 'app/modules/services/services.html',
                            controller: 'ServicesController',
                            controllerAs: 'ctrl'
                        })
                .state('charts',
                        {
                            url: '/workspace/{workspaceId:int}/charts',
                            templateUrl: 'app/charts/charts.html',
                            controller: 'ChartsController',
                            controllerAs: 'ctrl'
                        })
                .state('start',
                        {
                            url: '/start',
                            templateUrl: 'app/startsite/start.html',
                            controller: 'StartsiteController',
                            controllerAs: 'ctrl'
                        });

        $urlRouterProvider.otherwise('/start');
    }
})();
