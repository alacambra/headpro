/* global expect, jasmine */

describe('ServicesController', function () {
    var $rootScope, $scope, $q, LoadManager, Service, totalPreloadedServices;

    this.logPrefix = "ServicesControllerSpec:";
    beforeEach(module('services', 'templates'));

    beforeEach(inject(function (
            _$rootScope_,
            _$q_,
            _Service_,
            _$templateCache_,
            $compile,
            $controller
            ) {

        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $q = _$q_;
        Service = _Service_;

        totalPreloadedServices = getServices().length;

        var templateHtml = _$templateCache_.get("app/modules/services/services.html");
        
        expect(templateHtml).toBeDefined();
        
        var formElem = angular.element("<div>" + templateHtml + "</div>");
        $compile(formElem)($scope);
        
        LoadManager = {
            appInitilized: true
        };

        $controller('ServicesController as ctrl', {
            $scope: $scope,
            LoadManager: LoadManager,
            DataManager: {
                getServices: function () {
                    var deferred = $q.defer();
                    deferred.resolve(getServices());
                    return deferred.promise;
                },
                updateService: function (resource) {
                    expect(resource).toEqual(jasmine.any(Service));
                    var deferred = $q.defer();
                    deferred.resolve();
                    return deferred.promise;
                },
                createService: function (resource) {
                    expect(resource).toEqual(jasmine.any(Service));
                    var deferred = $q.defer();
                    deferred.resolve(1);
                    return deferred.promise;
                }
            }
        });
        $rootScope.$apply();
    }));

    it('should have initial state loaded', inject(function () {

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeTruthy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.ctrl.resources.length).toBe(totalPreloadedServices);

        expect($scope.createForm.$pristine).toBeTruthy();
        expect($scope.createForm.$dirty).toBeFalsy();
        expect($scope.createForm.$valid).toBeFalsy();
        expect($scope.createForm.$pending).toBeFalsy();

    }));

    it('should be on updating state', inject(function () {

        $scope.ctrl.setCurrentResource({});

        $scope.updateForm.name.$setViewValue("lala");

        expect($scope.ctrl.updateFormIsVisible()).toBeTruthy();
        expect($scope.ctrl.creationFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeTruthy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.ctrl.resources.length).toBe(totalPreloadedServices);

        expect($scope.updateForm.$pristine).toBeFalsy();
        expect($scope.updateForm.$dirty).toBeTruthy();
        expect($scope.updateForm.$valid).toBeTruthy();
        expect($scope.updateForm.$pending).toBeFalsy();

    }));

    it('should reset form', inject(function () {

        $scope.ctrl.setCurrentResource({});
        $scope.updateForm.name.$setViewValue("lala");
        $rootScope.$apply();

        expect($scope.ctrl.currentResource.name).toBe("lala");

        $scope.ctrl.resetForm($scope.updateForm, "currentResource");
        $rootScope.$apply();

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeTruthy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.ctrl.currentResource).toBe(undefined);
        expect($scope.updateForm.$pristine).toBeTruthy();
        expect($scope.updateForm.$dirty).toBeFalsy();
        expect($scope.updateForm.$valid).toBeFalsy();

    }));

    it('should show create form', inject(function () {

        expect($scope.ctrl.resourceToCreate).toBe(undefined);
        $scope.ctrl.createNewResource();
        $rootScope.$apply();

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeTruthy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeFalsy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.createForm.$pristine).toBeTruthy();
        expect($scope.createForm.$dirty).toBeFalsy();
        expect($scope.createForm.$valid).toBeFalsy();

    }));

    it('should be introducing data of new service ', inject(function () {

        expect($scope.ctrl.resourceToCreate).toBe(undefined);
        $scope.ctrl.createNewResource();
        $scope.createForm.name.$setViewValue("lala");
        $rootScope.$apply();

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeTruthy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeFalsy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect(angular.isDefined($scope.ctrl.resourceToCreate)).toBeTruthy();
        expect($scope.createForm.$pristine).toBeFalsy();
        expect($scope.createForm.$dirty).toBeTruthy();
        expect($scope.createForm.$valid).toBeTruthy();

    }));

    it('should assign an id when project creation succeeds', inject(function () {

        $scope.ctrl.resourceToCreate = {};
        $scope.ctrl.createResource();
        $rootScope.$apply();

        expect($scope.ctrl.resources.length).toBe(totalPreloadedServices + 1);
        $scope.ctrl.resources.forEach(function (p) {
            expect(p.id).toBeDefined();
        });

        expect($scope.ctrl.resourceToCreate).toBeUndefined();

    }));

    it('should update service using type Service', inject(function () {

        var name = "name updated";

        $scope.ctrl.currentResource = {id: 21, name: name};
        $scope.ctrl.updateResource();
        $rootScope.$apply();

        expect($scope.ctrl.currentResource).toBeUndefined();

        var found = false;

        $scope.ctrl.resources.forEach(function (resource) {
            if (resource.id === 21) {
                found = true;
                expect(resource.name).toBe(name);
            }
        });

        expect(found).toBeTruthy();

    }));
    
      it('should restore resources after cancel edit', inject(function () {

        var name = "name updated";
        var id = $scope.ctrl.resources[0].id;
        var originalName = $scope.ctrl.resources[0].name;
        $scope.ctrl.resources[0].name = name;

        expect(originalName).not.toBe($scope.ctrl.resources[0].name);

        var found = false;

        $scope.ctrl.resources.forEach(function (resource) {
            if (resource.id === id) {
                found = true;
                expect(resource.name).toBe(name);
            }
        });

        expect(found).toBeTruthy();

        $scope.ctrl.restoreResources();

        found = false;

        $scope.ctrl.resources.forEach(function (resource) {
            if (resource.id === id) {
                found = true;
                expect(resource.name).toBe(originalName);
            }
        });

        expect(found).toBeTruthy();
    }));

    function getServices() {
        var services = [new Service({
                name: "r1",
                id: 20
            }), new Service({
                name: "r2",
                id: 21
            })];

        return services;
    }
});