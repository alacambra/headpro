(function () {
    'use strict';
    angular.module('services').factory('Service', ServiceFactory);

    function ServiceFactory() {
        function Service(data) {

            function init() {
                if (data) {

                    //The object given should only be used to initialize. 
                    //Further changes on it will not take effect in the created project. 
                    //Object correctly encapsulated.
                    data = angular.copy(data);

                } else {
                    data = {
                        id: undefined,
                        name: undefined
                    };
                }
            }

            var that = this;

            this.getId = function () {
                return data.id;
            };

            this.getName = function () {
                return data.name;
            };

            this.getSlots = function () {
                return data.slots;
            };

            this.getStartDate = function () {
                return undefined;
            };

            this.getEndDate = function () {
                return undefined;
            };

            this.setId = function (id) {
                return data.id = id;
            };

            this.getStartIndex = function () {
                return undefined;
            };

            this.getEndIndex = function () {
                return undefined;
            };

            this.setName = function (name) {
                data.name = name;
                return that;
            };

            this.getDTO = function () {
                var dto = angular.copy(data);
                return dto;
            };

            this.setFromDto = function (dto) {
                data = dto;
            };

            this.hasLoadedSlots = function () {
                return angular.isArray(data.slots) && data.slots.length > 0;
            };
            
            this.getAttributes = function (){
                return angular.copy(data);
            };

            init();
        }

        return Service;
    }
})();
