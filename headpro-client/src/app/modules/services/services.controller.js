(function () {
    'use strict';

    angular
            .module('services')
            .controller('ServicesController', ServicesController);

    function ServicesController($scope, $log, $uibModal, toastr, DataManager, LoadManager, Service) {

        var vm = this;
        var logPrefix = "ResourcesController:";
        var masterResources = [];

        /**
         * state: start, creating, editing
         */
        var states = {
            start: "start",
            creating: "creating",
            updating: "updating"
        };

        var currentState = "start";

        vm.updateResource = updateResource;
        vm.createResource = createResource;
        vm.removeResource = removeResource;
        vm.setCurrentResource = setCurrentResource;
        vm.isActive = isActive;
        vm.restoreResources = restoreResources;
        vm.resetForm = resetForm;
        vm.confirmDeletion = confirmDeletion;

        vm.updateFormIsVisible = updateFormIsVisible;
        vm.creationFormIsVisible = creationFormIsVisible;
        vm.creationFormButtonIsVisible = creationFormButtonIsVisible;
        vm.resourcesListIsVisible = resourcesListIsVisible;
        vm.createNewResource = createNewResource;

        vm.resources = [];
        vm.showCreationForm = false;
        vm.currentResource = undefined;
        vm.resourceToCreate = undefined;

        if (LoadManager.appInitilized) {
            init();
        }

        $scope.$on(LoadManager.workspaceChangedEvent, function () {
            init();
        });

        $scope.$on(LoadManager.appInitilizedEvent, function () {
            init();
        });

        function init() {
            DataManager.getServices().then(function (resources) {
                resources.forEach(function (resource) {
                    masterResources.push(angular.copy(resource.getAttributes()));
                    vm.resources.push(angular.copy(resource.getAttributes()));
                });
            });
        }

        function isActive(resource) {
            return angular.isDefined(vm.currentResource) && vm.currentResource.id === resource.id;
        }

        function findMasterResource(resourceId, newResource) {

            if (!angular.isNumber(resourceId)) {
                throw new {
                    message: "Invalid id given",
                    value: resourceId
                };
            }

            for (var i = 0; i < masterResources.length; i++) {
                if (masterResources[i].id === resourceId) {

                    var oldResource = masterResources[i];

                    if (angular.isObject(newResource)) {
                        masterResources[i] = newResource;
                    } else if (angular.isString(newResource) && newResource === "remove") {
                        masterResources.splice(i, 1);
                    }

                    return oldResource;
                }
            }
        }

        function setCurrentResource(resource) {
            if (angular.isDefined(vm.currentResource)) {
                restoreResources();
                resetForm($scope.updateForm, vm.currentResource);
            }
            setState(states.updating);
            vm.currentResource = resource;
            $log.debug(logPrefix, "current resource is", resource);
            $log.debug(logPrefix, angular.isDefined(vm.currentResource));
        }

        function restoreResources() {
            vm.resources = angular.copy(masterResources);
            vm.currentResource = undefined;
        }

        function updateResource() {
            DataManager.updateService(new Service(vm.currentResource)).then(function () {

                toastr.info("Service " + vm.currentResource.name + " successfully updated");
                var oldResource = findMasterResource(vm.currentResource.id, vm.currentResource);

                if (angular.isUndefined(oldResource)) {
                    throwException("old service not found");
                }

                restoreResources();
                resetForm($scope.updateForm, vm.currentResource);

            }, function (error) {
                toastr.error("Not possible to update resource");
                $log.error("resource not updated: " + error);
            });
        }

        function createResource() {

            var resource = new Service(vm.resourceToCreate);

            DataManager.createService(resource).then(function (id) {

                resource.setId(id);
                masterResources.push(angular.copy(resource.getAttributes()));
                restoreResources();
                toastr.info("Service correctly created");
                resetForm($scope.createForm, "resourceToCreate");

            }, function (e) {
                var msg = angular.isDefined(e) ? (": " + e) : "";
                toastr.error("Not possible to create resource" + msg);
            });
        }

        function resetForm(form, entity) {
            setState(states.start);
            vm[entity] = undefined;
            form.$setPristine();
            form.$setUntouched();
        }

        function removeResource(index, resourceId) {
            $log.log(index, resourceId);
        }

        function setState(st) {
            currentState = st;
        }

        function updateFormIsVisible() {
            return currentState === states.updating;
        }

        function creationFormIsVisible() {
            return currentState === states.creating;
        }

        function creationFormButtonIsVisible() {
            return currentState === states.start;
        }

        function resourcesListIsVisible() {
            return currentState === states.start || currentState === states.updating;
        }

        function createNewResource() {
            currentState = states.creating;
        }

        function confirmDeletion() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/modules/projects/modal.delete.html',
                controller: 'ModalInstanceController',
                size: "md",
                resolve: {
                    resource: vm.currentResource
                }
            });

            modalInstance.result.then(function () {

                DataManager.deleteService(new Service(vm.currentResource)).then(function () {
                    toastr.success("Service " + vm.currentResource.name + " successfully removed");
                    findMasterResource(vm.currentResource.id, "remove");
                    restoreResources();
                    resetForm($scope.updateForm, "currentResource");
                }).catch(function (reason) {
                    toastr.error("Impossible to remove service:" + reason);
                });

            }, function () {
                $log.info(logPrefix, 'Modal dismissed at: ' + new Date());
            });
        }
    }

    function ServicesControllerException(message) {
        ServicesControllerException.prototype = new Error();
        this.name = 'ProjectsControllerException';
        this.message = message;
        this.toString = function () {
            return message;
        };
    }

    ServicesControllerException.prototype.constructor = ServicesControllerException;

    function throwException(message) {
        ServicesControllerException.prototype = new Error();
        throw new ServicesControllerException(message);
    }
})();
