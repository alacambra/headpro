(function () {
    'use strict';
    angular.module('timeRange').service('GlobalTimeRangeService', GlobalTimeRange);
    function GlobalTimeRange($log, $q, $rootScope, TimeRange) {

        var logPrefix = "GlobalTimeRange";
        var that = this;
        var broadcastChange = false;
        var timeRanges = [];

        this.timeRangeChangedEvent = "globalTimeRangeService:changed";
        this.setDatesRange = setDatesRange;

        this.appInitializied = function () {
            broadcastChange = true;
        };

        this.getTimeRange = function (timeRangeId) {

            var timeRange = undefined;
            checkIdOrException(timeRangeId);
            
            timeRanges.forEach(function (tr) {
                if (tr.getId() === timeRangeId) {
                    timeRange = tr;
                }
            });

            if (angular.isUndefined(timeRange)) {
                throw new GlobalTimeRangeServiceException("getTimeRange: TimeTange not found: " + timeRangeId);
            }

            return timeRange;
        };

        this.setTimeRange = function (timeRange) {

            timeRange = timeRange || {};

            checkIdOrException(timeRange.getId());

            if (angular.isUndefined(timeRange) || angular.isPrototypeOf(TimeRange)) {
                throw new GlobalTimeRangeServiceException("setTimeRangeRange: Invalid object type");
            }

            replaceOrInsertTimeRange(timeRange);
            notifyTimeRangeChange(timeRange);
        };

        function notifyTimeRangeChange(timeRange) {
            if (broadcastChange) {
                $rootScope.$broadcast(that.timeRangeChangedEvent, timeRange);
            }
        }

        function setDatesRange(startDate, endDate, timeRangeId) {

            checkIdOrException(timeRangeId);
            verifyDateOrException(startDate);
            verifyDateOrException(endDate);

            var timeRange = new TimeRange(startDate, endDate, timeRangeId);
            replaceOrInsertTimeRange(timeRange);

            timeRange.whenCompleted().then(function () {
                notifyTimeRangeChange(timeRange);

            }).catch(function (reason) {
                throw new GlobalTimeRangeServiceException("setDatesRange: " + reason);
            });

            return that;
        }


        function  timeRangeExist(timeRange) {
            for (var i = 0; i < timeRanges.length; i++) {
                if (timeRanges[i].getId() === timeRange.getId()) {
                    return true;
                }
            }
            return false;
        }

        function checkIdOrException(id, verifyUniqueness) {
            if (angular.isUndefined(id)) {
                throw new GlobalTimeRangeServiceException("checkIdOrException: an id must be given");
            }

            if (verifyUniqueness === true) {
                timeRanges.forEach(function (timeRange) {
                    if (angular.isDefined(timeRange.getId()) && timeRange.getId() === id) {
                        throw new GlobalTimeRangeServiceException("checkIdOrException: Id is not unique");
                    }
                });
            }
        }

        function replaceOrInsertTimeRange(timeRange) {
            for (var i = 0; i < timeRanges.length; i++) {
                if (timeRanges[i].getId() === timeRange.getId()) {
                    timeRanges[i] = timeRange;
                    return;
                }
            }

            timeRanges.push(timeRange);
        }

        function verifyDateOrException(date) {
            if (!angular.isDate(date) && angular.isNumber(date)) {
                date = new Date(date);
            } else if (!angular.isDate(date)) {
                throw new GlobalTimeRangeServiceException("verifyDateOrException: invalid date given");
            }
        }

        function GlobalTimeRangeServiceException(message) {
            this.name = 'GlobalTimeRangeServiceException';
            this.message = message;
        }

        GlobalTimeRangeServiceException.prototype = new Error();
        GlobalTimeRangeServiceException.prototype.constructor = GlobalTimeRangeServiceException;
    }
})();
