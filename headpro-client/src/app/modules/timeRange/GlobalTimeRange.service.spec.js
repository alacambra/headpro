/* global expect */

describe('GlobalTimeRangeService', function () {
    var GlobalTimeRangeService, $q, $rootScope, TimeRange;
    this.logPrefix = "SlotsServiceSpec:";
    beforeEach(module('timeRange'));
    beforeEach(module("dataManagement", function ($provide) {
        $provide.value("DataManager", {
            getIndexForDate: function (date) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                deferred.resolve({
                    index: 0,
                    date: date
                });
                return promise;
            }
        });
    }));

    beforeEach(inject(function (_$q_) {
        $q = _$q_;
    }));

    beforeEach(inject(function (_GlobalTimeRangeService_, _$rootScope_, _TimeRange_) {
        $rootScope = _$rootScope_;
        GlobalTimeRangeService = _GlobalTimeRangeService_;
        TimeRange = _TimeRange_;
        $rootScope.$apply();

    }));

    it('should update dates', function () {

        var executed = false;
        var dates = {
            startDate: new Date(0),
            endDate: new Date(1000000)
        };
        var tr = new TimeRange(dates.startDate, dates.endDate, "test", dates);
        GlobalTimeRangeService.setTimeRange(tr);
        $rootScope.$apply();

        tr = GlobalTimeRangeService.getTimeRange("test")
        expect(tr.getStartDate().getTime()).toEqual(dates.startDate.getTime());
        expect(tr.getEndDate().getTime()).toEqual(dates.endDate.getTime());

    });
});
