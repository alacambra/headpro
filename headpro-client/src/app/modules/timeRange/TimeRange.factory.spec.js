/* global expect */

describe('TimeRange', function () {
    var TimeRange, $resource, $rootScope;

    this.logPrefix = "TimeRangeSpec:";
    beforeEach(module('timeRange'));

    beforeEach(inject(function (_TimeRange_, _$resource_, _$rootScope_) {
        $resource = _$resource_;
        TimeRange = _TimeRange_;
        $rootScope = _$rootScope_;
    }));

    afterEach(inject(function ($httpBackend) {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    }));

    it('should create time range object given the indexes', inject(function ($httpBackend) {

        $httpBackend.expectGET('/resources/time/range?end=9&start=0&type=index').respond(200, angular.toJson({
            startIndex: 0,
            endIndex: 9,
            startDate: 2 * 7 * 3600000,
            endDate: 11 * 7 * 3600000,
            rangeLength: 10 - 0 + 1
        }));
        var tr = new TimeRange(0, 9, "id");
        $httpBackend.flush();
        expect(tr.getStartIndex()).toBe(0);
        expect(tr.getEndIndex()).toBe(9);
        expect(tr.getRangeLength()).toBe(11);
    }));
    
    it('should create time range object given the dates', inject(function ($httpBackend) {

        $httpBackend.expectGET('/resources/time/range?end=10000&start=0&type=date').respond(200, angular.toJson({
            startIndex: 0,
            endIndex: 10,
            startDate: 0,
            endDate: 10000,
            rangeLength: 11
        }));
        
        var tr = new TimeRange(new Date(0), new Date(10000), "id");
        $httpBackend.flush();
        
        expect(tr.getStartIndex()).toBe(0);
        expect(tr.getEndIndex()).toBe(10);
        expect(tr.getRangeLength()).toBe(11);
    }));
});