/* global moment */

(function () {
    'use strict';
    angular.module('timeRange').factory('TimeRange', TimeRangeFactory);

    function TimeRangeFactory($resource, $q, $timeout, $log) {

        var logPrefix = "TimeRangeFactory:";

        function TimeRange(start, end, id, resource) {

            if (!angular.isString(id)) {
                throw new Error("id must be given");
            }

            var that = this;
            var timeRangeIsCompleted = $q.defer();
            var conf = validatePeriodAndGetConf(start, end);

            if (angular.isUndefined(resource)) {

                resource = {
                    startIndex: undefined,
                    endIndex: undefined,
                    startDate: undefined,
                    endDate: undefined,
                    rangeLength: undefined
                };

                angular.extend(resource, $resource('/resources/time/range/'));

                this.resolved = false;

                resource.get({
                    type: conf.typeGiven,
                    start: conf.start,
                    end: conf.end
                }).$promise
                        .then(function (tr) {

                            tr.startDate = moment(tr.startDate).day("Monday").startOf('day').toDate().getTime();
                            tr.endDate = moment(tr.endDate).day("Monday").startOf('day').toDate().getTime();

                            angular.extend(resource, tr);
                            that.resolved = true;
                            timeRangeIsCompleted.resolve();

                        })
                        .catch(function (reason) {
                            timeRangeIsCompleted.reject(reason);
                        })
                        .finally();
            } else {
                this.resolved = true;
            }

            this.whenCompleted = function () {
                return timeRangeIsCompleted.promise;
            };

            this.clone = function (copyId) {
                var copy = new TimeRange(start, end, copyId, resource);
                return copy;
            };

            this.getId = function () {
                return id;
            };

            this.getStartIndex = function () {
                return resource.startIndex;
            };
            this.getEndIndex = function () {
                return resource.endIndex;
            };
            this.getStartDate = function () {
                return resource.startDate;
            };
            this.getEndDate = function () {
                return resource.endDate;
            };
            this.getRangeLength = function () {
                return resource.rangeLength;
            };

            this.toString = function () {
                return angular.toJson(resource);
            };
        }

        function validatePeriodAndGetConf(start, end) {
            if (angular.isUndefined(start) || angular.isUndefined(end)) {
                throw new Error("validatePeriodAndGetConf: start and end must be given");
            }

            if (start > end) {
                throw new Error("Start must be lower than end");
            }

            if (angular.isDate(start) && angular.isDate(end)) {
                return {
                    typeGiven: "date",
                    start: start.getTime(),
                    end: end.getTime()
                };
            } else if (angular.isNumber(start) && angular.isNumber(end)) {
                return {
                    typeGiven: "index",
                    start: start,
                    end: end
                };
            } else {
                throw new Error("validatePeriodAndGetConf: start and end must be of the same type(index-int or Date)");
            }
        }

        return TimeRange;
    }
})();
