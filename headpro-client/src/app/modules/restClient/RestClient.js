(function () {

  angular.module('dataManagement').factory('DataManager', RestClient);
  function RestClient($http, $log, $q, $location, $window, Project, Service, toastr) {

    var protocol = $location.protocol();
    var host = $location.host();
    var port = $location.port();
    var baseUrl = protocol + "://" + host + ":" + port + "/resources";
    var currentWorksapceId = undefined;
    var logErrors = true;
    var toastErrors = true;
    var waitForWs = [];
    var logPrefix = "RestClient:";

    function getBaseWorkspaceUrl() {
      return [baseUrl, "workspace", currentWorksapceId].join("/");
    }

    function getWorkspaces() {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [baseUrl, "user", "my", "workspaces"].join("/");

      $http.get(url).success(function (data) {

        deferred.resolve(data);

        while (waitForWs.length) {
          var subscriber = waitForWs.pop();
          subscriber.method.apply(this, subscriber.params).then(function (data) {
            subscriber.deferred.resolve(data);
          }, function (data) {
            subscriber.deferred.reject(data);
          });
        }

      }).error(
        function (data, status, headers, config, statusText) {
          trackError(data, status, headers, config, statusText, url);

          deferred.reject(data);

          while (waitForWs.length) {
            var subscriber = waitForWs.pop();
            subscriber.deferred.reject(data);
          }
        });
      return promise;
    }

    /**
     * Return all projects with their resourceSlots.
     * Projects with empty slots will be also returned
     * @returns {deferred.promise}
     */
    function getProjectsWithSlots(project, withServices) {

      if(angular.isUndefined(project) || angular.isUndefined(project.getId())){
        throw new Error("project or project id not set");
      }

      withServices = withServices || false;

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "project", project.getId(), "slots"].join("/");

      if (withServices) {
        url += "?services=true";
      }


      $http.get(url).success(function (data) {
        deferred.resolve(new Project(data));
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * Return all services with their serviceSlots.
     * Services with empty slots will be also returned
     * @returns {deferred.promise}
     */
    function getServicesWithSlots() {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "available"].join("/");
      $http.get(url).success(function (data) {
        deferred.resolve(convertToServiceEntity(data));
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * Return the serviceSlots of the given service.
     * Services with empty slots will be also returned
     */
    function getServiceSlots(serviceId) {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "service", serviceId, "slots"].join("/");
      $http.get(url).success(function (data) {
        deferred.resolve(convertToServiceEntity(data));
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * Load the projects with their resourceSlots for the given service. Projects without slots are not added
     * @param {int} serviceId
     * @returns {deferred.promise}
     */
    function getProjectsWithSlotsOfService(serviceId) {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "service", serviceId, "project/slots"].join("/");
      $http.get(url).success(function (data) {
        deferred.resolve(convertToProjectEntity(data));
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * return all projects without slots
     * @returns {deferred.promise}
     */
    function getProjects() {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "projects"].join("/");
      $http.get(url).success(function (data) {
        deferred.resolve(convertToProjectEntity(data));
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function createProject(project) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "project"].join("/");
      $http.post(url, project.getDTO()).success(function (data, status, headers) {

        var id = getIdFromLocation(headers("location"));
        deferred.resolve(id);
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function deleteProject(project) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "project", project.getId()].join("/");
      $http.delete(url).success(function (data, status, headers) {
        deferred.resolve();
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function updateProject(project) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      if (!angular.isNumber(project.getId())) {
        deferred.reject("project must have an ID");
        return promise;
      }

      var url = [getBaseWorkspaceUrl(), "project", project.getId()].join("/");
      // it should already be a project...
      $http.put(url, project.getDTO()).success(function (data, status) {
        deferred.resolve(data, status);
      }).error(function (data, status, headers, config) {
        trackError(data, status, headers, config, status, url);
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * Returns all resourceSlots converted in size 1. These slots are not persisted but prepared in the server
     * based on the persisted resourseSlots.
     *
     * @returns {deferred.promise}
     */
    function getSimpleSlots() {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "simpleslots"].join("/");
      $http.get(url).success(function (data) {

        var slots = {projects: {}, services: {}};
        slots.projects = convertToProjectEntity(data.projects);
        slots.services = convertToServiceEntity(data.services);
        deferred.resolve(slots);

      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * Return all services without slots
     * @returns {deferred.promise}
     */
    function getServices() {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "services"].join("/");
      $http.get(url).success(function (data) {
        deferred.resolve(convertToServiceEntity(data));
      }).error(function (data) {
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * Returns the service with the given id without slots
     * @param {type} serviceId
     * @returns {deferred.promise}
     */
    function getService(serviceId) {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "service", serviceId].join("/");
      $http.get(url).success(function (data) {
        deferred.resolve(new Service(data));
      }).error(function (data) {
        deferred.reject(data);
      });
      return promise;
    }

    function createService(service) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "service"].join("/");
      $http.post(url, service.getDTO()).success(function (data, status, headers) {
        deferred.resolve(getIdFromLocation(headers("location")));
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function deleteService(service) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "service", service.getId()].join("/");
      $http.delete(url).success(function (data, status, headers) {
        deferred.resolve();
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function updateService(service) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      if (!angular.isNumber(service.getId())) {
        $log.debug(logPrefix, service);
        deferred.reject("service must have an ID");
        return promise;
      }

      var url = [getBaseWorkspaceUrl(), "service", service.getId()].join("/");
      $http.put(url, service.getDTO()).success(function (data) {
        deferred.resolve(data);
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    /**
     * Adds a new resourceSlot
     * @param {int} projectId
     * @param {Object} slot
     * @returns {deferred.promise}
     */
    function addSlot(projectId, slot) {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "project", projectId, "slot"].join("/");
      $http.post(url, slot).success(function (data) {
        deferred.resolve(data);
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function addServiceSlot(serviceId, slot) {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "service", serviceId, "slot"].join("/");
      $http.post(url, slot).success(function (data) {
        deferred.resolve(data);
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function getRequiredResources(classifier, doubleClassifier) {

      doubleClassifier = doubleClassifier || false;
      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "required"].join("/");
      url = url + "?" + "classifier=" + classifier + "&doubleClassifier=" + doubleClassifier;

      $http.get(url).success(function (data) {

        if (classifier === "service") {
          data = convertToServiceEntity(data);
        } else {
          data = convertToProjectEntity(data);
        }

        deferred.resolve(data);

      }).error(function (data, status, headers, config, statusText) {

        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);

      });
      return promise;
    }

    function getAvailableResources() {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [getBaseWorkspaceUrl(), "available"].join("/");

      $http.get(url).success(function (data) {

        deferred.resolve(convertToServiceEntity(data));

      }).error(function (data, status, headers, config, statusText) {

        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);

      });
      return promise;

    }

    /**
     * Return tha absolute index for the given date
     * @param {Date} date
     * @returns {deferred.promise}
     */
    function getIndexForDate(date) {

      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [baseUrl, "time", date.getTime()].join("/");
      $http.get(url).success(function (data) {
        deferred.resolve(data);
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function convertToProjectEntity(projects) {
      return projects.map(function (project) {
        return new Project(project);
      });
    }

    function convertToServiceEntity(services) {
      return services.map(function (service) {
        return new Service(service);
      });
    }

    function logout() {
      var deferred = $q.defer();
      var promise = deferred.promise;
      var url = [baseUrl, "user", "logout"].join("/");
      $http.get(url).success(function (data, status, headers) {
        deferred.resolve(data);
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function getRangeTimestamps(start, end) {

      var deferred = $q.defer();
      var promise = deferred.promise;

      if (!angular.isNumber(start) || start < 0) {
        $log.error(logPrefix, "invalid start value:", start);
        deferred.reject();
        return promise;
      }

      var url = [baseUrl, "time", "timestamps"].join("/");
      url = url + "?start=" + start + (angular.isDefined(end) ? "&end=" + end : "");
      $http.get(url).success(function (data, status, headers) {
        deferred.resolve(data);
      }).error(function (data, status, headers, config, statusText) {
        trackError(data, status, headers, config, statusText, url);
        deferred.reject(data);
      });
      return promise;
    }

    function getIdFromLocation(loc) {
      var location = loc || "";
      var path = location.split("/");
      return parseInt(path.pop(), 10);
    }

    function trackError(data, status, headers, config, statusText, url) {

      if (logErrors) {
        $log.log("error:" + [data, status, angular.toJson(config), statusText].join("###"));
      }

      if (toastErrors) {
        var link = '<a href="' + url + '" target="_blank">' + url + '</a>';
        toastr.error("Server says: " + status + " for " + link);
      }
    }

    function setCurrentWorkspaceId(wsId) {
      currentWorksapceId = wsId;
      var deferred = $q.defer();
      deferred.resolve();
      return deferred.promise;
    }

    function invoke(fn) {
      return function () {
        if (angular.isDefined(currentWorksapceId) || true) {
          return fn.apply(this, arguments);
        } else {
          var deferred = $q.defer();
          var promise = deferred.promise;
          waitForWs.push(
            {method: fn, params: arguments, deferred: deferred}
          );

          return promise;
        }
      };
    }

    return {
      createProject: invoke(createProject),
      deleteProject: deleteProject,
      updateProject: updateProject,
      getProjects: invoke(getProjects),
      getProjectsWithSlots: getProjectsWithSlots,
      addSlot: invoke(addSlot),
      getSimpleSlots: invoke(getSimpleSlots),
      createService: invoke(createService),
      deleteService: deleteService,
      updateService: invoke(updateService),
      getServices: invoke(getServices),
      getIndexForDate: invoke(getIndexForDate),
      getProjectsWithSlotsOfService: invoke(getProjectsWithSlotsOfService),
      getService: invoke(getService),
      getServicesWithSlots: invoke(getServicesWithSlots),
      addServiceSlot: invoke(addServiceSlot),
      getServiceSlots: invoke(getServiceSlots),
      setCurrentWorkspaceId: setCurrentWorkspaceId,
      getRequiredResources: invoke(getRequiredResources),
      getAvailableResources: invoke(getAvailableResources),
      getRangeTimestamps: getRangeTimestamps,
      logout: logout,
      getWorkspaces: getWorkspaces
    };
  }
}());

