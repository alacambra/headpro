(function () {
  'use strict';
  angular.module('projects')
    .directive('projectTimeIssues', ProjectTimeIssues);


  function ProjectTimeIssues($log) {

    var logPrefix = "ProjectTimeIssues:"

    function link($scope, elm, attrs, ctrl) {

      $log.debug(logPrefix, $scope.ranges);

    }

    return {
      restrict: 'E',
      link: link,
      templateUrl: "app/modules/projects/project.issues.html",
      scope: {
        ranges: '=ranges'
      }
    };
  }
})();
