(function () {
  'use strict';
  angular.module('projects')
    .directive('projectXRay', ProjectXRay);


  function ProjectXRay($timeout, $q, $stateParams, $log,
                       DataManager, LoadManager, toastr, Service, localStorageService,
                       SlotService, ChartProcessor) {

    var logPrefix = "ProjectXRay:"

    function link($scope, elm, attrs, ctrl) {

      var currentProject;
      var totalUsedPerService = [];
      $scope.totalAllResources = 0;
      var requiredResourcesSimpleSlots;
      var services = [];

      load();
      initScope();

      function initScope() {
        $scope.totalUsedPerService = totalUsedPerService;
      }

      function loadChart() {
        var chartProcessor = new ChartProcessor();

        chartProcessor.setStartTime(0);
        $scope.chart.options = chartProcessor.getChartOptions();

        chartProcessor.setStackedResourcesSteps(requiredResourcesSimpleSlots);
        chartProcessor.setChartKeys(services);
        chartProcessor.setStartTime(currentProject.getStartDate().getTime());

        $scope.chart.data = chartProcessor.load().getChartItems();

        $timeout(function () {
          $scope.chart.api.updateWithOptions($scope.chart.options);
        });
      }

      function load(){

        DataManager.getProjectsWithSlots($scope.project, true).then(function (project) {

          currentProject = project;

          project.getServices().forEach(function (service) {

            var total = 0;
            services.push(service.getName());

            service.getSlots().forEach(function (slot) {
              total+=slot.duration * slot.valuePerStep;
            });

            totalUsedPerService.push({
              service:service,
              total:total
            });

            $scope.totalAllResources +=total;
          });

          requiredResourcesSimpleSlots = SlotService.toSimpleSlots(project.getServices(), {timeRange:currentProject.getTimeRange()});
          loadChart();
        });
      }
    }

    return {
      restrict: 'E',
      link: link,
      templateUrl: "app/modules/projects/project.xray.html",
      scope: {
        ranges: '=ranges',
        project:'=project'
      }
    };
  }
})();
