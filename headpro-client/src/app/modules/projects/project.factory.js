(function () {
  'use strict';
  angular.module('entities').factory('Project', ProjectFactory);

  function getFormatedDate(date) {
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
  }

  function ProjectFactory($log, Service, TimeRange) {

    var logPrefix = "ProjectFactory";

    function Project(data) {

      var services = [];

      function init() {
        if (data) {

          //The object given should only be used to initialize.
          //Further changes on it will not take effect in the created project.
          //Object correctly encapsulated.
          data = angular.copy(data);

          if (angular.isArray(data.services)) {
            data.services.forEach(function (service) {
              services.push(new Service(service));
            });
          }

          if (angular.isNumber(data.startDate)) {
            that.setFromDto(data);
          }

        } else {
          data = {
            id: undefined,
            startDate: undefined,
            endDate: undefined,
            startIndex: undefined,
            endIndex: undefined,
            name: undefined
          };
        }
      }

      var that = this;

      this.getId = function () {
        return data.id;
      };

      this.getStartDate = function () {
        return data.startDate;
      };

      this.getEndDate = function () {
        return data.endDate;
      };

      this.getStartIndex = function () {
        return data.startIndex;
      };

      this.getEndIndex = function () {
        return data.endIndex;
      };

      this.getName = function (name) {

        if (angular.isDefined(name)) {
          data.name = name;
        }

        return data.name;
      };

      this.getSlots = function () {
        return data.slots;
      };
      this.setId = function (id) {
        return data.id = id;
      };
      this.setStartDate = function (startDate) {
        data.startDate = startDate;
        return that;
      };

      this.setEndDate = function (endDate) {
        data.endDate = endDate;
        return that;
      };

      this.setStartIndex = function (startIndex) {
        data.startIndex = startIndex;
        return that;
      };

      this.setEndIndex = function (endIndex) {
        data.endIndex = endIndex;
        return that;
      };

      this.setFromTimeRange = function (timeRange) {
        data.startDate = new Date(timeRange.getStartDate());
        data.startIndex = timeRange.getStartIndex();
        data.endDate = new Date(timeRange.getEndDate());
        data.endIndex = timeRange.getEndIndex();

        return that;
      };

      this.getTimeRange = function () {
        var tr = new TimeRange(data.startDate, data.endDate, "",{
          startIndex: data.startIndex ,
          endIndex: data.endIndex,
          startDate: data.startDate,
          endDate: data.endDate,
          rangeLength: data.endIndex- data.startIndex +1
        });
        return tr;
      };

      this.setName = function (name) {
        data.name = name;
        return that;
      };

      this.getDTO = function () {
        var dto = angular.copy(data);
        dto.startDate = dto.startDate.getTime();
        dto.endDate = dto.endDate.getTime();

        return dto;
      };

      this.setFromDto = function (dto) {
        data = dto;
        data.startDate = new Date(dto.startDate);
        data.endDate = new Date(dto.endDate);
      };

      this.hasLoadedSlots = function () {
        return angular.isArray(data.slots) && data.slots.length > 0;
      };

      this.getAttributes = function () {
        return angular.copy(data);
      };

      this.getTimeDuration = function () {
        return data.endDate.getTime() - data.startDate.getTime();
      };

      this.getServices = function () {
        return services;
      };

      init();
    }

    return Project;
  }
})();
