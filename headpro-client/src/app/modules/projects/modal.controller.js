(function () {
    'use strict';
    angular.module('projects').controller('ModalInstanceController', function ($scope, $uibModalInstance, resource) {
        $scope.resource = resource;
        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    });
})();
