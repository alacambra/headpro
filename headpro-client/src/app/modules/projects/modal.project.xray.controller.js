(function () {
    'use strict';
    angular.module('projects').controller('ModalProjectXRayController', function ($scope, $uibModalInstance, ranges, project) {
        $scope.ranges = ranges;
        $scope.project = project;
        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    });
})();
