/* global expect, Service, jasmine */

describe('ProjectController', function () {
    var $rootScope, $scope, $q, LoadManager, Project, totalPreloadedProjects;

    this.logPrefix = "ProjectFormularSpec:";
    beforeEach(module('projects', 'templates'));

    beforeEach(inject(function (
            _$rootScope_,
            _$q_,
            _Project_,
            _$templateCache_,
            $compile,
            $controller
            ) {

        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $q = _$q_;
        Project = _Project_;

        totalPreloadedProjects = getProjects().length;

        var templateHtml = _$templateCache_.get("app/modules/projects/projects.html");
        var formElem = angular.element("<div>" + templateHtml + "</div>")
        $compile(formElem)($scope);

        LoadManager = {
            appInitilized: true
        };

        $controller('ProjectsController as ctrl', {
            $scope: $scope,
            LoadManager: LoadManager,
            DataManager: {
                getProjects: function () {
                    var deferred = $q.defer();
                    deferred.resolve(getProjects());
                    return deferred.promise;
                },
                updateProject: function (resource) {
                    expect(resource).toEqual(jasmine.any(Project));
                    var deferred = $q.defer();
                    deferred.resolve();
                    return deferred.promise;
                },
                createProject: function (resource) {
                    expect(resource).toEqual(jasmine.any(Project));
                    var deferred = $q.defer();
                    deferred.resolve(1);
                    return deferred.promise;
                }
            }
        });
        $rootScope.$apply();
    }));

    it('should have initial state loaded', inject(function () {

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeTruthy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.ctrl.resources.length).toBe(totalPreloadedProjects);

        expect($scope.createForm.$pristine).toBeTruthy();
        expect($scope.createForm.$dirty).toBeFalsy();
        expect($scope.createForm.$valid).toBeFalsy();
        expect($scope.createForm.$pending).toBeFalsy();

        expect($scope.updateForm.$pristine).toBeTruthy();
        expect($scope.updateForm.$dirty).toBeFalsy();
        expect($scope.updateForm.$valid).toBeFalsy();
        expect($scope.updateForm.$pending).toBeFalsy();

    }));

    it('should be on updating state', inject(function () {

        $scope.ctrl.setCurrentResource({});

        $scope.updateForm.pname.$setViewValue("lala");

        expect($scope.ctrl.updateFormIsVisible()).toBeTruthy();
        expect($scope.ctrl.creationFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeTruthy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.ctrl.resources.length).toBe(totalPreloadedProjects);

        expect($scope.updateForm.$pristine).toBeFalsy();
        expect($scope.updateForm.$dirty).toBeTruthy();
        expect($scope.updateForm.$valid).toBeFalsy();
        expect($scope.updateForm.$pending).toBeFalsy();

    }));

    it('should have have a valid update form', inject(function () {

        $scope.ctrl.setCurrentResource({});
        $scope.updateForm.pname.$setViewValue("lala");
        $scope.updateForm.startDate.$setViewValue("2016-05-09");
        $scope.updateForm.endDate.$setViewValue("2017-05-09");
        $rootScope.$apply();

        expect(angular.isDefined($scope.createForm)).toBeTruthy();
        expect(angular.isDefined($scope.ctrl.currentResource)).toBeTruthy();
        expect(angular.isUndefined($scope.ctrl.resourceToCreate)).toBeTruthy();

        expect($scope.ctrl.updateFormIsVisible()).toBeTruthy();
        expect($scope.ctrl.creationFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeTruthy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.updateForm.$pristine).toBeFalsy();
        expect($scope.updateForm.$dirty).toBeTruthy();
        expect($scope.updateForm.$valid).toBeTruthy();

    }));

    it('should reset form', inject(function () {

        $scope.ctrl.setCurrentResource({});
        $scope.updateForm.pname.$setViewValue("lala");
        $scope.updateForm.startDate.$setViewValue("2016-05-09");
        $scope.updateForm.endDate.$setViewValue("2017-05-09");
        $rootScope.$apply();

        expect($scope.ctrl.currentResource.name).toBe("lala");

        $scope.ctrl.resetForm($scope.updateForm, "currentResource");
        $rootScope.$apply();

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeTruthy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.ctrl.currentResource).toBe(undefined);
        expect($scope.updateForm.$pristine).toBeTruthy();
        expect($scope.updateForm.$dirty).toBeFalsy();
        expect($scope.updateForm.$valid).toBeFalsy();

    }));

    it('should show create form', inject(function () {

        expect($scope.ctrl.resourceToCreate).toBe(undefined);
        $scope.ctrl.createNewResource();
        $rootScope.$apply();

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeTruthy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeFalsy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect($scope.createForm.$pristine).toBeTruthy();
        expect($scope.createForm.$dirty).toBeFalsy();
        expect($scope.createForm.$valid).toBeFalsy();

    }));

    it('should be introducing data of new project ', inject(function () {

        expect($scope.ctrl.resourceToCreate).toBe(undefined);
        $scope.ctrl.createNewResource();
        $scope.createForm.pname.$setViewValue("lala");
        $scope.createForm.startDate.$setViewValue("2016-05-09");
        $scope.createForm.endDate.$setViewValue("2017-05-09");
        $rootScope.$apply();

        expect($scope.ctrl.updateFormIsVisible()).toBeFalsy();
        expect($scope.ctrl.creationFormIsVisible()).toBeTruthy();
        expect($scope.ctrl.resourcesListIsVisible()).toBeFalsy();
        expect($scope.ctrl.createNewResource()).toBeFalsy();

        expect(angular.isDefined($scope.ctrl.resourceToCreate)).toBeTruthy();
        expect($scope.createForm.$pristine).toBeFalsy();
        expect($scope.createForm.$dirty).toBeTruthy();
        expect($scope.createForm.$valid).toBeTruthy();

    }));

    it('should show dateLowerThan and dateGreaterThan errors for createForm', inject(function () {

        expect($scope.ctrl.resourceToCreate).toBe(undefined);
        $scope.ctrl.createNewResource();
        $scope.createForm.pname.$setViewValue("lala");
        $scope.createForm.startDate.$setViewValue("2016-05-09");
        $scope.createForm.endDate.$setViewValue("2015-05-09");
        $rootScope.$apply();

        expect(angular.isDefined($scope.createForm.$error.dateLowerThan)).toBeTruthy();
        expect(angular.isDefined($scope.createForm.$error.dateGreaterThan)).toBeTruthy();

    }));

    it('should show dateLowerThan and dateGreaterThan errors for updateForm', inject(function () {

        $scope.ctrl.setCurrentResource({});
        $scope.updateForm.pname.$setViewValue("lala");
        $scope.updateForm.startDate.$setViewValue("2016-05-09");
        $scope.updateForm.endDate.$setViewValue("2015-05-09");
        $rootScope.$apply();

        expect($scope.updateForm.$error.dateLowerThan).toBeDefined();
        expect($scope.updateForm.$error.dateGreaterThan).toBeDefined();

    }));

    it('should assign an id when project creation succeeds', inject(function () {

        $scope.ctrl.resourceToCreate = {};
        $scope.ctrl.createResource();
        $rootScope.$apply();

        expect($scope.ctrl.resources.length).toBe(totalPreloadedProjects + 1);
        $scope.ctrl.resources.forEach(function (p) {
            expect(p.id).toBeDefined();
        });

        expect($scope.ctrl.resourceToCreate).toBeUndefined();

    }));

    it('should update project using type project', inject(function () {

        var name = "name updated";

        $scope.ctrl.currentResource = {id: 21, name: name};
        $scope.ctrl.updateResource();
        $rootScope.$apply();

        expect($scope.ctrl.currentResource).toBeUndefined();

        var found = false;

        $scope.ctrl.resources.forEach(function (resource) {
            if (resource.id === 21) {
                found = true;
                expect(resource.name).toBe(name);
            }
        });

        expect(found).toBeTruthy();

    }));

    it('should restore resources after cancel edit', inject(function () {

        var name = "name updated";
        var id = $scope.ctrl.resources[0].id;
        var originalName = $scope.ctrl.resources[0].name;
        $scope.ctrl.resources[0].name = name;

        expect(originalName).not.toBe($scope.ctrl.resources[0].name);

        var found = false;

        $scope.ctrl.resources.forEach(function (resource) {
            if (resource.id === id) {
                found = true;
                expect(resource.name).toBe(name);
            }
        });

        expect(found).toBeTruthy();

        $scope.ctrl.restoreResources();

        found = false;

        $scope.ctrl.resources.forEach(function (resource) {
            if (resource.id === id) {
                found = true;
                expect(resource.name).toBe(originalName);
            }
        });

        expect(found).toBeTruthy();
    }));

    function getProjects() {
        var projects = [new Project({
                name: "p1",
                id: 20,
                startDate: 1458255600000,
                endDate: 1473375600000,
                startIndex: 0,
                endIndex: 24
            }), new Project({
                name: "p2",
                id: 21,
                startDate: 1458255600000,
                endDate: 1473375600000,
                startIndex: 12,
                endIndex: 99
            })];

        return projects;
    }
});