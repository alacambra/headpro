(function () {
    'use strict';
    angular.module('entities').factory('Slot', SlotFactory);

    function SlotFactory($log) {
        function Slot(serviceData) {
            this.setData(serviceData);
        }

        Slot.prototype = {
            setData: function (serviceData) {
                angular.extend(this, serviceData);
            },
            getDTO: function () {
                var dto = angular.copy(this);
                return dto;
            },
            setFromDto: function (dto) {
                this.setData(dto);
            },
            hasLoadedSlots: function () {
                return angular.isArray(this.slots) && this.slots.length > 0;
            }
        };

        return Slot;
    }
})();
