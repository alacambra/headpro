(function () {
    'use strict';

    angular
            .module('headproClient')
            .controller('StartsiteController', StartsiteController);

    function StartsiteController(DataManager) {

        var vm = this;
        vm.worksapces = [];

        DataManager.getWorkspaces().then(function (worksapces) {
            worksapces.forEach(function (ws) {
                vm.worksapces.push(ws);
            });
        });
    }
})();
