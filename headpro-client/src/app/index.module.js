(function () {
    'use strict';

    angular.module('entities', []);
    angular.module('validation', []);
    angular.module('projects', ['dataManagement', 'toastr', 'validation', 'ngAnimate', 'ui.bootstrap']);
    angular.module('services', ['dataManagement', 'toastr']);
    angular.module('dataManagement', ['entities', 'toastr', 'projects', 'services']);
    angular.module('timeRange', ['dataManagement', 'ngResource']);
    angular.module('headproClient', [
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'ngHandsontable',
        'nvd3',
        'ngVis',
        'toastr',
        'daterangepicker',
        'rzModule',
        'entities',
        'timeRange',
        'dataManagement',
        'projects',
        'services',
        'LocalStorageModule'
    ]);

})();
