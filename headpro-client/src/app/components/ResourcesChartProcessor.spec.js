describe('ChartProcessor', function () {

    var ChartProcessor;
    beforeEach(module('headproClient'));
    beforeEach(inject(function (_ChartProcessor_) {
        ChartProcessor = _ChartProcessor_;

    }));

    it('should have start time', function () {
        var rcp = new ChartProcessor();
        rcp.setStartTime(0);
        expect(rcp.getStartTime()).toEqual(0);
        
        rcp.setStartTime(10);
        expect(rcp.getStartTime()).toEqual(10);
    });
    
    it('should change date', function () {
        var rcp = new ChartProcessor();
        rcp.setStartTime(0);
        
        var options = rcp.getChartOptions();
        var dateBuilder = options.chart.xAxis.tickFormat;
        var expectedDate = "01/01/70 (00)";
        expect(dateBuilder(0)).toEqual(expectedDate);
        
        rcp.setStartTime(1000000000000);
        expect(dateBuilder(0)).not.toEqual(expectedDate);
        expect(dateBuilder(0)).toEqual("09/09/01 (35)");
        
    });
});
