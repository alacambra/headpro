(function () {
    'use strict';
    angular.module('headproClient').service('SlotService', SlotService);

    function SlotService($log) {

        var logPrefix = "SlotService:";

        function toSimpleSlots(resourcesWithSlots, options) {

            var defaultTr = {
                getStartIndex: function () {
                    return undefined;
                },
                getEndIndex: function () {
                    return undefined;
                },
                getRangeLength: function () {
                    return undefined;
                }
            };


            options = options || {timeRange: defaultTr};
            options.timeRange = options.timeRange || defaultTr;
            var rangeStartIndex = options.timeRange.getStartIndex() || 0;
            var rangeLength = options.timeRange.getRangeLength() || 0;
            var portViewAbsEndIndex = rangeStartIndex + rangeLength - 1;
            var implicitTimeRange = options.implicitTimeRange || false;

            var useTimeRangeOfBlockResource = implicitTimeRange &&
                    angular.isDefined(options.useTimeRangeOfBlockResource) ? options.useTimeRangeOfBlockResource : true;

            if (implicitTimeRange === true && useTimeRangeOfBlockResource === false) {
                throw new SlotServiceException("implicit time range only available with BlockResources");
            }

            if (implicitTimeRange) {
                $log.debug(logPrefix, "using implicitTimeRange");
                var timeRange = getTimeRange(resourcesWithSlots, useTimeRangeOfBlockResource);

                rangeStartIndex = timeRange.startIndex;
                portViewAbsEndIndex = timeRange.endIndex;
                rangeLength = timeRange.rangeLength;
            }

            var allSlots = [];
            resourcesWithSlots.forEach(function (resource) {

                var resourceSlots = [];

                for (var i = 0; i < rangeLength; i++) {
                    resourceSlots[i] = 0;
                }

                resource.getSlots().forEach(function (slot) {

                    var blockStartAbsIndex = resource.getStartIndex() || 0;
                    var slotAbsStartIndex = blockStartAbsIndex + slot.startPosition;
                    var slotAbsEndIndex = slotAbsStartIndex + slot.duration - 1;
                    var firstStepIndex = slotAbsStartIndex - rangeStartIndex;

                    if (firstStepIndex < 0) {
                        firstStepIndex = 0;
                    }

                    if (slotAbsStartIndex > portViewAbsEndIndex || slotAbsEndIndex < rangeStartIndex) {
                        return;
                    }

                    var totalSteps = slot.duration;

                    if (slotAbsStartIndex < rangeStartIndex) {
                        totalSteps -= (rangeStartIndex - slotAbsStartIndex);
                    }

                    if (slotAbsEndIndex > portViewAbsEndIndex) {
                        totalSteps -= (slotAbsEndIndex - portViewAbsEndIndex);
                    }

                    for (var i = 0; i < totalSteps; i++) {
                        resourceSlots[firstStepIndex + i] += slot.valuePerStep;
                    }

                });

                allSlots.push(resourceSlots);
            });

            return allSlots;
        }

        function getMaxProjectOverlap(projects, timeRange, marginsLength){

            marginsLength = marginsLength || 0;

            if(angular.isUndefined(projects || angular.isUndefined(timeRange))){
                throw new Error();
            }

            var arr = new Array(timeRange.getRangeLength());
            for(var i = 0; i<arr.length;i++){
                arr[i] = 0;
            }

            var max = 0;

            projects.forEach(function (project){
                var start = project.getStartIndex() < timeRange.getStartIndex() ? 0 : project.getStartIndex() - timeRange.getStartIndex();
                var end = project.getEndIndex() > timeRange.getEndIndex() ? (timeRange.getLength() - 1) : project.getEndIndex() - timeRange.getStartIndex();

                start = start > marginsLength ? start - marginsLength : 0;
                end = (end + marginsLength) < timeRange.getRangeLength() ? end + marginsLength : timeRange.getEndIndex() - timeRange.getStartIndex();

                for(var i = start; i<end;i++){
                    arr[i]++;
                    if(max < arr[i]){
                        max = arr[i];
                    }
                }
            });

            return max;

        }

        function aggregateSimpleSlots(simpleSlots) {

            var aggregated = [];

            simpleSlots.forEach(function (slotsSerie) {

                var length = slotsSerie.length;
                for (var i = 0; i < length; i++) {
                    if (angular.isUndefined(aggregated[i])) {
                        aggregated[i] = 0;
                    }
                    aggregated[i] += slotsSerie[i];
                }
            });
            return aggregated;

        }


        function validateTimeRangeOrException(tr) {
            if (angular.isUndefined(tr)) {
                throw new SlotServiceException("validateTimeRangeOrException: TimeRange data not given");
            }

            if (angular.isUndefined(tr.getStartDate())) {
                throw new SlotServiceException("validateTimeRangeOrException: Start date not given");
            }

            if (angular.isUndefined(tr.getEndDate())) {
                throw new SlotServiceException("validateTimeRangeOrException: End date not given");
            }

            if (angular.isUndefined(tr.getStartIndex())) {
                throw new SlotServiceException("validateTimeRangeOrException: Start index not given");
            }

            if (angular.isUndefined(tr.getEndIndex())) {
                throw new SlotServiceException("validateTimeRangeOrException: Ed index not given");
            }
        }

        function getTimeRange(resources) {

            var timeRange = {
                startIndex: -1,
                endIndex: 0,
                rangeLength: 0
            };

            resources.forEach(function (resource) {

                validateTimeRangeOrException(resource);

                if (resource.getStartIndex() < timeRange.startIndex || timeRange.startIndex === -1) {
                    timeRange.startIndex = resource.getStartIndex();
                    timeRange.startDate = resource.getStartDate();
                }

                if (resource.getEndIndex() > timeRange.endIndex) {
                    timeRange.endIndex = resource.getEndIndex();
                    timeRange.endDate = resource.getEndDate();
                }
            });

            if (timeRange.startIndex === -1) {
                timeRange.startIndex = 0;
                timeRange.endIndex = 0;
                timeRange.startDate = 0;
                timeRange.endDate = 0;
                timeRange.rangeLength = 0;

                return timeRange;
            }

            timeRange.rangeLength = timeRange.endIndex - timeRange.startIndex + 1;

            return timeRange;
        }

        function substractSimpleSlots(baseSlots, toSubstractSlots, offset) {

            baseSlots = baseSlots || [];
            toSubstractSlots = toSubstractSlots || [];
            offset = offset || 0;

            var absOffset = Math.abs(offset);
            var maxLength = baseSlots.length > toSubstractSlots.length ? baseSlots.length : toSubstractSlots.length;
            var totalLenght = absOffset + maxLength;

            var result = [];

            var i = 0;
            var j = 0;

            for (i = 0; i < totalLenght; i++) {
                result[i] = 0;
            }

            i = 0;
            if (offset > 0) {
                for (i = 0; i < absOffset; i++) {
                    result[i] = baseSlots[i] || 0;
                }
            } else if (offset < 0) {
                for (j = 0; j < absOffset; j++) {
                    result[j] = -toSubstractSlots[j] || 0;
                }
            }

            for (var k = absOffset; k < totalLenght; k++, i++, j++) {
                result[k] = (baseSlots[i] || 0) - (toSubstractSlots[j] || 0);
            }

            return result;
        }

        this.aggregateSimpleSlots = aggregateSimpleSlots;
        this.substractSimpleSlots = substractSimpleSlots;
//        this.getTimeRangeWithDates = getTimeRangeWithDates;
        this.toSimpleSlots = toSimpleSlots;
        this.getTimeRange = getTimeRange;
        this.getMaxProjectOverlap = getMaxProjectOverlap;
    }

    function SlotServiceException(message) {
        this.name = 'SlotServiceException';
        this.message = message;
    }

    SlotServiceException.prototype = new Error();
    SlotServiceException.prototype.constructor = SlotServiceException;

})();
