/* global expect */

describe('SlotsService', function () {

    var SlotService, projectsWithSlots, servicesWithSlots, Service, Project;
    beforeEach(module('headproClient'));
    beforeEach(inject(function (_SlotService_, _Service_, _Project_) {
        SlotService = _SlotService_;
        Service = _Service_;
        Project = _Project_;

    }));

    beforeEach(function () {
        servicesWithSlots = [
            new Service({
                name: "s1",
                id: 12,
                slots: [
                    {
                        id: 161,
                        duration: 9,
                        startPosition: 319,
                        service: 12,
                        valuePerStep: 1,
                        previousSlot: -1,
                        nextSlot: 163
                    },
                    {
                        id: 163,
                        duration: 16,
                        startPosition: 328,
                        service: 12,
                        valuePerStep: 2,
                        previousSlot: 161,
                        nextSlot: -1
                    }
                ]
            })
        ];

        projectsWithSlots = [new Project({
                name: "A&B Facilities",
                id: 21,
                startDate: 1458255600000,
                endDate: 1473375600000,
                startIndex: 0,
                endIndex: 24,
                slots: [
                    {
                        id: 3418,
                        duration: 25,
                        startPosition: 0,
                        service: 63,
                        valuePerStep: 123,
                        previousSlot: -1,
                        nextSlot: -1
                    }
                ]
            }), new Project({
                name: "A&B Facilities",
                id: 21,
                startDate: 1458255600000,
                endDate: 1473375600000,
                startIndex: 12,
                endIndex: 99,
                slots: [
                    {
                        id: 3418,
                        duration: 25,
                        startPosition: 0,
                        service: 63,
                        valuePerStep: 123,
                        previousSlot: -1,
                        nextSlot: -1
                    }
                ]
            })];
    });

    it('should return empty array', function () {

        var result = SlotService.toSimpleSlots([]);
        expect(result.length).toEqual(0);
    });

    it('should return exactly duration and values of whole slots', function () {

        var totalDuration = 25;
        var duration1 = 9;
        var duration2 = 16;

        var options = {
            timeRange: {
                getStartIndex: function () {
                    return 319;
                },
                getEndIndex: function () {
                    return 343;
                },
                getRangeLength: function () {
                    return 25;
                }
            }
        };

        var result = SlotService.toSimpleSlots(servicesWithSlots, options);
        expect(result.length).toEqual(1);
        expect(result[0].length).toEqual(totalDuration);

        for (var i = 0; i < totalDuration; i++) {
            if (i < duration1) {
                expect(result[0][i]).toEqual(1);
            } else {
                expect(result[0][i]).toEqual(2);
            }
        }
    });

    it('should return exactly duration and values of whole slots', function () {

        var totalDuration = 25;
        var duration1 = 9;
        var duration2 = 16;

        var options = {
            timeRange: {
                getStartIndex: function () {
                    return 319;
                },
                getEndIndex: function () {
                    return 343;
                },
                getRangeLength: function () {
                    return 25;
                }
            }
        };

        var result = SlotService.toSimpleSlots(servicesWithSlots, options);
        expect(result.length).toEqual(1);
        expect(result[0].length).toEqual(totalDuration);

        for (var i = 0; i < totalDuration; i++) {
            if (i < duration1) {
                expect(result[0][i]).toEqual(1);
            } else {
                expect(result[0][i]).toEqual(2);
            }
        }
    });

    it('should generate 0 for overflowing windows size', function () {

        var totalDuration = 101;
        var end1 = 19;
        var end2 = 9 + end1;
        var end3 = 16 + end2;
        var end4 = 101;

        var options = {
            timeRange: {
                getStartIndex: function () {
                    return 300;
                },
                getEndIndex: function () {
                    return 400;
                },
                getRangeLength: function () {
                    return 101;
                }
            }
        };

        var result = SlotService.toSimpleSlots(servicesWithSlots, options);
        expect(result.length).toEqual(1);
        expect(result[0].length).toEqual(totalDuration);

        for (var i = 0; i < totalDuration; i++) {
            if (i >= end3) {
                expect(result[0][i]).toEqual(0);
            } else if (i >= end2) {
                expect(result[0][i]).toEqual(2);
            } else if (i >= end1) {
                expect(result[0][i]).toEqual(1);
            } else {
                expect(result[0][i]).toEqual(0);
            }
        }
    });

    it('should return 25 simpleslots', function () {

        var options = {
            implicitTimeRange: true
        };

        projectsWithSlots[1].setStartIndex(0);
        projectsWithSlots[1].setEndIndex(24);

        var result = SlotService.toSimpleSlots(projectsWithSlots, options);
        expect(result.length).toEqual(2);
        expect(result[0].length).toEqual(25);
        expect(result[1].length).toEqual(25);
        expect(result[0][0]).toEqual(123);
    });

    it('should takes the longest possible period', function () {

        var options = {
            implicitTimeRange: true
        };

        projectsWithSlots[1].startIndex = 20;
        projectsWithSlots[1].endIndex = 99;

        var result = SlotService.toSimpleSlots(projectsWithSlots, options);
        expect(result.length).toEqual(2);
        expect(result[0].length).toEqual(100);
        expect(result[1].length).toEqual(100);
        expect(result[0][0]).toEqual(123);
    });

    it('should takes the longest possible period', function () {

        var options = {
            implicitTimeRange: true
        };

        projectsWithSlots[1].startIndex = 20;
        projectsWithSlots[1].endIndex = 99;

        var result = SlotService.toSimpleSlots(projectsWithSlots, options);
        expect(result.length).toEqual(2);
        expect(result[0].length).toEqual(100);
        expect(result[1].length).toEqual(100);
        expect(result[0][0]).toEqual(123);
    });


    it('should return emtpy array slot because resources are empty', function () {

        var options = {
            implicitTimeRange: true
        };

        var result = SlotService.toSimpleSlots([], options);
        expect(result.length).toEqual(0);
    });

    it('should aggregate simpleslots ', function () {

        var simpleslots = [[1, 1, 1], [1, 1, 1], [1, 1, 1]];
        var expected = [3, 3, 3];

        var result = SlotService.aggregateSimpleSlots(simpleslots);
        expect(result.length).toEqual(3);
        expect(result).toEqual(expected);
    });

    it('should substract simple slots without offset ', function () {

        var base = [1, 2, 3];
        var toSubstract = [3, 3, 3];
        var expected = [-2, -1, 0];

        var result = SlotService.substractSimpleSlots(base, toSubstract);
        expect(result.length).toEqual(3);
        expect(result).toEqual(expected);
    });

    it('should substract simple slots with positive offset ', function () {

        var base = [1, 2, 3];
        var toSubstract = [3, 3, 3];
        var expected = [1, -1, 0, -3];

        var result = SlotService.substractSimpleSlots(base, toSubstract, 1);
        expect(result.length).toEqual(4);
        expect(result).toEqual(expected);
    });

    it('should substract simple slots with positive offset and different lenght', function () {

        var base = [1, 2, 3];
        var toSubstract = [3, 3, 3, 5];
        var expected = [1, -1, 0, -3, -5];

        var result = SlotService.substractSimpleSlots(base, toSubstract, 1);
        expect(result.length).toEqual(5);
        expect(result).toEqual(expected);
    });

    it('should substract simple slots with negative offset ', function () {

        var base = [1, 2, 3];
        var toSubstract = [3, 3, 3];
        var expected = [-3, -2, -1, 3];

        var result = SlotService.substractSimpleSlots(base, toSubstract, -1);
        expect(result.length).toEqual(4);
        expect(result).toEqual(expected);
    });
});
