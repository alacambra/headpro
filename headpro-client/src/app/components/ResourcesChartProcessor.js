/* global yAxis, d3 */

(function () {

    'use strict';
    angular.module('headproClient').factory('ChartProcessor', ResourcesChartProcessor);
    function ResourcesChartProcessor($log) {

        var logPrefix = "ResourcesChartProcessor";

        function ChartProcessor(charType) {

            charType = charType || "multiBarChart";
            var that = this;
            this.stackedResourcesSteps;
            this.chartItems = [];
            this.counterResourceSteps;
            var counterResourceChart = [];
            this.chartKeys;
            this.load = loadChart;
            this.startTimeStamp = 10000;
            function loadChart() {

                that.chartItems = [];
                var rowIndex = 0;
                that.stackedResourcesSteps.forEach(function (step) {

                    var x = 0;
                    var d = [];
                    step.forEach(function (value) {
                        d.push({x: x, y: value});
                        x++;
                    });
                    that.chartItems.push({
                        type: "area",
                        key: that.chartKeys[rowIndex],
                        values: d,
                        yAxis: 1
                    });
                    rowIndex++;
                });
                var i = 0;
                if (angular.isDefined(that.counterResourceSteps)) {
                    that.counterResourceSteps.values.forEach(function (value) {
                        counterResourceChart.push({
                            x: i++,
                            y: value
                        });
                    });
                    that.chartItems.push({
                        color: "#000",
                        type: "line",
                        key: that.counterResourceSteps.name + " availability",
                        values: counterResourceChart,
                        yAxis: 1
                    });
                }

                return that;
            }

            var weekMilis = 1000 * 3600 * 24 * 7;
            that.chartOptions = {
                chart: {
                    type: charType,
                    height: 400,
                    margin: {
                        top: 20,
                        right: 35,
                        bottom: 45,
                        left: 75
                    },
                    useVoronoi: false,
                    clipEdge: true,
                    duration: 0,
                    stacked: true,
                    showControls: false,
                    useInteractiveGuideline: false,
//                    interactiveLayer: {
//                        toolip: {
//                            enabled: true,
//                            valueFormatter: function (d, i) {
//                                $log.debug(logPrefix, d, i);
//                                return yAxis.tickFormat()(d, i);
//                            }
//                        }},
                    x: function (d) {
                        return d.x;
                    },
                    y: function (d) {
                        return d.y;
                    },
                    xAxis: {
                        axisLabel: 'Date(Week)',
                        showMaxMin: true,
                        tickFormat: function (d) {
                            return d3.time.format('%d/%m/%y (%W)')(new Date(that.startTimeStamp + (d * weekMilis)));
                        }
                    },
                    yAxis: {
                        axisLabel: 'Resources (your unit)',
                        axisLabelDistance: -10,
                        showMaxMin: true,
                        tickFormat: function (d) {
                            return d3.format(',.2f')(d);
                        }
                    },
                    zoom: {
                        enabled: true,
                        scaleExtent: [
                            1,
                            10
                        ],
                        useFixedDomain: false,
                        useNiceScale: false,
                        horizontalOff: false,
                        verticalOff: true,
                        unzoomEventType: "dblclick.zoom"
                    }
                }
            };

        }

        ChartProcessor.prototype = {
            setStackedResourcesSteps: function (value) {
                this.stackedResourcesSteps = value;
            },
            setStartTime: function (st) {
                if(!angular.isNumber(st)){
                    throw new Error("Invalid startTime given. Timestamp expected");   
                }
                this.startTimeStamp = st;
            },
            getStartTime: function () {
                return this.startTimeStamp;
            },
            getStackedResourcesSteps: function () {
                return this.stackedResourcesSteps;
            },
            setCounterResourceSteps: function (value) {
                this.counterResourceSteps = value;
            },
            getCounterResourceSteps: function () {
                return this.counterResourceSteps;
            },
            setChartKeys: function (value) {
                this.chartKeys = value;
            },
            getChartKeys: function () {
                return this.chartKeys;
            },
            getChartOptions: function () {
                return this.chartOptions;
            },
            getChartItems: function () {
                return this.chartItems;
            }
        };
        return ChartProcessor;
    }
})();