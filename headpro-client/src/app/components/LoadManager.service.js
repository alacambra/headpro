(function () {
  'use strict';
  angular.module('headproClient').service('LoadManager', LoadManager);
  function LoadManager($log, $rootScope, $q, DataManager, GlobalTimeRangeService, toastr) {

    var that = this;
    this.currentWorkspace = undefined;
    this.appInitilized = false;
    var logPrefix = "LoadManager";
    var loadedWorkspaces = [];

    this.beginWorkspaceChangeEvent = "loadManager:workspace:changing";
    this.workspaceChangedEvent = "loadManager:workspace:changed";
    this.appLoadedEvent = "loadManager:app:loaded";
    this.appInitilizedEvent = "loadManager:app:initilized";

    function WorksapcesNotFoundException() {
      this.message = "Current user do not have any worksapce";
      this.toString = function () {
        return this.message;
      };
    }

    this.WorksapcesNotFoundException = WorksapcesNotFoundException;

    this.initApplication = function (urlWorkspaceId) {

      var defaultStartDate = new Date();
      var defaultEndDate = new Date(new Date().getTime() + 1000 * 3600 * 24 * 365);

      var timeRangeCompleted = GlobalTimeRangeService
        .setDatesRange(defaultStartDate, defaultEndDate, "master")
        .getTimeRange("master")
        .whenCompleted();

      var workspacesFetched = DataManager.getWorkspaces();

      $q.all([workspacesFetched, timeRangeCompleted])
        .then(function (result) {
          var workspaces = result[0];
          if (!(angular.isArray(workspaces) && workspaces.length > 0)) {
            throw new WorksapcesNotFoundException();
          }

          loadedWorkspaces = workspaces;
          if (angular.isDefined(urlWorkspaceId)) {
            workspaces.forEach(function (workspace) {
              if (workspace.id === urlWorkspaceId) {
                that.currentWorkspace = workspace;
              }
            });
          }

          if (angular.isUndefined(urlWorkspaceId)) {
            that.currentWorkspace = workspaces[0];
          } else if (angular.isUndefined(that.currentWorkspace)) {
            toastr.warning("Requested not workspace not found. Loading default workspace");
            that.currentWorkspace = workspaces[0];
          }

          DataManager.setCurrentWorkspaceId(that.currentWorkspace.id);

          var focusTimeRange = GlobalTimeRangeService.getTimeRange("master").clone("focus");
          GlobalTimeRangeService.setTimeRange(focusTimeRange);

          that.appInitilized = true;
          GlobalTimeRangeService.appInitializied();
          $rootScope.$broadcast(that.appInitilizedEvent, that.currentWorkspace);
        });
    };

    this.beginWorkspaceChange = function (workspaceId) {

      if (angular.isDefined(that.currentWorkspace) && that.currentWorkspace.id === workspaceId) {
        return;
      }

      that.appInitilized = false;
      var futureWorkspace = undefined;
      loadedWorkspaces.forEach(function (workspace) {
        if (workspace.id === workspaceId) {
          futureWorkspace = workspace;
        }
      });

      if (angular.isUndefined(futureWorkspace)) {
        $log.debug(logPrefix, "workspace not found", workspaceId);
        return;
      }

      that.currentWorkspace = futureWorkspace;
      $rootScope.$broadcast(this.beginWorkspaceChangeEvent);
      DataManager.setCurrentWorkspaceId(workspaceId).then(function () {
        that.appInitilized = true;
        $rootScope.$broadcast(that.workspaceChangedEvent, that.currentWorkspace);
      });
    };

    this.workspaceChanged = function () {
      $rootScope.$broadcast(this.workspaceChangedEvent, that.currentWorkspace);
    };

    this.appLoaded = function appLoaded() {
      $rootScope.$broadcast(this.appLoadedEvent);
    };

  }

})();
