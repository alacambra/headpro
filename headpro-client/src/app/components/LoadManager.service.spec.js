describe('LoadManager', function () {

    var LoadManager;
    beforeEach(module('headproClient'));
    beforeEach(inject(function (_LoadManager_) {
        LoadManager = _LoadManager_;

    }));

    it('shoulbe right excpetion', function () {
        try {
            throw new LoadManager.WorksapcesNotFoundException();
        } catch (e) {
            expect(e.message).toEqual("Current user do not have any worksapce");
        }
    });
});
