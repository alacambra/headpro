/* global numeral */

(function () {
    'use strict';
    angular.module('headproClient').factory('SimpleSlotsTable', SimpleSlotsTableFactory);
    function SimpleSlotsTableFactory($log, $q, toastr) {
        function SimpleSlotsTable(options) {

            options = options || {};

            var that = this;
            var logPrefix = "SimpleSlotsTableFactory:";
            this.individualRowOptions = [];
            this.timeRange = {};

            this.setOptions = setOptions;
            setOptions(options);
            var tableReadyDeferred = $q.defer();
            this.tableReadyPromise = tableReadyDeferred.promise;

            function setOptions(options) {

                var implicitTimeRange = angular.isDefined(options.implicitTimeRange) ? options.implicitTimeRange : false;
                var useTimeRangeOfBlockResource = angular.isDefined(options.useTimeRangeOfBlockResource) ?
                        options.useTimeRangeOfBlockResource : true;

                if (implicitTimeRange === true && useTimeRangeOfBlockResource === false) {
                    throw new SimpleSlotsTableException("implicit time range only available with BlockResources");
                }

                angular.extend(that, {
                    implicitTimeRange: implicitTimeRange,
                    useTimeRangeOfBlockResource: useTimeRangeOfBlockResource,
                    timeRange: options.timeRange || {},
                    resourcesWithSlots: [],
                    processedData: [],
                    colHeaders: [],
                    rowHeaders: [],
                    colTypes: [],
                    mappedResources: [],
                    settings: getSettings(),
                    addSlotAction: options.addSlotAction || function (resource, slotPosition, duration, value) {
                        var deferred = $q.defer();
                        var promise = deferred.promise;
                        deferred.resolve();
                        return promise;
                    },
                    editReady: options.editReady || function () {},
                    pagination: options.pagination || true
                });
            }

            this.load = function (tableScope) {
                that.rowHeaders = [];
                that.mappedResources = [];
                tableReadyDeferred = $q.defer();
                this.tableReadyPromise = tableReadyDeferred.promise;

                this.resourcesWithSlots.forEach(function (resource) {
                    that.rowHeaders.push(resource.getName());
                    that.mappedResources.push(resource);
                });

                prepareDataAndDates(tableScope);
            };

            function prepareDataAndDates(tableScope) {

                var weekMilis = 3600 * 24 * 1000 * 7;

                for (var i = 0; i < that.timeRange.getRangeLength(); i++) {
                    var date = new Date(that.timeRange.getStartDate() + i * weekMilis);
                    var prettyDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
                    that.colHeaders.push(prettyDate);
                    that.colTypes.push({
                        type: 'numeric'
                    });
                }

                bindDynamicSettings(tableScope);
                tableReadyDeferred.resolve(that.timeRange);
            }

            function getSettings() {
                return {
                    //manualColumnResize produces that the timeline items are no more ditable nor movable (also in different controllers)
                    manualColumnResize: false,
                    contextMenu: false,
                    autoWrapRow: true,
                    autoWrapCol: true,
                    disableVisualSelection: false,
                    fillHandle: 'horizontal',
                    colHeaders: true,
                    rowHeaders: true,
                    comments: true,
//                    colWidths: 80,
//                    rowHeights: 23,
                    beforeChange: tableEditedAction
                };
            }

            function addSlot(row, col, duration, newValue, oldValue) {

                var resource = that.mappedResources[row];
                var blockStartAbsIndex = resource.getStartIndex() || 0;
                var slotPosition = that.timeRange.getStartIndex() + col - blockStartAbsIndex;

                return that.addSlotAction(resource, slotPosition, duration, newValue, oldValue, row, col);
            }

            function bindDynamicSettings(tableScope) {

                if (angular.isUndefined(tableScope)) {
                    throw {
                        message: "table scope must be defined to perfom a binding",
                        toString: function () {
                            return this.message;
                        }
                    };
                }

                tableScope.beforeChange = tableEditedAction;
                var clicked = 0;
                tableScope.cells = function (row, col, prop) {
                    var cellProperties = {};

                    if (row < 0 || col < 0) {
                        throw new Error("ivalid row or column given:" + row + "," + col);
                    }

                    cellProperties.renderer = function (instance, td, row, col, prop, value, cellProperties) {

                        var rType = "none";

                        if (angular.isDefined(that.individualRowOptions[row])) {
                            rType = that.individualRowOptions[row].isResult || "none";
                        }

                        if (cellProperties.readOnly && rType === "none") {
                            if (angular.isNumber(value) && value > 0) {
                                angular.element(td).addClass("outOfRange");
                            } else {
                                angular.element(td).addClass("htDimmed");
                            }
                        }

                        if (angular.isDefined(tableScope.totalAvailablePerServicePerSimpleSlot) &&
                                angular.isDefined(tableScope.totalAvailablePerServicePerSimpleSlot[row])) {

                            if (tableScope.totalAvailablePerServicePerSimpleSlot[row][col] < 0) {
                                angular.element(td).addClass("deficit");
                                cellProperties.comment = "deficit: " + tableScope.totalAvailablePerServicePerSimpleSlot[row][col];
                            }
                        }

                        angular.element(td).addClass("htNumeric");

                        if (angular.isDefined(that.individualRowOptions[row])) {

                            if (rType === "posNeg") {
                                angular.element(td).addClass("total").addClass("htDimmed");
                                value < 0 ? angular.element(td).addClass("negative-value") : angular.element(td).addClass("positive-value");
//                                cellProperties.comment = 'some\ns';
                            } else if (rType === "static") {
                                angular.element(td).addClass("total");
                            }
                        }

//                        var moreInfo = angular.element("<span>+Info</span>").click(function () {
//                            if (clicked % 2 === 0) {
//                                //call something throw tableScope
//                                $log.debug(logPrefix, "cliked");
//
//                                clicked++;
//                            } else {
//                                clicked = 0;
//                            }
//                        });
                        //Exist alys 2 HT overlapped and therefore the event is twice binded
                        angular.element(td).empty()
//                                .append(moreInfo)
                                .append(numeral(value).format('0.00')).click(function () {
                            if (clicked % 2 === 0) {
                                //call something throw tableScope
                                clicked++;
                            } else {
                                clicked = 0;
                            }
                        });
                    }

                    if (
                            !angular.isArray(that.mappedResources) ||
                            that.mappedResources.length === 0 ||
                            angular.isUndefined(that.mappedResources[row])) {

                        return cellProperties;
                    }

                    if (that.mappedResources[row].getStartIndex() >= (col + 1 + that.timeRange.getStartIndex())) {
                        cellProperties.readOnly = true;
                    }

                    if (that.mappedResources[row].getEndIndex() < (col + that.timeRange.getStartIndex())) {
                        cellProperties.readOnly = true;
                    }

                    return cellProperties;
                };
            }

            function tableEditedAction(_changes_, source) {

                var changes = _changes_.map(function (change) {
                    return {
                        row: change[0],
                        column: change[1],
                        oldValue: change[2],
                        newValue: change[3]
                    };
                });

                for (var i = 0; i < changes.length; i++) {
                    var v = changes[i].newValue;
                    if (!angular.isNumber(v) || v < 0) {
                        toastr.error("Values must be bigger or equals to 0");
                        return false;
                    }
                }

                changes.forEach(function (change) {
                    //here analyze changes and group them into effective slots
                });

                var promise;
                var row = changes[0].row;
                var duration = changes.length;
                var firstCol = changes[0].column;

                if (source === "paste") {
                    var lastChange = _changes_[0][3];
                    for (var z in _changes_) {
                        if (lastChange !== _changes_[z][3]) {
                            toastr.info("All values must be the same");
                            return false;
                        }
                    }

                } else if (source !== "edit" && source !== "autofill") {
                    return false;
                }

                if (changes[0].newValue === changes[0].oldValue) {
                    that.editReady();
                }

                promise = addSlot(row, firstCol, duration, changes[0].newValue, changes[0].oldValue, changes[0].row);

                promise.then(function () {
                    that.editReady();
                });


            }
        }

        SimpleSlotsTable.prototype = {
            setOptions: function (options) {
                this.setOptions(options);
                return this;
            },
            loadTable: function (tableScope) {
                this.load(tableScope);
                return this;
            },
            setData: function (data) {
                this.resourcesWithSlots = data;
                return this;
            },
            getSettings: function () {
                return this.settings;
            },
            getColHeaders: function () {
                return this.colHeaders;
            },
            getRowHeaders: function () {
                return this.rowHeaders;
            },
            getColumns: function () {
                return this.colTypes;
            },
            setResouresWithSlots: function (resources) {
                this.resourcesWithSlots = resources;
                return this;
            },
            getTimeRange: function () {
                return this.tableReadyPromise;
            },
            addRow: function (rowHeader, props) {
                this.rowHeaders.push(rowHeader);
                this.individualRowOptions[this.rowHeaders.length - 1] = props;
            }
        };

        function SimpleSlotsTableException(message) {
            this.name = 'SimpleSlotsTableException';
            this.message = message;
        }

        SimpleSlotsTableException.prototype = new Error();
        SimpleSlotsTableException.prototype.constructor = SimpleSlotsTableException;

        return SimpleSlotsTable;
    }
})();
