(function () {
    'use strict';
    angular.module('validation')
            .directive('dateGreaterThan', DateGreaterThan)
            .directive('dateLowerThan', DateLowerThan);


    function isValidDateRange(fromDate, toDate) {
        if (fromDate === "" || toDate === "")
            return true;
        if (isValidDate(fromDate) === false) {
            return false;
        }
        if (isValidDate(toDate) === true) {
            var days = getDateDifference(fromDate, toDate);
            if (days < 0) {
                return false;
            }
        }
        return true;
    }

    function getDateDifference (fromDate, toDate) {
        return Date.parse(toDate) - Date.parse(fromDate);
    }

    function isValidDate(dateStr) {
        if (angular.isUndefined(dateStr)) {
            return false;
        }

        var dateTime = Date.parse(dateStr);

        if (isNaN(dateTime)) {
            return false;
        }
        return true;
    }

    function DateGreaterThan($log, $filter) {

        var logPrefix = "DateGreaterThan:"

        function link($scope, elm, attrs, ctrl) {

            var validateDateRange = function (inputValue) {

                var fromDate = $filter('date')(attrs.dateGreaterThan, 'short');
                var toDate = $filter('date')(inputValue, 'short');
                var isValid = isValidDateRange(fromDate, toDate);
                ctrl.$setValidity('dateGreaterThan', isValid);
                return inputValue;
            };

            ctrl.$parsers.unshift(validateDateRange);
            ctrl.$formatters.push(validateDateRange);
            attrs.$observe('dateGreaterThan', function () {
                validateDateRange(ctrl.$viewValue);
            });
        }

        return {
            require: 'ngModel',
            link: link
        };
    }

    function DateLowerThan($log, $filter) {

        var logPrefix = "DateLowerThan:"

        function link($scope, elm, attrs, ctrl) {

            var validateDateRange = function (inputValue) {
                var fromDate = $filter('date')(inputValue, 'short');
                var toDate = $filter('date')(attrs.dateLowerThan, 'short');
                var isValid = isValidDateRange(fromDate, toDate);
                ctrl.$setValidity('dateLowerThan', isValid);
                return inputValue;
            };

            ctrl.$parsers.unshift(validateDateRange);
            ctrl.$formatters.push(validateDateRange);
            attrs.$observe('dateLowerThan', function () {
                validateDateRange(ctrl.$viewValue);
            });
        }

        return {
            require: 'ngModel',
            link: link
        };
    }

})();
