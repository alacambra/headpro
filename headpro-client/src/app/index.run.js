(function () {
    'use strict';
    angular
            .module('headproClient')
            .run(runBlock);

    function runBlock($rootScope, LoadManager, $log, $stateParams, $location) {

        var contentLoadingDeregistration = $rootScope.$on('$viewContentLoading', function () {
            if (angular.isDefined($stateParams.workspaceId)) {
                LoadManager.beginWorkspaceChange($stateParams.workspaceId);
            }
        });

        var myRegexp = /\/workspace\/(\d+)\//g;
        var match = myRegexp.exec($location.url());

        if (angular.isArray(match) && match.length > 1) {
            LoadManager.initApplication(parseInt(match[1], 10));
        } else {
            LoadManager.initApplication();
        }

        $rootScope.$on("$destroy", function () {
            contentLoadingDeregistration();
        });
    }

})();
