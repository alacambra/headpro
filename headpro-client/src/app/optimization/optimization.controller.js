/* global d3, moment */

(function () {
    'use strict';
    angular
        .module('headproClient')
        .controller('OptimizationController', OptimizationController);
    function OptimizationController($scope, VisDataSet, $log, $q, $timeout, $window,
                                    DataManager, LoadManager, Project, TimeLineItem, SlotService,
                                    ChartProcessor, TimeRange, toastr) {

        var logPrefix = 'OptimizationController';
        var vm = this;
        var timeRange = undefined;
        var availableServicesNames = [];
        var projectsNames = [];
        var requiredServiceNames = [];
        var availableResources;
        var requiredResourcesByService;
        var requiredResourcesByProject;
        var projects;
        var requiredByServiceResourcesSimpleSlots;
        var requiredByProjectResourcesSimpleSlots;
        var availableResourcesSimplesSlots;
        var aggRequiredByProjectSimpleSlots;

        vm.timeline = {};
        vm.chart = {};
        vm.chart.projects = {};
        vm.chart.data = [];

        if (LoadManager.appInitilized) {
            init();
        }

        $scope.$on(LoadManager.workspaceChangedEvent, function () {
            init();
        });
        $scope.$on(LoadManager.appInitilizedEvent, function () {
            init();
        });
        function init() {
            loadChart();
        }

        var timeReference = {
            index: 0,
            date: undefined
        }

        function loadChart() {
            var waitAvailableResources = DataManager.getAvailableResources();
            var waitRequiredResourcesByService = DataManager.getRequiredResources("service", false);
            var waitRequiredResourcesByproject = DataManager.getRequiredResources("project", false);

            requiredServiceNames = [];
            availableServicesNames = [];

            $q.all([waitAvailableResources, waitRequiredResourcesByService, waitRequiredResourcesByproject]).then(function (data) {

                availableResources = data[0];
                requiredResourcesByService = data[1];
                requiredResourcesByProject = data[2];
                projects = data[2];
                loadTimeline();
                requiredResourcesByService.forEach(function (service) {
                    requiredServiceNames.push(service.getName());
                });

                availableResources.forEach(function (service) {
                    availableServicesNames.push(service.getName());
                });

                projectsNames = projects.map(function (project) {
                    return project.getName();
                });

                if (projects.length > 0) {
                    timeReference.index = projects[0].getStartIndex();
                    timeReference.date = moment(projects[0].getStartDate()).day("Monday");
                }

                refreshCharts(availableResources, requiredResourcesByService, requiredResourcesByProject);

            }).catch(function (reason) {
                toastr.error("not possible to load: " + reason);
            });
        }

        var refreshing = false;

        function refreshCharts(availableResources, requiredResourcesByService, requiredResourcesByProject, realtime) {

            if (refreshing) {
                return;
            }

            refreshing = true;

            var tr = SlotService.getTimeRange(projects);
            timeRange = new TimeRange(tr.startDate, tr.endDate, "optimization", tr);

            requiredByServiceResourcesSimpleSlots = SlotService.toSimpleSlots(requiredResourcesByService, {timeRange: timeRange});
            requiredByProjectResourcesSimpleSlots = SlotService.toSimpleSlots(requiredResourcesByProject, {timeRange: timeRange});
            availableResourcesSimplesSlots = SlotService.toSimpleSlots(availableResources, {timeRange: timeRange});
            aggRequiredByProjectSimpleSlots = SlotService.aggregateSimpleSlots(requiredByProjectResourcesSimpleSlots);

            $timeout(function () {

                var max = SlotService.getMaxProjectOverlap(projects, timeRange, 1);
                vm.timeline.height = 60 + max * 50;

                drawProjectRequiredResources();

                if (!realtime) {
                    drawTotalAvailableResources();
                    drawRemainingResourcesByServices();
                }

                $timeout(function () {
                    refreshing = false;
                }, 0);
            });

        }

        function loadTimeline() {


            var timelineItems = new VisDataSet();
            projects.forEach(function (project) {
                timelineItems.add(new TimeLineItem(project));
            });

            vm.timeline.data = {
                items: timelineItems
            };

            /*
             * Associates original object with those built in the timeline item.
             * When an update is done, the project object will be updated with the new dates,
             * This array will be passed to the chart loader to recalculate the resources
             * @type Array
             */

            vm.timeline.events = {
                onload: onTimelineLoaded
            };
            vm.timeline.options = {
                height: "100%",
                autoResize: false,
                onMove: projectDatesChanged,
                onMoving: onMovingTimelineItem,
                snap: null,
                clickToUse: false,
                showCurrentTime: true,
                editable: {
                    add: false, // add new items by double tapping
                    updateTime: true, // drag items horizontally
                    updateGroup: false, // drag items from one group to another
                    remove: false       // delete an item by tapping the delete button top right
                },
                "showMajorLabels": true,
                "showMinorLabels": true
            };
        }

        function drawTotalAvailableResources() {

            var aggregatedRequiredResourcesSimpleSlots = SlotService.aggregateSimpleSlots(requiredByServiceResourcesSimpleSlots);
            var aggregatedAvailableResourcesSimplesSlots = SlotService.aggregateSimpleSlots(availableResourcesSimplesSlots);
            var total = SlotService.substractSimpleSlots(aggregatedAvailableResourcesSimplesSlots, aggregatedRequiredResourcesSimpleSlots);

            var chartProcessor = new ChartProcessor();

            chartProcessor.setStartTime(timeRange.getStartDate().getTime());
            vm.chart.options = chartProcessor.getChartOptions();
            vm.chart.options.chart.height = getChartsHeight();
            chartProcessor.setStackedResourcesSteps([total]);
            chartProcessor.setChartKeys(["total"]);
            vm.chart.data = chartProcessor.load().getChartItems();
            $timeout(function () {
                vm.chart.api.updateWithOptions(vm.chart.options);
            });
        }

        function drawRemainingResourcesByServices() {

            var availableSimpleSlots = {
                columns: availableServicesNames,
                slots: availableResourcesSimplesSlots
            };
            var requiredSimpleSlots = {
                columns: requiredServiceNames,
                slots: requiredByServiceResourcesSimpleSlots
            };

            var data = {};

            var i = 0;
            var slots = [];
            var name = "";

            for (i = 0; i < availableSimpleSlots.columns.length; i++) {
                name = availableSimpleSlots.columns[i];
                slots = availableSimpleSlots.slots[i];
                if (angular.isUndefined(data[name])) {
                    data[name] = {
                        available: [],
                        required: []
                    };
                }

                data[name].available = slots;
            }

            for (i = 0; i < requiredSimpleSlots.columns.length; i++) {
                name = requiredSimpleSlots.columns[i];
                slots = requiredSimpleSlots.slots[i];
                if (angular.isUndefined(data[name])) {
                    data[name] = {
                        available: [],
                        required: []
                    };
                }

                data[name].required = slots;
            }

            vm.charts.remainingResourcesByServices.data = [];
            vm.charts.remainingResourcesByServices.rowHeaders = [];

            for (name in data) {
                if (angular.isUndefined(name) || name === "undefined") {
                    throwException("invalid service name found");
                }
                vm.charts.remainingResourcesByServices.rowHeaders.push(name);
                vm.charts.remainingResourcesByServices.data.push(SlotService.substractSimpleSlots(data[name].available, data[name].required));
            }

            var chartProcessor = new ChartProcessor();

            chartProcessor.setStackedResourcesSteps(vm.charts.remainingResourcesByServices.data);
            chartProcessor.setChartKeys(vm.charts.remainingResourcesByServices.rowHeaders);
            chartProcessor.setStartTime(timeRange.getStartDate().getTime());

            vm.charts.remainingResourcesByServices.options = chartProcessor.getChartOptions();
            vm.charts.remainingResourcesByServices.options.chart.height = getChartsHeight();
            vm.charts.remainingResourcesByServices.data = chartProcessor.load().getChartItems();

            $timeout(function () {
                vm.charts.remainingResourcesByServices.api.updateWithOptions(chartProcessor.getChartOptions());
            });
        }

        function drawProjectRequiredResources() {
            var chartProcessor = new ChartProcessor();
            vm.chart.projects.options = chartProcessor.getChartOptions();
            vm.chart.projects.options.chart.height = getChartsHeight();

            chartProcessor.setStackedResourcesSteps(requiredByProjectResourcesSimpleSlots);
            chartProcessor.setChartKeys(projectsNames);
            chartProcessor.setStartTime(timeRange.getStartDate().getTime());
            vm.chart.projects.data = chartProcessor.load().getChartItems();
            $timeout(function () {
                vm.chart.projects.api.updateWithOptions(vm.chart.projects.options);
            });
        }

        function onTimelineLoaded(graphRef) {
            vm.timeline.graphRef = graphRef;
        }

        function getChartsHeight() {
          var minHeight = 200;
            var h = Math.max(document.documentElement.clientHeight, $window.innerHeight || 0);
            var computed = (h - vm.timeline.height - 50) / 2;
            if(computed < minHeight){
              computed = minHeight;
            }
            return computed;
        }

        function onMovingTimelineItem(item, callback) {
            if (item.getProject().getTimeDuration() === item.getDuration()) {
                item.updateContentField();
                updateProjectDates(item);
                refreshCharts(availableResources, requiredResourcesByService, requiredResourcesByProject, true);
                callback(item);
            } else {
                $log.debug(logPrefix, "changing size");
            }
        }

        function projectDatesChanged(projectItem, callback) {

            DataManager.getIndexForDate(projectItem.start).then(function (absoluteIndex) {

                var project = projectItem.getProject();

                var endIndex = parseInt(project.getEndIndex()) - parseInt(project.getStartIndex()) + parseInt(absoluteIndex.index);
                var startIndex = absoluteIndex.index;

                $log.debug(logPrefix, "current", project.getStartIndex(), "new", absoluteIndex.index);

                project.setStartIndex(startIndex);
                project.setEndIndex(endIndex);
                project.setStartDate(projectItem.start);
                project.setEndDate(projectItem.end);

                DataManager.updateProject(project).then(function () {
                    loadChart();
                    callback(projectItem);
                }).catch(function (reason) {
                    toastr.error("Not possible to move project. " + reason);
                });
            });
        }

        function updateProjectDates(projectItem) {

            if (refreshing) {
                return;
            }

            var newDate = moment(projectItem.start).day("Monday");
            var steps = newDate.diff(timeReference.date, "weeks");

            if (steps === 0) {
                return;
            }

            var absoluteIndex = timeReference.index + steps;
            var project = projectItem.getProject();

            var endIndex = parseInt(project.getEndIndex()) - parseInt(project.getStartIndex()) + parseInt(absoluteIndex);
            var startIndex = absoluteIndex;

            project.setStartIndex(startIndex);
            project.setEndIndex(endIndex);
            project.setStartDate(projectItem.start);
            project.setEndDate(projectItem.end);

        }
    }

    function OptimizationControllerException(message) {
        OptimizationControllerException.prototype = new Error();
        this.name = 'OptimizationControllerException';
        this.message = message;
        this.toString = function () {
            return message;
        };
    }

    OptimizationControllerException.prototype.constructor = OptimizationControllerException;

    function throwException(message) {
        OptimizationControllerException.prototype = new Error();
        throw new OptimizationControllerException(message);
    }
})();
