(function () {
    'use strict';
    angular.module('headproClient').factory('TimeLineItem', TimeLineItemFactory);

    function getFormatedDate(date) {
        return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    }

    function TimeLineItemFactory($log) {

        var logPrefix = "TimeLineItemFactory";
        
        function TimeLineItem(project) {
            var that = this;

            function init() {
                if (angular.isUndefined(project)) {
                    throwException("project must be given");
                }

                that.start = project.getStartDate();
                that.end = project.getEndDate();
                that.content = undefined;
                that.updateFinalContentField();
            }

            this.updateContentField = function () {
                this.content = project.getName() + " ( " + getFormatedDate(this.start) + " to " + getFormatedDate(this.end) + ") ";
            };
            
            this.updateFinalContentField = function () {
                 this.updateContentField();
//                this.content = "<span>X" + project.getName() + " ( " + getFormatedDate(this.start) + " to " + getFormatedDate(this.end) + ") </span>";
            };

            this.getProject = function () {
                return project;
            };

            this.getDuration = function () {
                return this.end.getTime() - this.start.getTime();
            };
            
            init();
        }

        return TimeLineItem;
    }

    function TimeLineItemException(message) {
        TimeLineItemException.prototype = new Error();
        this.name = 'TimeLineItemException';
        this.message = message;
        this.toString = function () {
            return message;
        };
    }

    TimeLineItemException.prototype.constructor = TimeLineItemException;

    function throwException(message) {
        TimeLineItemException.prototype = new Error();
        throw new TimeLineItemException(message);
    }
})();
