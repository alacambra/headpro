(function () {
    'use strict';
    angular
            .module('headproClient')
            .directive('navigation', navigation);

    function navigation(DataManager, $log, LoadManager, $timeout) {

        var logPrefix = "navigation.directive";

        var directive = {
            restrict: 'E',
            templateUrl: "app/navigation/navigation.html",
            link: linkFunc,
            replace: true
        };

        function init($scope) {
            $scope.currentSectionName = 'start';
            loadServices($scope);
            DataManager.getWorkspaces().then(function (workspaces) {
                $scope.workspaces = workspaces;
            });
        }

        function registerEvents($scope) {
            $scope.$on(LoadManager.workspaceChangedEvent, function (event, workspace) {
                $log.debug(logPrefix, "workspaceChangedEvent received:", workspace);
                $scope.workspaceId = workspace.id;
                init($scope);
            });

            $scope.$on(LoadManager.appInitilizedEvent, function (event, workspace) {
                $scope.workspaceId = workspace.id;
                init($scope);
            });
        }

        function loadServices($scope) {
            $log.debug(logPrefix, "loading services");
            DataManager.getServices().then(function (services) {
                $scope.services = services;
            });
        }

        function linkFunc($scope) {

            if (LoadManager.appInitilized) {
                init($scope);
            }

            $scope.services = [];
            registerEvents($scope);

            $scope.changeWorkspace = function (ws) {
                LoadManager.beginWorkspaceChange(ws.id);
            };

            $scope.logout = function () {
                DataManager.logout();
            };

            $scope.isActive = function (sectionName) {
                return angular.isDefined($scope.currentSectionName) && $scope.currentSectionName === sectionName;
            };

            $scope.setCurrentTab = function (currentSectionName) {
                $scope.currentSectionName = currentSectionName;
            };
        }

        return directive;
    }
})();